// tslint:disable-next-line:no-var-requires
const tsConfig = require('../tsconfig.json');
// tslint:disable-next-line:no-var-requires no-implicit-dependencies
const tsConfigPaths = require('tsconfig-paths');
// tslint:disable-next-line:no-var-requires
const ConnectionString = require('connection-string');
// tslint:disable-next-line:no-var-requires
const chmod = require('chmod');
const NODE_ENV = process.env.NODE_ENV || 'develop';
if (NODE_ENV !== 'develop') {
  tsConfigPaths.register({
    baseUrl: __dirname,
    paths: tsConfig.compilerOptions.paths
  });
}

import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import {
  appFilters as authAppFilters,
  defaultJwtConfig,
  IJwtConfig,
  JWT_CONFIG_TOKEN
} from '@omgstore/auth-nestjs';
import {
  appFilters,
  appPipes,
  CORE_CONFIG_TOKEN,
  defaultCoreConfig,
  ICoreConfig
} from '@omgstore/core-nestjs';
import { AppModule } from './app/app.module';
import { load } from 'dotenv';
import { accessSync, readFileSync } from 'fs';
import * as path from 'path';

/* hmr
declare const module: any;
*/

async function bootstrap() {
  const packageBody = JSON.parse(readFileSync('./package.json').toString());

  Logger.log(NODE_ENV);
  const envFile = path.resolve(__dirname, '..', `${NODE_ENV}.env`);
  try {
    accessSync(envFile);
    load({ path: envFile });
    Logger.log(`env file: ${envFile}`, 'Main');
  } catch (error) {
    Logger.log(`error on get env file: ${envFile}`, 'Main');
    try {
      accessSync(`.env`);
      load();
      Logger.log(`env file: .env`, 'Main');
    } catch (error) {
      Logger.log(`error on get env file: .env`, 'Main');
    }
  }
  const connectionString = new ConnectionString(process.env.DATABASE_URL || '');
  if (connectionString.protocol === 'sqlite') {
    const dbFile =
      './' +
      (connectionString.hosts ? connectionString.hosts[0].name : '') +
      (connectionString.path ? '/' + connectionString.path[0] : '');
    try {
      chmod(dbFile, 777);
    } catch (error) {
      Logger.log(`error on set chmod 777 to database file ${dbFile}`, 'Main');
    }
  }
  const coreConfig: ICoreConfig = {
    ...defaultCoreConfig,
    debug: process.env.DEBUG === 'true',
    demo: process.env.DEMO === 'true',
    port: process.env.PORT ? +process.env.PORT : 3000,
    protocol: process.env.PROTOCOL === 'https' ? 'https' : 'http',
    externalPort: process.env.EXTERNAL_PORT
      ? +process.env.EXTERNAL_PORT
      : undefined,
    domain: process.env.DOMAIN
  };
  const jwtConfig: IJwtConfig = {
    ...defaultJwtConfig,
    authHeaderPrefix: process.env.JWT_AUTH_HEADER_PREFIX,
    expirationDelta: process.env.JWT_EXPIRATION_DELTA,
    secretKey: process.env.JWT_SECRET_KEY
  };
  const app = await NestFactory.create(
    AppModule.forRoot({
      providers: [
        { provide: CORE_CONFIG_TOKEN, useValue: coreConfig },
        { provide: JWT_CONFIG_TOKEN, useValue: jwtConfig },
        ...appFilters,
        ...authAppFilters,
        ...appPipes
      ]
    }),
    { cors: true }
  );

  let documentBuilder = new DocumentBuilder()
    .setTitle(packageBody.name)
    .setDescription(packageBody.description)
    .setContactEmail(packageBody.author.email)
    .setExternalDoc('External URL', packageBody.homepage)
    .setLicense(packageBody.license, '')
    .setVersion(packageBody.version)
    .addBearerAuth('Authorization', 'header');

  if (coreConfig.protocol === 'https') {
    documentBuilder = documentBuilder.setSchemes('https', 'http');
  } else {
    documentBuilder = documentBuilder.setSchemes('http', 'https');
  }
  const options = documentBuilder.build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('/docs', app, document);

  await app.listen(coreConfig.port);
  /* hmr
  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
  */
}
bootstrap();
