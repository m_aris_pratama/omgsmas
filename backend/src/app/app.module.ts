import { Module, DynamicModule, Provider } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoreModule } from '@omgstore/core-nestjs';
import { AuthModule } from '@omgstore/auth-nestjs';
import { ActivityModule } from '../modules/activity/activity.module';
import { ChatModule } from '../modules/chat/chat.module';
import { ClientModule } from '../modules/client/client.module';
import { ProjectModule } from '../modules/project/project.module';

@Module({
  imports: [TypeOrmModule.forRoot(), CoreModule, AuthModule, ProjectModule]
})
export class AppModule {
  static forRoot(options: { providers: Provider[] }): DynamicModule {
    return {
      module: AppModule,
      imports: [
        TypeOrmModule.forRoot(),
        AuthModule.forRoot(options),
        ActivityModule.forRoot(options),
        ChatModule.forRoot(options),
        ClientModule.forRoot(options),
        ProjectModule.forRoot(options),
        CoreModule.forRoot(options)
      ]
    };
  }
}
