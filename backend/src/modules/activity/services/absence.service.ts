import {
  Inject,
  Injectable,
  MethodNotAllowedException,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CORE_CONFIG_TOKEN, ICoreConfig } from '@omgstore/core-nestjs';
import { Absence } from '../entities/absence.entity';
import { Team } from '../../client/entities/team.entity';
import moment = require('moment');

@Injectable()
export class AbsenceService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(Absence) private readonly repository: Repository<Absence>,
    @InjectRepository(Team) private readonly repositoryTeam: Repository<Team>
  ) {}
  async create(options: { item: Absence }) {
    try {
      options.item = await this.repository.save(options.item);
      return { status: 'ok' };
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      const item = await this.repository.findOneOrFail(options.id);
      return { absence: item };
    } catch (error) {
      throw error;
    }
  }
  async findByDateStore(options: {
    absenceDate: number;
    teamId: number;
    storeId: number;
    type: string;
  }) {
    try {
      console.log(options);
      const item = await this.repository.findOneOrFail({
        where: {
          absenceDate: options.absenceDate,
          absenceType: options.type,
          // store: {id: options.storeId},
          team: { id: options.teamId }
        }
      });
      return { item };
    } catch (error) {
      throw error;
    }
  }
  async findAttdDayMembers(options: { date: number }) {
    try {
      let qb = this.repository.createQueryBuilder('absence');
      qb = qb.leftJoinAndSelect('absence.store', 'store');
      qb = qb.leftJoinAndSelect('absence.team', 'team');
      qb = qb.where('absence.absenceDate = :date', {
        date: options.date
      });
      return await qb.getMany();
    } catch (error) {
      throw error;
    }
  }

  async findAttdDayUser(options: { userId: number; date: number }) {
    try {
      let qb = this.repository.createQueryBuilder('absence');
      qb = qb.leftJoinAndSelect('absence.store', 'store');
      qb = qb.leftJoinAndSelect('absence.team', 'team');
      qb = qb.leftJoinAndSelect('team.user', 'user');
      qb = qb.where('absence.absenceDate = :date AND user.id = :userId', {
        userId: options.userId,
        date: options.date
      });
      qb = qb.orderBy('absence.createdAt', 'DESC');
      return await qb.getMany();
    } catch (error) {
      throw error;
    }
  }

  async findTodayLast(options: { id: number }) {
    try {
      let qb = this.repository.createQueryBuilder('absence');
      qb = qb.leftJoinAndSelect('absence.team', 'team');
      qb = qb.where('absence.absenceDate = :date AND team.id = :id', {
        id: options.id,
        date: parseInt(moment().format('YYYYMMDD'), 10)
      });
      qb = qb.orderBy('absence.createdAt', 'DESC');
      qb.limit(1);
      return await qb.getOne();
    } catch (error) {
      throw error;
    }
  }

  async findAttdMember(options: {
    storeId: number;
    memberId: number;
    date: number;
  }) {
    try {
      let qb = this.repository.createQueryBuilder('absence');
      qb = qb.leftJoinAndSelect('absence.store', 'store');
      qb = qb.leftJoinAndSelect('absence.team', 'team');
      qb = qb.where(
        'absence.absenceDate = :date AND (store.id = :storeId OR store.id is NULL) AND team.id = :memberId',
        {
          date: options.date,
          storeId: options.storeId,
          memberId: options.memberId
        }
      );

      return await qb.getMany();
    } catch (error) {
      throw error;
    }
  }

  async findLastPosition() {
    try {
      let qb = this.repository.createQueryBuilder('absence');
      qb = qb.innerJoin(
        query => {
          return query
            .from('absence', 'a')
            .select('a.team_id, MAX(a.updated_at)', 'date_updated')
            .groupBy('a.team_id');
        },
        'a',
        'updated_at = a.date_updated'
      );
      qb = qb.leftJoinAndSelect('absence.team', 'team');
      qb = qb.leftJoinAndSelect('absence.store', 'store');
      qb = qb.where(
        'team.position = "member" AND absence.absenceDate = :date AND absence.longitude !=0 AND absence.latitude !=0',
        {
          date: parseInt(moment().format('YYYYMMDD'), 10)
        }
      );
      return await qb.getMany();
    } catch (error) {
      throw error;
    }
  }

  async findAll(options: {
    curPage: number;
    perPage: number;
    q?: string;
    sort?: string;
    position?: string;
  }) {
    try {
      let objects: [Absence[], number];
      let qb = this.repository.createQueryBuilder('absence');
      qb = qb.leftJoinAndSelect('absence.team', 'team');
      qb = qb.leftJoinAndSelect('absence.store', 'store');
      if (options.q) {
        qb = qb.where('team.name like :q', {
          q: `%${options.q}%`
        });
      }
      if (options.position === 'lead') {
        qb = qb.andWhere('team.position = "lead"');
      }
      if (options.position === 'member') {
        qb = qb.andWhere('team.position = "member"');
      }
      options.sort =
        options.sort &&
        new Absence().hasOwnProperty(options.sort.replace('-', ''))
          ? options.sort
          : '-id';
      const field = options.sort.replace('-', '');
      if (options.sort) {
        if (options.sort[0] === '-') {
          qb = qb.orderBy('absence.' + field, 'DESC');
        } else {
          qb = qb.orderBy('absence.' + field, 'ASC');
        }
      }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        list: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }

  async findAbsenceMember(options: {
    curPage: number;
    perPage: number;
    q?: string;
    date?: number;
    dateFrom?: string;
    dateTo?: string;
  }) {
    try {
      let objects: [Team[], number];
      let qb = this.repositoryTeam.createQueryBuilder('team');
      qb = qb.leftJoinAndSelect('team.absences', 'absence');
      qb = qb.leftJoinAndSelect('absence.store', 'store');
      if (options.dateFrom && options.dateTo) {
        qb = qb.where(
          'absence.created_at >= :dateFrom AND absence.created_at < :dateTo AND team.position = "member" ',
          {
            dateFrom: options.dateFrom + ' 00:00:00',
            dateTo: options.dateTo + ' 23:59:59'
          }
        );
      } else {
        const date = options.date
          ? options.date
          : parseInt(moment().format('YYYYMMDD'), 10);
        qb = qb.where(
          'absence.absenceDate = :date AND team.position = "member" ',
          { date }
        );
      }

      if (options.q) {
        qb = qb.andWhere('team.name like :q', {
          q: `%${options.q}%`
        });
      }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        list: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }

  async findAbsenceLead(options: {
    curPage: number;
    perPage: number;
    q?: string;
    date?: number;
    dateFrom?: string;
    dateTo?: string;
  }) {
    try {
      let objects: [Team[], number];
      let qb = this.repositoryTeam.createQueryBuilder('team');
      qb = qb.leftJoinAndSelect('team.absences', 'absence');
      qb = qb.leftJoinAndSelect('absence.store', 'store');
      if (options.dateFrom && options.dateTo) {
        qb = qb.where(
          'absence.created_at >= :dateFrom AND absence.created_at < :dateTo AND team.position = "lead" ',
          {
            dateFrom: options.dateFrom + ' 00:00:00',
            dateTo: options.dateTo + ' 23:59:59'
          }
        );
      } else {
        const date = options.date
          ? options.date
          : parseInt(moment().format('YYYYMMDD'), 10);
        qb = qb.where(
          'absence.absenceDate = :date AND team.position = "lead" ',
          { date }
        );
      }
      if (options.q) {
        qb = qb.andWhere('team.name like :q', {
          q: `%${options.q}%`
        });
      }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        list: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }

  async findAbsenceLeave(options: {
    curPage: number;
    perPage: number;
    q?: string;
    date?: number;
    dateFrom?: string;
    dateTo?: string;
  }) {
    try {
      let objects: [Team[], number];
      let qb = this.repositoryTeam.createQueryBuilder('team');
      qb = qb.leftJoinAndSelect('team.absences', 'absence');
      qb = qb.leftJoinAndSelect('absence.store', 'store');
      if (options.dateFrom && options.dateTo) {
        qb = qb.where(
          'absence.created_at >= :dateFrom AND absence.created_at < :dateTo AND absence.absenceType="leave" ',
          {
            dateFrom: options.dateFrom + ' 00:00:00',
            dateTo: options.dateTo + ' 23:59:59'
          }
        );
      } else {
        const date = options.date
          ? options.date
          : parseInt(moment().format('YYYYMMDD'), 10);
        qb = qb.where(
          'absence.absenceDate = :date AND absence.absenceType="leave" ',
          { date }
        );
      }
      if (options.q) {
        qb = qb.andWhere('team.name like :q', {
          q: `%${options.q}%`
        });
      }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        list: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }
}
