import { AbsenceService } from './absence.service';
import { ActivityService } from './activity.service';

export const services = [AbsenceService, ActivityService];
