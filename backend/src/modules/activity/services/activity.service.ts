import {
  Inject,
  Injectable,
  MethodNotAllowedException,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CORE_CONFIG_TOKEN, ICoreConfig } from '@omgstore/core-nestjs';
import { Absence } from '../entities/absence.entity';

@Injectable()
export class ActivityService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(Absence) private readonly repository: Repository<Absence>
  ) {}
  async create(options: { item: Absence }) {
    try {
      options.item = await this.repository.save(options.item);
      // const { absence } = await this.findById({ id: options.item.id });
      return { status: 'ok' };
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      const item = await this.repository.findOneOrFail(options.id);
      return { absence: item };
    } catch (error) {
      throw error;
    }
  }
}
