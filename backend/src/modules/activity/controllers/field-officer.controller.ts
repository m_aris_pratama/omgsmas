import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  Inject,
  Res
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiImplicitParam,
  ApiImplicitQuery,
  ApiResponse,
  ApiUseTags,
  ApiImplicitFile,
  ApiConsumes
} from '@nestjs/swagger';
import {
  AccessGuard,
  ParseIntWithDefaultPipe,
  Permissions,
  Roles,
  ICoreConfig
} from '@omgstore/core-nestjs';
import { plainToClass } from 'class-transformer';
import { OutActivityDto } from '../dto/out-activity.dto';
import { OutStoresDto } from '../dto/out-stores.dto';
import { StoreDto } from '../dto/store.dto';
import { InUploadTaskDto } from '../dto/in-uploadtask.dto';

import { diskStorage } from 'multer';
import { extname } from 'path';
import { InUploadNAPDto } from '../dto/in-uploadnap.dto';
import { StoreTasksDto } from '../dto/store-tasks.dto';
import { ProjectAssignsService } from '../../project/services/project-assignment.service';
import * as moment from 'moment';
import { StoresService } from '../../client/services/stores.service';
import { TaskAssignsService } from '../../project/services/task-assignments.service';
import { ProjectsService } from '../../project/services/projects.service';
import { TasksService } from '../../project/services/tasks.service';
import { TeamsService } from '../../client/services/teams.service';
import { TaskAssign } from '../../project/entities/task-assignment.entity';
import { NapUploadsService } from '../../project/services/nap-upload.service';
import { NapUpload } from '../../project/entities/nap-upload.entity';
import { doesNotReject } from 'assert';
import { InStoreStatusDto } from '../dto/in-storestatus.dto';
import { StoreStatussService } from '../../client/services/store-status.service';
import { StoreStatus } from '../../client/entities/store-status.entity';
import { Store } from 'modules/client/entities/store.entity';

@ApiUseTags('field')
@ApiBearerAuth()
@Controller('/api/field')
@UseGuards(AccessGuard)
export class FieldController {
  constructor(
    private readonly teamService: TeamsService,
    private readonly projectService: ProjectsService,
    private readonly projectAssignService: ProjectAssignsService,
    private readonly storeService: StoresService,
    private readonly taskAssignService: TaskAssignsService,
    private readonly taskService: TasksService,
    private readonly napService: NapUploadsService,
    private readonly storestatusService: StoreStatussService
  ) {}
  @Roles('isSuperuser')
  @Permissions('mobile')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The record has been query.',
    type: OutActivityDto
  })
  @ApiBearerAuth()
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('activities')
  async activities(@Req() req): Promise<OutActivityDto> {
    try {
      return plainToClass(
        OutActivityDto,
        await this.projectAssignService.findTodayActiviesByUser({
          user_id: req.user.id
        })
      );
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('mobile')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The record has been query.'
  })
  @ApiBearerAuth()
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('lead')
  async lead(@Req() req) {
    try {
      const items = await this.storeService.findByMemberStore({
        userId: req.user.id
      });
      const res = [];
      items.forEach(x => {
        if (!res.find(v => v.id === x.lead.id)) {
          res.push({
            id: x.lead.id,
            name: x.lead.name
          });
        }
      });
      return res;
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('mobile')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The record has been query.',
    type: OutStoresDto
  })
  @ApiBearerAuth()
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('stores')
  async store(@Req() req): Promise<OutStoresDto> {
    try {
      const res = await this.projectAssignService.findStoreAssignByUser({
        user_id: req.user.id
      });

      return { stores: res };
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('mobile')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The record has been query.',
    type: StoreDto
  })
  @ApiBearerAuth()
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Get('storedetail/:id')
  async storedetail(@Param('id', new ParseIntPipe()) id) {
    try {
      const item = await this.storeService.findById({ id });
      if (item) {
        return {
          id: item.store.id,
          name: item.store.name,
          longitude: item.store.longitude,
          latitude: item.store.latitude,
          photo: item.store.avatar
            ? process.env.DOMAIN +
              '/api/clients/uploadImage/' +
              item.store.avatar
            : process.env.DOMAIN + '/api/clients/uploadImage/default_store.jpg',
          address: item.store.address,
          isOpen: item.store.is_open,
          isActive: item.store.is_active
        };
      } else return { status: 'error', id: 0, msg: 'store not found' };
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('mobile')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The record has been query.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post('storestatus')
  async storestatus(@Req() req, @Body() dto: InStoreStatusDto) {
    try {
      const team = await this.teamService.findById({ id: req.user.id });
      await this.storestatusService.create({
        item: await plainToClass(StoreStatus, {
          team: team.team,
          store: { id: dto.storeId },
          ...dto
        })
      });
      const st = await this.storeService.findById({ id: dto.storeId });
      st.store.is_active = dto.active;
      st.store.is_open = dto.opened;
      await this.storeService.update({
        id: st.store.id,
        item: await plainToClass(Store, st.store)
      });

      return {
        status: 'ok'
      };
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('mobile')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The record has been query.',
    type: StoreTasksDto
  })
  @ApiBearerAuth()
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'store_id', type: Number })
  @Get('storetasks/:store_id')
  async storetasks(@Req() req, @Param('store_id', new ParseIntPipe()) id) {
    try {
      const res = await this.projectAssignService.findNotCompletedTaskByStoreId(
        { store_id: id }
      );

      return { storetasks: res };
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('mobile')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'The record has been successfully created.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post('phototask')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/uploads/task',
        filename: (req, file, cb) => {
          // Generating a 32 random chars long string
          const randomName = Array(32)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');
          // Calling the callback passing the random name generated with the original extension name
          cb(null, `${randomName}${extname(file.originalname)}`);
        }
      })
    })
  )
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({
    name: 'file',
    required: true,
    description: 'Photo Task'
  })
  async photo(@Req() req, @UploadedFile() file) {
    try {
      // tslint:disable-next-line:no-console
      console.log(file);

      return { fileName: file.filename };
    } catch (error) {
      throw error;
    }
  }

  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'get task image record'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'name', type: String })
  @Get('phototask/:name')
  async getImage(@Param('name') name, @Res() res) {
    try {
      return res.sendFile('uploads/task/' + name, { root: 'public' });
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('mobile')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The record has been post.'
  })
  @ApiBearerAuth()
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post('uploadtask')
  async uploadtask(@Req() req, @Body() dto: InUploadTaskDto) {
    try {
      const member = await this.teamService.findById({ id: req.user.id });
      const pAssign = await this.projectAssignService.findByProjectStore({
        projectId: dto.projectId,
        storeId: dto.storeId
      });

      let assign = await this.taskAssignService.findByProjAssign({
        id: pAssign.id
      });

      let insertId = null;
      if (+dto.taskId === 0) {
        // Program
        pAssign.program = dto.value;
        await this.projectAssignService.update({
          id: pAssign.id,
          item: pAssign
        });
        return { status: 'ok' };
      }
      else {
         insertId = await this.taskAssignService.create({
          item: await plainToClass(TaskAssign, {
            team: member.team,
            projectAssign: pAssign,
            task: { id: dto.taskId },
            value: dto.value,
            note: dto.note,
            longitude: dto.longitude ? dto.longitude : 0,
            latitude: dto.latitude ? dto.latitude : 0
          })
        });
      }

      // update projectAssign status
      const proj = await this.projectService.findByIdTasks({
        id: dto.projectId
      });
      assign = await this.taskAssignService.findByProjAssign({
        id: pAssign.id
      });

      // survey
      let c = 0;

      if (
        proj.item.taskSurvey &&
        proj.item.taskSurvey.tasks.length > 0 &&
        pAssign.survey_completed === null
      ) {
        proj.item.taskSurvey.tasks.forEach(val => {
          if (assign.filter(x => x.task.id === val.id).length > 0) c++;
        });
        if (proj.item.taskSurvey.tasks.length === c) {
          pAssign.survey_completed = moment().toDate();
          await this.projectAssignService.update({
            id: pAssign.id,
            item: pAssign
          });
        }
      }

      // instalation
      c = 0;
      if (
        proj.item.taskInstalation &&
        proj.item.taskInstalation.tasks.length > 0
      ) {
        proj.item.taskInstalation.tasks.forEach(val => {
          if (assign.filter(x => x.task.id === val.id).length > 0) c++;
        });
        if (proj.item.taskInstalation.tasks.length === c) {
          if (!pAssign.instalation_completed)
            pAssign.instalation_completed = moment().toDate();
          else pAssign.last_maintenance = moment().toDate();
          await this.projectAssignService.update({
            id: pAssign.id,
            item: pAssign
          });
        }
      }

      // removed
      c = 0;
      if (
        proj.item.taskRemoved &&
        proj.item.taskRemoved.tasks.length > 0 &&
        pAssign.removed_completed === null
      ) {
        proj.item.taskRemoved.tasks.forEach(val => {
          if (assign.filter(x => x.task.id === val.id).length > 0) c++;
        });
        if (proj.item.taskRemoved.tasks.length === c) {
          pAssign.removed_completed = moment().toDate();
          await this.projectAssignService.update({
            id: pAssign.id,
            item: pAssign
          });
        }
      }

      // survey
      c = 0;
      if (
        proj.item.taskNotInstalled &&
        proj.item.taskNotInstalled.tasks.length > 0 &&
        moment(pAssign.last_maintenance).format('YYYY-MM-DD') !==
          moment().format('YYYY-MM-DD')
      ) {
        proj.item.taskNotInstalled.tasks.forEach(val => {
          if (assign.filter(x => x.task.id === val.id).length > 0) c++;
        });
        if (proj.item.taskNotInstalled.tasks.length === c) {
          pAssign.last_maintenance = moment().toDate();
          await this.projectAssignService.update({
            id: pAssign.id,
            item: pAssign
          });
        }
      }

      return {
        status: 'ok',
        id: insertId.taskAssign ? insertId.taskAssign.id : 0
      };
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('mobile')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The record has been post.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post('uploadnap')
  async uploadNAP(@Req() req, @Body() dto: InUploadNAPDto) {
    try {
      // check field clean
      const team = await this.teamService.findByUserId({ id: req.user.id });
      const store = await this.storeService.findById({ id: dto.storeId });
      await this.napService.create({
        item: await plainToClass(NapUpload, {
          store: store.store, // mandatory
          team,
          clean: dto.clean,
          brand: dto.brand,
          program: dto.program,
          promo: dto.promo,
          file: dto.file, // mandatory
          desc: dto.desc,
          latitude: 0,
          longitude: 0
        })
      });
      return {
        status: 'ok'
      };
    } catch (error) {
      throw error;
    }
  }
}
