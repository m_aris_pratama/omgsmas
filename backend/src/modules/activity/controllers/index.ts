import { AbsenceController } from './absence.controller';
import { FieldController } from './field-officer.controller';
import { LeadController } from './field-lead.controller';
import { ReportController } from './report.controller';

export const controllers = [
  AbsenceController,
  FieldController,
  LeadController,
  ReportController
];
