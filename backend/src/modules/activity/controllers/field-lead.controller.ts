import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  Inject,
  Res,
  HttpService
} from '@nestjs/common';

import {
  ApiBearerAuth,
  ApiImplicitParam,
  ApiImplicitQuery,
  ApiResponse,
  ApiUseTags,
  ApiImplicitFile,
  ApiConsumes
} from '@nestjs/swagger';
import {
  AccessGuard,
  ParseIntWithDefaultPipe,
  Permissions,
  Roles,
  ICoreConfig
} from '@omgstore/core-nestjs';
import { plainToClass } from 'class-transformer';
import { OutActivityDto } from '../dto/out-activity.dto';
import { OutStoresDto } from '../dto/out-stores.dto';
import { StoreDto } from '../dto/store.dto';
import { InUploadTaskDto } from '../dto/in-uploadtask.dto';

import { diskStorage } from 'multer';
import { extname } from 'path';
import { InUploadNAPDto } from '../dto/in-uploadnap.dto';
import { StoreTasksDto } from '../dto/store-tasks.dto';
import { ProjectAssignsService } from '../../project/services/project-assignment.service';
import * as moment from 'moment';
import { StoresService } from '../../client/services/stores.service';
import { TaskAssignsService } from '../../project/services/task-assignments.service';
import { ProjectsService } from '../../project/services/projects.service';
import { TasksService } from '../../project/services/tasks.service';
import { TeamsService } from '../../client/services/teams.service';
import { TaskAssign } from '../../project/entities/task-assignment.entity';
import { OutLeadActivityDto } from '../dto/out-lead-activity.dto';
import { AbsenceService } from '../services/absence.service';
import { Observable } from 'rxjs';
import { AxiosResponse } from 'axios';
import { map } from 'rxjs/operators';

@ApiUseTags('lead')
@ApiBearerAuth()
@Controller('/api/lead')
@UseGuards(AccessGuard)
export class LeadController {
  constructor(
    private readonly teamService: TeamsService,
    private readonly projectService: ProjectsService,
    private readonly projectAssignService: ProjectAssignsService,
    private readonly storeService: StoresService,
    private readonly taskAssignService: TaskAssignsService,
    private readonly taskService: TasksService,
    private readonly absenceService: AbsenceService,
    private readonly httpService: HttpService
  ) {}
  @Roles('isSuperuser')
  @Permissions('mobile')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The record has been query.',
    type: OutLeadActivityDto
  })
  @ApiBearerAuth()
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('activities')
  async activities(@Req() req): Promise<OutLeadActivityDto> {
    try {
      return plainToClass(
        OutLeadActivityDto,
        await this.projectAssignService.findTodayActiviesByLead({
          user_id: req.user.id
        })
      );
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('mobile')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The record has been query.'
  })
  @ApiBearerAuth()
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('members')
  async members(@Req() req) {
    try {
      const mb = await this.projectAssignService.findTodayMembersActivies({
        user_id: req.user.id
      });

      const res = [];

      for (const val of mb) {
        const s = await this.absenceService.findTodayLast({ id: val.id });
        res.push({
          id: val.id,
          name: val.name,
          status: s ? s.absenceType : '',
          storeTotal: val.stores.length,
          storeCompleted: val.stores.filter(x => x.total === x.complete).length,
          taskTotal: val.taskTotal,
          taskCompleted: val.taskCompleted,
          inbox: false
        });
      }

      return { list: res };
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('mobile')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The record has been query.'
  })
  @ApiBearerAuth()
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitQuery({
    name: 'member_id',
    required: true,
    type: Number,
    description: 'member ID'
  })
  @ApiImplicitQuery({
    name: 'longitude',
    type: Number,
    description: 'longitude'
  })
  @ApiImplicitQuery({
    name: 'latitude',
    type: Number,
    description: 'latitude'
  })
  @Get('stores')
  async stores(
    @Req() req,
    @Query('member_id', new ParseIntWithDefaultPipe(1)) memberId,
    @Query('longitude') longitude,
    @Query('latitude') latitude
  ) {
    try {
      const _user = await this.teamService.findById({ id: memberId });
      const items = await this.projectAssignService.findStoreAssignByUser({
        user_id: _user.team.user.id,
        lead_id: req.user.id
      });
      const res = [];
      for (const val of items) {
        // google map API
        const ori = { lat: latitude, lng: longitude };
        const dest = { lat: val.latitude, lng: val.longitude };

        const data = await this.getTravels(ori, dest);
        console.log(data);

        res.push({
          id: val.id,
          name: val.name,
          longitude: val.longitude,
          latitude: val.latitude,
          traveTime: data ? data.duration.value / 60 : 0,
          travelDistance: data ? data.distance.value / 1000 : 0,
          taskCompleted: val.totalTask > 0 ? false : true
        });
      }

      return { stores: res };
    } catch (error) {
      throw error;
    }
  }

  async getTravels(ori: any, dest: any) {
    let res;
    const _key = process.env.GOOGLEMAP_KEY;
    const g = await this.httpService
      .get(
        'https://maps.googleapis.com/maps/api/distancematrix/json?origins=' +
          ori.lat +
          ',' +
          ori.lng +
          '&destinations=' +
          dest.lat +
          ',' +
          dest.lng +
          '&mode=driving&language=id&key=' +
          _key
      )
      .pipe(map(response => response.data));
    await g.forEach(x => {
      if (x.status === 'OK') {
        x.rows.forEach(y => {
          y.elements.forEach(el => {
            if (el.status === 'OK') res = el;
          });
        });
      }
    });
    return res;
  }

  @Roles('isSuperuser')
  @Permissions('mobile')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The record has been query.'
  })
  @ApiBearerAuth()
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'memberId', type: Number })
  @ApiImplicitParam({ name: 'storeId', type: Number })
  @Get('attendance/:storeId/:memberId')
  async attendance(
    @Req() req,
    @Param('memberId', new ParseIntPipe()) memberId,
    @Param('storeId', new ParseIntPipe()) storeId
  ) {
    try {
      const item = await this.absenceService.findAttdMember({
        memberId,
        storeId,
        date: parseInt(moment().format('YYYYMMDD'), 10)
      });

      const res = {
        store: {
          id: 0,
          name: '',
          logitude: 0,
          latitude: 0
        },
        absence: {
          preCheckIn: '',
          checkIn: '',
          checkOut: '',
          break: ''
        },
        task: {
          dailyCompleted: false, // todo
          napCompleted: false // todo
        }
      };
      item.forEach(val => {
        if (val.store) {
          res.store = {
            id: val.store.id,
            name: val.store.name,
            logitude: val.store.longitude,
            latitude: val.store.latitude
          };
        }
        switch (val.absenceType) {
          case 'pre-check-in':
            res.absence.preCheckIn = moment(val.updatedAt).format(
              'YYYY-MM-DD HH:mm:ss'
            );
            break;
          case 'check-in':
            res.absence.checkIn = moment(val.updatedAt).format(
              'YYYY-MM-DD HH:mm:ss'
            );
            break;
          case 'check-out':
            res.absence.checkOut = moment(val.updatedAt).format(
              'YYYY-MM-DD HH:mm:ss'
            );
            break;
          case 'break':
            res.absence.break = moment(val.updatedAt).format(
              'YYYY-MM-DD HH:mm:ss'
            );
            break;
        }
      });

      return { att: res };
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('mobile')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The record has been query.'
  })
  @ApiBearerAuth()
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('projects')
  async projects(@Req() req) {
    try {
      const proj = await this.projectService.findNotCompleted();
      const res = {
        period: {
          cycle: Math.ceil(moment().week() / 4),
          week: Math.ceil(moment().week() % 4)
        },
        totalProject: 0,
        projects: []
      };
      proj.forEach(val => {
        res.totalProject += 1;
        res.projects.push({
          id: val.id,
          name: val.name,
          step: 'survey', // todo
          information: val.information,
          guide: val.information,
          photo: val.photo
        });
      });

      return res;
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('mobile')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The record has been query.'
  })
  @ApiBearerAuth()
  @ApiImplicitParam({ name: 'id', type: Number })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('projectStores/:id')
  async projectStores(@Req() req, @Param('id', new ParseIntPipe()) id) {
    try {
      const item = await this.projectAssignService.findStoreProjectAssign({
        project_id: id,
        user_id: req.user.id
      });
      const res = {
        storeTotal: 0,
        storeCompleted: 0,
        stores: []
      };

      item.forEach(val => {
        res.storeTotal += 1;

        res.stores.push({
          id: val.id,
          name: val.name,
          longitude: val.longitude,
          latitude: val.latitude,
          traveTime: 0, // todo
          travelDistance: 0, // todo
          taskCompleted: val.totalTask > 0 ? false : true
        });
      });

      return res;
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('mobile')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The record has been query.'
  })
  @ApiBearerAuth()
  @ApiImplicitParam({ name: 'projId', type: Number })
  @ApiImplicitParam({ name: 'storeId', type: Number })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('projectTask/:projId/:storeId')
  async projectTask(
    @Req() req,
    @Param('projId', new ParseIntPipe()) projId,
    @Param('storeId', new ParseIntPipe()) storeId
  ) {
    try {
      const tassign = await this.taskAssignService.findByProjnStore({
        projectId: projId,
        storeId
      });
      const passign = await this.projectAssignService.findTaskByStoreProject({
        projId,
        storeId
      });

      const tasks = [];

      const st = this.projectAssignService.projectStatusToday({
        area: passign.store.city.area,
        project: passign.project,
        assign: passign
      });
      switch (st.step) {
        case 'survey':
          for (const val of passign.project.taskSurvey.tasks) {
            tasks.push({
              id: val.id,
              name: val.name,
              completed:
                tassign.filter(x => x.task.id === val.id).length > 0
                  ? true
                  : false
            });
          }
          break;
        case 'instalation':
          passign.project.taskInstalation.tasks.forEach(val => {
            tasks.push({
              id: val.id,
              name: val.name,
              completed:
                tassign.filter(x => x.task.id === val.id).length > 0
                  ? true
                  : false
            });
          });
          break;
        case 'removed':
          passign.project.taskRemoved.tasks.forEach(val => {
            tasks.push({
              id: val.id,
              name: val.name,
              completed:
                tassign.filter(x => x.task.id === val.id).length > 0
                  ? true
                  : false
            });
          });
          break;
        default:
          passign.project.taskNotInstalled.tasks.forEach(val => {
            tasks.push({
              id: val.id,
              name: val.name,
              completed:
                tassign.filter(x => x.task.id === val.id).length > 0
                  ? true
                  : false
            });
          });
      }

      const res = {
        store: {
          id: passign.store.id,
          name: passign.store.name,
          longitude: passign.store.longitude,
          latitude: passign.store.latitude,
          nameFo: passign.store.member.name
        },
        tasks
      };

      return res;
    } catch (error) {
      throw error;
    }
  }
}
