import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  Res
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiImplicitParam,
  ApiImplicitQuery,
  ApiResponse,
  ApiUseTags,
  ApiImplicitFile,
  ApiConsumes
} from '@nestjs/swagger';
import {
  AccessGuard,
  ParseIntWithDefaultPipe,
  Permissions,
  Roles
} from '@omgstore/core-nestjs';
import { plainToClass } from 'class-transformer';
import { InAbsenceDto } from '../dto/in-absence.dto';
import { OutAbsenceDto } from '../dto/out-absence.dto';
import { OutAbsencesDto } from '../dto/out-absences.dto';

import { AbsenceService } from '../services/absence.service';
import { Absence } from '../entities/absence.entity';

import { diskStorage } from 'multer';
import { extname } from 'path';
import { LocationDto } from '../dto/location.dto';
import { AbsenceStatusDto } from '../dto/absence-status.dto';
import { TeamsService } from '../../client/services/teams.service';
import * as moment from 'moment';
import { StoresService } from '../../client/services/stores.service';

import * as geolib from 'geolib';

@ApiUseTags('absence')
@ApiBearerAuth()
@Controller('/api/absence')
@UseGuards(AccessGuard)
export class AbsenceController {
  constructor(
    private readonly service: AbsenceService,
    private readonly team: TeamsService,
    private readonly storeService: StoresService
  ) {}

  @Roles('isSuperuser')
  @Permissions('attendance')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'The record has been successfully created.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post('check')
  async check(@Req() req, @Body() dto: LocationDto) {
    try {
      const team = await this.team.findByUserId({ id: req.user.id });
      let stores = null;
      if (team.position === 'member')
        stores = await this.storeService.findByMemberStore({
          userId: req.user.id
        });
      else
        stores = await this.storeService.findByLeadStore({
          userId: req.user.id
        });

      const here = [];
      let last = 100000.0;
      let last_name = '';
      stores.forEach(val => {
        const ds = geolib.getDistance(
          { latitude: dto.latitude, longitude: dto.longitude },
          { latitude: val.latitude, longitude: val.longitude }
        );
        if (ds < last) {
          last = ds;
          last_name = val.name;
        }
        if (ds <= 500.0) {
          here.push({
            storeId: val.id,
            storeName: val.name
          });
        }
      });

      if (here.length > 0) {
        return {
          status: 'ok',
          msg: 'success',
          storeId: here[0].storeId,
          storeName: here[0].storeName,
          stores: here
        };
      } else {
        return {
          status: 'error',
          msg:
            'You area out of valid location, nearest location ' +
            last_name +
            ' ' +
            last +
            ' meters',
          storeId: 0,
          storeName: '',
          stores: []
        };
      }
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('attendance')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'The record has been successfully created.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post('photo')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/uploads/absence',
        filename: (req, file, cb) => {
          // Generating a 32 random chars long string
          const randomName = Array(32)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');
          // Calling the callback passing the random name generated with the original extension name
          cb(null, `${randomName}${extname(file.originalname)}`);
        }
      })
    })
  )
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({
    name: 'file',
    required: true,
    description: 'Photo Selfie'
  })
  async photo(@Req() req, @UploadedFile() file) {
    try {
      // tslint:disable-next-line:no-console
      console.log(file);

      return { fileName: file.filename };
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('attendance')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'The record has been successfully created.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post()
  async create(@Req() req, @Body() dto: InAbsenceDto) {
    try {
      const member = await this.team.findByUserId({ id: req.user.id });

      return plainToClass(
        OutAbsenceDto,
        await this.service.create({
          item: await plainToClass(Absence, {
            team: { id: member.id },
            absenceDate: parseInt(
              moment()
                .add(7, 'h')
                .format('YYYYMMDD'),
              10
            ),
            store: { id: dto.storeId },
            ...dto
          })
        })
      );
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('attendance')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'get absence today',
    type: AbsenceStatusDto
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('today')
  async today(@Req() req) {
    try {
      const atts = await this.service.findAttdDayUser({
        userId: req.user.id,
        date: parseInt(
          moment()
            .add(7, 'h')
            .format('YYYYMMDD'),
          10
        )
      });
      if (atts.length <= 0) {
        return {
          preCheckIn: false,
          checkIn: false,
          break: false,
          leave: false,
          checkOut: false,
          storeId: 0
        };
      }
      const res = [];
      let _preCheckIn = false;
      let _checkIn = false;
      let _break = false;
      let _leave = false;
      let _store = 0;
      _preCheckIn =
        atts.filter(x => x.absenceType === 'pre-check-in').length > 0
          ? true
          : false;
      _leave =
        atts.filter(x => x.absenceType === 'leave').length > 0 ? true : false;

      let out = [];
      if (atts[0].team.position === 'member') {
        out = atts.filter(x => x.store !== null && x.absenceType !== 'break');
        if (out.length > 0) {
          if (out[0].absenceType === 'check-in') {
            _checkIn = true;
            _store = out[0].store.id;
            _break =
              atts.filter(
                x => x.absenceType === 'break' && x.store.id === out[0].store.id
              ).length > 0;
          }
        }
      } else {
        out = atts.filter(x => x.absenceType !== 'break');
        if (out.length > 0) {
          if (out[0].absenceType === 'check-in') {
            _checkIn = true;
            _store = 0;
            _break = atts.filter(x => x.absenceType === 'break').length > 0;
          }
        }
      }

      return {
        preCheckIn: _preCheckIn,
        checkIn: _checkIn,
        break: _break,
        leave: _leave,
        checkOut: false,
        storeId: _store
      };
    } catch (error) {
      throw error;
    }
  }

  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'get absence image record'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'name', type: String })
  @Get('photo/:name')
  async getImage(@Param('name') name, @Res() res) {
    try {
      return res.sendFile('uploads/absence/' + name, { root: 'public' });
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('attendance')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'get last absence'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('last')
  async getLastPosition() {
    try {
      const items = await this.service.findLastPosition();

      return { pos: items };
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('attendance')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutAbsencesDto,
    description: ''
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitQuery({
    name: 'q',
    required: false,
    type: String,
    description: 'Text for search (default: empty)'
  })
  @ApiImplicitQuery({
    name: 'sort',
    required: false,
    type: String,
    description: 'Column name for sort (default: -id)'
  })
  @ApiImplicitQuery({
    name: 'per_page',
    required: false,
    type: Number,
    description: 'Number of results to return per page. (default: 10)'
  })
  @ApiImplicitQuery({
    name: 'cur_page',
    required: false,
    type: Number,
    description: 'A page number within the paginated result set. (default: 1)'
  })
  @ApiImplicitQuery({
    name: 'position',
    required: false,
    type: String,
    description: 'position'
  })
  @Get()
  async findAll(
    @Query('cur_page', new ParseIntWithDefaultPipe(1)) curPage,
    @Query('per_page', new ParseIntWithDefaultPipe(10)) perPage,
    @Query('q') q,
    @Query('sort') sort,
    @Query('position') position
  ) {
    try {
      return plainToClass(
        OutAbsencesDto,
        await this.service.findAll({
          curPage,
          perPage,
          q,
          sort,
          position
        })
      );
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('attendance')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: ''
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitQuery({
    name: 'q',
    required: false,
    type: String,
    description: 'Text for search (default: empty)'
  })
  @ApiImplicitQuery({
    name: 'per_page',
    required: false,
    type: Number,
    description: 'Number of results to return per page. (default: 10)'
  })
  @ApiImplicitQuery({
    name: 'cur_page',
    required: false,
    type: Number,
    description: 'A page number within the paginated result set. (default: 1)'
  })
  @ApiImplicitQuery({
    name: 'date',
    required: false,
    type: Number,
    description: 'Date selected, format YYYYMMDD'
  })
  @Get('members')
  async findAbsenceMember(
    @Query('cur_page', new ParseIntWithDefaultPipe(1)) curPage,
    @Query('per_page', new ParseIntWithDefaultPipe(10)) perPage,
    @Query('q') q,
    @Query('date') date,
    @Query('dateFrom') dateFrom,
    @Query('dateTo') dateTo
  ) {
    try {
      return plainToClass(
        OutAbsencesDto,
        await this.service.findAbsenceMember({
          curPage,
          perPage,
          q,
          date,
          dateFrom,
          dateTo
        })
      );
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('attendance')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: ''
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitQuery({
    name: 'q',
    required: false,
    type: String,
    description: 'Text for search (default: empty)'
  })
  @ApiImplicitQuery({
    name: 'per_page',
    required: false,
    type: Number,
    description: 'Number of results to return per page. (default: 10)'
  })
  @ApiImplicitQuery({
    name: 'cur_page',
    required: false,
    type: Number,
    description: 'A page number within the paginated result set. (default: 1)'
  })
  @ApiImplicitQuery({
    name: 'date',
    required: false,
    type: Number,
    description: 'Date selected, format YYYYMMDD'
  })
  @Get('leads')
  async findAbsenceLead(
    @Query('cur_page', new ParseIntWithDefaultPipe(1)) curPage,
    @Query('per_page', new ParseIntWithDefaultPipe(10)) perPage,
    @Query('q') q,
    @Query('date') date,
    @Query('dateFrom') dateFrom,
    @Query('dateTo') dateTo
  ) {
    try {
      return plainToClass(
        OutAbsencesDto,
        await this.service.findAbsenceLead({
          curPage,
          perPage,
          q,
          date,
          dateFrom,
          dateTo
        })
      );
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('attendance')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: ''
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitQuery({
    name: 'q',
    required: false,
    type: String,
    description: 'Text for search (default: empty)'
  })
  @ApiImplicitQuery({
    name: 'per_page',
    required: false,
    type: Number,
    description: 'Number of results to return per page. (default: 10)'
  })
  @ApiImplicitQuery({
    name: 'cur_page',
    required: false,
    type: Number,
    description: 'A page number within the paginated result set. (default: 1)'
  })
  @ApiImplicitQuery({
    name: 'date',
    required: false,
    type: Number,
    description: 'Date selected, format YYYYMMDD'
  })
  @Get('leaves')
  async findAbsenceLeave(
    @Query('cur_page', new ParseIntWithDefaultPipe(1)) curPage,
    @Query('per_page', new ParseIntWithDefaultPipe(10)) perPage,
    @Query('q') q,
    @Query('date') date,
    @Query('dateFrom') dateFrom,
    @Query('dateTo') dateTo
  ) {
    try {
      return plainToClass(
        OutAbsencesDto,
        await this.service.findAbsenceLeave({
          curPage,
          perPage,
          q,
          date,
          dateFrom,
          dateTo
        })
      );
    } catch (error) {
      throw error;
    }
  }
}
