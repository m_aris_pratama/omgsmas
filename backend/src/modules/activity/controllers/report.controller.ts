import {
  Controller,
  HttpCode,
  HttpStatus,
  Req,
  UseGuards,
  Get,
  Put,
  Param,
  ParseIntPipe,
  Res,
  Header,
  Query
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiResponse,
  ApiUseTags,
  ApiImplicitParam
} from '@nestjs/swagger';
import {
  AccessGuard,
  Roles,
  Permissions,
  ParseIntWithDefaultPipe
} from '@omgstore/core-nestjs';
import { ProjectsService } from 'modules/project/services/projects.service';
import { ProjectAssignsService } from 'modules/project/services/project-assignment.service';
import moment = require('moment');
import * as XLSX from 'xlsx';
import { NapUploadsService } from 'modules/project/services/nap-upload.service';
import { plainToClass } from 'class-transformer';
import { StoreStatussService } from 'modules/client/services/store-status.service';

@ApiUseTags('report')
@ApiBearerAuth()
@Controller('/api/report')
@UseGuards(AccessGuard)
export class ReportController {
  constructor(
    private readonly projectService: ProjectsService,
    private readonly napService: NapUploadsService,
    private readonly storeStatusService: StoreStatussService
  ) { }

  @Roles('isSuperuser')
  @Permissions('report')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'The record has been successfully created.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('summary')
  async summary() {
    try {
      return 'Haloooo';
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('report')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'The record has been successfully created.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('storeStatus')
  async findStoreStatus(
    @Query('cur_page', new ParseIntWithDefaultPipe(1)) curPage,
    @Query('per_page', new ParseIntWithDefaultPipe(10)) perPage,
    @Query('q') q,
    @Query('dateFrom') dateFrom,
    @Query('dateTo') dateTo
  ) {
    try {
      return await this.storeStatusService.findAll({
        curPage,
        perPage,
        q,
        dateFrom,
        dateTo
      });
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('report')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'The record has been successfully created.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('storeStatus/toggleActive')
  async update(
    @Query('id', new ParseIntPipe()) id
  ) {
    try {
      const store = await this.storeStatusService.findById({ id });
      store.storeStatus.active = store.storeStatus.active ? false : true;
      const result = await this.storeStatusService.update({ id, item: store.storeStatus })
      return result;
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('report')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'The record has been successfully created.'
  })
  @ApiImplicitParam({ name: 'id', type: Number })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('instalation/:id')
  async instalation(@Param('id', new ParseIntPipe()) id) {
    try {
      const res = await this.projectService.projectInstalationReport({ id });

      return res;
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('report')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'The record has been successfully created.'
  })
  @ApiImplicitParam({ name: 'id', type: Number })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('survey/:id')
  async survey(@Param('id', new ParseIntPipe()) id) {
    try {
      const res = await this.projectService.projectSurveyReport({ id });
      return res;
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('report')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'The record has been successfully created.'
  })
  @ApiImplicitParam({ name: 'id', type: Number })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('removed/:id')
  async removed(@Param('id', new ParseIntPipe()) id) {
    try {
      const res = await this.projectService.projectRemovedReport({ id });
      return res;
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'download data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('downloadInstalationSummary/:id')
  async downloadInstalationSummary(
    @Param('id', new ParseIntPipe()) id,
    @Res() res
  ) {
    try {
      const item = await this.projectService.projectInstalationReport({ id });
      const rs = [];
      // titles
      rs.push(['Client : ', item.client]);
      rs.push(['Brand/ Kategory : ', item.brand + ' - ' + item.category]);
      rs.push(['Program : ', item.program]);
      rs.push(['CYCLE : ', item.cycle]);
      rs.push(['report generate : ', moment().format('DD MMM YYYY HH:mm')]);
      rs.push([' ']);
      rs.push(['SUMMARY']);
      rs.push([
        'NO.',
        'RETAIL',
        'JUMLAH TOKO',
        'RINCIAN TERPASANG',
        ...Array(item.programs.split(',').length - 1),
        'TOTAL TERPASANG',
        'TOTAL TIDAK TERPASANG',
        'RINCIAN TIDAK TERPASANG',
        '',
        ''
      ]);
      rs.push([
        '',
        '',
        '',
        ...item.programs.split(','),
        '',
        '',
        'PBT',
        'RNV',
        'DLL'
      ]);
      const retails = [];
      item.assign.forEach(el => {
        if (!retails.find(x => x.code === el.retailCode)) {
          const programs = [];
          const notInstalled = [];
          item.programs.split(',').forEach(e => {
            programs.push({
              x: e,
              y: item.assign.filter(
                a => a.program === e && a.retailCode === el.retailCode
              ).length
            });
          });
          ['PBT', 'RNV'].forEach(e => {
            notInstalled.push({
              x: e,
              y: item.assign.filter(
                a => a.notInstall === e && a.retailCode === el.retailCode
              ).length
            });
          });
          notInstalled.push({
            x: 'DLL',
            y: item.assign.filter(
              a =>
                a.retailCode === el.retailCode &&
                a.notInstall === '' &&
                a.program === ''
            ).length
          });
          retails.push({
            code: el.retailCode,
            name: el.retailName,
            store: item.assign.filter(a => a.retailCode === el.retailCode)
              .length,
            programs,
            notInstalled,
            instalation: item.assign.filter(
              a => a.retailCode === el.retailCode && a.program !== ''
            ).length
          });
        }
      });
      let i = 0;
      retails.forEach(val => {
        rs.push([
          ++i,
          val.name,
          val.store,
          ...val.programs.map(a => a.y),
          val.instalation,
          val.store - val.instalation,
          ...val.notInstalled.map(a => a.y)
        ]);
      });

      const ws = XLSX.utils.aoa_to_sheet(rs);
      const wb = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'summary');
      const wr = XLSX.write(wb, {
        bookType: 'xlsx',
        bookSST: false,
        type: 'buffer'
      });
      res.setHeader(
        'Content-Type',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      );
      res.setHeader(
        'Content-Disposition',
        'attachment; filename="instalationSummary.xlsx"'
      );
      return res.status(200).send(wr);
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'download data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('downloadInstalationDetail/:id')
  async downloadInstalationDetail(
    @Param('id', new ParseIntPipe()) id,
    @Res() res
  ) {
    try {
      const item = await this.projectService.projectInstalationReport({ id });
      const rs = [];
      // titles
      rs.push(['Client : ', item.client]);
      rs.push(['Brand/ Kategory : ', item.brand + ' - ' + item.category]);
      rs.push(['Program : ', item.program]);
      rs.push(['CYCLE : ', item.cycle]);
      rs.push(['report generate : ', moment().format('DD MMM YYYY HH:mm')]);
      rs.push([' ']);
      rs.push([
        'NO.',
        'NAMA TOKO',
        'RETAIL',
        'TANGGAL PASANG',
        'PROGRAM',
        ...item.tasks.map(x => x.name)
      ]);

      let i = 0;
      item.assign.forEach(val => {
        const tasks_val = [];
        item.tasks.forEach(ts => {
          let v = '';
          val.tasks.forEach(t => {
            const go = t.items.find(r => r.task.id === ts.id);
            if (go) v = go.value;
          });
          tasks_val.push(v);
        });
        rs.push([
          ++i,
          val.store,
          val.retailName,
          val.dateInstalled
            ? moment(val.dateInstalled).format('DD MMM YYYY HH:mm')
            : '',
          val.program,
          ...tasks_val
        ]);
      });

      res.setHeader(
        'Content-Type',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      );
      res.setHeader(
        'Content-Disposition',
        'attachment; filename="instalationDetails.xlsx"'
      );

      const Workbook = require('./../../../../libs/exceljs').Workbook;
      const wb = new Workbook();
      const ws = wb.addWorksheet('detail');
      ws.addRows(rs);
      let j = 0;
      for (i = 0; i < rs.length; i++) {
        for (j = 0; j < rs[i].length; j++) {
          if (typeof rs[i][j] === 'string') {
            if (rs[i][j].lastIndexOf('.jpg') > 0) {
              ws.getRow(i + 1).getCell(j + 1).value = '';
              const imageId = wb.addImage({
                filename: 'public/uploads/task/' + rs[i][j],
                extension: 'jpg'
              });
              ws.addImage(imageId, {
                tl: { col: j + 0.1, row: i + 0.1 },
                br: { col: j + 1, row: i + 1 },
                editAs: 'oneCell'
              });
              ws.getRow(i + 1).height = 150;
              ws.getRow(i + 1).alignment = {
                vertical: 'top',
                horizontal: 'left'
              };
              ws.getColumn(j + 1).width = 22;
            }
          }
        }
      }

      await wb.xlsx.write(res);
      return res.end();
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'download data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('downloadInstalationImage/:id')
  async downloadInstalationImage(
    @Param('id', new ParseIntPipe()) id,
    @Res() res
  ) {
    try {
      res.setHeader(
        'Content-Type',
        'application/zip, application/octet-stream, application/x-zip-compressed, multipart/x-zip'
      );
      res.setHeader(
        'Content-Disposition',
        'attachment; filename="instalationImages.zip"'
      );
      const item = await this.projectService.projectInstalationReport({ id });

      const fs = require('fs');
      const JSZip = require('jszip');
      const zip = new JSZip();

      item.assign.forEach(val => {
        val.tasks.forEach(ts => {
          ts.items.forEach(it => {
            if (it.value.indexOf('.jpg') >= 0) {
              const name =
                ts.date + '-' + val.store + '-' + it.task.name + '.jpg';
              const ph = 'public/uploads/task/' + it.value;
              zip.file(name.replace(/ |\//gi, '_'), fs.readFileSync(ph));
            }
          });
        });
      });

      zip.generateAsync({ type: 'nodebuffer' }).then(content => {
        return res.status(200).send(content);
      });
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'download data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('downloadSurveySummary/:id')
  async downloadSurveySummary(@Param('id', new ParseIntPipe()) id, @Res() res) {
    try {
      const item = await this.projectService.projectSurveyReport({ id });
      const taskSurveys = [];
      const taskSurveyIds = [];
      item.tasks.forEach(x => {
        if (x.type === 'dropdown') {
          x.options.split(',').forEach(y => {
            taskSurveys.push(x.name + '-' + y);
            taskSurveyIds.push({ id: x.id, val: y });
          });
        }
        if (x.type === 'text') taskSurveys.push(x.name);
      });
      const rs = [];
      // titles
      rs.push(['Client : ', item.client]);
      rs.push(['Brand/ Kategory : ', item.brand + ' - ' + item.category]);
      rs.push(['Program : ', item.program]);
      rs.push(['CYCLE : ', item.cycle]);
      rs.push(['report generate : ', moment().format('DD MMM YYYY HH:mm')]);
      rs.push([' ']);
      rs.push(['SUMMARY']);
      rs.push(['NO.', 'RETAIL', 'JUMLAH TOKO', ...taskSurveys, 'TOTAl SURVEY']);

      const retails = [];
      item.assign.forEach(el => {
        if (!retails.find(x => x.code === el.retailCode)) {
          const taskVal = [];
          taskSurveyIds.forEach(tk => {
            let c = 0;
            item.assign
              .filter(a => a.retailCode === el.retailCode)
              .forEach(x => {
                x.tasks.forEach(y => {
                  y.items.forEach(z => {
                    if (z.task.id === tk.id && z.value === tk.val) c++;
                  });
                });
              });
            taskVal.push(c);
          });
          retails.push({
            code: el.retailCode,
            name: el.retailName,
            taskVal,
            store: item.assign.filter(a => a.retailCode === el.retailCode)
              .length,
            total: item.assign.filter(
              a => a.retailCode === el.retailCode && a.dateSurveyed !== null
            ).length
          });
        }
      });
      let i = 0;
      retails.forEach(val => {
        rs.push([++i, val.name, val.store, ...val.taskVal, val.total]);
      });

      const ws = XLSX.utils.aoa_to_sheet(rs);
      const wb = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'summary');
      const wr = XLSX.write(wb, {
        bookType: 'xlsx',
        bookSST: false,
        type: 'buffer'
      });
      res.setHeader(
        'Content-Type',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      );
      res.setHeader(
        'Content-Disposition',
        'attachment; filename="surveySummary.xlsx"'
      );
      return res.status(200).send(wr);
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'download data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('downloadSurveyDetail/:id')
  async downloadSurveyDetail(@Param('id', new ParseIntPipe()) id, @Res() res) {
    try {
      const item = await this.projectService.projectSurveyReport({ id });
      const rs = [];
      // titles
      rs.push(['Client : ', item.client]);
      rs.push(['Brand/ Kategory : ', item.brand + ' - ' + item.category]);
      rs.push(['Program : ', item.program]);
      rs.push(['CYCLE : ', item.cycle]);
      rs.push(['report generate : ', moment().format('DD MMM YYYY HH:mm')]);
      rs.push([' ']);
      rs.push([
        'NO.',
        'NAMA TOKO',
        'RETAIL',
        'TANGGAL',
        ...item.tasks.map(x => x.name)
      ]);

      let i = 0;
      item.assign.forEach(val => {
        const tasks_val = [];
        item.tasks.forEach(ts => {
          let v = '';
          val.tasks.forEach(t => {
            const go = t.items.find(r => r.task.id === ts.id);
            if (go) v = go.value;
          });
          tasks_val.push(v);
        });
        rs.push([
          ++i,
          val.store,
          val.retailName,
          val.dateSurveyed
            ? moment(val.dateSurveyed).format('DD MMM YYYY HH:mm')
            : '',
          ...tasks_val
        ]);
      });

      res.setHeader(
        'Content-Type',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      );
      res.setHeader(
        'Content-Disposition',
        'attachment; filename="surveyDetails.xlsx"'
      );

      const Workbook = require('./../../../../libs/exceljs').Workbook;
      const wb = new Workbook();
      const ws = wb.addWorksheet('details');
      ws.addRows(rs);
      let j = 0;
      for (i = 0; i < rs.length; i++) {
        for (j = 0; j < rs[i].length; j++) {
          if (typeof rs[i][j] === 'string') {
            if (rs[i][j].lastIndexOf('.jpg') > 0) {
              ws.getRow(i + 1).getCell(j + 1).value = '';
              const imageId = wb.addImage({
                filename: 'public/uploads/task/' + rs[i][j],
                extension: 'jpg'
              });
              ws.addImage(imageId, {
                tl: { col: j + 0.1, row: i + 0.1 },
                br: { col: j + 1, row: i + 1 },
                editAs: 'oneCell'
              });
              ws.getRow(i + 1).height = 150;
              ws.getRow(i + 1).alignment = {
                vertical: 'top',
                horizontal: 'left'
              };
              ws.getColumn(j + 1).width = 22;
            }
          }
        }
      }

      await wb.xlsx.write(res);
      return res.end();
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'download data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('downloadSurveyImage/:id')
  async downloadSurveyImage(@Param('id', new ParseIntPipe()) id, @Res() res) {
    try {
      res.setHeader(
        'Content-Type',
        'application/zip, application/octet-stream, application/x-zip-compressed, multipart/x-zip'
      );
      res.setHeader(
        'Content-Disposition',
        'attachment; filename="surveyImages.zip"'
      );
      const item = await this.projectService.projectSurveyReport({ id });

      const fs = require('fs');
      const JSZip = require('jszip');
      const zip = new JSZip();

      item.assign.forEach(val => {
        val.tasks.forEach(ts => {
          ts.items.forEach(it => {
            if (it.value.indexOf('.jpg') >= 0) {
              const name =
                ts.date + '-' + val.store + '-' + it.task.name + '.jpg';
              const ph = 'public/uploads/task/' + it.value;
              zip.file(name.replace(/ |\//gi, '_'), fs.readFileSync(ph));
            }
          });
        });
      });

      zip.generateAsync({ type: 'nodebuffer' }).then(content => {
        return res.status(200).send(content);
      });
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'download data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('downloadRemovedSummary/:id')
  async downloadRemovedSummary(
    @Param('id', new ParseIntPipe()) id,
    @Res() res
  ) {
    try {
      const item = await this.projectService.projectRemovedReport({ id });
      const taskRemoveds = [];
      const taskRemovedIds = [];
      item.tasks.forEach(x => {
        if (x.type === 'dropdown') {
          x.options.split(',').forEach(y => {
            taskRemoveds.push(x.name + '-' + y);
            taskRemovedIds.push({ id: x.id, val: y });
          });
        }
        if (x.type === 'text') taskRemoveds.push(x.name);
      });
      const rs = [];
      // titles
      rs.push(['Client : ', item.client]);
      rs.push(['Brand/ Kategory : ', item.brand + ' - ' + item.category]);
      rs.push(['Program : ', item.program]);
      rs.push(['CYCLE : ', item.cycle]);
      rs.push(['report generate : ', moment().format('DD MMM YYYY HH:mm')]);
      rs.push([' ']);
      rs.push(['SUMMARY']);
      rs.push([
        'NO.',
        'RETAIL',
        'JUMLAH TOKO',
        ...taskRemoveds,
        'TOTAL PENCABUTAN'
      ]);

      const retails = [];
      item.assign.forEach(el => {
        if (!retails.find(x => x.code === el.retailCode)) {
          const taskVal = [];
          taskRemovedIds.forEach(tk => {
            let c = 0;
            item.assign
              .filter(a => a.retailCode === el.retailCode)
              .forEach(x => {
                x.tasks.forEach(y => {
                  y.items.forEach(z => {
                    if (z.task.id === tk.id && z.value === tk.val) c++;
                  });
                });
              });
            taskVal.push(c);
          });
          retails.push({
            code: el.retailCode,
            name: el.retailName,
            taskVal,
            store: item.assign.filter(a => a.retailCode === el.retailCode)
              .length,
            total: item.assign.filter(
              a => a.retailCode === el.retailCode && a.dateRemoved !== null
            ).length
          });
        }
      });
      let i = 0;
      retails.forEach(val => {
        rs.push([++i, val.name, val.store, ...val.taskVal, val.total]);
      });

      const ws = XLSX.utils.aoa_to_sheet(rs);
      const wb = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'summary');
      const wr = XLSX.write(wb, {
        bookType: 'xlsx',
        bookSST: false,
        type: 'buffer'
      });
      res.setHeader(
        'Content-Type',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      );
      res.setHeader(
        'Content-Disposition',
        'attachment; filename="removedSummary.xlsx"'
      );
      return res.status(200).send(wr);
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'download data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('downloadRemovedDetail/:id')
  async downloadRemovedDetail(@Param('id', new ParseIntPipe()) id, @Res() res) {
    try {
      const item = await this.projectService.projectRemovedReport({ id });
      const rs = [];
      // titles
      rs.push(['Client : ', item.client]);
      rs.push(['Brand/ Kategory : ', item.brand + ' - ' + item.category]);
      rs.push(['Program : ', item.program]);
      rs.push(['CYCLE : ', item.cycle]);
      rs.push(['report generate : ', moment().format('DD MMM YYYY HH:mm')]);
      rs.push([' ']);
      rs.push([
        'NO.',
        'NAMA TOKO',
        'RETAIL',
        'TANGGAL',
        ...item.tasks.map(x => x.name)
      ]);

      let i = 0;
      item.assign.forEach(val => {
        const tasks_val = [];
        item.tasks.forEach(ts => {
          let v = '';
          val.tasks.forEach(t => {
            const go = t.items.find(r => r.task.id === ts.id);
            if (go) v = go.value;
          });
          tasks_val.push(v);
        });
        rs.push([
          ++i,
          val.store,
          val.retailName,
          val.dateSurveyed
            ? moment(val.dateSurveyed).format('DD MMM YYYY HH:mm')
            : '',
          ...tasks_val
        ]);
      });

      res.setHeader(
        'Content-Type',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      );
      res.setHeader(
        'Content-Disposition',
        'attachment; filename="removedDetails.xlsx"'
      );

      const Workbook = require('./../../../../libs/exceljs').Workbook;
      const wb = new Workbook();
      const ws = wb.addWorksheet('details');
      ws.addRows(rs);
      let j = 0;
      for (i = 0; i < rs.length; i++) {
        for (j = 0; j < rs[i].length; j++) {
          if (typeof rs[i][j] === 'string') {
            if (rs[i][j].lastIndexOf('.jpg') > 0) {
              ws.getRow(i + 1).getCell(j + 1).value = '';
              const imageId = wb.addImage({
                filename: 'public/uploads/task/' + rs[i][j],
                extension: 'jpg'
              });
              ws.addImage(imageId, {
                tl: { col: j + 0.1, row: i + 0.1 },
                br: { col: j + 1, row: i + 1 },
                editAs: 'oneCell'
              });
              ws.getRow(i + 1).height = 150;
              ws.getRow(i + 1).alignment = {
                vertical: 'top',
                horizontal: 'left'
              };
              ws.getColumn(j + 1).width = 22;
            }
          }
        }
      }

      await wb.xlsx.write(res);
      return res.end();
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'download data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('downloadRemovedImage/:id')
  async downloadRemovedImage(@Param('id', new ParseIntPipe()) id, @Res() res) {
    try {
      res.setHeader(
        'Content-Type',
        'application/zip, application/octet-stream, application/x-zip-compressed, multipart/x-zip'
      );
      res.setHeader(
        'Content-Disposition',
        'attachment; filename="removedImages.zip"'
      );
      const item = await this.projectService.projectRemovedReport({ id });

      const fs = require('fs');
      const JSZip = require('jszip');
      const zip = new JSZip();

      item.assign.forEach(val => {
        val.tasks.forEach(ts => {
          ts.items.forEach(it => {
            if (it.value.indexOf('.jpg') >= 0) {
              const name =
                ts.date + '-' + val.store + '-' + it.task.name + '.jpg';
              const ph = 'public/uploads/task/' + it.value;
              zip.file(name.replace(/ |\//gi, '_'), fs.readFileSync(ph));
            }
          });
        });
      });

      zip.generateAsync({ type: 'nodebuffer' }).then(content => {
        return res.status(200).send(content);
      });
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'download data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('downloadNAPSummary/:dFrom/:dTo')
  async downloadNAPSummary(
    @Param('dFrom') dFrom,
    @Param('dTo') dTo,
    @Res() res
  ) {
    try {
      if (dFrom <= 0)
        dFrom = moment()
          .subtract(7, 'days')
          .format('YYYY-MM-DD');
      if (dTo <= 0) dTo = moment().format('YYYY-MM-DD');
      const items = await this.napService.findAll({
        curPage: 1,
        perPage: 0,
        dateFrom: dFrom,
        dateTo: dTo
      });
      const rs = [];
      // titles
      rs.push(['NAP REPORT']);
      rs.push(['From :', dFrom]);
      rs.push(['To : ', dTo]);
      rs.push(['report generate : ', moment().format('YYYY-MM-DD HH:mm')]);
      rs.push([' ']);
      rs.push([
        'NO.',
        'NAMA TOKO',
        'RETAIL',
        'City',
        'Area',
        'Tanggal',
        'Jumlah NAP',
        'Lorong Bersih'
      ]);

      let i = 0;
      items.list.forEach(el => {
        const stn = [
          ++i,
          el.name,
          el.retail.name,
          el.city.name,
          el.city.area.name
        ];
        // group by date
        const gr = [];
        el.naps.forEach(x => {
          let a = gr.find(
            g => g.date === moment(x.created).format('YYYY-MM-DD')
          );
          if (!a)
            gr.push(
              (a = {
                date: moment(x.created).format('YYYY-MM-DD'),
                nap: 0,
                clean: 0
              })
            );
          if (x.clean) a.clean++;
          else a.nap++;
        });

        gr.forEach(x => {
          rs.push([...stn, x.date, x.nap, x.clean]);
        });
      });

      const ws = XLSX.utils.aoa_to_sheet(rs);
      const wb = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'summary');
      const wr = XLSX.write(wb, {
        bookType: 'xlsx',
        bookSST: false,
        type: 'buffer'
      });
      res.setHeader(
        'Content-Type',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      );
      res.setHeader(
        'Content-Disposition',
        'attachment; filename="napSummary.xlsx"'
      );
      return res.status(200).send(wr);
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'download data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('downloadNAPDetail/:dFrom/:dTo')
  async downloadNAPDetail(
    @Param('dFrom') dFrom,
    @Param('dTo') dTo,
    @Res() res
  ) {
    try {
      if (dFrom <= 0)
        dFrom = moment()
          .subtract(7, 'days')
          .format('YYYY-MM-DD');
      if (dTo <= 0) dTo = moment().format('YYYY-MM-DD');
      const items = await this.napService.findAll({
        curPage: 1,
        perPage: 0,
        dateFrom: dFrom,
        dateTo: dTo
      });

      res.setHeader(
        'Content-Type',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      );
      res.setHeader(
        'Content-Disposition',
        'attachment; filename="napDetail.xlsx"'
      );

      const rs = [];
      // titles
      rs.push(['NAP REPORT']);
      rs.push(['From :', dFrom]);
      rs.push(['To : ', dTo]);
      rs.push(['report generate : ', moment().format('YYYY-MM-DD HH:mm')]);
      rs.push([' ']);
      rs.push([
        'NAMA TOKO',
        'RETAIL',
        'City',
        'Area',
        'Tanggal',
        'Foto',
        'Brand',
        'Program',
        'Promo',
        'Bersih?'
      ]);
      const photos = [];
      items.list.forEach(el => {
        const stn = [el.name, el.retail.name, el.city.name, el.city.area.name];
        el.naps.forEach(x => {
          rs.push([
            ...stn,
            moment(x.created).format('YYYY-MM-DD'),
            '',
            x.brand,
            x.program,
            x.promo,
            x.clean ? 'Ya' : 'Tidak'
          ]);
          // photo
          photos.push(x.file);
        });
      });

      const Workbook = require('./../../../../libs/exceljs').Workbook;
      const wb = new Workbook();
      const ws = wb.addWorksheet('detail');
      ws.addRows(rs);
      ws.getColumn(6).width = 22;
      for (let i = 0; i < photos.length; i++) {
        if (!photos[i]) continue;
        const imageId = wb.addImage({
          filename: 'public/uploads/task/' + photos[i],
          extension: 'jpg'
        });
        ws.addImage(imageId, {
          tl: { col: 5.1, row: i + 6.1 },
          br: { col: 6, row: i + 7 },
          editAs: 'oneCell'
        });
        ws.getRow(i + 7).height = 150;
        ws.getRow(i + 7).alignment = { vertical: 'top', horizontal: 'left' };
      }

      await wb.xlsx.write(res);
      return res.end();
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'download data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('downloadNAPImage/:dFrom/:dTo')
  async downloadNAPImage(@Param('dFrom') dFrom, @Param('dTo') dTo, @Res() res) {
    try {
      if (dFrom <= 0)
        dFrom = moment()
          .subtract(7, 'days')
          .format('YYYY-MM-DD');
      if (dTo <= 0) dTo = moment().format('YYYY-MM-DD');
      res.setHeader(
        'Content-Type',
        'application/zip, application/octet-stream, application/x-zip-compressed, multipart/x-zip'
      );
      res.setHeader(
        'Content-Disposition',
        'attachment; filename="napImages.zip"'
      );
      const items = await this.napService.findAll({
        curPage: 1,
        perPage: 0,
        dateFrom: dFrom,
        dateTo: dTo
      });

      const fs = require('fs');
      const JSZip = require('jszip');
      const zip = new JSZip();

      items.list.forEach(val => {
        val.naps.forEach(x => {
          if (x.file.indexOf('.jpg') >= 0) {
            const name =
              moment(x.created).format('YYYYMMDD') +
              '-' +
              val.name +
              '-' +
              x.promo +
              '.jpg';
            const ph = 'public/uploads/task/' + x.file;
            zip.file(name.replace(/ |\//gi, '_'), fs.readFileSync(ph));
          }
        });
      });

      zip.generateAsync({ type: 'nodebuffer' }).then(content => {
        return res.status(200).send(content);
      });
    } catch (error) {
      throw error;
    }
  }
}
