import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';
import { Exclude, Type } from 'class-transformer';

export class StoreDto {
  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  id: number;

  @MaxLength(256)
  @ApiModelProperty()
  name: string;

  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  longitude: number;

  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  latitude: number;

  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  totalTask: number;
}
