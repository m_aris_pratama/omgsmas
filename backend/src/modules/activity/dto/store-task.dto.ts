import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { ProjectDto } from './project.dto';
import { SurveyDto } from './survey.dto';
import { TaskDto } from './task.dto';

export class StoreTaskDto {
  @Type(() => ProjectDto)
  @ApiModelProperty({ type: ProjectDto })
  project: ProjectDto;

  @Type(() => SurveyDto)
  @ApiModelProperty({ type: SurveyDto, isArray: true })
  surveys: SurveyDto[];

  @Type(() => TaskDto)
  @ApiModelProperty({ type: TaskDto, isArray: true })
  tasks: TaskDto[];
}
