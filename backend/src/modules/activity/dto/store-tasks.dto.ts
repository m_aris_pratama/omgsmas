import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { StoreTaskDto } from '../dto/store-task.dto';

export class StoreTasksDto {
  @Type(() => StoreTaskDto)
  @ApiModelProperty({ type: StoreTaskDto, isArray: true })
  storetasks: StoreTaskDto[];
}
