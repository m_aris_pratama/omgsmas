import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength, IsOptional } from 'class-validator';
import { Exclude, Type } from 'class-transformer';

export class InStoreStatusDto {
  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  storeId: number;

  @IsNotEmpty()
  @ApiModelProperty()
  opened: boolean;

  @IsNotEmpty()
  @ApiModelProperty()
  active: boolean;

  @IsOptional()
  @ApiModelProperty()
  report: string;
}
