import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';
import { Exclude, Type } from 'class-transformer';

export class ProjectDto {
  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  id: number;

  @MaxLength(256)
  @ApiModelProperty()
  name: string;

  @ApiModelProperty()
  type: number;

  @ApiModelProperty()
  information: string;

  @ApiModelProperty()
  guide: string;
}
