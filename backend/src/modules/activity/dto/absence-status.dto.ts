import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';
import { Exclude, Type } from 'class-transformer';

export class AbsenceStatusDto {
  @ApiModelProperty({ type: Boolean })
  preCheckIn: boolean;

  @ApiModelProperty({ type: Boolean })
  CheckIn: boolean;

  @ApiModelProperty({ type: Boolean })
  break: boolean;

  @ApiModelProperty({ type: Boolean })
  leave: boolean;

  @ApiModelProperty({ type: Boolean })
  checkOut: boolean;
}
