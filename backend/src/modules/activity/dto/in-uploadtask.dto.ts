import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';
import { Exclude, Type } from 'class-transformer';

export class InUploadTaskDto {
  @ApiModelProperty({ type: Number })
  projectId: number;

  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  storeId: number;

  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  taskId: number;

  @IsNotEmpty()
  @ApiModelProperty()
  value: string;

  @ApiModelProperty()
  note: string;

  @ApiModelPropertyOptional()
  longitude: number;

  @ApiModelPropertyOptional()
  latitude: number;
}
