import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { AbsenceDto } from '../dto/absence.dto';

export class OutAbsenceDto {
  @Type(() => AbsenceDto)
  @ApiModelProperty({ type: AbsenceDto })
  absence: AbsenceDto;
}
