import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';
import { Exclude, Type } from 'class-transformer';
import { StoreDto } from '../dto/store.dto';

export class OutStoresDto {
  @Type(() => StoreDto)
  @ApiModelProperty({ type: StoreDto, isArray: true })
  stores: StoreDto[];
}
