import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength, IsOptional } from 'class-validator';
import { Exclude, Type } from 'class-transformer';
import { OutUserDto } from '@omgstore/core-nestjs';

export class AbsenceDto {
  @ApiModelProperty({ type: Number })
  id: number;

  @ApiModelPropertyOptional()
  absenceDate: number;

  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  storeId: number;

  @IsNotEmpty()
  @ApiModelProperty({
    enum: ['pre-check-in', 'check-in', 'break', 'leave', 'check-out']
  })
  absenceType: string;

  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  longitude: number;

  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  latitude: number;

  @IsOptional()
  @ApiModelProperty({ type: String })
  photo: string;

  @IsOptional()
  @ApiModelProperty({ type: String })
  note: string;

  @Type(() => OutUserDto)
  @ApiModelProperty({ type: OutUserDto })
  userId: OutUserDto;
}
