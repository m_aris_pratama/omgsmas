import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';
import { Exclude, Type } from 'class-transformer';

export class OutLeadActivityDto {
  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  cycle: number;

  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  week: number;

  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  totalOfficer: number;

  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  totalStore: number;

  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  totalProject: number;
}
