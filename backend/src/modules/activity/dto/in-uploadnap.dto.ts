import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';
import { Exclude, Type } from 'class-transformer';

export class InUploadNAPDto {
  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  storeId: number;

  @ApiModelProperty()
  clean: boolean;

  @ApiModelPropertyOptional()
  brand: string;

  @ApiModelPropertyOptional()
  program: string;

  @ApiModelPropertyOptional()
  promo: string;

  @ApiModelProperty()
  file: string;

  @ApiModelPropertyOptional()
  desc: string;
}
