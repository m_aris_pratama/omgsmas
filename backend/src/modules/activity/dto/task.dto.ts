import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';
import { Exclude, Type } from 'class-transformer';

export class TaskDto {
  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  id: number;

  @MaxLength(256)
  @ApiModelProperty()
  name: string;

  @ApiModelProperty({ type: Boolean })
  required: boolean;

  @MaxLength(256)
  @ApiModelProperty()
  type: string;

  @ApiModelPropertyOptional()
  options: string;

  @ApiModelPropertyOptional()
  logic: string;
}
