import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';
import { Exclude, Type } from 'class-transformer';

export class LocationDto {
  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  longitude: number;

  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  latitude: number;
}
