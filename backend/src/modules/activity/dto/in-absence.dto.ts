import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';

export class InAbsenceDto {
  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  storeId: number;

  @IsNotEmpty()
  @ApiModelProperty({
    enum: ['pre-check-in', 'check-in', 'break', 'leave', 'check-out']
  })
  absenceType: string;

  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  longitude: number;

  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  latitude: number;

  @IsNotEmpty()
  @ApiModelProperty({ type: String })
  photo: string;
}
