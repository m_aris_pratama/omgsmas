import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';
import { Exclude, Type } from 'class-transformer';

export class SurveyDto {
  @IsNotEmpty()
  @ApiModelProperty({ type: Number })
  id: number;

  @MaxLength(256)
  @ApiModelProperty()
  question: string;

  @IsNotEmpty()
  @ApiModelProperty({ type: Array })
  answers: [];
}
