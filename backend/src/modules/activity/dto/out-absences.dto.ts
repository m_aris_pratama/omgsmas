import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { MetaDto } from '@omgstore/core-nestjs';
import { AbsenceDto } from '../dto/absence.dto';

export class OutAbsencesDto {
  @Type(() => AbsenceDto)
  @ApiModelProperty({ type: AbsenceDto, isArray: true })
  list: AbsenceDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
