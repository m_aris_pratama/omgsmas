import { DynamicModule, Module, Provider, HttpModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configs } from './configs';
import { controllers } from './controllers';
import { entities } from './entities';
import { services } from './services';
import { CoreModule } from '@omgstore/core-nestjs';
import { ProjectModule } from '../project/project.module';
import { ClientModule } from '../client/client.module';

@Module({
  imports: [
    HttpModule,
    TypeOrmModule.forFeature(entities),
    CoreModule.forFeature(),
    ProjectModule.forFeature(),
    ClientModule.forFeature()
  ],
  controllers: [...controllers],
  providers: [...configs, ...services],
  exports: [...configs, ...services]
})
export class ActivityModule {
  static forFeature(): DynamicModule {
    return {
      module: ActivityModule,
      providers: [...services],
      exports: [...configs, ...services]
    };
  }
  static forRoot(options: { providers: Provider[] }): DynamicModule {
    return {
      module: ActivityModule,
      imports: [
        TypeOrmModule.forFeature(entities),
        CoreModule.forFeature(),
        ProjectModule.forFeature(),
        ClientModule.forFeature()
      ],
      controllers: [...controllers],
      providers: [...configs, ...options.providers, ...services],
      exports: [...configs, ...services]
    };
  }
}
