import { ContentType, Group, Permission } from '@omgstore/core-nestjs';
import { plainToClass } from 'class-transformer';
import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class AddTaskTable1537203127693 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    // create table
    await queryRunner.createTable(
      new Table({
        name: 'absence',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'absence_date',
            type: 'integer',
            isNullable: false
          },
          {
            name: 'team_id',
            type: 'integer',
            isNullable: false
          },
          {
            name: 'store_id',
            type: 'integer',
            isNullable: false
          },
          {
            name: 'absence_type',
            type: 'varchar(50)',
            isNullable: false
          },
          {
            name: 'longitude',
            type: 'double',
            isNullable: false
          },
          {
            name: 'latitude',
            type: 'double',
            isNullable: false
          },
          {
            name: 'photo',
            type: 'varchar(255)',
            isNullable: true
          },
          {
            name: 'note',
            type: 'varchar(512)',
            isNullable: true
          },
          {
            name: 'created_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            isNullable: false,
            default: 'CURRENT_TIMESTAMP'
          },
          {
            name: 'updated_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            default: 'CURRENT_TIMESTAMP'
          }
        ]
      }),
      true
    );
    // create/load content type
    const ctNewEntity = await queryRunner.manager
      .getRepository<ContentType>(ContentType)
      .save(plainToClass(ContentType, { name: 'absence', title: 'Absence' }));
    // create permissions
    const nPermissions = await queryRunner.manager
      .getRepository<Permission>(Permission)
      .save(
        plainToClass(Permission, [
          {
            title: 'Can create absence',
            name: 'add_absence',
            contentType: ctNewEntity
          },
          {
            title: 'Can read photo absence',
            name: 'get_photo_absence',
            contentType: ctNewEntity
          }
        ])
      );

    // add permissions to groups
    const gUserFO = await queryRunner.manager
      .getRepository<Group>(Group)
      .findOneOrFail({
        where: {
          name: 'userFO'
        },
        relations: ['permissions']
      });
    const gUserTL = await queryRunner.manager
      .getRepository<Group>(Group)
      .findOneOrFail({
        where: {
          name: 'userTL'
        },
        relations: ['permissions']
      });
    const gAdmin = await queryRunner.manager
      .getRepository<Group>(Group)
      .findOneOrFail({
        where: {
          name: 'cms-admin'
        },
        relations: ['permissions']
      });
    gUserFO.permissions = [...gUserFO.permissions, ...nPermissions];
    gUserTL.permissions = [...gUserTL.permissions, ...nPermissions];
    gAdmin.permissions = [...gAdmin.permissions, ...nPermissions];
    await queryRunner.manager
      .getRepository<Group>(Group)
      .save([gUserFO, gUserTL, gAdmin]);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
