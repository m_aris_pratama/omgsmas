import { CustomValidationError, User } from '@omgstore/core-nestjs';
import { Store } from 'modules/client/entities/store.entity';
import { Team } from 'modules/client/entities/team.entity';
import {
  IsNotEmpty,
  IsOptional,
  MaxLength,
  validateSync
} from 'class-validator';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn
} from 'typeorm';

/* id : int
absence_date : int ( YYYYMMDD )
user_id: int
store_id: int
absence_type: enum( pre-check-in, check-in, break, leave, check-out)
longitude: double
latitude: double
photo: varchar(255)
note: varchar(512)
created_at : date
updated_at : date
*/
@Entity({ name: 'absence' })
export class Absence {
  @PrimaryGeneratedColumn()
  id: number = undefined;

  @Column({ name: 'absence_date' })
  @IsNotEmpty()
  absenceDate: number = undefined;

  @ManyToOne(type => Team, { eager: true, nullable: true })
  @JoinColumn({ name: 'team_id' })
  team: Team = undefined;

  @ManyToOne(type => Store, { eager: true, nullable: true })
  @JoinColumn({ name: 'store_id' })
  store: Store = undefined;

  @Column({ name: 'absence_type' })
  @IsNotEmpty()
  absenceType: string;

  @Column()
  @IsNotEmpty()
  longitude: number;

  @Column()
  @IsNotEmpty()
  latitude: number;

  @Column()
  @IsOptional()
  photo: string;

  @Column()
  @IsOptional()
  note: string;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date = undefined;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date = undefined;

  @BeforeInsert()
  doBeforeInsertion() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }

  @BeforeUpdate()
  doBeforeUpdate() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }
}
