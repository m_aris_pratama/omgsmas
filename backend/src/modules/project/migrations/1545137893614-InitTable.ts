import { ContentType, Group, Permission } from '@omgstore/core-nestjs';
import { plainToClass } from 'class-transformer';
import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableIndex,
  TableForeignKey
} from 'typeorm';

export class InitTable1545137893614 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(
      new Table({
        name: 'project',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'brand_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'name',
            type: 'varchar(150)',
            isNullable: false
          },
          {
            name: 'programs',
            type: 'varchar(1024)',
            isNullable: false
          },
          {
            name: 'information',
            type: 'text'
          },
          {
            name: 'photo',
            type: 'varchar(512)',
            isNullable: true
          },
          {
            name: 'business_unit_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'cycle_year',
            type: 'integer'
          },
          {
            name: 'cycle_start',
            type: 'integer'
          },
          {
            name: 'cycle_end',
            type: 'integer'
          },
          {
            name: 'survey_date',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            isNullable: true
          },
          {
            name: 'survey_target',
            default: 0,
            type: 'integer'
          },
          {
            name: 'instalation_date',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            isNullable: true
          },
          {
            name: 'instalation_date_jabo',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            isNullable: true
          },
          {
            name: 'instalation_target',
            default: 0,
            type: 'integer'
          },
          {
            name: 'removed_date',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            isNullable: true
          },
          {
            name: 'removed_target',
            default: 0,
            type: 'integer'
          },
          {
            name: 'task_survey_template_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'task_instalation_template_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'task_removed_template_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'task_not_installed_template_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'is_active',
            type: 'boolean',
            default: true,
            isNullable: false
          },
          {
            name: 'created_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            isNullable: false,
            default: 'CURRENT_TIMESTAMP'
          },
          {
            name: 'updated_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            default: 'CURRENT_TIMESTAMP'
          }
        ]
      }),
      true
    );

    await queryRunner.createTable(
      new Table({
        name: 'project_assignment',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'project_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'store_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'program',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'survey_completed',
            isNullable: true,
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString()
          },
          {
            name: 'instalation_completed',
            isNullable: true,
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString()
          },
          {
            name: 'removed_completed',
            isNullable: true,
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString()
          },
          {
            name: 'last_maintenance',
            isNullable: true,
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString()
          },
          {
            name: 'survey_approved',
            type: 'boolean',
            default: false
          },
          {
            name: 'survey_count',
            type: 'integer',
            default: 0
          },
          {
            name: 'is_active',
            type: 'boolean',
            default: true,
            isNullable: false
          },
          {
            name: 'is_priority',
            type: 'boolean',
            default: false,
            isNullable: false
          },
          {
            name: 'created_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            isNullable: false,
            default: 'CURRENT_TIMESTAMP'
          },
          {
            name: 'updated_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            default: 'CURRENT_TIMESTAMP'
          }
        ]
      }),
      true
    );

    await queryRunner.createTable(
      new Table({
        name: 'task_assignment',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'team_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'project_assignment_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'task_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'value',
            type: 'varchar(1024)'
          },
          {
            name: 'note',
            type: 'varchar(1024)'
          },
          {
            name: 'longitude',
            type: 'double',
            isNullable: false
          },
          {
            name: 'latitude',
            type: 'double',
            isNullable: false
          },
          {
            name: 'created_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            isNullable: false,
            default: 'CURRENT_TIMESTAMP'
          },
          {
            name: 'updated_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            default: 'CURRENT_TIMESTAMP'
          }
        ]
      }),
      true
    );

    await queryRunner.createTable(
      new Table({
        name: 'nap_upload',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'team_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'store_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'clean',
            type: 'boolean',
            default: false
          },
          {
            name: 'brand',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'program',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'promo',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'file',
            type: 'varchar(255)',
            isNullable: false
          },
          {
            name: 'desc',
            type: 'varchar(512)',
            isNullable: true
          },
          {
            name: 'longitude',
            type: 'double',
            isNullable: false
          },
          {
            name: 'latitude',
            type: 'double',
            isNullable: false
          },
          {
            name: 'created_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            isNullable: false,
            default: 'CURRENT_TIMESTAMP'
          },
          {
            name: 'updated_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            default: 'CURRENT_TIMESTAMP'
          }
        ]
      }),
      true
    );

    await queryRunner.createTable(
      new Table({
        name: 'task',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'name',
            type: 'varchar(255)',
            isNullable: false
          },
          {
            name: 'template_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'required',
            type: 'boolean'
          },
          {
            name: 'type',
            type: 'varchar(100)'
          },
          {
            name: 'options',
            type: 'varchar(1024)'
          },
          {
            name: 'logic',
            type: 'varchar(1024)'
          },
          {
            name: 'is_active',
            type: 'boolean',
            default: true,
            isNullable: false
          },
          {
            name: 'created_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            isNullable: false,
            default: 'CURRENT_TIMESTAMP'
          },
          {
            name: 'updated_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            default: 'CURRENT_TIMESTAMP'
          }
        ]
      }),
      true
    );

    await queryRunner.createTable(
      new Table({
        name: 'task_template',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'category',
            type: 'integer',
            isNullable: false
          },
          {
            name: 'name',
            type: 'varchar(255)',
            isNullable: false
          }
        ]
      }),
      true
    );

    await queryRunner.createTable(
      new Table({
        name: 'program',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'name',
            type: 'varchar(255)',
            isNullable: false
          },
          {
            name: 'desc',
            type: 'varchar(512)',
            isNullable: true
          }
        ]
      }),
      true
    );

    await queryRunner.createIndex(
      'program',
      new TableIndex({
        name: 'IDX_PRG_NM',
        columnNames: ['name']
      })
    );

    await queryRunner.createForeignKey(
      'project',
      new TableForeignKey({
        name: 'FK_PG_BR_ID',
        columnNames: ['brand_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'brand',
        onDelete: 'SET NULL'
      })
    );

    await queryRunner.createForeignKey(
      'project',
      new TableForeignKey({
        name: 'FK_PJC_BU_ID',
        columnNames: ['business_unit_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'business_unit',
        onDelete: 'SET NULL'
      })
    );

    await queryRunner.createForeignKey(
      'project_assignment',
      new TableForeignKey({
        name: 'FK_PGA_PG_ID',
        columnNames: ['project_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'project',
        onDelete: 'SET NULL'
      })
    );

    await queryRunner.createForeignKey(
      'project_assignment',
      new TableForeignKey({
        name: 'FK_PGA_ST_ID',
        columnNames: ['store_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'store',
        onDelete: 'SET NULL'
      })
    );

    await queryRunner.createForeignKey(
      'task_assignment',
      new TableForeignKey({
        name: 'FK_TSA_ST_ID',
        columnNames: ['project_assignment_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'project_assignment',
        onDelete: 'SET NULL'
      })
    );

    await queryRunner.createForeignKey(
      'task_assignment',
      new TableForeignKey({
        name: 'FK_TSA_TS_ID',
        columnNames: ['task_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'task',
        onDelete: 'SET NULL'
      })
    );

    await queryRunner.createForeignKey(
      'task',
      new TableForeignKey({
        name: 'FK_TAS_TM_ID',
        columnNames: ['template_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'task_template',
        onDelete: 'SET NULL'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
