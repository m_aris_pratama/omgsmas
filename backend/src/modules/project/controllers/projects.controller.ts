import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UseGuards,
  Req
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiImplicitParam,
  ApiImplicitQuery,
  ApiResponse,
  ApiUseTags
} from '@nestjs/swagger';
import { plainToClass } from 'class-transformer';
import { Permissions } from '@omgstore/core-nestjs';
import { Roles } from '@omgstore/core-nestjs';
import { InProjectDto } from '../dto/in-project.dto';
import { OutProjectDto } from '../dto/out-project.dto';
import { OutProjectsDto } from '../dto/out-projects.dto';
import { Project } from '../entities/project.entity';
import { AccessGuard } from '@omgstore/core-nestjs';
import { ParseIntWithDefaultPipe } from '@omgstore/core-nestjs';
import { ProjectsService } from '../services/projects.service';
import { ProjectAssignsService } from '../services/project-assignment.service';
import { StoresService } from '../../client/services/stores.service';
import { ProjectAssign } from '../entities/project-assignment.entity';
import { UnitAdminsService } from 'modules/client/services/unit-admin.service';

@ApiUseTags('projects')
@ApiBearerAuth()
@Controller('/api/projects')
@UseGuards(AccessGuard)
export class ProjectsController {
  constructor(
    private readonly service: ProjectsService,
    private readonly assignService: ProjectAssignsService,
    private readonly storeService: StoresService,
    private readonly uadmService: UnitAdminsService
  ) {}
  @Roles('isSuperuser')
  @Permissions('project')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: OutProjectDto,
    description: 'The record has been successfully created.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post()
  async create(@Body() dto: InProjectDto) {
    try {
      const item = await this.service.create({
        item: await plainToClass(Project, dto)
      });
      // add project assignments
      const st = await this.storeService.findByBuUnit({
        id: item.project.businessUnit.id
      });
      st.forEach(val => {
        this.assignService.create({
          item: plainToClass(ProjectAssign, {
            store: { id: val.id },
            project: { id: item.project.id }
          })
        });
      });

      return plainToClass(OutProjectDto, item);
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('project')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutProjectDto,
    description: 'The record has been successfully updated.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Put(':id')
  async update(@Param('id', new ParseIntPipe()) id, @Body() dto: InProjectDto) {
    try {
      return plainToClass(
        OutProjectDto,
        await this.service.update({
          id,
          item: await plainToClass(Project, dto)
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('project')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The record has been successfully deleted.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Delete(':id')
  async delete(@Param('id', new ParseIntPipe()) id) {
    try {
      return plainToClass(
        OutProjectDto,
        await this.service.delete({
          id
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('project')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutProjectDto,
    description: ''
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Get(':id')
  async findById(@Param('id', new ParseIntPipe()) id) {
    try {
      return plainToClass(
        OutProjectDto,
        await this.service.findById({
          id
        })
      );
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('project')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutProjectsDto,
    description: ''
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitQuery({
    name: 'q',
    required: false,
    type: String,
    description: 'Text for search (default: empty)'
  })
  @ApiImplicitQuery({
    name: 'sort',
    required: false,
    type: String,
    description: 'Column name for sort (default: -id)'
  })
  @ApiImplicitQuery({
    name: 'per_page',
    required: false,
    type: Number,
    description: 'Number of results to return per page. (default: 10)'
  })
  @ApiImplicitQuery({
    name: 'cur_page',
    required: false,
    type: Number,
    description: 'A page number within the paginated result set. (default: 1)'
  })
  @Get()
  async findAll(
    @Req() req,
    @Query('cur_page', new ParseIntWithDefaultPipe(1)) curPage,
    @Query('per_page', new ParseIntWithDefaultPipe(10)) perPage,
    @Query('q') q,
    @Query('sort') sort
  ) {
    try {
      let unit = null;
      if (req.user && req.user.groups.find(x => x.id === 4)) {
        const usr = await this.uadmService.findByUserId({ id: req.user.id });
        if (usr) unit = usr.businessUnit.id;
      }

      return plainToClass(
        OutProjectsDto,
        await this.service.findAll({
          curPage,
          perPage,
          q,
          sort,
          unit
        })
      );
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('report')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'report'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Get('summary/:id')
  async summary(@Param('id', new ParseIntPipe()) id) {
    try {
      const prj = await this.service.projectSummaryReport({ id });

      const res = {
        summary: {
          survey: 0,
          instalation: 0,
          removed: 0,
          store: 0,
          programs: []
        },
        retails: []
      };
      prj.forEach(val => {
        res.summary.survey += parseInt(val.surveyed, 10);
        res.summary.instalation += parseInt(val.installed, 10);
        res.summary.removed += parseInt(val.removed, 10);
        res.summary.store += parseInt(val.store, 10);
        // program
        const pg = [];
        Object.keys(val).forEach(key => {
          if (key.indexOf('_') === 7) {
            pg.push({
              name: key.replace('program_', ''),
              value: parseInt(val[key], 10)
            });
            const a = res.summary.programs.find(
              x => x.x === key.replace('program_', '')
            );
            if (a) a.y += parseInt(val[key], 10);
            else
              res.summary.programs.push({
                x: key.replace('program_', ''),
                y: parseInt(val[key], 10)
              });
          }
        });
        res.retails.push({
          id: val.retail_id,
          code: val.retail_code,
          name: val.retail_name,
          store: val.store,
          survey: parseInt(val.surveyed, 10),
          instalation: parseInt(val.installed, 10),
          removed: parseInt(val.removed, 10),
          programs: pg
        });
      });

      return res;
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('report')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'report'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Get('report/:id')
  async report(@Param('id', new ParseIntPipe()) id) {
    try {
      const prj = await this.service.findProjectReport({ id });

      return prj;
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('report')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'report'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Get('instaledReport/:id')
  async InstaledReport(@Param('id', new ParseIntPipe()) id) {
    try {
      const prj = await this.service.projectInstalationReportByRetail({ id });

      return prj;
    } catch (error) {
      throw error;
    }
  }

  @Roles('isSuperuser')
  @Permissions('dash')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'dashboard'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Get('dash/:id')
  async Dashboard(@Req() req) {
    try {
      const items = await this.service.projectDashboard();
      const res = [];
      items.forEach(val => {
        const a = res.find(x => x.id === val.proj_id);
        if (a) {
          a.assigns += parseInt(val.total, 10);
          a.program.push({
            name: val.program ? val.program : 'Not Installed',
            total: parseInt(val.total, 10)
          });
        } else {
          res.push({
            id: val.proj_id,
            name: val.proj_name,
            client: val.client_name,
            brand: val.brand_name,
            assigns: parseInt(val.total, 10),
            program: [
              {
                name: val.program ? val.program : 'Not Installed',
                total: parseInt(val.total, 10)
              }
            ]
          });
        }
      });
      return res;
    } catch (error) {
      throw error;
    }
  }
  
  @Roles('isSuperuser')
  @Permissions('dash')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'dashboard'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Get('dashboardByClient/:id')
  async DashboardByClient(@Param('id', new ParseIntPipe()) id) {
    try {
      const items = await this.service.projectDashboardByClient(id);
      
      return items;
    } catch (error) {
      throw error;
    }
  }
  
  @Roles('isSuperuser')
  @Permissions('dash')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'dashboard'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'businessUnitId', type: Number })
  @Get('dashboardByClientDetail/:businessUnitId/:clientId')
  async DashboardByClientDetail(@Param('businessUnitId', new ParseIntPipe()) bid,@Param('clientId', new ParseIntPipe()) cid) {
    try {
      const items = await this.service.projectDashboardByClientDetail(bid,cid);
      
      return items;
    } catch (error) {
      throw error;
    }
  }
  
}
