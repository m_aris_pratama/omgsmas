import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UseGuards,
  Req,
  Res,
  UseInterceptors,
  FileInterceptor,
  UploadedFile
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiImplicitParam,
  ApiImplicitQuery,
  ApiResponse,
  ApiUseTags,
  ApiConsumes,
  ApiImplicitFile
} from '@nestjs/swagger';
import { plainToClass } from 'class-transformer';
import { Permissions } from '@omgstore/core-nestjs';
import { Roles } from '@omgstore/core-nestjs';
import { InTaskDto } from '../dto/in-task.dto';
import { OutTaskDto } from '../dto/out-task.dto';
import { OutTasksDto } from '../dto/out-tasks.dto';
import { Task } from '../entities/task.entity';
import { AccessGuard } from '@omgstore/core-nestjs';
import { ParseIntWithDefaultPipe } from '@omgstore/core-nestjs';
import { TasksService } from '../services/tasks.service';
import * as XLSX from 'xlsx';
import { TaskTemplatesService } from '../services/task-template.service';

@ApiUseTags('tasks')
@ApiBearerAuth()
@Controller('/api/tasks')
@UseGuards(AccessGuard)
export class TasksController {
  constructor(
    private readonly service: TasksService,
    private readonly serviceTpl: TaskTemplatesService
  ) {}
  @Roles('isSuperuser')
  @Permissions('project')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: OutTaskDto,
    description: 'The record has been successfully created.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post()
  async create(@Body() dto: InTaskDto) {
    try {
      return plainToClass(
        OutTaskDto,
        await this.service.create({
          item: await plainToClass(Task, dto)
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('project')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutTaskDto,
    description: 'The record has been successfully updated.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Put(':id')
  async update(@Param('id', new ParseIntPipe()) id, @Body() dto: InTaskDto) {
    try {
      return plainToClass(
        OutTaskDto,
        await this.service.update({
          id,
          item: await plainToClass(Task, dto)
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('project')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The record has been successfully deleted.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Delete(':id')
  async delete(@Param('id', new ParseIntPipe()) id) {
    try {
      return plainToClass(
        OutTaskDto,
        await this.service.delete({
          id
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('project')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutTaskDto,
    description: ''
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Get(':id')
  async findById(@Param('id', new ParseIntPipe()) id) {
    try {
      return plainToClass(
        OutTaskDto,
        await this.service.findById({
          id
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('project')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutTasksDto,
    description: ''
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitQuery({
    name: 'q',
    required: false,
    type: String,
    description: 'Text for search (default: empty)'
  })
  @ApiImplicitQuery({
    name: 'sort',
    required: false,
    type: String,
    description: 'Column name for sort (default: -id)'
  })
  @ApiImplicitQuery({
    name: 'per_page',
    required: false,
    type: Number,
    description: 'Number of results to return per page. (default: 10)'
  })
  @ApiImplicitQuery({
    name: 'cur_page',
    required: false,
    type: Number,
    description: 'A page number within the paginated result set. (default: 1)'
  })
  @ApiImplicitQuery({
    name: 'template_id',
    required: false,
    type: Number,
    description: 'Filter by template'
  })
  @Get()
  async findAll(
    @Query('cur_page', new ParseIntWithDefaultPipe(1)) curPage,
    @Query('per_page', new ParseIntWithDefaultPipe(10)) perPage,
    @Query('q') q,
    @Query('sort') sort,
    @Query('template_id') template_id
  ) {
    try {
      return plainToClass(
        OutTasksDto,
        await this.service.findAll({
          curPage,
          perPage,
          q,
          sort,
          template_id
        })
      );
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'upload data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post('importData')
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({
    name: 'file',
    required: true,
    description: 'Import Data'
  })
  async importData(@Req() req, @UploadedFile() file) {
    try {
      const wb = XLSX.read(file.buffer, { type: 'buffer' });
      const first_sheet_name = wb.SheetNames[0];
      const worksheet = wb.Sheets[first_sheet_name];

      for (let i = 1; i < 1000; i++) {
        const cellC = worksheet['C' + (i + 1)];
        const name = cellC ? cellC.w : undefined;
        if (name === undefined) break;
        // check template
        const ntpl = worksheet['A' + (i + 1)]
          ? await this.serviceTpl.createIfNoExist({
              cat: this.getCategoryId(worksheet['B' + (i + 1)].w),
              name: worksheet['A' + (i + 1)].w
            })
          : null;
        const drow = {
          template: ntpl,
          name,
          required: true,
          type: worksheet['D' + (i + 1)] ? worksheet['D' + (i + 1)].w : '',
          options: worksheet['E' + (i + 1)] ? worksheet['E' + (i + 1)].w : '',
          logic: worksheet['F' + (i + 1)] ? worksheet['F' + (i + 1)].w : ''
        };

        const isE = await this.service.isNameExist({ name });
        if (!isE) {
          // create new
          await this.service.create({
            item: await plainToClass(Task, drow)
          });
        } else {
          // update
          await this.service.update({
            id: isE.id,
            item: await plainToClass(Task, { ...isE, ...drow })
          });
        }
      }
      return { status: 'OK' };
    } catch (error) {
      throw error;
    }
  }

  getCategory = inp => {
    const cat = [
      { id: 1, name: 'survey' },
      { id: 2, name: 'instalation' },
      { id: 3, name: 'not installed' },
      { id: 4, name: 'removed' }
    ];
    return cat.find(x => x.id === inp).name;
  };

  getCategoryId = inp => {
    const cat = [
      { id: 1, name: 'survey' },
      { id: 2, name: 'instalation' },
      { id: 3, name: 'not installed' },
      { id: 4, name: 'removed' }
    ];
    return cat.find(x => x.name === inp).id;
  };

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'download data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('exportData/:id')
  async exportData(@Param('id', new ParseIntPipe()) id, @Res() res) {
    try {
      const items = await this.service.findAll({ curPage: 1, perPage: 0 });
      const rs = [];
      items.list.forEach(val => {
        rs.push([
          val.template.name,
          this.getCategory(val.template.category),
          val.name,
          val.type,
          val.options,
          val.logic
        ]);
      });
      const wb = XLSX.readFile('public/templates/task.xlsx', {
        cellStyles: true
      });
      const ws = wb.Sheets[wb.SheetNames[0]];
      XLSX.utils.sheet_add_aoa(ws, rs, { origin: 'A2' });
      const wr = XLSX.write(wb, {
        bookType: 'xlsx',
        bookSST: false,
        type: 'buffer'
      });
      res.setHeader(
        'Content-Type',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      );
      res.setHeader('Content-Disposition', 'attachment; filename="tasks.xlsx"');
      return res.status(200).send(wr);
    } catch (error) {
      throw error;
    }
  }
}
