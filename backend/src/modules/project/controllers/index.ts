import { ProjectsController } from './projects.controller';
import { ProjectAssignsController } from './project-assignment.controller';
import { TasksController } from './tasks.controller';
import { TaskAssignsController } from './task-assignment.controller';
import { NapUploadsController } from './nap-upload.controller';
import { ProgramsController } from './programs.controller';
import { TaskTemplatesController } from './task-templates.controller';

export const controllers = [
  ProjectsController,
  ProjectAssignsController,
  TasksController,
  TaskAssignsController,
  NapUploadsController,
  ProgramsController,
  TaskTemplatesController
];
