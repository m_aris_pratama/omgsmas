import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UseGuards,
  Req
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiImplicitParam,
  ApiImplicitQuery,
  ApiResponse,
  ApiUseTags
} from '@nestjs/swagger';
import { plainToClass } from 'class-transformer';
import { Permissions } from '@omgstore/core-nestjs';
import { Roles } from '@omgstore/core-nestjs';
import { InProjectAssignDto } from '../dto/in-project-assignment.dto';
import { OutProjectAssignDto } from '../dto/out-project-assignment.dto';
import { OutProjectAssignsDto } from '../dto/out-project-assignments.dto';

import { ProjectAssign } from '../entities/project-assignment.entity';
import { AccessGuard } from '@omgstore/core-nestjs';
import { ParseIntWithDefaultPipe } from '@omgstore/core-nestjs';
import { ProjectAssignsService } from '../services/project-assignment.service';

@ApiUseTags('projectAssigns')
@ApiBearerAuth()
@Controller('/api/projectAssigns')
@UseGuards(AccessGuard)
export class ProjectAssignsController {
  constructor(private readonly service: ProjectAssignsService) {}
  @Roles('isSuperuser')
  @Permissions('project')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: OutProjectAssignDto,
    description: 'The record has been successfully created.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post()
  async create(@Body() dto: InProjectAssignDto) {
    try {
      return plainToClass(
        OutProjectAssignDto,
        await this.service.create({
          item: await plainToClass(ProjectAssign, dto)
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('project')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutProjectAssignDto,
    description: 'The record has been successfully updated.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Put(':id')
  async update(
    @Param('id', new ParseIntPipe()) id,
    @Body() dto: InProjectAssignDto
  ) {
    try {
      return plainToClass(
        OutProjectAssignDto,
        await this.service.update({
          id,
          item: await plainToClass(ProjectAssign, dto)
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('project')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The record has been successfully deleted.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Delete(':id')
  async delete(@Param('id', new ParseIntPipe()) id) {
    try {
      return plainToClass(
        OutProjectAssignDto,
        await this.service.delete({
          id
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('project')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutProjectAssignDto,
    description: ''
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Get(':id')
  async findById(@Param('id', new ParseIntPipe()) id) {
    try {
      return plainToClass(
        OutProjectAssignDto,
        await this.service.findById({
          id
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('project')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutProjectAssignsDto,
    description: ''
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitQuery({
    name: 'q',
    required: false,
    type: String,
    description: 'Text for search (default: empty)'
  })
  @ApiImplicitQuery({
    name: 'sort',
    required: false,
    type: String,
    description: 'Column name for sort (default: -id)'
  })
  @ApiImplicitQuery({
    name: 'per_page',
    required: false,
    type: Number,
    description: 'Number of results to return per page. (default: 10)'
  })
  @ApiImplicitQuery({
    name: 'cur_page',
    required: false,
    type: Number,
    description: 'A page number within the paginated result set. (default: 1)'
  })
  @ApiImplicitQuery({
    name: 'project_id',
    required: false,
    type: Number,
    description: 'filter by project id'
  })
  @ApiImplicitQuery({
    name: 'retail_id',
    required: false,
    type: Number,
    description: 'filter by retail id'
  })
  @Get()
  async findAll(
    @Query('cur_page', new ParseIntWithDefaultPipe(1)) curPage,
    @Query('per_page', new ParseIntWithDefaultPipe(10)) perPage,
    @Query('q') q,
    @Query('sort') sort,
    @Query('project_id') projectId,
    @Query('retail_id') retailId,
    @Query('is_active') isActive,
    @Query('approval') approval,
    @Query('installed') installed
  ) {
    try {
      return plainToClass(
        OutProjectAssignsDto,
        await this.service.findAll({
          curPage,
          perPage,
          q,
          sort,
          projectId,
          retailId,
          isActive,
          approval,
          installed
        })
      );
    } catch (error) {
      throw error;
    }
  }
}
