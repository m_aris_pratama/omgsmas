import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { Exclude, Type } from 'class-transformer';
import { ProjectAssignDto } from './project-assignment.dto';
import { TaskDto } from './task.dto';
import { TeamDto } from 'modules/client/dto/team.dto';

export class TaskAssignDto {
  @ApiModelProperty({ type: Number })
  id: number;

  @Type(() => TeamDto)
  @ApiModelProperty({ type: TeamDto })
  team: TeamDto;

  @Type(() => ProjectAssignDto)
  @ApiModelProperty({ type: ProjectAssignDto })
  projectAssign: ProjectAssignDto;

  @Type(() => TaskDto)
  @ApiModelProperty({ type: TaskDto })
  task: TaskDto;

  @ApiModelProperty()
  value: string;

  @ApiModelProperty()
  note: string;

  @ApiModelProperty()
  longitude: number;

  @ApiModelProperty()
  latitude: number;

  @ApiModelPropertyOptional({ type: String })
  created: Date;

  @ApiModelPropertyOptional({ type: String })
  updated: Date;
}
