import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { Exclude, Type } from 'class-transformer';
import { MaxLength } from 'class-validator';
import { UserDto } from '@omgstore/core-nestjs/dto/user.dto';
import { ClientDto } from '../../client/dto/client.dto';
import { BrandDto } from '../../client/dto/brand.dto';
import { ProjectAssignDto } from './project-assignment.dto';
import { BusinessUnitDto } from 'modules/client/dto/business-unit.dto';

export class ProjectDto {
  @ApiModelProperty({ type: Number })
  id: number;

  @Type(() => BrandDto)
  @ApiModelProperty({ type: BrandDto })
  brand: BrandDto;

  @ApiModelProperty()
  name: string;

  @ApiModelProperty()
  information: string;

  @ApiModelPropertyOptional()
  photo: string;

  @Type(() => BusinessUnitDto)
  @ApiModelProperty({ type: BusinessUnitDto })
  businessUnit: BusinessUnitDto;

  @Type(() => ProjectAssignDto)
  @ApiModelProperty({ type: ProjectAssignDto, isArray: true })
  projectAssign: ProjectAssignDto[];

  @ApiModelPropertyOptional({ type: String })
  created: Date;

  @ApiModelPropertyOptional({ type: String })
  updated: Date;
}
