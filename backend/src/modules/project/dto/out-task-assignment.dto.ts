import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { TaskAssignDto } from '../dto/task-assignment.dto';

export class OutTaskAssignDto {
  @Type(() => TaskAssignDto)
  @ApiModelProperty({ type: TaskAssignDto })
  taskAssign: TaskAssignDto;
}
