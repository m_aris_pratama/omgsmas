import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { MetaDto } from '@omgstore/core-nestjs';
import { ProgramDto } from '../dto/program.dto';

export class OutProgramsDto {
  @Type(() => ProgramDto)
  @ApiModelProperty({ type: ProgramDto, isArray: true })
  list: ProgramDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
