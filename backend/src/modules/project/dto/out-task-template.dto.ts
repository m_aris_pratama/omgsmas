import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { TaskTemplateDto } from '../dto/task-template.dto';

export class OutTaskTemplateDto {
  @Type(() => TaskTemplateDto)
  @ApiModelProperty({ type: TaskTemplateDto })
  task: TaskTemplateDto;
}
