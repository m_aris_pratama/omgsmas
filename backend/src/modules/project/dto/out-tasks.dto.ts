import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { MetaDto } from '@omgstore/core-nestjs';
import { TaskDto } from '../dto/task.dto';

export class OutTasksDto {
  @Type(() => TaskDto)
  @ApiModelProperty({ type: TaskDto, isArray: true })
  list: TaskDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
