import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { MetaDto } from '@omgstore/core-nestjs';
import { ProjectAssignDto } from '../dto/project-assignment.dto';

export class OutProjectAssignsDto {
  @Type(() => ProjectAssignDto)
  @ApiModelProperty({ type: ProjectAssignDto, isArray: true })
  list: ProjectAssignDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
