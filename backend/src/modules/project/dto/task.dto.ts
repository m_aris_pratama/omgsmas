import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { Exclude, Type } from 'class-transformer';
import { MaxLength } from 'class-validator';
import { UserDto } from '@omgstore/core-nestjs/dto/user.dto';
import { ClientDto } from '../../client/dto/client.dto';
import { BrandDto } from '../../client/dto/brand.dto';
import { ProjectAssignDto } from './project-assignment.dto';
import { TaskTemplateDto } from './task-template.dto';

export class TaskDto {
  @ApiModelProperty({ type: Number })
  id: number;

  @ApiModelProperty()
  name: string;

  @Type(() => TaskTemplateDto)
  @ApiModelProperty({ type: TaskTemplateDto })
  template: TaskTemplateDto;

  @ApiModelPropertyOptional()
  required: boolean;

  @ApiModelProperty()
  type: string;

  @ApiModelProperty()
  options: string;

  @ApiModelPropertyOptional()
  logic: string;

  @ApiModelPropertyOptional()
  is_active: boolean;

  @ApiModelPropertyOptional({ type: String })
  created: Date;

  @ApiModelPropertyOptional({ type: String })
  updated: Date;
}
