import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { ProgramDto } from '../dto/program.dto';

export class OutProgramDto {
  @Type(() => ProgramDto)
  @ApiModelProperty({ type: ProgramDto })
  program: ProgramDto;
}
