import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { MetaDto } from '@omgstore/core-nestjs';
import { NapUploadDto } from './nap-upload.dto';

export class OutNapUploadsDto {
  @Type(() => NapUploadDto)
  @ApiModelProperty({ type: NapUploadDto, isArray: true })
  list: NapUploadDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
