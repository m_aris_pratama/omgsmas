import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { Exclude, Type } from 'class-transformer';
import { ProjectAssignDto } from './project-assignment.dto';
import { TaskDto } from './task.dto';
import { TeamDto } from 'modules/client/dto/team.dto';
import { StoreDto } from '../../client/dto/store.dto';

export class NapUploadDto {
  @ApiModelProperty({ type: Number })
  id: number;

  @Type(() => TeamDto)
  @ApiModelProperty({ type: TeamDto })
  team: TeamDto;

  @Type(() => StoreDto)
  @ApiModelProperty({ type: StoreDto })
  store: StoreDto;

  @ApiModelPropertyOptional()
  brand: string;

  @ApiModelPropertyOptional()
  program: string;

  @ApiModelPropertyOptional()
  promo: string;

  @ApiModelProperty()
  file: string;

  @ApiModelProperty()
  desc: string;

  @ApiModelProperty()
  longitude: number;

  @ApiModelProperty()
  latitude: number;

  @ApiModelPropertyOptional({ type: String })
  created: Date;

  @ApiModelPropertyOptional({ type: String })
  updated: Date;
}
