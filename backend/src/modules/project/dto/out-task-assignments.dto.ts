import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { MetaDto } from '@omgstore/core-nestjs';
import { TaskAssignDto } from '../dto/task-assignment.dto';

export class OutTaskAssignsDto {
  @Type(() => TaskAssignDto)
  @ApiModelProperty({ type: TaskAssignDto, isArray: true })
  list: TaskAssignDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
