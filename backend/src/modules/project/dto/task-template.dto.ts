import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class TaskTemplateDto {
  @ApiModelProperty({ type: Number })
  id: number;

  @ApiModelProperty()
  name: string;
}
