import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class ProgramDto {
  @ApiModelProperty({ type: Number })
  id: number;

  @ApiModelProperty()
  name: string;

  @ApiModelPropertyOptional()
  desc: string;
}
