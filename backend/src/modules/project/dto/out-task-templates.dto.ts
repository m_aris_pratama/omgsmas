import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { MetaDto } from '@omgstore/core-nestjs';
import { TaskTemplateDto } from '../dto/task-template.dto';

export class OutTaskTemplatesDto {
  @Type(() => TaskTemplateDto)
  @ApiModelProperty({ type: TaskTemplateDto, isArray: true })
  list: TaskTemplateDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
