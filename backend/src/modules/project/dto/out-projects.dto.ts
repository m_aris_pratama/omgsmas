import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { MetaDto } from '@omgstore/core-nestjs';
import { ProjectDto } from '../dto/project.dto';

export class OutProjectsDto {
  @Type(() => ProjectDto)
  @ApiModelProperty({ type: ProjectDto, isArray: true })
  list: ProjectDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
