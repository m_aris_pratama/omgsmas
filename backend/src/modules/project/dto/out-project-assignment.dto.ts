import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { ProjectAssignDto } from './project-assignment.dto';

export class OutProjectAssignDto {
  @Type(() => ProjectAssignDto)
  @ApiModelProperty({ type: ProjectAssignDto })
  projectAssign: ProjectAssignDto;
}
