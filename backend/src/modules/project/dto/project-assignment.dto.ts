import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { Exclude, Type } from 'class-transformer';
import { MaxLength } from 'class-validator';
import { UserDto } from '@omgstore/core-nestjs/dto/user.dto';
import { ClientDto } from '../../client/dto/client.dto';
import { BrandDto } from '../../client/dto/brand.dto';
import { ProjectDto } from './project.dto';
import { StoreDto } from '../../client/dto/store.dto';
import { TaskAssignDto } from './task-assignment.dto';

export class ProjectAssignDto {
  @ApiModelProperty({ type: Number })
  id: number;

  @Type(() => ProjectDto)
  @ApiModelProperty({ type: ProjectDto })
  project: ProjectDto;

  @Type(() => StoreDto)
  @ApiModelProperty({ type: StoreDto })
  store: StoreDto;

  @ApiModelPropertyOptional()
  program: string;

  @ApiModelPropertyOptional({ type: String })
  survey_completed: Date;

  @ApiModelPropertyOptional({ type: String })
  instalation_completed: Date;

  @ApiModelPropertyOptional({ type: String })
  removed_completed: Date;

  @ApiModelProperty({ default: true })
  is_active: boolean;

  @ApiModelPropertyOptional({ type: String })
  created: Date;

  @ApiModelPropertyOptional({ type: String })
  updated: Date;
}
