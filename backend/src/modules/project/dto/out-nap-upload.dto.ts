import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { NapUploadDto } from './nap-upload.dto';

export class OutNapUploadDto {
  @Type(() => NapUploadDto)
  @ApiModelProperty({ type: NapUploadDto })
  nap: NapUploadDto;
}
