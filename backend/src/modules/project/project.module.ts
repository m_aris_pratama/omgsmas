import { DynamicModule, Module, Provider } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configs } from './configs';
import { controllers } from './controllers';
import { entities } from './entities';
import { services } from './services';
import { CoreModule } from '@omgstore/core-nestjs';
import { ClientModule } from '../client/client.module';

@Module({
  imports: [
    TypeOrmModule.forFeature(entities),
    CoreModule.forFeature(),
    ClientModule.forFeature()
  ],
  controllers: [...controllers],
  providers: [...configs, ...services],
  exports: [...configs, ...services]
})
export class ProjectModule {
  static forFeature(): DynamicModule {
    return {
      module: ProjectModule,
      providers: [...services],
      exports: [...configs, ...services]
    };
  }
  static forRoot(options: { providers: Provider[] }): DynamicModule {
    return {
      module: ProjectModule,
      imports: [
        TypeOrmModule.forFeature(entities),
        CoreModule.forFeature(),
        ClientModule.forFeature()
      ],
      controllers: [...controllers],
      providers: [...configs, ...options.providers, ...services],
      exports: [...configs, ...services]
    };
  }
}
