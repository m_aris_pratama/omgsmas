import {
  Inject,
  Injectable,
  MethodNotAllowedException,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CORE_CONFIG_TOKEN, ICoreConfig } from '@omgstore/core-nestjs';
import { TaskTemplate } from '../entities/task-template.entity';

@Injectable()
export class TaskTemplatesService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(TaskTemplate)
    private readonly repository: Repository<TaskTemplate>
  ) {}
  async create(options: { item: TaskTemplate }) {
    try {
      options.item = await this.repository.save(options.item);
      return {};
    } catch (error) {
      throw error;
    }
  }
  async update(options: { id: number; item: TaskTemplate }) {
    options.item.id = options.id;
    try {
      options.item = await this.repository.save(options.item);
      return {};
    } catch (error) {
      throw error;
    }
  }
  async delete(options: { id: number }) {
    try {
      await this.repository.delete(options.id);
      return {};
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      const item = await this.repository.findOneOrFail(options.id);
      return { TaskTemplate: item };
    } catch (error) {
      throw error;
    }
  }
  async isNameExist(options: { name: string }) {
    try {
      const qb = await this.repository
        .createQueryBuilder('TaskTemplate')
        .where('UPPER(TaskTemplate.name) = UPPER(:name)', {
          name: options.name
        })
        .getOne();
      return qb;
    } catch (error) {
      throw error;
    }
  }
  async createIfNoExist(options: { cat: number; name: string }) {
    try {
      const qb = await this.repository
        .createQueryBuilder('TaskTemplate')
        .where('UPPER(TaskTemplate.name) = UPPER(:name)', {
          name: options.name
        })
        .getOne();
      if (!qb) {
        // create
        const item = await this.repository.save({
          name: options.name,
          category: options.cat
        });
        return (await this.findById({ id: item.id })).TaskTemplate;
      } else return qb;
    } catch (error) {
      throw error;
    }
  }
  async findAll(options: {
    curPage: number;
    perPage: number;
    q?: string;
    sort?: string;
    category?: number;
  }) {
    try {
      let objects: [TaskTemplate[], number];
      let qb = this.repository.createQueryBuilder('TaskTemplate');
      if (options.q) {
        qb = qb.where('TaskTemplate.name like :q', {
          q: `%${options.q}%`
        });
      }
      if (options.category) {
        qb = qb.andWhere('TaskTemplate.category = :category', {
          category: options.category
        });
      }
      options.sort =
        options.sort &&
        new TaskTemplate().hasOwnProperty(options.sort.replace('-', ''))
          ? options.sort
          : '-id';
      const field = options.sort.replace('-', '');
      if (options.sort) {
        if (options.sort[0] === '-') {
          qb = qb.orderBy('TaskTemplate.' + field, 'DESC');
        } else {
          qb = qb.orderBy('TaskTemplate.' + field, 'ASC');
        }
      }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        list: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }
}
