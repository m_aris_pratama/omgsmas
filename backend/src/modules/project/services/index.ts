import { ProjectsService } from './projects.service';
import { TasksService } from './tasks.service';
import { TaskAssignsService } from './task-assignments.service';
import { ProjectAssignsService } from './project-assignment.service';
import { NapUploadsService } from './nap-upload.service';
import { ProgramsService } from './program.service';
import { TaskTemplatesService } from './task-template.service';

export const services = [
  ProjectsService,
  TasksService,
  TaskAssignsService,
  ProjectAssignsService,
  NapUploadsService,
  ProgramsService,
  TaskTemplatesService
];
