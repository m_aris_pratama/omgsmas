import {
  Inject,
  Injectable,
  MethodNotAllowedException,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CORE_CONFIG_TOKEN, ICoreConfig } from '@omgstore/core-nestjs';
import { Task } from '../entities/task.entity';

@Injectable()
export class TasksService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(Task) private readonly repository: Repository<Task>
  ) {}
  async create(options: { item: Task }) {
    try {
      options.item = await this.repository.save(options.item);
      const { task } = await this.findById({ id: options.item.id });
      return { task };
    } catch (error) {
      throw error;
    }
  }
  async update(options: { id: number; item: Task }) {
    options.item.id = options.id;
    try {
      options.item = await this.repository.save(options.item);
      const { task } = await this.findById({ id: options.item.id });
      return { task };
    } catch (error) {
      throw error;
    }
  }
  async delete(options: { id: number }) {
    try {
      let object = await this.repository.findOneOrFail(options.id);
      object = await this.repository.save(object);
      await this.repository.delete(options.id);
      return { task: null };
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      const item = await this.repository.findOneOrFail(options.id);
      return { task: item };
    } catch (error) {
      throw error;
    }
  }
  async isNameExist(options: { name: string }) {
    try {
      const qb = await this.repository
        .createQueryBuilder('task')
        .where('UPPER(task.name) = UPPER(:name)', { name: options.name })
        .getOne();
      return qb;
    } catch (error) {
      throw error;
    }
  }
  async findAll(options: {
    curPage: number;
    perPage: number;
    q?: string;
    sort?: string;
    template_id?: string;
  }) {
    try {
      let objects: [Task[], number];
      let qb = this.repository.createQueryBuilder('task');
      qb = qb.leftJoinAndSelect('task.template', 'template');
      if (options.q) {
        qb = qb.where('task.name like :q', {
          q: `%${options.q}%`,
          id: +options.q
        });
      }
      if (options.template_id) {
        qb = qb.where('template.id = :template', {
          template: options.template_id
        });
      }
      options.sort =
        options.sort && new Task().hasOwnProperty(options.sort.replace('-', ''))
          ? options.sort
          : '-id';
      const field = options.sort.replace('-', '');
      if (options.sort) {
        if (options.sort[0] === '-') {
          qb = qb.orderBy('task.' + field, 'DESC');
        } else {
          qb = qb.orderBy('task.' + field, 'ASC');
        }
      }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        list: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }
}
