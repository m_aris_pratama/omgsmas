import {
  Inject,
  Injectable,
  MethodNotAllowedException,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CORE_CONFIG_TOKEN, ICoreConfig } from '@omgstore/core-nestjs';
import { TaskAssign } from '../entities/task-assignment.entity';
import moment = require('moment');

@Injectable()
export class TaskAssignsService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(TaskAssign)
    private readonly repository: Repository<TaskAssign>
  ) {}
  async create(options: { item: TaskAssign }) {
    try {
      options.item = await this.repository.save(options.item);
      const { taskAssign } = await this.findById({ id: options.item.id });
      return { taskAssign };
    } catch (error) {
      throw error;
    }
  }
  async update(options: { id: number; item: TaskAssign }) {
    options.item.id = options.id;
    try {
      options.item = await this.repository.save(options.item);
      const { taskAssign } = await this.findById({ id: options.item.id });
      return { taskAssign };
    } catch (error) {
      throw error;
    }
  }
  async delete(options: { id: number }) {
    try {
      let object = await this.repository.findOneOrFail(options.id);
      object = await this.repository.save(object);
      await this.repository.delete(options.id);
      return { taskAssign: null };
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      const item = await this.repository.findOneOrFail(options.id);
      return { taskAssign: item };
    } catch (error) {
      throw error;
    }
  }
  async findByProjAssign(options: { id: number }) {
    try {
      const item = await this.repository.find({
        where: { projectAssign: { id: options.id } }
      });

      let qb = this.repository.createQueryBuilder('taskAssign');
      qb = qb.leftJoinAndSelect('taskAssign.projectAssign', 'assign');
      qb = qb.leftJoinAndSelect('taskAssign.task', 'task');
      qb = qb.where('assign.id = :id and DATE(taskAssign.created) = :date', {
        id: options.id,
        date: moment().format('YYYY-MM-DD')
      });

      return await qb.getMany();
    } catch (error) {
      throw error;
    }
  }

  async findByProjnStore(options: { projectId: number; storeId: number }) {
    try {
      let qb = this.repository.createQueryBuilder('taskAssign');
      qb = qb.leftJoinAndSelect('taskAssign.projectAssign', 'assign');
      qb = qb.leftJoinAndSelect('assign.project', 'project');
      qb = qb.leftJoinAndSelect('assign.store', 'store');
      qb = qb.leftJoinAndSelect('taskAssign.task', 'task');
      qb = qb.where('project.id = :projectId and store.id = :storeId', {
        projectId: options.projectId,
        storeId: options.storeId
      });
      return await qb.getMany();
    } catch (error) {
      throw error;
    }
  }

  async findAll(options: {
    curPage: number;
    perPage: number;
    q?: string;
    sort?: string;
  }) {
    try {
      let objects: [TaskAssign[], number];
      let qb = this.repository.createQueryBuilder('taskAssign');
      qb = qb.leftJoinAndSelect('taskAssign.task', 'task');
      if (options.q) {
        qb = qb.where('taskAssign.value like :q', {
          q: `%${options.q}%`
        });
      }
      options.sort =
        options.sort &&
        new TaskAssign().hasOwnProperty(options.sort.replace('-', ''))
          ? options.sort
          : '-id';
      const field = options.sort.replace('-', '');
      if (options.sort) {
        if (options.sort[0] === '-') {
          qb = qb.orderBy('taskAssign.' + field, 'DESC');
        } else {
          qb = qb.orderBy('taskAssign.' + field, 'ASC');
        }
      }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        list: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }
}
