import {
  Inject,
  Injectable,
  MethodNotAllowedException,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CORE_CONFIG_TOKEN, ICoreConfig } from '@omgstore/core-nestjs';
import { NapUpload } from '../entities/nap-upload.entity';
import { Store } from '../../client/entities/store.entity';
import moment = require('moment');

@Injectable()
export class NapUploadsService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(NapUpload)
    private readonly repository: Repository<NapUpload>,
    @InjectRepository(Store) private readonly repoStore: Repository<Store>
  ) {}
  async create(options: { item: NapUpload }) {
    try {
      options.item = await this.repository.save(options.item);
      return {};
    } catch (error) {
      throw error;
    }
  }
  async update(options: { id: number; item: NapUpload }) {
    options.item.id = options.id;
    try {
      options.item = await this.repository.save(options.item);
      return {};
    } catch (error) {
      throw error;
    }
  }
  async delete(options: { id: number }) {
    try {
      await this.repository.delete(options.id);
      return {};
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      const item = await this.repository.findOneOrFail(options.id);
      return { napUpload: item };
    } catch (error) {
      throw error;
    }
  }
  async findAll(options: {
    curPage: number;
    perPage: number;
    q?: string;
    sort?: string;
    dateFrom?: string;
    dateTo?: string;
  }) {
    try {
      let objects: [Store[], number];
      let qb = this.repoStore.createQueryBuilder('store');
      qb = qb.leftJoinAndSelect('store.retail', 'retail');
      qb = qb.leftJoinAndSelect('store.city', 'city');
      qb = qb.leftJoinAndSelect('city.area', 'area');
      qb = qb.innerJoinAndSelect('store.naps', 'napUpload');
      if (options.q) {
        qb = qb.where('napUpload.program like :q', {
          q: `%${options.q}%`
        });
      }
      const dateFrom = options.dateFrom
        ? options.dateFrom
        : moment()
            .subtract(7, 'days')
            .format('YYYY-MM-DD');
      const dateTo = options.dateTo
        ? options.dateTo
        : moment().format('YYYY-MM-DD');
      qb = qb.where(
        'napUpload.created_at >= :dateFrom AND napUpload.created_at < :dateTo',
        {
          dateFrom: dateFrom + ' 00:00:00',
          dateTo: dateTo + ' 23:59:59'
        }
      );

      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        list: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }
}
