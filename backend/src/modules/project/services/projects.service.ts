import {
  Inject,
  Injectable,
  MethodNotAllowedException,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CORE_CONFIG_TOKEN, ICoreConfig } from '@omgstore/core-nestjs';
import { Project } from '../entities/project.entity';
import { ProgramsService } from './program.service';
import { ProjectAssign } from '../entities/project-assignment.entity';
import moment = require('moment');

@Injectable()
export class ProjectsService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(Project) private readonly repository: Repository<Project>,
    @InjectRepository(ProjectAssign)
    private readonly repositoryAssign: Repository<ProjectAssign>,
    private readonly programService: ProgramsService
  ) { }
  async create(options: { item: Project }) {
    try {
      options.item = await this.repository.save(options.item);
      const { project } = await this.findById({ id: options.item.id });
      return { project };
    } catch (error) {
      throw error;
    }
  }
  async update(options: { id: number; item: Project }) {
    options.item.id = options.id;
    try {
      options.item = await this.repository.save(options.item);
      const { project } = await this.findById({ id: options.item.id });
      return { project };
    } catch (error) {
      throw error;
    }
  }
  async delete(options: { id: number }) {
    try {
      let object = await this.repository.findOneOrFail(options.id);
      object = await this.repository.save(object);
      await this.repository.delete(options.id);
      return { project: null };
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      let qb = this.repository.createQueryBuilder('project');
      qb = qb.leftJoinAndSelect('project.businessUnit', 'businessUnit');
      qb = qb.where('project.id = :id', {
        id: options.id
      });
      return { project: await qb.getOne() };
    } catch (error) {
      throw error;
    }
  }
  async findNotCompleted() {
    try {
      let qb = this.repository.createQueryBuilder('project');
      qb = qb.where('is_active = true');
      return await qb.getMany();
    } catch (error) {
      throw error;
    }
  }
  async findByIdTasks(options: { id: number }) {
    try {
      let qb = this.repository.createQueryBuilder('project');
      qb = qb.where('project.id = :id', {
        id: options.id
      });
      qb = qb.leftJoinAndSelect('project.taskSurvey', 'taskSurvey');
      qb = qb.leftJoinAndSelect('project.taskInstalation', 'taskInstalation');
      qb = qb.leftJoinAndSelect('project.taskNotInstalled', 'taskNotInstalled');
      qb = qb.leftJoinAndSelect('project.taskRemoved', 'taskRemoved');
      qb = qb.leftJoinAndSelect('taskSurvey.tasks', 'surveys');
      qb = qb.leftJoinAndSelect('taskInstalation.tasks', 'instalations');
      qb = qb.leftJoinAndSelect('taskNotInstalled.tasks', 'notInstalleds');
      qb = qb.leftJoinAndSelect('taskRemoved.tasks', 'removeds');
      return { item: await qb.getOne() };
    } catch (error) {
      throw error;
    }
  }
  async findProjectReport(options: { id: number }) {
    try {
      let qb = this.repository.createQueryBuilder('project');
      qb = qb.leftJoinAndSelect('project.brand', 'brand');

      qb = qb.leftJoinAndSelect('project.taskSurvey', 'taskSurvey');
      qb = qb.leftJoinAndSelect('project.taskInstalation', 'taskInstalation');
      qb = qb.leftJoinAndSelect('project.taskNotInstalled', 'taskNotInstalled');
      qb = qb.leftJoinAndSelect('project.taskRemoved', 'taskRemoved');
      qb = qb.leftJoinAndSelect('taskSurvey.tasks', 'surveys');
      qb = qb.leftJoinAndSelect('taskInstalation.tasks', 'instalations');
      qb = qb.leftJoinAndSelect('taskNotInstalled.tasks', 'notInstalleds');
      qb = qb.leftJoinAndSelect('taskRemoved.tasks', 'removeds');

      qb = qb.leftJoinAndSelect('project.businessUnit', 'units');
      qb = qb.leftJoinAndSelect('project.projectAssign', 'pAssign');
      qb = qb.leftJoinAndSelect('pAssign.store', 'store');
      qb = qb.leftJoinAndSelect('store.retail', 'retail');
      qb = qb.leftJoinAndSelect('pAssign.taskAssign', 'tassign');

      qb = qb.where('pAssign.is_active = true AND project.id = :id', {
        id: options.id
      });

      return qb.getMany();
    } catch (error) {
      throw error;
    }
  }
  async projectInstalationReportByRetail(options: { id: number }) {
    try {
      let qb = this.repository.createQueryBuilder('project');
      qb = qb.leftJoinAndSelect('project.taskInstalation', 'taskInstalation');
      qb = qb.leftJoinAndSelect('taskInstalation.tasks', 'instalations');

      qb = qb.leftJoinAndSelect('project.projectAssign', 'pAssign');
      qb = qb.leftJoinAndSelect('pAssign.store', 'store');
      qb = qb.leftJoinAndSelect('store.retail', 'retail');

      qb = qb.select('retail.code');
      qb = qb.addSelect('count(store.id)', 'store');
      qb = qb.addSelect('count(program="ML")', 'ML');
      qb = qb.addSelect('count(instalation_completed)', 'installed');
      qb = qb.addSelect('count(survey_completed)', 'surveyed');
      qb.groupBy('retail.id');

      qb = qb.where('pAssign.is_active = true AND project.id = :id', {
        id: options.id
      });

      return qb.getRawMany();
    } catch (error) {
      throw error;
    }
  }

  async projectInstalationReport(options: { id: number }) {
    try {
      let qb = this.repository.createQueryBuilder('project');
      qb = qb.leftJoinAndSelect('project.brand', 'brand');
      qb = qb.leftJoinAndSelect('brand.client', 'client');
      qb = qb.leftJoinAndSelect('brand.brandSubcategory', 'brandSubcategory');

      qb = qb.leftJoinAndSelect('project.taskInstalation', 'taskInstalation');
      qb = qb.leftJoinAndSelect('taskInstalation.tasks', 'tasks');
      qb = qb.leftJoinAndSelect('project.taskNotInstalled', 'taskNotInstalled');
      qb = qb.leftJoinAndSelect('taskNotInstalled.tasks', 'tasks2');

      qb = qb.leftJoinAndSelect('project.projectAssign', 'projectAssign');
      qb = qb.leftJoinAndSelect('projectAssign.store', 'store');
      qb = qb.leftJoinAndSelect('store.city', 'city');
      qb = qb.leftJoinAndSelect('city.area', 'area');
      qb = qb.leftJoinAndSelect('store.retail', 'retail');

      qb = qb.leftJoinAndSelect(
        'projectAssign.taskAssign',
        'taskAssign',
        'taskAssign.task_id = tasks.id OR taskAssign.task_id = tasks2.id'
      );
      qb = qb.leftJoinAndSelect('taskAssign.task', 'task');

      qb = qb.where('projectAssign.is_active = true AND project.id = :id', {
        id: options.id
      });

      const item = await qb.getOne();
      const res = {
        client: item.brand.client.name,
        brand: item.brand.name,
        category: item.brand.brandSubcategory.name,
        program: item.name,
        programs: item.programs,
        cycle: item.cycle_start,
        start_jabo: item.instalation_date_jabo,
        start_nonjabo: item.instalation_date,
        target: item.instalation_target,
        tasks: item.taskInstalation
          ? item.taskInstalation.tasks.map(x => ({ id: x.id, name: x.name }))
          : [],
        assign: []
      };
      item.projectAssign.forEach(val => {
        const tasks = [];
        let notInstall = '';
        val.taskAssign.forEach(t => {
          const d = tasks.find(
            x => x.date === moment(t.created).format('DD/MM/YYYY')
          );
          if (!d)
            tasks.push({
              date: moment(t.created).format('DD/MM/YYYY'),
              items: [t]
            });
          else d.items.push(t);
          if (val.program === '' && t.value === 'PBT') notInstall = 'PBT';
        });
        if (val.program === '' && notInstall === '') {
          // check store status
          if (val.store.is_open === false) notInstall = 'RNV';
        }
        const rw = {
          store: val.store.name,
          retailCode: val.store.retail.code,
          retailName: val.store.retail.name,
          dateInstalled: val.instalation_completed,
          area: val.store.city.area.name,
          program: val.program,
          notInstall,
          tasks
        };
        res.assign.push(rw);
      });

      return res;
    } catch (error) {
      throw error;
    }
  }

  async projectSurveyReport(options: { id: number }) {
    try {
      let qb = this.repository.createQueryBuilder('project');
      qb = qb.leftJoinAndSelect('project.brand', 'brand');
      qb = qb.leftJoinAndSelect('brand.client', 'client');
      qb = qb.leftJoinAndSelect('brand.brandSubcategory', 'brandSubcategory');

      qb = qb.leftJoinAndSelect('project.taskSurvey', 'taskSurvey').orderBy({ 'survey_completed': 'DESC' });
      qb = qb.leftJoinAndSelect('taskSurvey.tasks', 'tasks');

      qb = qb.leftJoinAndSelect('project.projectAssign', 'projectAssign');
      qb = qb.leftJoinAndSelect('projectAssign.store', 'store');
      qb = qb.leftJoinAndSelect('store.city', 'city');
      qb = qb.leftJoinAndSelect('city.area', 'area');
      qb = qb.leftJoinAndSelect('store.retail', 'retail');

      qb = qb.leftJoinAndSelect(
        'projectAssign.taskAssign',
        'taskAssign',
        'taskAssign.task_id = tasks.id'
      );
      qb = qb.leftJoinAndSelect('taskAssign.task', 'task');

      qb = qb.where('projectAssign.is_active = true AND project.id = :id', {
        id: options.id
      });

      const item = await qb.getOne();
      const res = {
        client: item.brand.client.name,
        brand: item.brand.name,
        category: item.brand.brandSubcategory.name,
        program: item.name,
        programs: item.programs,
        cycle: item.cycle_start,
        start: item.survey_date,
        target: item.survey_target,
        tasks: item.taskSurvey
          ? item.taskSurvey.tasks.map(x => ({
            id: x.id,
            name: x.name,
            type: x.type,
            options: x.options
          }))
          : [],
        assign: []
      };
      item.projectAssign.forEach(val => {
        const tasks = [];
        val.taskAssign.forEach(t => {
          const d = tasks.find(
            x => x.date === moment(t.created).format('DD/MM/YYYY')
          );
          if (!d)
            tasks.push({
              date: moment(t.created).format('DD/MM/YYYY'),
              items: [t]
            });
          else d.items.push(t);
        });
        const rw = {
          store: val.store.name,
          retailCode: val.store.retail.code,
          retailName: val.store.retail.name,
          dateSurveyed: val.survey_completed,
          approved: val.survey_approved,
          tasks
        };
        res.assign.push(rw);
      });

      return res;
    } catch (error) {
      throw error;
    }
  }

  async projectRemovedReport(options: { id: number }) {
    try {
      let qb = this.repository.createQueryBuilder('project');
      qb = qb.leftJoinAndSelect('project.brand', 'brand');
      qb = qb.leftJoinAndSelect('brand.client', 'client');
      qb = qb.leftJoinAndSelect('brand.brandSubcategory', 'brandSubcategory');

      qb = qb.leftJoinAndSelect('project.taskRemoved', 'taskRemoved');
      qb = qb.leftJoinAndSelect('taskRemoved.tasks', 'tasks');

      qb = qb.leftJoinAndSelect('project.projectAssign', 'projectAssign');
      qb = qb.leftJoinAndSelect('projectAssign.store', 'store');
      qb = qb.leftJoinAndSelect('store.retail', 'retail');

      qb = qb.leftJoinAndSelect(
        'projectAssign.taskAssign',
        'taskAssign',
        'taskAssign.task_id = tasks.id'
      );
      qb = qb.leftJoinAndSelect('taskAssign.task', 'task');

      qb = qb.where('projectAssign.is_active = true AND project.id = :id', {
        id: options.id
      });

      const item = await qb.getOne();
      const res = {
        client: item.brand.client.name,
        brand: item.brand.name,
        category: item.brand.brandSubcategory.name,
        program: item.name,
        programs: item.programs,
        cycle: item.cycle_start,
        start: item.removed_date,
        target: item.removed_target,
        tasks: item.taskRemoved
          ? item.taskRemoved.tasks.map(x => ({
            id: x.id,
            name: x.name,
            type: x.type,
            options: x.options
          }))
          : [],
        assign: []
      };
      item.projectAssign.forEach(val => {
        const tasks = [];
        val.taskAssign.forEach(t => {
          const d = tasks.find(
            x => x.date === moment(t.created).format('DD/MM/YYYY')
          );
          if (!d)
            tasks.push({
              date: moment(val.created).format('DD/MM/YYYY'),
              items: [t]
            });
          else d.items.push(t);
        });
        const rw = {
          store: val.store.name,
          retailCode: val.store.retail.code,
          retailName: val.store.retail.name,
          dateRemoved: val.removed_completed,
          tasks
        };
        res.assign.push(rw);
      });

      return res;
    } catch (error) {
      throw error;
    }
  }

  async projectDashboard() {
    try {
      let qb = this.repositoryAssign.createQueryBuilder('assign');
      qb = qb.select('project_id,program,count(program)', 'total');
      qb = qb.innerJoinAndSelect('assign.project', 'proj');
      qb = qb.leftJoinAndSelect('proj.brand', 'brand');
      qb = qb.leftJoinAndSelect('brand.client', 'client');
      qb = qb.where('assign.is_active = true');
      qb.groupBy('project_id,program');
      return await qb.getRawMany();
    } catch (error) {
      throw error;
    }
  }

  async projectDashboardByClient(id) {
    try {
        let qb = this.repository.createQueryBuilder('project');
        qb = qb.select('client.id, client.name as clientName, count(project.id)', 'jumlah');
        qb = qb.leftJoin('project.brand', 'brand');
        qb = qb.leftJoin('brand.client', 'client');
        qb = qb.where(`project.business_unit_id = ${id}`);
        qb.groupBy('client.id');
        return await qb.getRawMany();
    } catch (error) {
      throw error;
    }
  }
  
  async projectDashboardByClientDetail(bid,cid) {
    try {
        let qb = this.repository.createQueryBuilder('project');
        qb = qb.select('brand.name as brandName, programs, instalation_target, survey_target, removed_target, count(pAssign.id) as storeNum, cycle_year, cycle_start, cycle_end');
        qb = qb.leftJoin('project.brand', 'brand');
        qb = qb.leftJoin('project.projectAssign', 'pAssign', 'pAssign.is_active = true');
        qb = qb.where(`project.business_unit_id = ${bid} AND brand.client_id = ${cid}`);
        qb.groupBy('project.id');
        return await qb.getRawMany();
    } catch (error) {
      throw error;
    }
  }

  async projectSummaryReport(options: { id: number }) {
    try {
      const pj = await this.findById({ id: options.id });
      const pg = pj.project.programs.split(',');

      let qb = this.repository.createQueryBuilder('project');

      qb = qb.leftJoinAndSelect('project.projectAssign', 'pAssign');
      qb = qb.leftJoinAndSelect('pAssign.store', 'store');
      qb = qb.leftJoinAndSelect('store.retail', 'retail');

      qb = qb.select('retail.id');
      qb = qb.addSelect('retail.code');
      qb = qb.addSelect('retail.name');
      qb = qb.addSelect('count(store.id)', 'store');
      qb = qb.addSelect('count(survey_completed)', 'surveyed');
      qb = qb.addSelect('count(instalation_completed)', 'installed');
      qb = qb.addSelect('count(removed_completed)', 'removed');
      pg.forEach(x => {
        qb = qb.addSelect(
          'count(if(program="' + x + '",1,NULL))',
          'program_' + x
        );
      });
      qb.groupBy('retail.id');

      qb = qb.where('project.id = :id and pAssign.is_active = true', {
        id: options.id
      });

      return qb.getRawMany();
    } catch (error) {
      throw error;
    }
  }
  async findAll(options: {
    curPage: number;
    perPage: number;
    q?: string;
    sort?: string;
    unit?: number;
  }) {
    try {
      let objects: [Project[], number];
      let qb = this.repository.createQueryBuilder('project');
      qb = qb.leftJoinAndSelect('project.brand', 'brand');
      qb = qb.leftJoinAndSelect('project.taskSurvey', 'taskSurvey');
      qb = qb.leftJoinAndSelect('project.taskInstalation', 'taskInstalation');
      qb = qb.leftJoinAndSelect('project.taskNotInstalled', 'taskNotInstalled');
      qb = qb.leftJoinAndSelect('project.taskRemoved', 'taskRemoved');
      qb = qb.leftJoinAndSelect('project.businessUnit', 'unit');
      qb = qb.leftJoinAndSelect('brand.client', 'client');
      if (options.q) {
        qb = qb.where('project.name like :q', {
          q: `%${options.q}%`
        });
      }
      if (options.unit) {
        qb = qb.andWhere('unit.id = :unitId', {
          unitId: options.unit
        });
      }
      options.sort =
        options.sort &&
          new Project().hasOwnProperty(options.sort.replace('-', ''))
          ? options.sort
          : '-id';
      const field = options.sort.replace('-', '');
      if (options.sort) {
        if (options.sort[0] === '-') {
          qb = qb.orderBy('project.' + field, 'DESC');
        } else {
          qb = qb.orderBy('project.' + field, 'ASC');
        }
      }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        list: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }
}
