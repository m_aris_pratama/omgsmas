import {
  Inject,
  Injectable,
  MethodNotAllowedException,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, MongoEntityManager } from 'typeorm';
import { CORE_CONFIG_TOKEN, ICoreConfig } from '@omgstore/core-nestjs';
import { ProjectAssign } from '../entities/project-assignment.entity';
import { Project } from '../entities/project.entity';
import { Store } from '../../client/entities/store.entity';
import { Area } from '../../client/entities/area.entity';
import moment = require('moment');
import { TaskAssignsService } from './task-assignments.service';
import { StoresService } from 'modules/client/services/stores.service';
import { ProjectsService } from './projects.service';
import { plainToClass } from 'class-transformer';

@Injectable()
export class ProjectAssignsService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(ProjectAssign)
    private readonly repository: Repository<ProjectAssign>,
    private readonly taskAssignService: TaskAssignsService,
    private readonly storeService: StoresService,
    private readonly projectService: ProjectsService
  ) {}
  async create(options: { item: ProjectAssign }) {
    try {
      options.item = await this.repository.save(options.item);
      return { status: 'OK' };
    } catch (error) {
      throw error;
    }
  }
  async update(options: { id: number; item: ProjectAssign }) {
    options.item.id = options.id;
    try {
      options.item = await this.repository.save(options.item);
      return { status: 'OK' };
    } catch (error) {
      throw error;
    }
  }
  async delete(options: { id: number }) {
    try {
      await this.repository.delete(options.id);
      return { status: 'OK' };
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      const item = await this.repository.findOneOrFail(options.id);
      return { projectAssign: item };
    } catch (error) {
      throw error;
    }
  }

  projectStatusToday(options: {
    area: Area;
    project: Project;
    assign: ProjectAssign;
  }) {
    try {
      // get project phase
      if (options.assign.removed_completed)
        return { step: 'removed', status: true }; // project completed

      if (moment().isBefore(options.project.survey_date))
        return { step: '', status: true }; // belum mulai

      if (moment().isSameOrAfter(options.project.removed_date)) {
        // fase removed
        return { step: 'removed', status: false };
      }

      // area JABO or no
      if (Area.name === 'JABODETABEK') {
        if (moment().isSameOrAfter(options.project.instalation_date_jabo)) {
          // fase instalation
          if (
            options.assign.instalation_completed &&
            moment(options.assign.instalation_completed).format(
              'YYYY-MM-DD'
            ) === moment().format('YYYY-MM-DD')
          )
            return { step: 'instalation', status: true };
          else {
            if (
              options.assign.last_maintenance &&
              moment(options.assign.last_maintenance).format('YYYY-MM-DD') ===
                moment().format('YYYY-MM-DD')
            )
              return { step: 'instalation', status: true };
            else return { step: 'instalation', status: false };
          }
        }
      } else {
        if (moment().isSameOrAfter(options.project.instalation_date)) {
          // fase instalation
          if (
            options.assign.instalation_completed &&
            moment(options.assign.instalation_completed).format(
              'YYYY-MM-DD'
            ) === moment().format('YYYY-MM-DD')
          )
            return { step: 'instalation', status: true };
          else {
            if (
              options.assign.last_maintenance &&
              moment(options.assign.last_maintenance).format('YYYY-MM-DD') ===
                moment().format('YYYY-MM-DD')
            )
              return { step: 'instalation', status: true };
            else return { step: 'instalation', status: false };
          }
        }
      }
      // fase survey
      if (options.assign.survey_completed)
        return { step: 'survey', status: true };
      else return { step: 'survey', status: false };
    } catch (error) {
      throw error;
    }
  }

  async findTodayActiviesByUser(options: { user_id: number }) {
    try {
      const qb = this.repository
        .createQueryBuilder('projectAssign')
        .innerJoinAndSelect('projectAssign.project', 'project')
        .leftJoinAndSelect('projectAssign.store', 'store')
        .leftJoinAndSelect('store.city', 'city')
        .leftJoinAndSelect('city.area', 'area')
        .leftJoinAndSelect('store.member', 'team')
        .where('projectAssign.is_active = true and team.user_id = :id', {
          id: options.user_id
        });
      const items = await qb.getMany();
      const st = [];
      let pj = 0;
      items.forEach(val => {
        const h = this.projectStatusToday({
          area: val.store.city.area,
          project: val.project,
          assign: val
        });
        console.log(h);
        if (!h.status) {
          if (!st.find(x => x.id === val.store.id))
            st.push({ id: val.store.id });
          pj++;
        }
      });
      const res = {
        cycle: Math.ceil(moment().week() / 4),
        week: Math.ceil(moment().week() % 4),
        totalStore: st.length,
        totalProject: pj
      };

      return res;
    } catch (error) {
      throw error;
    }
  }

  async findTodayActiviesByLead(options: { user_id: number }) {
    try {
      const qb = this.repository
        .createQueryBuilder('projectAssign')
        .innerJoinAndSelect('projectAssign.project', 'project')
        .leftJoinAndSelect('projectAssign.store', 'store')
        .leftJoinAndSelect('store.city', 'city')
        .leftJoinAndSelect('city.area', 'area')
        .leftJoinAndSelect('store.lead', 'team')
        .leftJoinAndSelect('store.member', 'member')
        .where('projectAssign.is_active = true and team.user_id = :id', {
          id: options.user_id
        });
      const items = await qb.getMany();
      const st = [];
      const mb = [];
      let pj = 0;
      items.forEach(val => {
        const h = this.projectStatusToday({
          area: val.store.city.area,
          project: val.project,
          assign: val
        });
        console.log(h);
        if (!h.status) {
          if (!st.find(x => x.id === val.store.id))
            st.push({ id: val.store.id });
          if (!mb.find(x => x.id === val.store.member.id))
            mb.push({ id: val.store.member.id });
          pj++;
        }
      });
      const res = {
        cycle: Math.ceil(moment().week() / 4),
        week: Math.ceil(moment().week() % 4),
        totalStore: st.length,
        totalProject: pj,
        totalOfficer: mb.length
      };

      return res;
    } catch (error) {
      throw error;
    }
  }

  async findTodayMembersActivies(options: { user_id: number }) {
    try {
      const qb = this.repository
        .createQueryBuilder('projectAssign')
        .innerJoinAndSelect('projectAssign.project', 'project')
        .leftJoinAndSelect('projectAssign.store', 'store')
        .leftJoinAndSelect('store.city', 'city')
        .leftJoinAndSelect('city.area', 'area')
        .leftJoinAndSelect('store.lead', 'team')
        .leftJoinAndSelect('store.member', 'member')
        .where('projectAssign.is_active = true and team.user_id = :id', {
          id: options.user_id
        });
      const items = await qb.getMany();
      const mb = [];

      items.forEach(val => {
        if (!mb.find(x => x.id === val.store.member.id)) {
          // await this.absenceService.findTodayLast({id: val.store.member.id});
          mb.push({
            id: val.store.member.id,
            name: val.store.member.name,
            status: '',
            stores: [],
            taskTotal: 0,
            taskCompleted: 0
          });
        }
        const m = mb.find(x => x.id === val.store.member.id);
        if (!m.stores.find(x => x.id === val.store.id)) {
          m.stores.push({ id: val.store.id, total: 0, complete: 0 });
        }
        const h = this.projectStatusToday({
          area: val.store.city.area,
          project: val.project,
          assign: val
        });
        const c = m.stores.find(x => x.id === val.store.id);

        if (h.status) {
          m.taskCompleted += 1;
          c.complete += 1;
        }
        c.total += 1;
        m.taskTotal += 1;
      });

      return mb;
    } catch (error) {
      throw error;
    }
  }

  async findStoreAssignByUser(options: { user_id: number; lead_id?: number }) {
    try {
      const qb = this.repository
        .createQueryBuilder('projectAssign')
        .innerJoinAndSelect('projectAssign.project', 'project')
        .leftJoinAndSelect('projectAssign.store', 'store')
        .leftJoinAndSelect('store.city', 'city')
        .leftJoinAndSelect('city.area', 'area')
        .leftJoinAndSelect('store.member', 'member')
        .leftJoinAndSelect('store.lead', 'lead')
        .where(
          'projectAssign.is_active = true and member.user_id = :memberId and store.is_active = true and project.is_active = true',
          {
            memberId: options.user_id
          }
        );
      if (options.lead_id) {
        qb.andWhere('lead.user_id = :leadId', { leadId: options.lead_id });
      }
      const items = await qb.getMany();

      const res = [];

      items.forEach(val => {
        const h = this.projectStatusToday({
          area: val.store.city.area,
          project: val.project,
          assign: val
        });
        const a = res.find(x => x.id === val.store.id);
        if (a) {
          // STATUS PROJECT NYA FALSE
          if (!h.status) a.totalTask += 1;
        } else {
          res.push({
            id: val.store.id,
            name: val.store.name,
            longitude: val.store.longitude,
            latitude: val.store.latitude,
            totalTask: h.status ? 0 : 1
          });
        }
      });

      return res;
    } catch (error) {
      throw error;
    }
  }

  async findStoreProjectAssign(options: {
    project_id: number;
    user_id: number;
  }) {
    try {
      const qb = this.repository
        .createQueryBuilder('projectAssign')
        .innerJoinAndSelect('projectAssign.project', 'project')
        .leftJoinAndSelect('projectAssign.store', 'store')
        .leftJoinAndSelect('store.city', 'city')
        .leftJoinAndSelect('city.area', 'area')
        .leftJoinAndSelect('store.member', 'member')
        .leftJoinAndSelect('store.lead', 'lead')
        .where(
          'projectAssign.is_active = true and project.id = :projectId and lead.user_id = :leadId',
          {
            projectId: options.project_id,
            leadId: options.user_id
          }
        );
      const items = await qb.getMany();
      const res = [];

      items.forEach(val => {
        const h = this.projectStatusToday({
          area: val.store.city.area,
          project: val.project,
          assign: val
        });
        const a = res.find(x => x.id === val.store.id);
        if (a) {
          if (!h.status) a.totalTask += 1;
        } else {
          res.push({
            id: val.store.id,
            name: val.store.name,
            longitude: val.store.longitude,
            latitude: val.store.latitude,
            totalTask: h.status ? 0 : 1
          });
        }
      });

      return res;
    } catch (error) {
      throw error;
    }
  }

  async findByProjectStore(options: { projectId: number; storeId: number }) {
    try {
      let qb = this.repository.createQueryBuilder('projectAssign');
      qb = qb.innerJoinAndSelect('projectAssign.project', 'project');
      qb = qb.leftJoinAndSelect('projectAssign.store', 'store');
      qb = qb.where(
        'projectAssign.is_active = true AND project.id = :projectId AND store.id = :storeId',
        {
          projectId: options.projectId,
          storeId: options.storeId
        }
      );
      return await qb.getOne();
    } catch (error) {
      throw error;
    }
  }

  async findNotCompletedTaskByStoreId(options: { store_id: number }) {
    try {
      const qb = this.repository
        .createQueryBuilder('projectAssign')
        .innerJoinAndSelect('projectAssign.project', 'project')
        .leftJoinAndSelect('project.brand', 'brand')
        .leftJoinAndSelect('project.taskSurvey', 'taskSurvey')
        .leftJoinAndSelect('project.taskInstalation', 'taskInstalation')
        .leftJoinAndSelect('project.taskRemoved', 'taskRemoved')
        .leftJoinAndSelect('project.taskNotInstalled', 'taskNotInstalled')

        .leftJoinAndSelect('taskSurvey.tasks', 'surveys')
        .leftJoinAndSelect('taskInstalation.tasks', 'instalations')
        .leftJoinAndSelect('taskRemoved.tasks', 'removeds')
        .leftJoinAndSelect('taskNotInstalled.tasks', 'notInstalleds')

        .leftJoinAndSelect('projectAssign.store', 'store')
        .leftJoinAndSelect('store.city', 'city')
        .leftJoinAndSelect('city.area', 'area')
        .leftJoinAndSelect('store.member', 'team')
        .where('projectAssign.is_active = true and store.id = :id and project.is_active = true', {
          id: options.store_id
        });
      const items = await qb.getMany();
      const res = [];

      for (const val of items) {
        const tasks = [];
        const notInstalleds = [];

        const status = this.projectStatusToday({
          area: val.store.city.area,
          project: val.project,
          assign: val
        });
        if (status.status) continue;

        const assign = await this.taskAssignService.findByProjAssign({
          id: val.id
        });

        switch (status.step) {
          case 'survey':
            for (const val2 of val.project.taskSurvey.tasks) {
              tasks.push({
                id: val2.id,
                name: val2.name,
                required: val2.required,
                type: val2.type,
                options: val2.options.split(','),
                logic: val2.logic,
                completed:
                  assign.filter(x => x.task.id === val2.id).length > 0
                    ? true
                    : false
              });
            }
            break;
          case 'instalation':
            if (!val.program) {
              tasks.push({
                id: 0,
                name: 'Program',
                required: true,
                type: 'dropdown',
                options: val.project.programs.split(','),
                completed: false
              });
            }
            val.project.taskInstalation
              ? val.project.taskInstalation.tasks.forEach(val2 => {
                  tasks.push({
                    id: val2.id,
                    name: val2.name,
                    required: val2.required,
                    type: val2.type,
                    options: val2.options.split(','),
                    logic: val2.logic,
                    completed:
                      assign.filter(x => x.task.id === val2.id).length > 0
                        ? true
                        : false
                  });
                })
              : '';
            val.project.taskNotInstalled
              ? val.project.taskNotInstalled.tasks.forEach(val2 => {
                  notInstalleds.push({
                    id: val2.id,
                    name: val2.name,
                    required: val2.required,
                    type: val2.type,
                    options: val2.options.split(','),
                    logic: val2.logic,
                    completed:
                      assign.filter(x => x.task.id === val2.id).length > 0
                        ? true
                        : false
                  });
                })
              : '';
            break;
          case 'removed':
            val.project.taskRemoved
              ? val.project.taskRemoved.tasks.forEach(val2 => {
                  tasks.push({
                    id: val2.id,
                    name: val2.name,
                    required: val2.required,
                    type: val2.type,
                    options: val2.options.split(','),
                    logic: val2.logic,
                    completed:
                      assign.filter(x => x.task.id === val2.id).length > 0
                        ? true
                        : false
                  });
                })
              : '';
            break;
        }
        // TODO : STEP = REMOVED EXCLUDE DARI LIST
        res.push({
          project: {
            id: val.project.id,
            name: val.project.name,
            brand: val.project.brand.name,
            step: status.step,
            type: 0,
            information: val.project.information,
            guide: val.project.information,
            photo:
              process.env.DOMAIN +
              '/api/clients/uploadImage/' +
              val.project.photo
          },
          tasks,
          notInstalleds
        });
      }

      return res;
    } catch (error) {
      throw error;
    }
  }

  async findTaskByStoreProject(options: { projId: number; storeId: number }) {
    try {
      let qb = this.repository.createQueryBuilder('projectAssign');
      qb = qb.leftJoinAndSelect('projectAssign.store', 'store');
      qb = qb.leftJoinAndSelect('store.city', 'city');
      qb = qb.leftJoinAndSelect('city.area', 'area');
      qb = qb.leftJoinAndSelect('store.member', 'member');
      qb = qb.innerJoinAndSelect('projectAssign.project', 'project');

      qb = qb.leftJoinAndSelect('project.taskSurvey', 'taskSurvey');
      qb = qb.leftJoinAndSelect('project.taskInstalation', 'taskInstalation');
      qb = qb.leftJoinAndSelect('project.taskNotInstalled', 'taskNotInstalled');
      qb = qb.leftJoinAndSelect('project.taskRemoved', 'taskRemoved');

      qb = qb.leftJoinAndSelect('taskSurvey.tasks', 'surveys');
      qb = qb.leftJoinAndSelect('taskInstalation.tasks', 'instalations');
      qb = qb.leftJoinAndSelect('taskNotInstalled.tasks', 'notInstalleds');
      qb = qb.leftJoinAndSelect('taskRemoved.tasks', 'removeds');
      qb = qb.where(
        'projectAssign.is_active = true and project.id = :projId and store.id = :storeId',
        {
          storeId: options.storeId,
          projId: options.projId
        }
      );

      return await qb.getOne();
    } catch (error) {
      throw error;
    }
  }

  async syncStore(projectId) {
    console.log('sync stores with project');
    // get store assign
    const qb = this.repository
      .createQueryBuilder('projectAssign')
      .leftJoinAndSelect('projectAssign.store', 'store')
      .where('project_id = :id', { id: projectId });
    const assign_store = await qb.getMany();
    // get store by unit
    const prj = await this.projectService.findById({ id: projectId });
    const st = await this.storeService.findByBuUnit({
      id: prj.project.businessUnit.id
    });
    for (const val of st) {
      if (!assign_store.find(x => x.store.id === val.id)) {
        await this.create({
          item: plainToClass(ProjectAssign, {
            store: { id: val.id },
            project: { id: projectId },
            is_active: false
          })
        });
      }
    }
  }

  async findAll(options: {
    curPage: number;
    perPage: number;
    q?: string;
    sort?: string;
    projectId?: number;
    retailId?: number;
    isActive?: boolean;
    approval?: boolean;
    installed?: boolean;
  }) {
    try {
      // sync store assign and new store
      if (options.projectId && !options.retailId)
        await this.syncStore(options.projectId);

      let objects: [ProjectAssign[], number];
      let qb = this.repository.createQueryBuilder('projectAssign');
      qb = qb.innerJoinAndSelect('projectAssign.project', 'project');
      qb = qb.leftJoinAndSelect('projectAssign.store', 'store');
      qb = qb.leftJoinAndSelect('store.retail', 'retail');

      if (options.projectId) {
        qb = qb.andWhere('project.id = :id', { id: options.projectId });
      }
      if (options.retailId) {
        qb = qb.andWhere('retail.id = :rid', { rid: options.retailId });
      }
      if (options.q) {
        qb = qb.andWhere('store.name like :q', {
          q: `%${options.q}%`
        });
      }
      if (options.isActive) {
        qb = qb.andWhere('projectAssign.is_active = true');
      }
      if (options.approval) {
        qb = qb.andWhere(
          'projectAssign.survey_approved = false AND projectAssign.survey_completed is not NULL'
        );
        qb = qb.leftJoinAndSelect('store.member', 'member');
        qb = qb.leftJoinAndSelect('project.taskSurvey', 'taskSurvey');
        qb = qb.leftJoinAndSelect('taskSurvey.tasks', 'tasks');
        qb = qb.leftJoinAndSelect(
          'projectAssign.taskAssign',
          'taskAssign',
          'taskAssign.task_id = tasks.id'
        );
        qb = qb.innerJoin(
          query => {
            return query
              .from('task_assignment', 'a')
              .select('a.task_id, MAX(a.updated_at)', 'date_updated')
              .groupBy('a.task_id');
          },
          'a',
          'taskAssign.updated_at = a.date_updated'
        );
        qb = qb.leftJoinAndSelect('taskAssign.task', 'task');
      }
      if (options.installed) {
        qb = qb.leftJoinAndSelect('project.taskInstalation', 'taskInstalation');
        qb = qb.leftJoinAndSelect('taskInstalation.tasks', 'tasks');
        qb = qb.leftJoinAndSelect(
          'projectAssign.taskAssign',
          'taskAssign',
          'taskAssign.task_id = tasks.id'
        );
        qb = qb.innerJoin(
          query => {
            return query
              .from('task_assignment', 'a')
              .select('a.task_id, MAX(a.updated_at)', 'date_updated')
              .groupBy('a.task_id');
          },
          'a',
          'taskAssign.updated_at = a.date_updated'
        );
        qb = qb.leftJoinAndSelect('taskAssign.task', 'task');
      }
      options.sort =
        options.sort &&
        new ProjectAssign().hasOwnProperty(options.sort.replace('-', ''))
          ? options.sort
          : '-id';
      const field = options.sort.replace('-', '');
      if (options.sort) {
        if (options.sort[0] === '-') {
          qb = qb.orderBy('projectAssign.' + field, 'DESC');
        } else {
          qb = qb.orderBy('projectAssign.' + field, 'ASC');
        }
      }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        list: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }
}
