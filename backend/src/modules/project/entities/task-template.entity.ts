import { CustomValidationError, User } from '@omgstore/core-nestjs';
import { Store } from '../../client/entities/store.entity';
import {
  IsNotEmpty,
  IsOptional,
  MaxLength,
  validateSync
} from 'class-validator';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn
} from 'typeorm';
import { Task } from './task.entity';

@Entity({ name: 'task_template' })
export class TaskTemplate {
  @PrimaryGeneratedColumn()
  id: number = undefined;

  @Column()
  @IsNotEmpty()
  category: number = undefined;

  @Column({ length: 255 })
  @IsNotEmpty()
  @MaxLength(255)
  name: string = undefined;

  @OneToMany(type => Task, task => task.template)
  tasks: Task[];

  @BeforeInsert()
  doBeforeInsertion() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }

  @BeforeUpdate()
  doBeforeUpdate() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }
}
