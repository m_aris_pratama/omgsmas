import { Project } from './project.entity';
import { ProjectAssign } from './project-assignment.entity';
import { TaskAssign } from './task-assignment.entity';
import { Task } from './task.entity';
import { NapUpload } from './nap-upload.entity';
import { Program } from './program.entity';
import { TaskTemplate } from './task-template.entity';

export const entities = [
  Project,
  ProjectAssign,
  TaskAssign,
  Task,
  NapUpload,
  Program,
  TaskTemplate
];
