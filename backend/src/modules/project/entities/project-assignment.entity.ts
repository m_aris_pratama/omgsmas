import { CustomValidationError, User } from '@omgstore/core-nestjs';
import { Store } from '../../client/entities/store.entity';
import {
  IsNotEmpty,
  IsOptional,
  MaxLength,
  validateSync
} from 'class-validator';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn
} from 'typeorm';
import { Project } from './project.entity';
import { TaskAssign } from './task-assignment.entity';

/* id : int
project_id : int
store_id : int
survey_completed : date
instalation_completed : date
removed_completed : date
created_at : date
updated_at : date
is_active : bool
*/
@Entity({ name: 'project_assignment' })
export class ProjectAssign {
  @PrimaryGeneratedColumn()
  id: number = undefined;

  @ManyToOne(type => Project, { nullable: true })
  @JoinColumn({ name: 'project_id' })
  project: Project = undefined;

  @ManyToOne(type => Store, { nullable: true })
  @JoinColumn({ name: 'store_id' })
  store: Store = undefined;

  @Column()
  @IsOptional()
  program: string = '';

  @Column({ default: null })
  @IsOptional()
  survey_completed: Date = undefined;

  @Column({ default: false })
  @IsOptional()
  survey_approved: boolean = undefined;

  @Column({ default: 0 })
  @IsOptional()
  survey_count: number = undefined;

  @Column({ default: null })
  @IsOptional()
  instalation_completed: Date = undefined;

  @Column({ default: null })
  @IsOptional()
  removed_completed: Date = undefined;

  @Column({ default: null })
  @IsOptional()
  last_maintenance: Date = undefined;

  @Column({ default: true })
  is_active: boolean;

  @Column({ default: false })
  is_priority: boolean;

  @CreateDateColumn({ name: 'created_at' })
  created: Date = undefined;

  @UpdateDateColumn({ name: 'updated_at' })
  updated: Date = undefined;

  @OneToMany(type => TaskAssign, taskAssign => taskAssign.projectAssign)
  taskAssign: TaskAssign[];

  @BeforeInsert()
  doBeforeInsertion() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }

  @BeforeUpdate()
  doBeforeUpdate() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }
}
