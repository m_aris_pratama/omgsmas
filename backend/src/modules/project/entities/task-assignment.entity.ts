import { CustomValidationError, User } from '@omgstore/core-nestjs';
import { Store } from '../../client/entities/store.entity';
import {
  IsNotEmpty,
  IsOptional,
  MaxLength,
  validateSync
} from 'class-validator';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn
} from 'typeorm';
import { ProjectAssign } from './project-assignment.entity';
import { Task } from './task.entity';
import { Team } from 'modules/client/entities/team.entity';

/* id : int
team_id : int
project_assignment_id: int
task_id: int
value: varchar(1024)
note: varchar(1024)
longitude: double
latitude: double
created_at : date
updated_at : date
*/
@Entity({ name: 'task_assignment' })
export class TaskAssign {
  @PrimaryGeneratedColumn()
  id: number = undefined;

  @ManyToOne(type => Team, { nullable: true })
  @JoinColumn({ name: 'team_id' })
  team: Team = undefined;

  @ManyToOne(type => ProjectAssign, { nullable: true })
  @JoinColumn({ name: 'project_assignment_id' })
  projectAssign: ProjectAssign = undefined;

  @ManyToOne(type => Task, { nullable: true })
  @JoinColumn({ name: 'task_id' })
  task: Task = undefined;

  @Column({ type: 'text' })
  value: string = undefined;

  @Column({ type: 'text' })
  note: string = undefined;

  @Column({ type: 'double' })
  @IsNotEmpty()
  longitude: number = undefined;

  @Column({ type: 'double' })
  @IsNotEmpty()
  latitude: number = undefined;

  @CreateDateColumn({ name: 'created_at' })
  created: Date = undefined;

  @UpdateDateColumn({ name: 'updated_at' })
  updated: Date = undefined;

  @BeforeInsert()
  doBeforeInsertion() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }

  @BeforeUpdate()
  doBeforeUpdate() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }
}
