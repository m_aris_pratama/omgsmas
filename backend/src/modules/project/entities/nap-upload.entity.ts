import { CustomValidationError, User } from '@omgstore/core-nestjs';
import { Store } from '../../client/entities/store.entity';
import {
  IsNotEmpty,
  IsOptional,
  MaxLength,
  validateSync
} from 'class-validator';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn
} from 'typeorm';
import { Team } from 'modules/client/entities/team.entity';

/* id : int
team_id: int
store_id: int,
brand: varchar(100),
program: varchar(100),
promo: varchar(100),
file: varchar(255),
desc: varchar(512)
longitude: double
latitude: double
created_at : date
updated_at : date
*/
@Entity({ name: 'nap_upload' })
export class NapUpload {
  @PrimaryGeneratedColumn()
  id: number = undefined;

  @ManyToOne(type => Team, { nullable: true })
  @JoinColumn({ name: 'team_id' })
  team: Team = undefined;

  @ManyToOne(type => Store, { nullable: true })
  @JoinColumn({ name: 'store_id' })
  store: Store = undefined;

  @Column()
  clean: boolean = false;

  @Column()
  brand: string = undefined;

  @Column()
  program: string = undefined;

  @Column()
  promo: string = undefined;

  @Column()
  file: string = undefined;

  @Column()
  desc: string = undefined;

  @Column({ type: 'double' })
  @IsNotEmpty()
  longitude: number = undefined;

  @Column({ type: 'double' })
  @IsNotEmpty()
  latitude: number = undefined;

  @CreateDateColumn({ name: 'created_at' })
  created: Date = undefined;

  @UpdateDateColumn({ name: 'updated_at' })
  updated: Date = undefined;

  @BeforeInsert()
  doBeforeInsertion() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }

  @BeforeUpdate()
  doBeforeUpdate() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }
}
