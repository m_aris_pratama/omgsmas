import { CustomValidationError, User } from '@omgstore/core-nestjs';
import { Store } from '../../client/entities/store.entity';
import { TaskTemplate } from './task-template.entity';
import {
  IsNotEmpty,
  IsOptional,
  MaxLength,
  validateSync
} from 'class-validator';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn
} from 'typeorm';

/* id: int
name: varchar(255)
desc: varchar(512)
*/
@Entity({ name: 'program' })
export class Program {
  @PrimaryGeneratedColumn()
  id: number = undefined;

  @Column({ length: 255 })
  @IsNotEmpty()
  @MaxLength(150)
  name: string = undefined;

  @Column({ length: 512 })
  @IsOptional()
  @MaxLength(512)
  desc: string = undefined;

  @BeforeInsert()
  doBeforeInsertion() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }

  @BeforeUpdate()
  doBeforeUpdate() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }
}
