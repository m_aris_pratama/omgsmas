import { CustomValidationError, User } from '@omgstore/core-nestjs';
import { Store } from '../../client/entities/store.entity';
import { TaskTemplate } from './task-template.entity';
import {
  IsNotEmpty,
  IsOptional,
  MaxLength,
  validateSync
} from 'class-validator';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn
} from 'typeorm';

/* id: int
name: varchar(255)
template_id : int
required: bool
type: varchar(100)
options: varchar(1024)
logic : varchar(1024)
created_at : date
updated_at : date
is_active : bool
*/
@Entity({ name: 'task' })
export class Task {
  @PrimaryGeneratedColumn()
  id: number = undefined;

  @Column({ length: 150 })
  @IsNotEmpty()
  @MaxLength(150)
  name: string = undefined;

  @ManyToOne(type => TaskTemplate, { nullable: true })
  @JoinColumn({ name: 'template_id' })
  template: TaskTemplate = undefined;

  @Column({ default: true })
  required: boolean = undefined;

  @Column({ length: 150 })
  @IsNotEmpty()
  @MaxLength(150)
  type: string = undefined;

  @Column({ type: 'text' })
  @IsOptional()
  options: string = undefined;

  @Column({ type: 'text' })
  @IsOptional()
  logic: string = undefined;

  @Column()
  is_active: boolean = undefined;

  @CreateDateColumn({ name: 'created_at' })
  created: Date = undefined;

  @UpdateDateColumn({ name: 'updated_at' })
  updated: Date = undefined;

  @BeforeInsert()
  doBeforeInsertion() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }

  @BeforeUpdate()
  doBeforeUpdate() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }
}
