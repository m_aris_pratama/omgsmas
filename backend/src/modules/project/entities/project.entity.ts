import { CustomValidationError, User } from '@omgstore/core-nestjs';
import { Store } from '../../client/entities/store.entity';
import { ProjectAssign } from './project-assignment.entity';
import { Client } from '../../client/entities/client.entity';
import {
  IsNotEmpty,
  IsOptional,
  MaxLength,
  validateSync
} from 'class-validator';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn
} from 'typeorm';
import { Brand } from 'modules/client/entities/brand.entity';
import { BusinessUnit } from '../../client/entities/business-unit.entity';
import { TaskTemplate } from './task-template.entity';

/* id : int
brand_id : int
name : varchar(255)
information: text
photo: varchar(512)
business_unit_id : int
cycle_year : int
cycle_start : int
cycle_end : int
instalation_date : date
removed_date : date
task_survey_template_id : int
task_instalation_template_id : int
task_maintenance_template_id : int
task_removed_template_id : int
created_at : date
updated_at : date
is_active : bool
*/
@Entity({ name: 'project' })
export class Project {
  @PrimaryGeneratedColumn()
  id: number = undefined;

  @ManyToOne(type => Brand, { nullable: false })
  @JoinColumn({ name: 'brand_id' })
  brand: Brand = undefined;

  @Column({ length: 150 })
  @IsNotEmpty()
  @MaxLength(150)
  name: string = undefined;

  @Column({ length: 1024 })
  @IsNotEmpty()
  @MaxLength(1024)
  programs: string = undefined;

  @Column({ type: 'text' })
  @IsOptional()
  information: string = undefined;

  @Column({ length: 512, default: '' })
  @MaxLength(512)
  @IsOptional()
  photo: string = undefined;

  @ManyToOne(type => BusinessUnit, { nullable: false })
  @JoinColumn({ name: 'business_unit_id' })
  businessUnit: BusinessUnit = undefined;

  @Column()
  cycle_year: number = 0;

  @Column()
  cycle_start: number = 0;

  @Column()
  cycle_end: number = 0;

  @Column()
  survey_date: Date = null;

  @Column()
  @IsOptional()
  survey_target: number = 0;

  @Column()
  @IsOptional()
  instalation_date: Date = null;

  @Column()
  @IsOptional()
  instalation_date_jabo: Date = null;

  @Column()
  @IsOptional()
  instalation_target: number = 0;

  @Column()
  @IsOptional()
  removed_date: Date = null;

  @Column()
  @IsOptional()
  removed_target: number = 0;

  @ManyToOne(type => TaskTemplate, { nullable: true })
  @JoinColumn({ name: 'task_survey_template_id' })
  taskSurvey: TaskTemplate = null;

  @ManyToOne(type => TaskTemplate, { nullable: true })
  @JoinColumn({ name: 'task_instalation_template_id' })
  taskInstalation: TaskTemplate = null;

  @ManyToOne(type => TaskTemplate, { nullable: true })
  @JoinColumn({ name: 'task_removed_template_id' })
  taskRemoved: TaskTemplate = null;

  @ManyToOne(type => TaskTemplate, { nullable: true })
  @JoinColumn({ name: 'task_not_installed_template_id' })
  taskNotInstalled: TaskTemplate = null;

  @Column({ default: true })
  is_active: boolean = true;

  @CreateDateColumn({ name: 'created_at' })
  created: Date = undefined;

  @UpdateDateColumn({ name: 'updated_at' })
  updated: Date = undefined;

  @OneToMany(type => ProjectAssign, projectAssign => projectAssign.project)
  projectAssign: ProjectAssign[];

  @BeforeInsert()
  doBeforeInsertion() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }

  @BeforeUpdate()
  doBeforeUpdate() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }
}
