import { ClientsController } from './clients.controller';
import { RetailsController } from './retails.controller';
import { BrandsController } from './brands.controller';

import { ClientCategoriesController } from './client-categories.controller';
import { RetailCategoriesController } from './retail-categories.controller';
import { BrandCategoriesController } from './brand-categories.controller';
import { BrandSubcategoriesController } from './brand-subcategories.controller';

import { AreasController } from './areas.controller';
import { CitiesController } from './cities.controller';
import { StoresController } from './stores.controller';
import { TeamsController } from './teams.controller';
import { BusinessUnitsController } from './units.controller';
import { UnitAdminsController } from './unit-admin.controller';

export const controllers = [
  ClientsController,
  RetailsController,
  BrandsController,
  ClientCategoriesController,
  RetailCategoriesController,
  BrandCategoriesController,
  BrandSubcategoriesController,
  AreasController,
  CitiesController,
  StoresController,
  TeamsController,
  BusinessUnitsController,
  UnitAdminsController
];
