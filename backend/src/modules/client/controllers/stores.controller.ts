import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UseGuards,
  Req,
  Res,
  UseInterceptors,
  FileInterceptor,
  UploadedFile
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiImplicitParam,
  ApiImplicitQuery,
  ApiResponse,
  ApiUseTags,
  ApiConsumes,
  ApiImplicitFile
} from '@nestjs/swagger';
import { plainToClass } from 'class-transformer';
import { Permissions } from '@omgstore/core-nestjs';
import { Roles } from '@omgstore/core-nestjs';
import { InStoreDto } from '../dto/in-store.dto';
import { OutStoreDto } from '../dto/out-store.dto';
import { OutStoresDto } from '../dto/out-stores.dto';
import { Store } from '../entities/store.entity';
import { AccessGuard } from '@omgstore/core-nestjs';
import { ParseIntWithDefaultPipe } from '@omgstore/core-nestjs';
import { StoresService } from '../services/stores.service';
import * as XLSX from 'xlsx';
import { RetailsService } from '../services/retails.service';
import { CitiesService } from '../services/cities.service';
import { AreasService } from '../services/areas.service';
import { TeamsService } from '../services/teams.service';
import { exportNormalize } from '../../../libs/core/utils/custom-transforms';

@ApiUseTags('stores')
@ApiBearerAuth()
@Controller('/api/stores')
@UseGuards(AccessGuard)
export class StoresController {
  constructor(
    private readonly service: StoresService,
    private readonly serviceRetail: RetailsService,
    private readonly serviceCity: CitiesService,
    private readonly serviceArea: AreasService,
    private readonly serviceTeam: TeamsService
  ) { }

  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: OutStoreDto,
    description: 'The record has been successfully created.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post()
  async create(@Body() dto: InStoreDto) {
    try {
      return plainToClass(
        OutStoreDto,
        await this.service.create({
          item: await plainToClass(Store, dto)
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutStoreDto,
    description: 'The record has been successfully updated.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Put(':id')
  async update(@Param('id', new ParseIntPipe()) id, @Body() dto: InStoreDto) {
    try {
      return plainToClass(
        OutStoreDto,
        await this.service.update({
          id,
          item: await plainToClass(Store, dto)
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The record has been successfully deleted.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Delete(':id')
  async delete(@Param('id', new ParseIntPipe()) id) {
    try {
      return plainToClass(
        OutStoreDto,
        await this.service.delete({
          id
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutStoreDto,
    description: ''
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Get(':id')
  async findById(@Param('id', new ParseIntPipe()) id) {
    try {
      return plainToClass(
        OutStoreDto,
        await this.service.findById({
          id
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutStoresDto,
    description: ''
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitQuery({
    name: 'q',
    required: false,
    type: String,
    description: 'Text for search (default: empty)'
  })
  @ApiImplicitQuery({
    name: 'sort',
    required: false,
    type: String,
    description: 'Column name for sort (default: -id)'
  })
  @ApiImplicitQuery({
    name: 'per_page',
    required: false,
    type: Number,
    description: 'Number of results to return per page. (default: 10)'
  })
  @ApiImplicitQuery({
    name: 'cur_page',
    required: false,
    type: Number,
    description: 'A page number within the paginated result set. (default: 1)'
  })
  @ApiImplicitQuery({
    name: 'retail_id',
    required: false,
    type: Number,
    description: 'Retail id for filter data by retail. (default: empty)'
  })
  @ApiImplicitQuery({
    name: 'area_id',
    required: false,
    type: Number,
    description: 'Area id for filter data by area. (default: empty)'
  })
  @ApiImplicitQuery({
    name: 'city_id',
    required: false,
    type: Number,
    description: 'City id for filter data by city. (default: empty)'
  })
  @Get()
  async findAll(
    @Query('cur_page', new ParseIntWithDefaultPipe(1)) curPage,
    @Query('per_page', new ParseIntWithDefaultPipe(10)) perPage,
    @Query('q') q,
    @Query('sort') sort,
    @Query('retail_id') retailId,
    @Query('area_id') areaId,
    @Query('city_id') cityId
  ) {
    try {
      return plainToClass(
        OutStoresDto,
        await this.service.findAll({
          curPage,
          perPage,
          q,
          sort,
          retailId,
          areaId,
          cityId
        })
      );
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'upload data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post('importData')
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({
    name: 'file',
    required: true,
    description: 'Import Data'
  })
  async importData(@Req() req, @UploadedFile() file) {
    try {
      const wb = XLSX.read(file.buffer, { type: 'buffer' });
      const first_sheet_name = wb.SheetNames[0];
      const worksheet = wb.Sheets[first_sheet_name];

      for (let i = 2; i < 1000; i++) {
        const name = worksheet['E' + (i + 1)]
          ? worksheet['E' + (i + 1)].w
          : undefined;
        if (name === undefined) break;
        const code = worksheet['D' + (i + 1)]
          ? worksheet['D' + (i + 1)].w
          : undefined;
        if (code === undefined) break;
        // check client name
        const nret = worksheet['A' + (i + 1)]
          ? await this.serviceRetail.createIfNoExist({
            code: worksheet['A' + (i + 1)].w
          })
          : null;
        const narea = worksheet['B' + (i + 1)]
          ? await this.serviceArea.createIfNoExist({
            name: worksheet['B' + (i + 1)].w
          })
          : null;
        const ncity = worksheet['C' + (i + 1)]
          ? await this.serviceCity.createIfNoExist({
            area: narea,
            name: worksheet['C' + (i + 1)].w
          })
          : null;
        const nlead = worksheet['F' + (i + 1)]
          ? await this.serviceTeam.createIfNoExist({
            position: 'lead',
            name: worksheet['F' + (i + 1)].w
          })
          : null;
        const nmember = worksheet['G' + (i + 1)]
          ? await this.serviceTeam.createIfNoExist({
            position: 'member',
            name: worksheet['G' + (i + 1)].w
          })
          : null;

        const drow = {
          retail: nret,
          city: ncity,
          code,
          name,
          lead: nlead,
          member: nmember,
          address: worksheet['H' + (i + 1)] ? worksheet['H' + (i + 1)].w : '',
          telp: worksheet['I' + (i + 1)] ? worksheet['I' + (i + 1)].w : '',
          longitude: worksheet['J' + (i + 1)] ? worksheet['J' + (i + 1)].w : '',
          latitude: worksheet['K' + (i + 1)] ? worksheet['K' + (i + 1)].w : '',
          contact_person: worksheet['L' + (i + 1)]
            ? worksheet['L' + (i + 1)].w
            : '',
          contact_person_telp: worksheet['M' + (i + 1)]
            ? worksheet['M' + (i + 1)].w
            : '',
          contact_person_position: worksheet['N' + (i + 1)]
            ? worksheet['N' + (i + 1)].w
            : '',
          contact_person_email: worksheet['O' + (i + 1)]
            ? worksheet['O' + (i + 1)].w
            : ''
        };

        const isE = await this.service.isCodeExist({ code });
        if (!isE) {
          // create new
          await this.service.create({
            item: await plainToClass(Store, drow)
          });
        } else {
          // update
          await this.service.update({
            id: isE.id,
            item: await plainToClass(Store, { ...isE, ...drow })
          });
        }
      }
      return { status: 'OK' };
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'download data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('exportData/:id')
  async exportData(@Param('id', new ParseIntPipe()) id, @Res() res) {
    try {
      const items = await this.service.findAll({ curPage: 1, perPage: 0 });
      const rs = [];
      items.list.forEach(val => {
        const data = exportNormalize(val, ['retail.code', 'city.area.name', 'city.name', 'code', 'name', 'lead.name', 'member.name', 'address', 'telp', 'longitude', 'latitude', 'contact_person', 'contact_person_telp', 'contact_person_position', 'contact_person_email']);
        rs.push(data);
      });
      const wb = XLSX.readFile('public/templates/store.xlsx', {
        cellStyles: true
      });
      const ws = wb.Sheets[wb.SheetNames[0]];
      XLSX.utils.sheet_add_aoa(ws, rs, { origin: 'A3' });
      const wr = XLSX.write(wb, {
        bookType: 'xlsx',
        bookSST: false,
        type: 'buffer'
      });
      res.setHeader(
        'Content-Type',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      );
      res.setHeader(
        'Content-Disposition',
        'attachment; filename="stores.xlsx"'
      );
      return res.status(200).send(wr);
    } catch (error) {
      throw error;
    }
  }
}
