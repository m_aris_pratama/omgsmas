import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UseGuards,
  Req,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  Res
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiImplicitParam,
  ApiImplicitQuery,
  ApiResponse,
  ApiUseTags,
  ApiConsumes,
  ApiImplicitFile
} from '@nestjs/swagger';
import { plainToClass } from 'class-transformer';
import { Permissions, Group } from '@omgstore/core-nestjs';
import { Roles } from '@omgstore/core-nestjs';
import { InTeamDto } from '../dto/in-team.dto';
import { OutTeamDto } from '../dto/out-team.dto';
import { OutTeamsDto } from '../dto/out-teams.dto';
import { Team } from '../entities/team.entity';
import { AccessGuard } from '@omgstore/core-nestjs';
import { ParseIntWithDefaultPipe } from '@omgstore/core-nestjs';
import { TeamsService } from '../services/teams.service';
import { UsersService } from '../../../libs/core/services/users.service';
import { User } from '../../../libs/core/entities/user.entity';
import { GroupsService } from '../../../libs/core/services/groups.service';
import * as XLSX from 'xlsx';

@ApiUseTags('teams')
@ApiBearerAuth()
@Controller('/api/teams')
@UseGuards(AccessGuard)
export class TeamsController {
  constructor(
    private readonly service: TeamsService,
    private readonly userService: UsersService,
    private readonly groupService: GroupsService
  ) {}
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: OutTeamDto,
    description: 'The record has been successfully created.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post()
  async create(@Body() dto: InTeamDto) {
    try {
      const grp =
        dto.position === 'lead'
          ? await this.groupService.findById({ id: 2 })
          : await this.groupService.findById({ id: 3 });
      // create user
      const user = await this.userService.create({
        item: await plainToClass(User, {
          isSuperuser: false,
          firstName: dto.name,
          lastName: '',
          username: dto.username,
          password: dto.password,
          isStaff: true,
          email: '',
          groups: [grp.group],
          isActive: true
        }).setPassword(dto.password)
      });
      // create team profile
      const profile = await this.service.create({
        item: await plainToClass(Team, { user: { id: user.user.id }, ...dto })
      });

      return plainToClass(OutTeamDto, profile);
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutTeamDto,
    description: 'The record has been successfully updated.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Put(':id')
  async update(@Param('id', new ParseIntPipe()) id, @Body() dto: InTeamDto) {
    try {
      const item = await this.service.findById({ id });
      const grp =
        dto.position === 'lead'
          ? await this.groupService.findById({ id: 2 })
          : await this.groupService.findById({ id: 3 });

      let user = null;
      // create
      if (!item.team.user) {
        const user1 = await this.userService.create({
          item: await plainToClass(User, {
            isSuperuser: false,
            firstName: dto.name,
            lastName: '',
            username: dto.username,
            password: dto.password,
            isStaff: true,
            email: '',
            groups: [grp.group],
            isActive: true
          }).setPassword(dto.password)
        });
        user = user1.user;
      } else {
        // update
        item.team.user.username = dto.username;
        item.team.user.password = dto.password;
        item.team.user.firstName = dto.name;
        item.team.user.lastName = '';
        item.team.user.groups = [grp.group];
        user = await this.userService.update({
          id: item.team.user.id,
          item: await plainToClass(User, item.team.user).setPassword(
            dto.password
          )
        });
      }

      return plainToClass(
        OutTeamDto,
        await this.service.update({
          id,
          item: await plainToClass(Team, { user, ...dto })
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The record has been successfully deleted.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Delete(':id')
  async delete(@Param('id', new ParseIntPipe()) id) {
    try {
      const item = await this.service.findById({ id });
      if (item.team.user) {
        await this.userService.delete({ id: item.team.user.id });
      }
      return plainToClass(
        OutTeamDto,
        await this.service.delete({
          id
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutTeamDto,
    description: ''
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Get(':id')
  async findById(@Param('id', new ParseIntPipe()) id) {
    try {
      return plainToClass(
        OutTeamDto,
        await this.service.findById({
          id
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutTeamsDto,
    description: ''
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitQuery({
    name: 'q',
    required: false,
    type: String,
    description: 'Text for search (default: empty)'
  })
  @ApiImplicitQuery({
    name: 'sort',
    required: false,
    type: String,
    description: 'Column name for sort (default: -id)'
  })
  @ApiImplicitQuery({
    name: 'per_page',
    required: false,
    type: Number,
    description: 'Number of results to return per page. (default: 10)'
  })
  @ApiImplicitQuery({
    name: 'cur_page',
    required: false,
    type: Number,
    description: 'A page number within the paginated result set. (default: 1)'
  })
  @Get()
  async findAll(
    @Query('cur_page', new ParseIntWithDefaultPipe(1)) curPage,
    @Query('per_page', new ParseIntWithDefaultPipe(10)) perPage,
    @Query('q') q,
    @Query('sort') sort
  ) {
    try {
      return plainToClass(
        OutTeamsDto,
        await this.service.findAll({
          curPage,
          perPage,
          q,
          sort
        })
      );
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'upload data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post('importData')
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({
    name: 'file',
    required: true,
    description: 'Import Data'
  })
  async importData(@Req() req, @UploadedFile() file) {
    try {
      const wb = XLSX.read(file.buffer, { type: 'buffer' });
      const first_sheet_name = wb.SheetNames[0];
      const worksheet = wb.Sheets[first_sheet_name];

      for (let i = 1; i < 1000; i++) {
        const name = worksheet['A' + (i + 1)]
          ? worksheet['A' + (i + 1)].w
          : undefined;
        if (name === undefined) break;
        const username = worksheet['B' + (i + 1)]
          ? worksheet['B' + (i + 1)].w
          : '';
        const position = worksheet['C' + (i + 1)]
          ? worksheet['C' + (i + 1)].w
          : 'member';
        const email = worksheet['F' + (i + 1)]
          ? worksheet['F' + (i + 1)].w
          : '';
        // check client name
        const user = username
          ? await this.userService.createIfNoExist({
              name,
              username,
              position,
              email
            })
          : null;

        const drow = {
          name,
          user,
          position,
          address: worksheet['D' + (i + 1)] ? worksheet['D' + (i + 1)].w : '',
          telp: worksheet['E' + (i + 1)] ? worksheet['E' + (i + 1)].w : '',
          email
        };

        const isE = await this.service.isNameExist({ name });
        if (!isE) {
          // create new
          await this.service.create({
            item: await plainToClass(Team, drow)
          });
        } else {
          // update
          await this.service.update({
            id: isE.id,
            item: await plainToClass(Team, { ...isE, ...drow })
          });
        }
      }
      return { status: 'OK' };
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'download data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('exportData/:id')
  async exportData(@Param('id', new ParseIntPipe()) id, @Res() res) {
    try {
      const items = await this.service.findAll({ curPage: 1, perPage: 0 });
      const rs = [];
      items.list.forEach(val => {
        rs.push([
          val.name,
          val.user ? val.user.username : '',
          val.position,
          val.address,
          val.telp,
          val.email
        ]);
      });
      const wb = XLSX.readFile('public/templates/team.xlsx', {
        cellStyles: true
      });
      const ws = wb.Sheets[wb.SheetNames[0]];
      XLSX.utils.sheet_add_aoa(ws, rs, { origin: 'A2' });
      const wr = XLSX.write(wb, {
        bookType: 'xlsx',
        bookSST: false,
        type: 'buffer'
      });
      res.setHeader(
        'Content-Type',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      );
      res.setHeader('Content-Disposition', 'attachment; filename="teams.xlsx"');
      return res.status(200).send(wr);
    } catch (error) {
      throw error;
    }
  }
}
