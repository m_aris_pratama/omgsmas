import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UseGuards,
  Req,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  Res
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiImplicitParam,
  ApiImplicitQuery,
  ApiResponse,
  ApiUseTags,
  ApiConsumes,
  ApiImplicitFile
} from '@nestjs/swagger';
import { plainToClass } from 'class-transformer';
import { Permissions } from '@omgstore/core-nestjs';
import { Roles } from '@omgstore/core-nestjs';
import { InClientDto } from '../dto/in-client.dto';
import { OutClientDto } from '../dto/out-client.dto';
import { OutClientsDto } from '../dto/out-clients.dto';
import { Client } from '../entities/client.entity';
import { AccessGuard } from '@omgstore/core-nestjs';
import { ParseIntWithDefaultPipe } from '@omgstore/core-nestjs';
import { ClientsService } from '../services/clients.service';
import { diskStorage } from 'multer';
import { extname } from 'path';
import * as XLSX from 'xlsx';
import moment = require('moment');

@ApiUseTags('clients')
@ApiBearerAuth()
@Controller('/api/clients')
@UseGuards(AccessGuard)
export class ClientsController {
  constructor(private readonly service: ClientsService) {}
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: OutClientDto,
    description: 'The record has been successfully created.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post()
  async create(@Body() dto: InClientDto) {
    try {
      return plainToClass(
        OutClientDto,
        await this.service.create({
          item: await plainToClass(Client, dto)
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutClientDto,
    description: 'The record has been successfully updated.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Put(':id')
  async update(@Param('id', new ParseIntPipe()) id, @Body() dto: InClientDto) {
    try {
      return plainToClass(
        OutClientDto,
        await this.service.update({
          id,
          item: await plainToClass(Client, dto)
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The record has been successfully deleted.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Delete(':id')
  async delete(@Param('id', new ParseIntPipe()) id) {
    try {
      return plainToClass(
        OutClientDto,
        await this.service.delete({
          id
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutClientDto,
    description: ''
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Get(':id')
  async findById(@Param('id', new ParseIntPipe()) id) {
    try {
      return plainToClass(
        OutClientDto,
        await this.service.findById({
          id
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutClientsDto,
    description: ''
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitQuery({
    name: 'q',
    required: false,
    type: String,
    description: 'Text for search (default: empty)'
  })
  @ApiImplicitQuery({
    name: 'sort',
    required: false,
    type: String,
    description: 'Column name for sort (default: -id)'
  })
  @ApiImplicitQuery({
    name: 'per_page',
    required: false,
    type: Number,
    description: 'Number of results to return per page. (default: 10)'
  })
  @ApiImplicitQuery({
    name: 'cur_page',
    required: false,
    type: Number,
    description: 'A page number within the paginated result set. (default: 1)'
  })
  @Get()
  async findAll(
    @Query('cur_page', new ParseIntWithDefaultPipe(1)) curPage,
    @Query('per_page', new ParseIntWithDefaultPipe(10)) perPage,
    @Query('q') q,
    @Query('sort') sort
  ) {
    try {
      return plainToClass(
        OutClientsDto,
        await this.service.findAll({
          curPage,
          perPage,
          q,
          sort
        })
      );
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'The record has been successfully created.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post('uploadImage')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/uploads/cms',
        filename: (req, file, cb) => {
          // Generating a 32 random chars long string
          const randomName = Array(32)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');
          // Calling the callback passing the random name generated with the original extension name
          cb(null, `${randomName}${extname(file.originalname)}`);
        }
      })
    })
  )
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({
    name: 'file',
    required: true,
    description: 'Photo Selfie'
  })
  async photo(@Req() req, @UploadedFile() file) {
    try {
      // tslint:disable-next-line:no-console
      console.log(file);

      return { fileName: file.filename };
    } catch (error) {
      throw error;
    }
  }

  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'get image record'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'name', type: String })
  @Get('uploadImage/:name')
  async getImage(@Param('name') name, @Res() res) {
    try {
      return res.sendFile('uploads/cms/' + name, { root: 'public' });
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'upload data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post('importData')
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({
    name: 'file',
    required: true,
    description: 'Import Data'
  })
  async importData(@Req() req, @UploadedFile() file) {
    try {
      const wb = XLSX.read(file.buffer, { type: 'buffer' });
      const first_sheet_name = wb.SheetNames[0];
      const worksheet = wb.Sheets[first_sheet_name];

      for (let i = 2; i < 1000; i++) {
        const cellA = worksheet['A' + (i + 1)];
        const name = cellA ? cellA.v : undefined;
        if (name === undefined) break;
        const drow = {
          name,
          registered: worksheet['B' + (i + 1)]
            ? worksheet['B' + (i + 1)].w
            : null,
          npwp: worksheet['C' + (i + 1)] ? worksheet['C' + (i + 1)].w : '',
          address: worksheet['D' + (i + 1)] ? worksheet['D' + (i + 1)].w : '',
          telp: worksheet['E' + (i + 1)] ? worksheet['E' + (i + 1)].w : '',
          fax: worksheet['F' + (i + 1)] ? worksheet['F' + (i + 1)].w : '',
          email: worksheet['G' + (i + 1)] ? worksheet['G' + (i + 1)].w : '',
          contact_project: worksheet['H' + (i + 1)]
            ? worksheet['H' + (i + 1)].w
            : '',
          contact_project_telp: worksheet['I' + (i + 1)]
            ? worksheet['I' + (i + 1)].w
            : '',
          contact_project_position: worksheet['J' + (i + 1)]
            ? worksheet['J' + (i + 1)].w
            : '',
          contact_project_email: worksheet['K' + (i + 1)]
            ? worksheet['K' + (i + 1)].w
            : '',
          contact_project_address: worksheet['L' + (i + 1)]
            ? worksheet['L' + (i + 1)].w
            : '',
          contact_billing: worksheet['M' + (i + 1)]
            ? worksheet['M' + (i + 1)].w
            : '',
          contact_billing_telp: worksheet['N' + (i + 1)]
            ? worksheet['N' + (i + 1)].w
            : '',
          contact_billing_position: worksheet['O' + (i + 1)]
            ? worksheet['O' + (i + 1)].w
            : '',
          contact_billing_email: worksheet['P' + (i + 1)]
            ? worksheet['P' + (i + 1)].w
            : '',
          contact_billing_address: worksheet['Q' + (i + 1)]
            ? worksheet['Q' + (i + 1)].w
            : ''
        };

        const isE = await this.service.isNameExist({ name });
        if (!isE) {
          // create new
          await this.service.create({
            item: await plainToClass(Client, drow)
          });
        } else {
          // update
          await this.service.update({
            id: isE.id,
            item: await plainToClass(Client, { ...isE, ...drow })
          });
        }
      }
      return { status: 'OK' };
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'download data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  //  @Header('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
  //  @Header('Content-Disposition', 'attachment; filename="export-programs.xlsx"')
  @Get('exportData/:id')
  async exportData(@Param('id', new ParseIntPipe()) id, @Res() res) {
    try {
      const items = await this.service.findAll({ curPage: 1, perPage: 0 });
      const rs = [];
      items.list.forEach(val => {
        rs.push([
          val.name,
          moment(val.registered).format('DD MMM YYYY'),
          val.npwp,
          val.address,
          val.telp,
          val.fax,
          val.email,
          val.contact_project,
          val.contact_project_telp,
          val.contact_project_position,
          val.contact_project_email,
          val.contact_project_address,
          val.contact_billing,
          val.contact_billing_telp,
          val.contact_billing_position,
          val.contact_billing_email,
          val.contact_billing_address
        ]);
      });
      const wb = XLSX.readFile('public/templates/client.xlsx', {
        cellStyles: true
      });
      const ws = wb.Sheets[wb.SheetNames[0]];
      XLSX.utils.sheet_add_aoa(ws, rs, { origin: 'A3' });
      const wr = XLSX.write(wb, {
        bookType: 'xlsx',
        bookSST: false,
        type: 'buffer'
      });
      res.setHeader(
        'Content-Type',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      );
      res.setHeader(
        'Content-Disposition',
        'attachment; filename="clients.xlsx"'
      );
      return res.status(200).send(wr);
    } catch (error) {
      throw error;
    }
  }
}
