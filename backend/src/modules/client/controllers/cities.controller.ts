import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UseGuards,
  Req,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  Res
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiImplicitParam,
  ApiImplicitQuery,
  ApiResponse,
  ApiUseTags,
  ApiConsumes,
  ApiImplicitFile
} from '@nestjs/swagger';
import { plainToClass } from 'class-transformer';
import { Permissions } from '@omgstore/core-nestjs';
import { Roles } from '@omgstore/core-nestjs';
import { InCityDto } from '../dto/in-city.dto';
import { OutCityDto } from '../dto/out-city.dto';
import { OutCitiesDto } from '../dto/out-cities.dto';
import { City } from '../entities/city.entity';
import { AccessGuard } from '@omgstore/core-nestjs';
import { ParseIntWithDefaultPipe } from '@omgstore/core-nestjs';
import { CitiesService } from '../services/cities.service';
import * as XLSX from 'xlsx';
import { AreasService } from '../services/areas.service';

@ApiUseTags('cities')
@ApiBearerAuth()
@Controller('/api/cities')
@UseGuards(AccessGuard)
export class CitiesController {
  constructor(
    private readonly service: CitiesService,
    private readonly serviceArea: AreasService
  ) {}
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: OutCityDto,
    description: 'The record has been successfully created.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post()
  async create(@Body() dto: InCityDto) {
    try {
      return plainToClass(
        OutCityDto,
        await this.service.create({
          item: await plainToClass(City, dto)
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutCityDto,
    description: 'The record has been successfully updated.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Put(':id')
  async update(@Param('id', new ParseIntPipe()) id, @Body() dto: InCityDto) {
    try {
      return plainToClass(
        OutCityDto,
        await this.service.update({
          id,
          item: await plainToClass(City, dto)
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The record has been successfully deleted.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Delete(':id')
  async delete(@Param('id', new ParseIntPipe()) id) {
    try {
      return plainToClass(
        OutCityDto,
        await this.service.delete({
          id
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutCityDto,
    description: ''
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Get(':id')
  async findById(@Param('id', new ParseIntPipe()) id) {
    try {
      return plainToClass(
        OutCityDto,
        await this.service.findById({
          id
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutCitiesDto,
    description: ''
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitQuery({
    name: 'q',
    required: false,
    type: String,
    description: 'Text for search (default: empty)'
  })
  @ApiImplicitQuery({
    name: 'sort',
    required: false,
    type: String,
    description: 'Column name for sort (default: -id)'
  })
  @ApiImplicitQuery({
    name: 'per_page',
    required: false,
    type: Number,
    description: 'Number of results to return per page. (default: 10)'
  })
  @ApiImplicitQuery({
    name: 'cur_page',
    required: false,
    type: Number,
    description: 'A page number within the paginated result set. (default: 1)'
  })
  @ApiImplicitQuery({
    name: 'area_id',
    required: false,
    type: Number,
    description: 'Filter by Area. (default: 1)'
  })
  @Get()
  async findAll(
    @Query('cur_page', new ParseIntWithDefaultPipe(1)) curPage,
    @Query('per_page', new ParseIntWithDefaultPipe(10)) perPage,
    @Query('q') q,
    @Query('sort') sort,
    @Query('area_id') areaId
  ) {
    try {
      return plainToClass(
        OutCitiesDto,
        await this.service.findAll({
          curPage,
          perPage,
          q,
          sort,
          areaId
        })
      );
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'upload data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post('importData')
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({
    name: 'file',
    required: true,
    description: 'Import Data'
  })
  async importData(@Req() req, @UploadedFile() file) {
    try {
      const wb = XLSX.read(file.buffer, { type: 'buffer' });
      const first_sheet_name = wb.SheetNames[0];
      const worksheet = wb.Sheets[first_sheet_name];

      for (let i = 1; i < 1000; i++) {
        const cellB = worksheet['B' + (i + 1)];
        const name = cellB ? cellB.w : undefined;
        if (name === undefined) break;
        // check brand category
        const narea = worksheet['A' + (i + 1)]
          ? await this.serviceArea.createIfNoExist({
              name: worksheet['A' + (i + 1)].w
            })
          : null;
        const drow = {
          name,
          area: narea
        };

        const isE = await this.service.isNameExist({ name });
        if (!isE) {
          // create new
          await this.service.create({
            item: await plainToClass(City, drow)
          });
        } else {
          // update
          await this.service.update({
            id: isE.id,
            item: await plainToClass(City, { ...isE, ...drow })
          });
        }
      }
      return { status: 'OK' };
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'download data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('exportData/:id')
  async exportData(@Param('id', new ParseIntPipe()) id, @Res() res) {
    try {
      const items = await this.service.findAll({ curPage: 1, perPage: 0 });
      const rs = [];
      items.list.forEach(val => {
        rs.push([val.area.name, val.name]);
      });
      const wb = XLSX.readFile('public/templates/city.xlsx', {
        cellStyles: true
      });
      const ws = wb.Sheets[wb.SheetNames[0]];
      XLSX.utils.sheet_add_aoa(ws, rs, { origin: 'A2' });
      const wr = XLSX.write(wb, {
        bookType: 'xlsx',
        bookSST: false,
        type: 'buffer'
      });
      res.setHeader(
        'Content-Type',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      );
      res.setHeader(
        'Content-Disposition',
        'attachment; filename="cities.xlsx"'
      );
      return res.status(200).send(wr);
    } catch (error) {
      throw error;
    }
  }
}
