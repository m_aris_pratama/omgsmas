import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UseGuards,
  Req,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  Res
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiImplicitParam,
  ApiImplicitQuery,
  ApiResponse,
  ApiUseTags,
  ApiConsumes,
  ApiImplicitFile
} from '@nestjs/swagger';
import { plainToClass } from 'class-transformer';
import { Permissions } from '@omgstore/core-nestjs';
import { Roles } from '@omgstore/core-nestjs';
import { InRetailDto } from '../dto/in-retail.dto';
import { OutRetailDto } from '../dto/out-retail.dto';
import { OutRetailsDto } from '../dto/out-retails.dto';
import { Retail } from '../entities/retail.entity';
import { AccessGuard } from '@omgstore/core-nestjs';
import { ParseIntWithDefaultPipe } from '@omgstore/core-nestjs';
import { RetailsService } from '../services/retails.service';
import { Brand } from '../entities/brand.entity';
import * as XLSX from 'xlsx';
import { RetailCategoriesService } from '../services/retail-categories.service';
import { UnitsService } from '../services/units.service';
import { exportNormalize } from '../../../libs/core/utils/custom-transforms';

@ApiUseTags('retails')
@ApiBearerAuth()
@Controller('/api/retails')
@UseGuards(AccessGuard)
export class RetailsController {
  constructor(
    private readonly service: RetailsService,
    private readonly serviceRetailCat: RetailCategoriesService,
    private readonly serviceBU: UnitsService
  ) { }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: OutRetailDto,
    description: 'The record has been successfully created.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post()
  async create(@Body() dto: InRetailDto) {
    try {
      return plainToClass(
        OutRetailDto,
        await this.service.create({
          item: await plainToClass(Retail, dto)
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutRetailDto,
    description: 'The record has been successfully updated.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Put(':id')
  async update(@Param('id', new ParseIntPipe()) id, @Body() dto: InRetailDto) {
    try {
      return plainToClass(
        OutRetailDto,
        await this.service.update({
          id,
          item: await plainToClass(Retail, dto)
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The record has been successfully deleted.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Delete(':id')
  async delete(@Param('id', new ParseIntPipe()) id) {
    try {
      return plainToClass(
        OutRetailDto,
        await this.service.delete({
          id
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutRetailDto,
    description: ''
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Get(':id')
  async findById(@Param('id', new ParseIntPipe()) id) {
    try {
      return plainToClass(
        OutRetailDto,
        await this.service.findById({
          id
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutRetailsDto,
    description: ''
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitQuery({
    name: 'q',
    required: false,
    type: String,
    description: 'Text for search (default: empty)'
  })
  @ApiImplicitQuery({
    name: 'sort',
    required: false,
    type: String,
    description: 'Column name for sort (default: -id)'
  })
  @ApiImplicitQuery({
    name: 'per_page',
    required: false,
    type: Number,
    description: 'Number of results to return per page. (default: 10)'
  })
  @ApiImplicitQuery({
    name: 'cur_page',
    required: false,
    type: Number,
    description: 'A page number within the paginated result set. (default: 1)'
  })
  @ApiImplicitQuery({
    name: 'type_id',
    required: false,
    type: Number,
    description:
      'Retail category id for filter data by retail category. (default: empty)'
  })
  @ApiImplicitQuery({
    name: 'unit',
    required: false,
    type: Number,
    description: 'Retail business unit id for filter (default: empty)'
  })
  @Get()
  async findAll(
    @Query('cur_page', new ParseIntWithDefaultPipe(1)) curPage,
    @Query('per_page', new ParseIntWithDefaultPipe(10)) perPage,
    @Query('q') q,
    @Query('sort') sort,
    @Query('type_id') retailCategory,
    @Query('unit') unit
  ) {
    try {
      return plainToClass(
        OutRetailsDto,
        await this.service.findAll({
          curPage,
          perPage,
          q,
          sort,
          retailCategory,
          unit
        })
      );
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'upload data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post('importData')
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({
    name: 'file',
    required: true,
    description: 'Import Data'
  })
  async importData(@Req() req, @UploadedFile() file) {
    try {
      const wb = XLSX.read(file.buffer, { type: 'buffer' });
      const first_sheet_name = wb.SheetNames[0];
      const worksheet = wb.Sheets[first_sheet_name];

      for (let i = 2; i < 1000; i++) {
        const name = worksheet['D' + (i + 1)]
          ? worksheet['D' + (i + 1)].w
          : undefined;
        if (name === undefined) break;
        const code = worksheet['C' + (i + 1)]
          ? worksheet['C' + (i + 1)].w
          : undefined;
        if (code === undefined) break;
        // check client name
        const nBU = worksheet['A' + (i + 1)]
          ? await this.serviceBU.createIfNoExist({
            name: worksheet['A' + (i + 1)].w
          })
          : null;
        const ncat = worksheet['B' + (i + 1)]
          ? await this.serviceRetailCat.createIfNoExist({
            name: worksheet['B' + (i + 1)].w
          })
          : null;

        const drow = {
          businessUnit: nBU,
          retailCategory: ncat,
          code,
          name,
          company: worksheet['E' + (i + 1)] ? worksheet['E' + (i + 1)].w : '',
          npwp: worksheet['F' + (i + 1)] ? worksheet['F' + (i + 1)].w : '',
          address: worksheet['G' + (i + 1)] ? worksheet['G' + (i + 1)].w : '',
          telp: worksheet['H' + (i + 1)] ? worksheet['H' + (i + 1)].w : '',
          fax: worksheet['I' + (i + 1)] ? worksheet['I' + (i + 1)].w : '',
          contact_person: worksheet['J' + (i + 1)]
            ? worksheet['J' + (i + 1)].w
            : '',
          contact_person_telp: worksheet['K' + (i + 1)]
            ? worksheet['K' + (i + 1)].w
            : '',
          contact_person_position: worksheet['L' + (i + 1)]
            ? worksheet['L' + (i + 1)].w
            : '',
          contact_person_email: worksheet['M' + (i + 1)]
            ? worksheet['M' + (i + 1)].w
            : '',
          contact_project: worksheet['N' + (i + 1)]
            ? worksheet['N' + (i + 1)].w
            : '',
          contact_project_telp: worksheet['O' + (i + 1)]
            ? worksheet['O' + (i + 1)].w
            : '',
          contact_project_position: worksheet['P' + (i + 1)]
            ? worksheet['P' + (i + 1)].w
            : '',
          contact_project_email: worksheet['Q' + (i + 1)]
            ? worksheet['Q' + (i + 1)].w
            : '',
          contact_project_address: worksheet['R' + (i + 1)]
            ? worksheet['R' + (i + 1)].w
            : ''
        };

        const isE = await this.service.isCodeExist({ code });
        if (!isE) {
          // create new
          await this.service.create({
            item: await plainToClass(Retail, drow)
          });
        } else {
          // update
          await this.service.update({
            id: isE.id,
            item: await plainToClass(Retail, { ...isE, ...drow })
          });
        }
      }
      return { status: 'OK' };
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'download data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('exportData/:id')
  async exportData(@Param('id', new ParseIntPipe()) id, @Res() res) {
    try {
      const items = await this.service.findAll({ curPage: 1, perPage: 0 });
      const rs = [];
      items.list.forEach(val => {
        const data = exportNormalize(val, [
          'businessUnit.name',
          'retailCategory.name',
          'code',
          'name',
          'company',
          'npwp',
          'address',
          'telp',
          'fax',
          'contact_person',
          'contact_person_telp',
          'contact_person_position',
          'contact_person_email',
          'contact_project',
          'contact_project_telp',
          'contact_project_position',
          'contact_project_email',
          'contact_project_address'
        ]);
        rs.push(data);
      });
      const wb = XLSX.readFile('public/templates/retail.xlsx', {
        cellStyles: true
      });
      const ws = wb.Sheets[wb.SheetNames[0]];
      XLSX.utils.sheet_add_aoa(ws, rs, { origin: 'A3' });
      const wr = XLSX.write(wb, {
        bookType: 'xlsx',
        bookSST: false,
        type: 'buffer'
      });
      res.setHeader(
        'Content-Type',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      );
      res.setHeader(
        'Content-Disposition',
        'attachment; filename="retails.xlsx"'
      );
      return res.status(200).send(wr);
    } catch (error) {
      throw error;
    }
  }
}
