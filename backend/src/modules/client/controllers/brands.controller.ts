import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UseGuards,
  Req,
  Res,
  UseInterceptors,
  FileInterceptor,
  UploadedFile
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiImplicitParam,
  ApiImplicitQuery,
  ApiResponse,
  ApiUseTags,
  ApiImplicitFile,
  ApiConsumes
} from '@nestjs/swagger';
import { plainToClass } from 'class-transformer';
import { Permissions } from '@omgstore/core-nestjs';
import { Roles } from '@omgstore/core-nestjs';
import { InBrandDto } from '../dto/in-brand.dto';
import { OutBrandDto } from '../dto/out-brand.dto';
import { OutBrandsDto } from '../dto/out-brands.dto';
import { Brand } from '../entities/brand.entity';
import { AccessGuard } from '@omgstore/core-nestjs';
import { ParseIntWithDefaultPipe } from '@omgstore/core-nestjs';
import { BrandsService } from '../services/brands.service';
import * as XLSX from 'xlsx';
import { ClientsService } from '../services/clients.service';
import { BrandCategoriesService } from '../services/brand-categories.service';
import { BrandSubcategoriesService } from '../services/brand-subcategories.service';

@ApiUseTags('brands')
@ApiBearerAuth()
@Controller('/api/brands')
@UseGuards(AccessGuard)
export class BrandsController {
  constructor(
    private readonly service: BrandsService,
    private readonly serviceClient: ClientsService,
    private readonly serviceBrandCat: BrandCategoriesService,
    private readonly serviceBrandSubCat: BrandSubcategoriesService
  ) {}
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: OutBrandDto,
    description: 'The record has been successfully created.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post()
  async create(@Body() dto: InBrandDto) {
    try {
      return plainToClass(
        OutBrandDto,
        await this.service.create({
          item: await plainToClass(Brand, dto)
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutBrandDto,
    description: 'The record has been successfully updated.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Put(':id')
  async update(@Param('id', new ParseIntPipe()) id, @Body() dto: InBrandDto) {
    try {
      return plainToClass(
        OutBrandDto,
        await this.service.update({
          id,
          item: await plainToClass(Brand, dto)
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The record has been successfully deleted.'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Delete(':id')
  async delete(@Param('id', new ParseIntPipe()) id) {
    try {
      return plainToClass(
        OutBrandDto,
        await this.service.delete({
          id
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutBrandDto,
    description: ''
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'id', type: Number })
  @Get(':id')
  async findById(@Param('id', new ParseIntPipe()) id) {
    try {
      return plainToClass(
        OutBrandDto,
        await this.service.findById({
          id
        })
      );
    } catch (error) {
      throw error;
    }
  }
  @Roles('isSuperuser')
  @Permissions('manage')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: OutBrandsDto,
    description: ''
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @ApiImplicitQuery({
    name: 'q',
    required: false,
    type: String,
    description: 'Text for search (default: empty)'
  })
  @ApiImplicitQuery({
    name: 'sort',
    required: false,
    type: String,
    description: 'Column name for sort (default: -id)'
  })
  @ApiImplicitQuery({
    name: 'per_page',
    required: false,
    type: Number,
    description: 'Number of results to return per page. (default: 10)'
  })
  @ApiImplicitQuery({
    name: 'cur_page',
    required: false,
    type: Number,
    description: 'A page number within the paginated result set. (default: 1)'
  })
  @ApiImplicitQuery({
    name: 'client_id',
    required: false,
    type: Number,
    description: 'Filter by client. (default: empty)'
  })
  @ApiImplicitQuery({
    name: 'type_id',
    required: false,
    type: Number,
    description: 'Filter by brand category. (default: empty)'
  })
  @ApiImplicitQuery({
    name: 'subtype_id',
    required: false,
    type: Number,
    description: 'Filter by brand subcategory. (default: empty)'
  })
  @Get()
  async findAll(
    @Query('cur_page', new ParseIntWithDefaultPipe(1)) curPage,
    @Query('per_page', new ParseIntWithDefaultPipe(10)) perPage,
    @Query('q') q,
    @Query('sort') sort,
    @Query('client_id') client_id,
    @Query('type_id') type_id,
    @Query('subtype_id') subtype_id
  ) {
    try {
      return plainToClass(
        OutBrandsDto,
        await this.service.findAll({
          curPage,
          perPage,
          q,
          sort,
          client_id,
          type_id,
          subtype_id
        })
      );
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'upload data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Post('importData')
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({
    name: 'file',
    required: true,
    description: 'Import Data'
  })
  async importData(@Req() req, @UploadedFile() file) {
    try {
      const wb = XLSX.read(file.buffer, { type: 'buffer' });
      const first_sheet_name = wb.SheetNames[0];
      const worksheet = wb.Sheets[first_sheet_name];

      for (let i = 2; i < 1000; i++) {
        const cellB = worksheet['B' + (i + 1)];
        const name = cellB ? cellB.w : undefined;
        if (name === undefined) break;
        // check client name
        const nclient = worksheet['A' + (i + 1)]
          ? await this.serviceClient.createIfNoExist({
              name: worksheet['A' + (i + 1)].w
            })
          : null;
        const ncat = worksheet['C' + (i + 1)]
          ? await this.serviceBrandCat.createIfNoExist({
              name: worksheet['C' + (i + 1)].w
            })
          : null;
        const nsubcat = worksheet['D' + (i + 1)]
          ? await this.serviceBrandSubCat.createIfNoExist({
              cat: ncat,
              name: worksheet['D' + (i + 1)].w
            })
          : null;

        const drow = {
          client: nclient,
          name,
          brandSubcategory: nsubcat,
          address: worksheet['E' + (i + 1)] ? worksheet['E' + (i + 1)].w : '',
          telp: worksheet['F' + (i + 1)] ? worksheet['F' + (i + 1)].w : '',
          fax: worksheet['G' + (i + 1)] ? worksheet['G' + (i + 1)].w : '',
          email: worksheet['H' + (i + 1)] ? worksheet['H' + (i + 1)].w : '',
          contact_person: worksheet['I' + (i + 1)]
            ? worksheet['I' + (i + 1)].w
            : '',
          contact_person_telp: worksheet['J' + (i + 1)]
            ? worksheet['J' + (i + 1)].w
            : '',
          contact_person_position: worksheet['K' + (i + 1)]
            ? worksheet['K' + (i + 1)].w
            : '',
          contact_person_email: worksheet['L' + (i + 1)]
            ? worksheet['L' + (i + 1)].w
            : '',
          contact_person2: worksheet['M' + (i + 1)]
            ? worksheet['M' + (i + 1)].w
            : '',
          contact_person2_telp: worksheet['N' + (i + 1)]
            ? worksheet['N' + (i + 1)].w
            : '',
          contact_person2_position: worksheet['O' + (i + 1)]
            ? worksheet['O' + (i + 1)].w
            : '',
          contact_person2_email: worksheet['P' + (i + 1)]
            ? worksheet['P' + (i + 1)].w
            : ''
        };

        const isE = await this.service.isNameExist({ name });
        if (!isE) {
          // create new
          await this.service.create({
            item: await plainToClass(Brand, drow)
          });
        } else {
          // update
          await this.service.update({
            id: isE.id,
            item: await plainToClass(Brand, { ...isE, ...drow })
          });
        }
      }
      return { status: 'OK' };
    } catch (error) {
      throw error;
    }
  }

  //  @Roles('isSuperuser')
  //  @Permissions('add_absence')
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'download data'
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: 'Forbidden.' })
  @Get('exportData/:id')
  async exportData(@Param('id', new ParseIntPipe()) id, @Res() res) {
    try {
      const items = await this.service.findAll({ curPage: 1, perPage: 0 });
      const rs = [];
      items.list.forEach(val => {
        rs.push([
          val.client.name,
          val.name,
          val.brandSubcategory.brandCategory.name,
          val.brandSubcategory.name,
          val.address,
          val.telp,
          val.fax,
          val.email,
          val.contact_person,
          val.contact_person_telp,
          val.contact_person_position,
          val.contact_person_email,
          val.contact_person2,
          val.contact_person2_telp,
          val.contact_person2_position,
          val.contact_person2_email
        ]);
      });
      const wb = XLSX.readFile('public/templates/brand.xlsx', {
        cellStyles: true
      });
      const ws = wb.Sheets[wb.SheetNames[0]];
      XLSX.utils.sheet_add_aoa(ws, rs, { origin: 'A3' });
      const wr = XLSX.write(wb, {
        bookType: 'xlsx',
        bookSST: false,
        type: 'buffer'
      });
      res.setHeader(
        'Content-Type',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      );
      res.setHeader(
        'Content-Disposition',
        'attachment; filename="brands.xlsx"'
      );
      return res.status(200).send(wr);
    } catch (error) {
      throw error;
    }
  }
}
