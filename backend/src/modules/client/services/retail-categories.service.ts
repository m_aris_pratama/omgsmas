import {
  Inject,
  Injectable,
  MethodNotAllowedException,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CORE_CONFIG_TOKEN, ICoreConfig } from '@omgstore/core-nestjs';
import { RetailCategory } from '../entities/retail-category.entity';

@Injectable()
export class RetailCategoriesService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(RetailCategory)
    private readonly repository: Repository<RetailCategory>
  ) {}
  async create(options: { item: RetailCategory }) {
    try {
      options.item = await this.repository.save(options.item);
      const { retailCategory } = await this.findById({ id: options.item.id });
      return { retailCategory };
    } catch (error) {
      throw error;
    }
  }
  async update(options: { id: number; item: RetailCategory }) {
    options.item.id = options.id;
    try {
      options.item = await this.repository.save(options.item);
      const { retailCategory } = await this.findById({ id: options.item.id });
      return { retailCategory };
    } catch (error) {
      throw error;
    }
  }
  async delete(options: { id: number }) {
    try {
      let object = await this.repository.findOneOrFail(options.id);
      object = await this.repository.save(object);
      await this.repository.delete(options.id);
      return { retailCategory: null };
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      const item = await this.repository.findOneOrFail(options.id);
      return { retailCategory: item };
    } catch (error) {
      throw error;
    }
  }
  async createIfNoExist(options: { name: string }) {
    try {
      const qb = await this.repository
        .createQueryBuilder('retailCategory')
        .where('UPPER(retailCategory.name) = UPPER(:name)', {
          name: options.name
        })
        .getOne();
      if (!qb) {
        // create
        const item = await this.repository.save({ name: options.name });
        return (await this.findById({ id: item.id })).retailCategory;
      } else return qb;
    } catch (error) {
      throw error;
    }
  }
  async findAll(options: {
    curPage: number;
    perPage: number;
    q?: string;
    sort?: string;
  }) {
    try {
      let objects: [RetailCategory[], number];
      let qb = this.repository.createQueryBuilder('retailCategory');
      if (options.q) {
        qb = qb.where('retailCategory.name like :q', {
          q: `%${options.q}%`
        });
      }
      options.sort =
        options.sort &&
        new RetailCategory().hasOwnProperty(options.sort.replace('-', ''))
          ? options.sort
          : '-id';
      const field = options.sort.replace('-', '');
      if (options.sort) {
        if (options.sort[0] === '-') {
          qb = qb.orderBy('retailCategory.' + field, 'DESC');
        } else {
          qb = qb.orderBy('retailCategory.' + field, 'ASC');
        }
      }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        list: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }
}
