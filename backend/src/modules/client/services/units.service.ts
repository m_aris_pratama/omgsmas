import {
  Inject,
  Injectable,
  MethodNotAllowedException,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CORE_CONFIG_TOKEN, ICoreConfig } from '@omgstore/core-nestjs';
import { BusinessUnit } from '../entities/business-unit.entity';

@Injectable()
export class UnitsService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(BusinessUnit)
    private readonly repository: Repository<BusinessUnit>
  ) {}
  async create(options: { item: BusinessUnit }) {
    try {
      options.item = await this.repository.save(options.item);
      return {};
    } catch (error) {
      throw error;
    }
  }
  async update(options: { id: number; item: BusinessUnit }) {
    options.item.id = options.id;
    try {
      options.item = await this.repository.save(options.item);
      return {};
    } catch (error) {
      throw error;
    }
  }
  async delete(options: { id: number }) {
    try {
      await this.repository.delete(options.id);
      return {};
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      const item = await this.repository.findOneOrFail(options.id);
      return { unit: item };
    } catch (error) {
      throw error;
    }
  }
  async createIfNoExist(options: { name: string }) {
    try {
      const qb = await this.repository
        .createQueryBuilder('unit')
        .where('UPPER(unit.name) = UPPER(:name)', { name: options.name })
        .getOne();
      if (!qb) {
        // create
        const item = await this.repository.save({
          name: options.name,
          cycle_started_day: 1
        });
        return (await this.findById({ id: item.id })).unit;
      } else return qb;
    } catch (error) {
      throw error;
    }
  }
  async findAll(options: {
    curPage: number;
    perPage: number;
    q?: string;
    sort?: string;
  }) {
    try {
      let objects: [BusinessUnit[], number];
      let qb = this.repository.createQueryBuilder('unit');
      if (options.q) {
        qb = qb.where('unit.name like :q', {
          q: `%${options.q}%`
        });
      }
      options.sort =
        options.sort &&
        new BusinessUnit().hasOwnProperty(options.sort.replace('-', ''))
          ? options.sort
          : '-id';
      const field = options.sort.replace('-', '');
      if (options.sort) {
        if (options.sort[0] === '-') {
          qb = qb.orderBy('unit.' + field, 'DESC');
        } else {
          qb = qb.orderBy('unit.' + field, 'ASC');
        }
      }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        list: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }
  
  async dashboard(id?) {
    
    try {

      let qb = this.repository.createQueryBuilder('bisnis');
      qb = qb.select('bisnis.id,bisnis.name, count(project.id)', 'projects');
      qb = qb.leftJoin('bisnis.project', 'project');
      if (id) {
        qb.where(`bisnis.id = ${id}`);
      }
      qb.groupBy('bisnis.id');
      return await qb.getRawMany();

    } catch (error) {
      throw error;
    }
  }
  
  async dashboardById(id) {
    try {
      let qb = this.repository.createQueryBuilder('bisnis');
      qb = qb.select('bisnis.id,bisnis.name, count(project.id)', 'projects');
      qb = qb.leftJoin('bisnis.project', 'project');
      qb.where(`bisnis.id = ${id}`);
      qb.groupBy('bisnis.id');
      return await qb.getRawOne();
    } catch (error) {
      throw error;
    }
  }
  
}
