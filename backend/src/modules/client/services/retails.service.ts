import {
  Inject,
  Injectable,
  MethodNotAllowedException,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CORE_CONFIG_TOKEN, ICoreConfig } from '@omgstore/core-nestjs';
import { Retail } from '../entities/retail.entity';

@Injectable()
export class RetailsService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(Retail) private readonly repository: Repository<Retail>
  ) {}
  async create(options: { item: Retail }) {
    try {
      options.item = await this.repository.save(options.item);
      const { retail } = await this.findById({ id: options.item.id });
      return { retail };
    } catch (error) {
      throw error;
    }
  }
  async update(options: { id: number; item: Retail }) {
    options.item.id = options.id;
    try {
      options.item = await this.repository.save(options.item);
      const { retail } = await this.findById({ id: options.item.id });
      return { retail };
    } catch (error) {
      throw error;
    }
  }
  async delete(options: { id: number }) {
    try {
      let object = await this.repository.findOneOrFail(options.id);
      object = await this.repository.save(object);
      await this.repository.delete(options.id);
      return { retail: null };
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      const item = await this.repository.findOneOrFail(options.id);
      return { retail: item };
    } catch (error) {
      throw error;
    }
  }
  async isCodeExist(options: { code: string }) {
    try {
      const qb = this.repository
        .createQueryBuilder('retail')
        .where('UPPER(retail.code) = UPPER(:code)', { code: options.code });
      return await qb.getOne();
    } catch (error) {
      throw error;
    }
  }
  async createIfNoExist(options: { code: string }) {
    try {
      const qb = await this.repository
        .createQueryBuilder('retail')
        .where('UPPER(retail.code) = UPPER(:code)', { code: options.code })
        .getOne();
      if (!qb) {
        // create
        const item = await this.repository.save({
          code: options.code,
          name: options.code
        });
        return (await this.findById({ id: item.id })).retail;
      } else return qb;
    } catch (error) {
      throw error;
    }
  }
  async findAll(options: {
    curPage: number;
    perPage: number;
    q?: string;
    sort?: string;
    unit?: number;
    retailCategory?: number;
  }) {
    try {
      let objects: [Retail[], number];
      let qb = this.repository.createQueryBuilder('retail');
      qb = qb.leftJoinAndSelect('retail.retailCategory', 'retailCategory');
      qb = qb.leftJoinAndSelect('retail.businessUnit', 'businessUnit');
      if (options.q) {
        qb = qb.where('retail.name like :q or retail.code like :q', {
          q: `%${options.q}%`
        });
      }
      if (options.retailCategory) {
        qb = qb.andWhere('retailCategory.id = :retailCategory', {
          retailCategory: options.retailCategory
        });
      }
      if (options.unit) {
        qb = qb.andWhere('businessUnit.id = :unit', {
          unit: options.unit
        });
      }
      options.sort =
        options.sort &&
        new Retail().hasOwnProperty(options.sort.replace('-', ''))
          ? options.sort
          : '-id';
      const field = options.sort.replace('-', '');
      if (options.sort) {
        if (options.sort[0] === '-') {
          qb = qb.orderBy('retail.' + field, 'DESC');
        } else {
          qb = qb.orderBy('retail.' + field, 'ASC');
        }
      }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        list: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }
}
