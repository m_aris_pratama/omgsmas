import {
  Inject,
  Injectable,
  MethodNotAllowedException,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CORE_CONFIG_TOKEN, ICoreConfig } from '@omgstore/core-nestjs';
import { Store } from '../entities/store.entity';
import { Team } from '../entities/team.entity';

@Injectable()
export class StoresService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(Store) private readonly repository: Repository<Store>
  ) {}
  async create(options: { item: Store }) {
    try {
      options.item = await this.repository.save(options.item);
      return {};
    } catch (error) {
      throw error;
    }
  }
  async update(options: { id: number; item: Store }) {
    options.item.id = options.id;
    try {
      options.item = await this.repository.save(options.item);
      return {};
    } catch (error) {
      throw error;
    }
  }
  async delete(options: { id: number }) {
    try {
      await this.repository.delete(options.id);
      return {};
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      const item = await this.repository.findOneOrFail(options.id);
      return { store: item };
    } catch (error) {
      throw error;
    }
  }
  async isCodeExist(options: { code: string }) {
    try {
      const qb = this.repository
        .createQueryBuilder('store')
        .where('UPPER(store.code) = UPPER(:code)', { code: options.code });
      return await qb.getOne();
    } catch (error) {
      throw error;
    }
  }
  async findByLeadStore(options: { userId: number }) {
    try {
      let qb = this.repository.createQueryBuilder('store');
      qb = qb.leftJoinAndSelect('store.member', 'member');
      qb = qb.leftJoinAndSelect('store.lead', 'lead');
      qb = qb.leftJoinAndSelect('lead.user', 'user');
      qb = qb.where('user.id = :id', {
        id: options.userId
      });
      return await qb.getMany();
    } catch (error) {
      throw error;
    }
  }
  async findByMemberStore(options: { userId: number }) {
    try {
      let qb = this.repository.createQueryBuilder('store');
      qb = qb.leftJoinAndSelect('store.member', 'member');
      qb = qb.leftJoinAndSelect('store.lead', 'lead');
      qb = qb.leftJoinAndSelect('member.user', 'user');
      qb = qb.where('user.id = :id', {
        id: options.userId
      });
      return await qb.getMany();
    } catch (error) {
      throw error;
    }
  }

  async findByBuUnit(options: { id: number }) {
    try {
      let qb = this.repository.createQueryBuilder('store');
      qb = qb.leftJoinAndSelect('store.retail', 'retail');
      qb = qb.leftJoinAndSelect('retail.businessUnit', 'units');
      qb = qb.where('units.id = :id', {
        id: options.id
      });
      return await qb.getMany();
    } catch (error) {
      throw error;
    }
  }

  async findAll(options: {
    curPage: number;
    perPage: number;
    q?: string;
    sort?: string;
    retailId?: number;
    areaId?: number;
    cityId?: number;
  }) {
    try {
      let objects: [Store[], number];
      let qb = this.repository.createQueryBuilder('store');
      qb = qb.leftJoinAndSelect('store.retail', 'retail');
      qb = qb.leftJoinAndSelect('store.city', 'city');
      qb = qb.leftJoinAndSelect('city.area', 'area');
      qb = qb.leftJoinAndSelect('store.lead', 'lead');
      qb = qb.leftJoinAndSelect('store.member', 'member');
      if (options.q) {
        // @TODO BACA AING status = inactive
        qb = qb.where(
          'store.name like :q or store.code like :q or store.address like :q and store.is_active = true',
          {
            q: `%${options.q}%`
          }
        );
      }
      if (options.retailId) {
        qb = qb.andWhere('retail.id = :retailId', {
          retailId: options.retailId
        });
      }
      if (options.cityId) {
        qb = qb.andWhere('city.id = :cityId', {
          cityId: options.cityId
        });
      }
      if (options.areaId) {
        qb = qb.andWhere('area.id = :areaId', {
          areaId: options.areaId
        });
      }
      options.sort =
        options.sort &&
        new Store().hasOwnProperty(options.sort.replace('-', ''))
          ? options.sort
          : '-id';
      const field = options.sort.replace('-', '');
      if (options.sort) {
        if (options.sort[0] === '-') {
          qb = qb.orderBy('store.' + field, 'DESC');
        } else {
          qb = qb.orderBy('store.' + field, 'ASC');
        }
      }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        list: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }
}
