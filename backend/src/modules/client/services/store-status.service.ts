import {
  Inject,
  Injectable,
  MethodNotAllowedException,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CORE_CONFIG_TOKEN, ICoreConfig } from '@omgstore/core-nestjs';
import { StoreStatus } from '../entities/store-status.entity';
import moment = require('moment');

@Injectable()
export class StoreStatussService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(StoreStatus)
    private readonly repository: Repository<StoreStatus>
  ) {}
  async create(options: { item: StoreStatus }) {
    try {
      options.item = await this.repository.save(options.item);
      const { storeStatus } = await this.findById({ id: options.item.id });
      return { storeStatus };
    } catch (error) {
      throw error;
    }
  }
  async update(options: { id: number; item: StoreStatus }) {
    options.item.id = options.id;
    try {
      options.item = await this.repository.save(options.item);
      const { storeStatus } = await this.findById({ id: options.item.id });
      return { storeStatus };
    } catch (error) {
      throw error;
    }
  }
  async delete(options: { id: number }) {
    try {
      let object = await this.repository.findOneOrFail(options.id);
      object = await this.repository.save(object);
      await this.repository.delete(options.id);
      return { storeStatus: null };
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      const item = await this.repository.findOneOrFail(options.id);
      return { storeStatus: item };
    } catch (error) {
      throw error;
    }
  }
  async findAll(options: {
    curPage: number;
    perPage: number;
    q?: string;
    sort?: string;
    dateFrom?: string;
    dateTo?: string;
  }) {
    try {
      let objects: [StoreStatus[], number];
      let qb = this.repository.createQueryBuilder('storeStatus');
      qb = qb.leftJoinAndSelect('storeStatus.store', 'store');
      if (options.dateFrom && options.dateTo) {
        qb = qb.where(
          'storeStatus.created_at >= :dateFrom AND storeStatus.created_at < :dateTo',
          {
            dateFrom: options.dateFrom + ' 00:00:00',
            dateTo: options.dateTo + ' 23:59:59'
          }
        );
      }
      if (options.q) {
        qb = qb.where('storeStatus.name like :q', {
          q: `%${options.q}%`
        });
      }
      options.sort =
        options.sort &&
        new StoreStatus().hasOwnProperty(options.sort.replace('-', ''))
          ? options.sort
          : '-id';
      const field = options.sort.replace('-', '');
      if (options.sort) {
        if (options.sort[0] === '-') {
          qb = qb.orderBy('storeStatus.' + field, 'DESC');
        } else {
          qb = qb.orderBy('storeStatus.' + field, 'ASC');
        }
      }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        list: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }
}
