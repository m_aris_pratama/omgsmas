import {
  Inject,
  Injectable,
  MethodNotAllowedException,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CORE_CONFIG_TOKEN, ICoreConfig } from '@omgstore/core-nestjs';
import { BrandSubcategory } from '../entities/brand-subcategory.entity';
import { BrandCategory } from '../entities/brand-category.entity';

@Injectable()
export class BrandSubcategoriesService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(BrandSubcategory)
    private readonly repository: Repository<BrandSubcategory>
  ) {}
  async create(options: { item: BrandSubcategory }) {
    try {
      options.item = await this.repository.save(options.item);
      const { brandSubcategory } = await this.findById({ id: options.item.id });
      return { brandSubcategory };
    } catch (error) {
      throw error;
    }
  }
  async update(options: { id: number; item: BrandSubcategory }) {
    options.item.id = options.id;
    try {
      options.item = await this.repository.save(options.item);
      const { brandSubcategory } = await this.findById({ id: options.item.id });
      return { brandSubcategory };
    } catch (error) {
      throw error;
    }
  }
  async delete(options: { id: number }) {
    try {
      let object = await this.repository.findOneOrFail(options.id);
      object = await this.repository.save(object);
      await this.repository.delete(options.id);
      return { brandSubcategory: null };
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      const item = await this.repository.findOneOrFail(options.id);
      return { brandSubcategory: item };
    } catch (error) {
      throw error;
    }
  }
  async isNameExist(options: { name: string }) {
    try {
      const qb = this.repository
        .createQueryBuilder('brandSubcategory')
        .where('UPPER(brandSubcategory.name) = UPPER(:name)', {
          name: options.name
        });
      return await qb.getOne();
    } catch (error) {
      throw error;
    }
  }
  async createIfNoExist(options: { cat: any; name: string }) {
    try {
      const qb = await this.repository
        .createQueryBuilder('brandSubcategory')
        .where('UPPER(brandSubcategory.name) = UPPER(:name)', {
          name: options.name
        })
        .getOne();
      if (!qb) {
        // create
        const item = await this.repository.save({
          name: options.name,
          brandCategory: options.cat
        });
        return (await this.findById({ id: item.id })).brandSubcategory;
      } else return qb;
    } catch (error) {
      throw error;
    }
  }

  async findAll(options: {
    curPage: number;
    perPage: number;
    q?: string;
    sort?: string;
    type_id?: number;
  }) {
    try {
      let objects: [BrandSubcategory[], number];
      let qb = this.repository.createQueryBuilder('brandSubcategory');
      qb = qb.leftJoinAndSelect(
        'brandSubcategory.brandCategory',
        'brandCategory'
      );
      if (options.q) {
        qb = qb.where('brandSubcategory.name like :q', {
          q: `%${options.q}%`
        });
      }
      if (options.type_id) {
        qb = qb.andWhere('brandCategory.id = :typeId', {
          typeId: options.type_id
        });
      }
      options.sort =
        options.sort &&
        new BrandSubcategory().hasOwnProperty(options.sort.replace('-', ''))
          ? options.sort
          : '-id';
      const field = options.sort.replace('-', '');
      if (options.sort) {
        if (options.sort[0] === '-') {
          qb = qb.orderBy('brandSubcategory.' + field, 'DESC');
        } else {
          qb = qb.orderBy('brandSubcategory.' + field, 'ASC');
        }
      }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        list: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }
}
