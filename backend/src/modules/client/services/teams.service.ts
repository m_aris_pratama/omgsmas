import {
  Inject,
  Injectable,
  MethodNotAllowedException,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CORE_CONFIG_TOKEN, ICoreConfig } from '@omgstore/core-nestjs';
import { Team } from '../entities/team.entity';

@Injectable()
export class TeamsService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(Team) private readonly repository: Repository<Team>
  ) {}
  async create(options: { item: Team }) {
    try {
      options.item = await this.repository.save(options.item);
      return {};
    } catch (error) {
      throw error;
    }
  }
  async update(options: { id: number; item: Team }) {
    options.item.id = options.id;
    try {
      options.item = await this.repository.save(options.item);
      return {};
    } catch (error) {
      throw error;
    }
  }
  async delete(options: { id: number }) {
    try {
      await this.repository.delete(options.id);
      return {};
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      let qb = this.repository.createQueryBuilder('team');
      qb = qb.leftJoinAndSelect('team.user', 'user');
      qb = qb.where('team.id = :id', {
        id: options.id
      });
      return { team: await qb.getOne() };
    } catch (error) {
      throw error;
    }
  }
  async isNameExist(options: { name: string }) {
    try {
      const qb = await this.repository
        .createQueryBuilder('team')
        .where('UPPER(team.name) = UPPER(:name)', { name: options.name })
        .getOne();
      return qb;
    } catch (error) {
      throw error;
    }
  }
  async createIfNoExist(options: { position: string; name: string }) {
    try {
      const qb = await this.repository
        .createQueryBuilder('team')
        .where('UPPER(team.name) = UPPER(:name)', { name: options.name })
        .getOne();
      if (!qb) {
        // create
        const item = await this.repository.save({
          position: options.position,
          name: options.name
        });
        return (await this.findById({ id: item.id })).team;
      } else return qb;
    } catch (error) {
      throw error;
    }
  }
  async findByUserId(options: { id: number }) {
    try {
      let qb = this.repository.createQueryBuilder('team');
      qb = qb.leftJoinAndSelect('team.user', 'user');
      qb = qb.where('user.id = :id', {
        id: options.id
      });

      return await qb.getOne();
    } catch (error) {
      throw error;
    }
  }
  async findAll(options: {
    curPage: number;
    perPage: number;
    q?: string;
    sort?: string;
  }) {
    try {
      let objects: [Team[], number];
      let qb = this.repository.createQueryBuilder('team');
      qb = qb.leftJoinAndSelect('team.user', 'user');
      if (options.q) {
        qb = qb.where('team.name like :q', {
          q: `%${options.q}%`
        });
      }
      options.sort =
        options.sort && new Team().hasOwnProperty(options.sort.replace('-', ''))
          ? options.sort
          : '-id';
      const field = options.sort.replace('-', '');
      if (options.sort) {
        if (options.sort[0] === '-') {
          qb = qb.orderBy('team.' + field, 'DESC');
        } else {
          qb = qb.orderBy('team.' + field, 'ASC');
        }
      }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        list: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }
}
