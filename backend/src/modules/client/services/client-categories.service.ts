import {
  Inject,
  Injectable,
  MethodNotAllowedException,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CORE_CONFIG_TOKEN, ICoreConfig } from '@omgstore/core-nestjs';
import { ClientCategory } from '../entities/client-category.entity';

@Injectable()
export class ClientCategoriesService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(ClientCategory)
    private readonly repository: Repository<ClientCategory>
  ) {}
  async create(options: { item: ClientCategory }) {
    try {
      options.item = await this.repository.save(options.item);
      const { clientCategory } = await this.findById({ id: options.item.id });
      return { clientCategory };
    } catch (error) {
      throw error;
    }
  }
  async update(options: { id: number; item: ClientCategory }) {
    options.item.id = options.id;
    try {
      options.item = await this.repository.save(options.item);
      const { clientCategory } = await this.findById({ id: options.item.id });
      return { clientCategory };
    } catch (error) {
      throw error;
    }
  }
  async delete(options: { id: number }) {
    try {
      let object = await this.repository.findOneOrFail(options.id);
      object = await this.repository.save(object);
      await this.repository.delete(options.id);
      return { clientCategory: null };
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      const item = await this.repository.findOneOrFail(options.id);
      return { clientCategory: item };
    } catch (error) {
      throw error;
    }
  }
  async findAll(options: {
    curPage: number;
    perPage: number;
    q?: string;
    sort?: string;
  }) {
    try {
      let objects: [ClientCategory[], number];
      let qb = this.repository.createQueryBuilder('clientCategory');
      if (options.q) {
        qb = qb.where(
          'clientCategory.name like :q or clientCategory.code like :q or clientCategory.address like :q or clientCategory.id = :id',
          {
            q: `%${options.q}%`,
            id: +options.q
          }
        );
      }
      options.sort =
        options.sort &&
        new ClientCategory().hasOwnProperty(options.sort.replace('-', ''))
          ? options.sort
          : '-id';
      const field = options.sort.replace('-', '');
      if (options.sort) {
        if (options.sort[0] === '-') {
          qb = qb.orderBy('clientCategory.' + field, 'DESC');
        } else {
          qb = qb.orderBy('clientCategory.' + field, 'ASC');
        }
      }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        clientCategories: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }
}
