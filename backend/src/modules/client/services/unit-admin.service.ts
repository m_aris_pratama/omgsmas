import {
  Inject,
  Injectable,
  MethodNotAllowedException,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CORE_CONFIG_TOKEN, ICoreConfig } from '@omgstore/core-nestjs';
import { UnitAdmin } from '../entities/unit-admin.entity';

@Injectable()
export class UnitAdminsService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(UnitAdmin)
    private readonly repository: Repository<UnitAdmin>
  ) {}
  async create(options: { item: UnitAdmin }) {
    try {
      options.item = await this.repository.save(options.item);
      return {};
    } catch (error) {
      throw error;
    }
  }
  async update(options: { id: number; item: UnitAdmin }) {
    options.item.id = options.id;
    try {
      options.item = await this.repository.save(options.item);
      return {};
    } catch (error) {
      throw error;
    }
  }
  async delete(options: { id: number }) {
    try {
      await this.repository.delete(options.id);
      return {};
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      let qb = this.repository.createQueryBuilder('unitAdmin');
      qb = qb.leftJoinAndSelect('unitAdmin.user', 'user');
      qb = qb.where('unitAdmin.id = :id', {
        id: options.id
      });
      return { unitAdmin: await qb.getOne() };
    } catch (error) {
      throw error;
    }
  }
  async isNameExist(options: { name: string }) {
    try {
      const qb = await this.repository
        .createQueryBuilder('unitAdmin')
        .where('UPPER(unitAdmin.name) = UPPER(:name)', { name: options.name })
        .getOne();
      return qb;
    } catch (error) {
      throw error;
    }
  }
  async createIfNoExist(options: { position: string; name: string }) {
    try {
      const qb = await this.repository
        .createQueryBuilder('unitAdmin')
        .where('UPPER(unitAdmin.name) = UPPER(:name)', { name: options.name })
        .getOne();
      if (!qb) {
        // create
        const item = await this.repository.save({
          position: options.position,
          name: options.name
        });
        return (await this.findById({ id: item.id })).unitAdmin;
      } else return qb;
    } catch (error) {
      throw error;
    }
  }
  async findByUserId(options: { id: number }) {
    try {
      let qb = this.repository.createQueryBuilder('unitAdmin');
      qb = qb.leftJoinAndSelect('unitAdmin.user', 'user');
      qb = qb.leftJoinAndSelect('unitAdmin.businessUnit', 'businessUnit');
      qb = qb.where('user.id = :id', {
        id: options.id
      });

      return await qb.getOne();
    } catch (error) {
      throw error;
    }
  }
  async findAll(options: {
    curPage: number;
    perPage: number;
    q?: string;
    sort?: string;
    unit?: number;
  }) {
    try {
      let objects: [UnitAdmin[], number];
      let qb = this.repository.createQueryBuilder('unitAdmin');
      qb = qb.leftJoinAndSelect('unitAdmin.user', 'user');
      qb = qb.leftJoinAndSelect('unitAdmin.businessUnit', 'businessUnit');
      if (options.q) {
        qb = qb.where('unitAdmin.name like :q', {
          q: `%${options.q}%`
        });
      }
      if (options.unit) {
        qb = qb.andWhere('businessUnit.id = ' + options.unit);
      }
      options.sort =
        options.sort &&
        new UnitAdmin().hasOwnProperty(options.sort.replace('-', ''))
          ? options.sort
          : '-id';
      const field = options.sort.replace('-', '');
      if (options.sort) {
        if (options.sort[0] === '-') {
          qb = qb.orderBy('unitAdmin.' + field, 'DESC');
        } else {
          qb = qb.orderBy('unitAdmin.' + field, 'ASC');
        }
      }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        list: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }
}
