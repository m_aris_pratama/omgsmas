import {
  Inject,
  Injectable,
  MethodNotAllowedException,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CORE_CONFIG_TOKEN, ICoreConfig } from '@omgstore/core-nestjs';
import { Brand } from '../entities/brand.entity';

@Injectable()
export class BrandsService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(Brand) private readonly repository: Repository<Brand>
  ) {}
  async create(options: { item: Brand }) {
    try {
      options.item = await this.repository.save(options.item);
      const { brand } = await this.findById({ id: options.item.id });
      return { brand };
    } catch (error) {
      throw error;
    }
  }
  async update(options: { id: number; item: Brand }) {
    options.item.id = options.id;
    try {
      options.item = await this.repository.save(options.item);
      const { brand } = await this.findById({ id: options.item.id });
      return { brand };
    } catch (error) {
      throw error;
    }
  }
  async delete(options: { id: number }) {
    try {
      let object = await this.repository.findOneOrFail(options.id);
      object = await this.repository.save(object);
      await this.repository.delete(options.id);
      return { brand: null };
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      const item = await this.repository.findOneOrFail(options.id);
      return { brand: item };
    } catch (error) {
      throw error;
    }
  }
  async isNameExist(options: { name: string }) {
    try {
      const qb = this.repository
        .createQueryBuilder('brand')
        .where('UPPER(brand.name) = UPPER(:name)', { name: options.name });
      return await qb.getOne();
    } catch (error) {
      throw error;
    }
  }
  async findAll(options: {
    curPage: number;
    perPage: number;
    q?: string;
    sort?: string;
    client_id?: number;
    type_id?: number;
    subtype_id?: number;
  }) {
    try {
      let objects: [Brand[], number];
      let qb = this.repository.createQueryBuilder('brand');
      qb = qb.leftJoinAndSelect('brand.client', 'client');
      qb = qb.leftJoinAndSelect('brand.brandSubcategory', 'brandSubcategory');
      qb = qb.leftJoinAndSelect(
        'brandSubcategory.brandCategory',
        'brandCategory'
      );
      if (options.q) {
        qb = qb.where('brand.name like :q', {
          q: `%${options.q}%`
        });
      }
      if (options.client_id) {
        qb = qb.andWhere('client.id = :clientId', {
          clientId: options.client_id
        });
      }
      if (options.type_id) {
        qb = qb.andWhere('brandCategory.id = :typeId', {
          typeId: options.type_id
        });
      }
      if (options.subtype_id) {
        qb = qb.andWhere('brandSubcategory.id = :subtypeId', {
          subtypeId: options.subtype_id
        });
      }
      options.sort =
        options.sort &&
        new Brand().hasOwnProperty(options.sort.replace('-', ''))
          ? options.sort
          : '-id';
      const field = options.sort.replace('-', '');
      if (options.sort) {
        if (options.sort[0] === '-') {
          qb = qb.orderBy('brand.' + field, 'DESC');
        } else {
          qb = qb.orderBy('brand.' + field, 'ASC');
        }
      }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        list: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }
}
