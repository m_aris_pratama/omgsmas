import {
  Inject,
  Injectable,
  MethodNotAllowedException,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CORE_CONFIG_TOKEN, ICoreConfig } from '@omgstore/core-nestjs';
import { Client } from '../entities/client.entity';

@Injectable()
export class ClientsService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(Client) private readonly repository: Repository<Client>
  ) {}
  async create(options: { item: Client }) {
    try {
      options.item = await this.repository.save(options.item);
      const { client } = await this.findById({ id: options.item.id });
      return { client };
    } catch (error) {
      throw error;
    }
  }
  async update(options: { id: number; item: Client }) {
    options.item.id = options.id;
    try {
      options.item = await this.repository.save(options.item);
      const { client } = await this.findById({ id: options.item.id });
      return { client };
    } catch (error) {
      throw error;
    }
  }
  async delete(options: { id: number }) {
    try {
      let object = await this.repository.findOneOrFail(options.id);
      object = await this.repository.save(object);
      await this.repository.delete(options.id);
      return { client: null };
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      const item = await this.repository.findOneOrFail(options.id);
      return { client: item };
    } catch (error) {
      throw error;
    }
  }
  async isNameExist(options: { name: string }) {
    try {
      const qb = this.repository
        .createQueryBuilder('client')
        .where('UPPER(client.name) = UPPER(:name)', { name: options.name });
      return await qb.getOne();
    } catch (error) {
      throw error;
    }
  }
  async createIfNoExist(options: { name: string }) {
    try {
      const qb = await this.repository
        .createQueryBuilder('client')
        .where('UPPER(client.name) = UPPER(:name)', { name: options.name })
        .getOne();
      if (!qb) {
        // create
        const item = await this.repository.save({ name: options.name });
        return (await this.findById({ id: item.id })).client;
      } else return qb;
    } catch (error) {
      throw error;
    }
  }
  async findAll(options: {
    curPage: number;
    perPage: number;
    q?: string;
    sort?: string;
  }) {
    try {
      let objects: [Client[], number];
      let qb = this.repository.createQueryBuilder('client');
      if (options.q) {
        qb = qb.where('client.name like :q', {
          q: `%${options.q}%`
        });
      }
      options.sort =
        options.sort &&
        new Client().hasOwnProperty(options.sort.replace('-', ''))
          ? options.sort
          : '-id';
      const field = options.sort.replace('-', '');
      if (options.sort) {
        if (options.sort[0] === '-') {
          qb = qb.orderBy('client.' + field, 'DESC');
        } else {
          qb = qb.orderBy('client.' + field, 'ASC');
        }
      }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        list: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }
}
