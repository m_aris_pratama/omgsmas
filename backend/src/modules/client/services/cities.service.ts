import {
  Inject,
  Injectable,
  MethodNotAllowedException,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CORE_CONFIG_TOKEN, ICoreConfig } from '@omgstore/core-nestjs';
import { City } from '../entities/city.entity';

@Injectable()
export class CitiesService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(City) private readonly repository: Repository<City>
  ) {}
  async create(options: { item: City }) {
    try {
      options.item = await this.repository.save(options.item);
      const { city } = await this.findById({ id: options.item.id });
      return { city };
    } catch (error) {
      throw error;
    }
  }
  async update(options: { id: number; item: City }) {
    options.item.id = options.id;
    try {
      options.item = await this.repository.save(options.item);
      const { city } = await this.findById({ id: options.item.id });
      return { city };
    } catch (error) {
      throw error;
    }
  }
  async delete(options: { id: number }) {
    try {
      let object = await this.repository.findOneOrFail(options.id);
      object = await this.repository.save(object);
      await this.repository.delete(options.id);
      return { city: null };
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      const item = await this.repository.findOneOrFail(options.id);
      return { city: item };
    } catch (error) {
      throw error;
    }
  }
  async isNameExist(options: { name: string }) {
    try {
      const qb = await this.repository
        .createQueryBuilder('city')
        .where('UPPER(city.name) = UPPER(:name)', { name: options.name })
        .getOne();
      return qb;
    } catch (error) {
      throw error;
    }
  }
  async createIfNoExist(options: { area: any; name: string }) {
    try {
      const qb = await this.repository
        .createQueryBuilder('city')
        .where('UPPER(city.name) = UPPER(:name)', { name: options.name })
        .getOne();
      if (!qb) {
        // create
        const item = await this.repository.save({
          name: options.name,
          area: options.area
        });
        return (await this.findById({ id: item.id })).city;
      } else return qb;
    } catch (error) {
      throw error;
    }
  }
  async findAll(options: {
    curPage: number;
    perPage: number;
    q?: string;
    sort?: string;
    areaId?: number;
  }) {
    try {
      let objects: [City[], number];
      let qb = this.repository.createQueryBuilder('city');
      qb = qb.leftJoinAndSelect('city.area', 'area');
      if (options.q) {
        qb = qb.where('city.name like :q', {
          q: `%${options.q}%`
        });
      }
      if (options.areaId) {
        qb = qb.andWhere('area.id = :areaId', {
          areaId: options.areaId
        });
      }
      options.sort =
        options.sort && new City().hasOwnProperty(options.sort.replace('-', ''))
          ? options.sort
          : '-id';
      const field = options.sort.replace('-', '');
      if (options.sort) {
        if (options.sort[0] === '-') {
          qb = qb.orderBy('city.' + field, 'DESC');
        } else {
          qb = qb.orderBy('city.' + field, 'ASC');
        }
      }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        list: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }
}
