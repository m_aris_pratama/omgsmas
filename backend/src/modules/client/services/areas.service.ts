import {
  Inject,
  Injectable,
  MethodNotAllowedException,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CORE_CONFIG_TOKEN, ICoreConfig } from '@omgstore/core-nestjs';
import { Area } from '../entities/area.entity';

@Injectable()
export class AreasService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(Area) private readonly repository: Repository<Area>
  ) {}
  async create(options: { item: Area }) {
    try {
      options.item = await this.repository.save(options.item);
      const { area } = await this.findById({ id: options.item.id });
      return { area };
    } catch (error) {
      throw error;
    }
  }
  async update(options: { id: number; item: Area }) {
    options.item.id = options.id;
    try {
      options.item = await this.repository.save(options.item);
      const { area } = await this.findById({ id: options.item.id });
      return { area };
    } catch (error) {
      throw error;
    }
  }
  async delete(options: { id: number }) {
    try {
      let object = await this.repository.findOneOrFail(options.id);
      object = await this.repository.save(object);
      await this.repository.delete(options.id);
      return { area: null };
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      const item = await this.repository.findOneOrFail(options.id);
      return { area: item };
    } catch (error) {
      throw error;
    }
  }
  async createIfNoExist(options: { name: string }) {
    try {
      const qb = await this.repository
        .createQueryBuilder('area')
        .where('UPPER(area.name) = UPPER(:name)', { name: options.name })
        .getOne();
      if (!qb) {
        // create
        const item = await this.repository.save({ name: options.name });
        return (await this.findById({ id: item.id })).area;
      } else return qb;
    } catch (error) {
      throw error;
    }
  }
  async findAll(options: {
    curPage: number;
    perPage: number;
    q?: string;
    sort?: string;
  }) {
    try {
      let objects: [Area[], number];
      let qb = this.repository.createQueryBuilder('area');
      if (options.q) {
        qb = qb.where('area.name like :q', {
          q: `%${options.q}%`
        });
      }
      options.sort =
        options.sort && new Area().hasOwnProperty(options.sort.replace('-', ''))
          ? options.sort
          : '-id';
      const field = options.sort.replace('-', '');
      if (options.sort) {
        if (options.sort[0] === '-') {
          qb = qb.orderBy('area.' + field, 'DESC');
        } else {
          qb = qb.orderBy('area.' + field, 'ASC');
        }
      }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        list: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }
}
