import { ClientsService } from '../services/clients.service';
import { RetailsService } from '../services/retails.service';
import { BrandsService } from '../services/brands.service';
import { ClientCategoriesService } from '../services/client-categories.service';
import { RetailCategoriesService } from '../services/retail-categories.service';
import { BrandCategoriesService } from '../services/brand-categories.service';
import { BrandSubcategoriesService } from '../services/brand-subcategories.service';
import { AreasService } from '../services/areas.service';
import { StoresService } from '../services/stores.service';
import { CitiesService } from './cities.service';
import { TeamsService } from './teams.service';
import { UnitsService } from './units.service';
import { StoreStatussService } from './store-status.service';
import { UnitAdminsService } from './unit-admin.service';

export const services = [
  ClientsService,
  RetailsService,
  BrandsService,
  ClientCategoriesService,
  RetailCategoriesService,
  BrandCategoriesService,
  BrandSubcategoriesService,
  AreasService,
  CitiesService,
  StoresService,
  TeamsService,
  UnitsService,
  StoreStatussService,
  UnitAdminsService
];
