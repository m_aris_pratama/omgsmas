import { Brand } from './brand.entity';
import { Client } from './client.entity';
import { Retail } from './retail.entity';
import { Store } from './store.entity';
import { BrandCategory } from './brand-category.entity';
import { BrandSubcategory } from './brand-subcategory.entity';
import { ClientCategory } from './client-category.entity';
import { RetailCategory } from './retail-category.entity';
import { Area } from './area.entity';
import { City } from './city.entity';
import { Team } from './team.entity';
import { BusinessUnit } from './business-unit.entity';
import { StoreStatus } from './store-status.entity';
import { UnitAdmin } from './unit-admin.entity';

export const entities = [
  Brand,
  BrandCategory,
  BrandSubcategory,
  Client,
  Retail,
  Store,
  ClientCategory,
  RetailCategory,
  Area,
  City,
  Team,
  BusinessUnit,
  StoreStatus,
  UnitAdmin
];
