import { CustomValidationError, User } from '@omgstore/core-nestjs';
import {
  IsNotEmpty,
  IsOptional,
  MaxLength,
  validateSync
} from 'class-validator';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn
} from 'typeorm';

import { ClientCategory } from './client-category.entity';

/* 
id : int
avatar : varchar(255)
registered : date
client_category_id : int
name : varchar(255)
npwp : varchar(50)
address : varchar(512)
telp : varchar(25)
fax : varchar(25)
email : varchar(100)
contact_project : varchar (255)
ontact_project_telp : varchar (25)
contact_project_position : varchar(100)
contact_project_email : varchar(100)
contact_project_address : varchar(512)
contact_billing : varchar(255)
contact_billing_telp : varchar(25)
contact_billing_email : varchar(100)
contact_billing_position : varchar(100)
contact_billing_address : varchar(512)
is_active : bool
user_id : int
created_at : date
updated_at : date
*/

@Entity({ name: 'client' })
export class Client {
  @PrimaryGeneratedColumn()
  id: number = undefined;

  @Column({ length: 1024 })
  @MaxLength(1024)
  avatar: string = '';

  @Column({ name: 'registered' })
  registered: Date = undefined;

  @ManyToOne(type => ClientCategory, { nullable: true })
  @JoinColumn({ name: 'client_category_id' })
  clientCategory: ClientCategory = undefined;

  @Column({ length: 255 })
  @IsNotEmpty()
  @MaxLength(255)
  name: string = undefined;

  @Column({ length: 50 })
  @MaxLength(50)
  @IsOptional()
  npwp: string = undefined;

  @Column({ length: 512 })
  @MaxLength(512)
  @IsOptional()
  address: string = undefined;

  @Column({ length: 25 })
  @MaxLength(25)
  @IsOptional()
  telp: string = undefined;

  @Column({ length: 25 })
  @MaxLength(25)
  @IsOptional()
  fax: string = undefined;

  @Column({ length: 100 })
  @MaxLength(100)
  @IsOptional()
  email: string = undefined;

  @Column({ length: 255 })
  @MaxLength(255)
  @IsOptional()
  contact_project: string = undefined;

  @Column({ length: 25 })
  @MaxLength(25)
  @IsOptional()
  contact_project_telp: string = undefined;

  @Column({ length: 100 })
  @MaxLength(100)
  @IsOptional()
  contact_project_position: string = undefined;

  @Column({ length: 100 })
  @MaxLength(100)
  @IsOptional()
  contact_project_email: string = undefined;

  @Column({ length: 512 })
  @MaxLength(512)
  @IsOptional()
  contact_project_address: string = undefined;

  @Column({ length: 255 })
  @MaxLength(255)
  @IsOptional()
  contact_billing: string = undefined;

  @Column({ length: 25 })
  @MaxLength(25)
  @IsOptional()
  contact_billing_telp: string = undefined;

  @Column({ length: 100 })
  @MaxLength(100)
  @IsOptional()
  contact_billing_position: string = undefined;

  @Column({ length: 100 })
  @MaxLength(100)
  @IsOptional()
  contact_billing_email: string = undefined;

  @Column({ length: 512 })
  @MaxLength(512)
  @IsOptional()
  contact_billing_address: string = undefined;

  @Column({ default: false })
  is_active: boolean;

  @ManyToOne(type => User, { nullable: true })
  @JoinColumn({ name: 'user_id' })
  user: User = undefined;

  @CreateDateColumn({ name: 'created_at' })
  created: Date = undefined;

  @UpdateDateColumn({ name: 'updated_at' })
  updated: Date = undefined;

  @BeforeInsert()
  doBeforeInsertion() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }

  @BeforeUpdate()
  doBeforeUpdate() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }
}
