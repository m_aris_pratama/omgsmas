import { CustomValidationError, User } from '@omgstore/core-nestjs';
import {
  IsNotEmpty,
  IsOptional,
  MaxLength,
  validateSync
} from 'class-validator';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn
} from 'typeorm';

import { RetailCategory } from './retail-category.entity';
import { BusinessUnit } from './business-unit.entity';

/* id : int
name : varchar(255)
code : varchar(10)
category_id : int
business_unit_id : int
company : varchar(100)
npwp : varchar(50)
address : varchar(512)
telp : varchar(25)
fax : varchar(25)
contact_person : varchar(255)
contact_person_telp : varchar(25)
contact_person_position :  varchar(100)
contact_person_email : varchar(100)
contact_project : varchar (255)
contact_project_telp : varchar (25)
contact_project_position : varchar(100)
contact_project_email : varchar(100)
contact_project_address : varchar (512)
contact_billing : varchar(255)
contact_billing_telp : varchar(25)
contact_billing_email : varchar(100)
contact_billing_position : varchar(100)
contact_billing_address : varchar(512)
is_active : bool
user_id : int
created_at : date
updated_at : date
*/
@Entity({ name: 'retail' })
export class Retail {
  @PrimaryGeneratedColumn()
  id: number = undefined;

  @Column({ length: 100 })
  @IsNotEmpty()
  @MaxLength(100)
  name: string = undefined;

  @Column({ length: 10 })
  @IsNotEmpty()
  @MaxLength(10)
  code: string = undefined;

  @ManyToOne(type => RetailCategory, { nullable: true })
  @JoinColumn({ name: 'category_id' })
  retailCategory: RetailCategory = undefined;

  @ManyToOne(type => BusinessUnit, { nullable: true })
  @JoinColumn({ name: 'business_unit_id' })
  businessUnit: BusinessUnit = undefined;

  @Column({ length: 100 })
  @MaxLength(100)
  @IsOptional()
  company: string = undefined;

  @Column({ length: 50 })
  @MaxLength(50)
  @IsOptional()
  npwp: string = undefined;

  @Column({ length: 512 })
  @MaxLength(512)
  @IsOptional()
  address: string = undefined;

  @Column({ length: 25 })
  @MaxLength(25)
  @IsOptional()
  telp: string = undefined;

  @Column({ length: 25 })
  @MaxLength(25)
  @IsOptional()
  fax: string = undefined;

  @Column({ length: 255 })
  @MaxLength(255)
  @IsOptional()
  contact_person: string = undefined;

  @Column({ length: 25 })
  @MaxLength(25)
  @IsOptional()
  contact_person_telp: string = undefined;

  @Column({ length: 100 })
  @MaxLength(100)
  @IsOptional()
  contact_person_position: string = undefined;

  @Column({ length: 100 })
  @MaxLength(100)
  @IsOptional()
  contact_person_email: string = undefined;

  @Column({ length: 255 })
  @MaxLength(255)
  @IsOptional()
  contact_project: string = undefined;

  @Column({ length: 25 })
  @MaxLength(25)
  @IsOptional()
  contact_project_telp: string = undefined;

  @Column({ length: 100 })
  @MaxLength(100)
  @IsOptional()
  contact_project_position: string = undefined;

  @Column({ length: 100 })
  @MaxLength(100)
  @IsOptional()
  contact_project_email: string = undefined;

  @Column({ length: 512 })
  @MaxLength(512)
  @IsOptional()
  contact_project_address: string = undefined;

  @Column({ length: 255 })
  @MaxLength(255)
  @IsOptional()
  contact_billing: string = undefined;

  @Column({ length: 25 })
  @MaxLength(25)
  @IsOptional()
  contact_billing_telp: string = undefined;

  @Column({ length: 100 })
  @MaxLength(100)
  @IsOptional()
  contact_billing_position: string = undefined;

  @Column({ length: 100 })
  @MaxLength(100)
  @IsOptional()
  contact_billing_email: string = undefined;

  @Column({ length: 512 })
  @MaxLength(512)
  @IsOptional()
  contact_billing_address: string = undefined;

  @Column({ default: false })
  is_active: boolean;

  @ManyToOne(type => User, { nullable: true })
  @JoinColumn({ name: 'user_id' })
  user: User = undefined;

  @CreateDateColumn({ name: 'created_at' })
  created: Date = undefined;

  @UpdateDateColumn({ name: 'updated_at' })
  updated: Date = undefined;

  @BeforeInsert()
  doBeforeInsertion() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }

  @BeforeUpdate()
  doBeforeUpdate() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }
}
