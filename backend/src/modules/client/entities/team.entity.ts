import {
  IsNotEmpty,
  MaxLength,
  validateSync,
  IsOptional
} from 'class-validator';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany
} from 'typeorm';
import { CustomValidationError } from '@omgstore/core-nestjs';
import { User } from '@omgstore/core-nestjs/entities/user.entity';
import { Absence } from '../../activity/entities/absence.entity';

/* id : int
avatar: varchar(512)
name : varchar(255)
title_name: enum(Mr,Ms,Mrs)
telp : varchar(25)
email : varchar(100)
address : varchar(512)
gender: enum(male,female)
report_to : int
user_id : int
position : enum (fo,tl,spv,coord)
store_id : int
is_active : bool
created_at : date
updated_at : date
*/
@Entity({ name: 'team' })
export class Team {
  @PrimaryGeneratedColumn()
  id: number = undefined;

  @Column({ length: 512 })
  @MaxLength(512)
  @IsOptional()
  avatar: string = undefined;

  @Column({ length: 255 })
  @IsNotEmpty()
  @MaxLength(255)
  name: string = undefined;

  @Column({ length: 10 })
  @MaxLength(10)
  @IsOptional()
  title_name: string = undefined;

  @Column({ length: 25 })
  @MaxLength(25)
  @IsOptional()
  telp: string = undefined;

  @Column({ length: 100 })
  @MaxLength(100)
  @IsOptional()
  email: string = undefined;

  @Column({ length: 512 })
  @MaxLength(512)
  @IsOptional()
  address: string = undefined;

  @Column({ length: 10 })
  @MaxLength(10)
  @IsOptional()
  gender: string = undefined;

  @Column({ name: 'report_to' })
  @IsOptional()
  reportTo: number = undefined;

  @ManyToOne(type => User, { nullable: true })
  @JoinColumn({ name: 'user_id' })
  user: User = undefined;

  @Column({ length: 10 })
  @MaxLength(10)
  @IsOptional()
  position: string = undefined;

  @Column({ default: false })
  is_active: boolean;

  @CreateDateColumn({ name: 'created_at' })
  created: Date = undefined;

  @UpdateDateColumn({ name: 'updated_at' })
  updated: Date = undefined;

  @OneToMany(type => Absence, absence => absence.team)
  absences: Absence[];

  @BeforeInsert()
  doBeforeInsertion() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }

  @BeforeUpdate()
  doBeforeUpdate() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }
}
