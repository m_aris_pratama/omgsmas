import { IsNotEmpty, MaxLength, validateSync } from 'class-validator';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn
} from 'typeorm';
import { CustomValidationError } from '@omgstore/core-nestjs';
import { BrandCategory } from './brand-category.entity';

@Entity({ name: 'brand_subcategory' })
export class BrandSubcategory {
  @PrimaryGeneratedColumn()
  id: number = undefined;

  @ManyToOne(() => BrandCategory, { eager: true, nullable: true })
  @JoinColumn({ name: 'brand_category_id' })
  brandCategory: BrandCategory = undefined;

  @Column({ length: 100 })
  @IsNotEmpty()
  @MaxLength(100)
  name: string = undefined;

  @BeforeInsert()
  doBeforeInsertion() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }

  @BeforeUpdate()
  doBeforeUpdate() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }
}
