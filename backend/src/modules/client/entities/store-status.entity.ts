import {
  IsNotEmpty,
  MaxLength,
  validateSync,
  IsOptional
} from 'class-validator';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  PrimaryGeneratedColumn,
  JoinColumn,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn
} from 'typeorm';
import { CustomValidationError } from '@omgstore/core-nestjs';
import { Store } from './store.entity';
import { Team } from './team.entity';

@Entity({ name: 'store_status' })
export class StoreStatus {
  @PrimaryGeneratedColumn()
  id: number = undefined;

  @ManyToOne(type => Store, { nullable: true })
  @JoinColumn({ name: 'store_id' })
  store: Store = undefined;

  @ManyToOne(type => Team, { nullable: true })
  @JoinColumn({ name: 'team_id' })
  team: Team = undefined;

  @Column()
  @IsNotEmpty()
  opened: boolean = undefined;

  @Column()
  @IsNotEmpty()
  active: boolean = undefined;

  @Column()
  @IsOptional()
  report: string = undefined;

  @CreateDateColumn({ name: 'created_at' })
  created: Date = undefined;

  @UpdateDateColumn({ name: 'updated_at' })
  updated: Date = undefined;

  @BeforeInsert()
  doBeforeInsertion() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }

  @BeforeUpdate()
  doBeforeUpdate() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }
}
