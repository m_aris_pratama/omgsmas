import { IsNotEmpty, MaxLength } from 'class-validator';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn
} from 'typeorm';

import { BrandCategory } from './brand-category.entity';
import { Client } from './client.entity';
import { BrandSubcategory } from './brand-subcategory.entity';
import { User } from '@omgstore/core-nestjs/entities/user.entity';

/* id : int
client_id : int
subcategory_id : int
name : varchar(255)
address : varchar(512)
telp : varchar(25)
fax : varchar(25)
email : varchar(100)
contact_person : varchar(255)
contact_person_telp : varchar(25)
contact_person_position :  varchar(100)
contact_person_email : varchar(100)
contact_person2 : varchar(255)
contact_person2_telp : varchar(25)
contact_person2_position :  varchar(100)
contact_person2_email : varchar(100)
cost_by_project : int
is_active : bool
user_id : int
created_at : date
updated_at : date
*/

@Entity({ name: 'brand' })
export class Brand {
  @PrimaryGeneratedColumn()
  id: number = undefined;

  @ManyToOne(() => Client, { nullable: true })
  @JoinColumn({ name: 'client_id' })
  client: Client = undefined;

  @ManyToOne(() => BrandSubcategory, { nullable: true })
  @JoinColumn({ name: 'subcategory_id' })
  brandSubcategory: BrandSubcategory = undefined;

  @Column({ length: 255 })
  @IsNotEmpty()
  @MaxLength(255)
  name: string = undefined;

  @Column({ length: 512 })
  @MaxLength(512)
  address: string = undefined;

  @Column({ length: 25 })
  @MaxLength(25)
  telp: string = undefined;

  @Column({ length: 25 })
  @MaxLength(25)
  fax: string = undefined;

  @Column({ length: 100 })
  @MaxLength(100)
  email: string = undefined;

  @Column({ length: 255 })
  @MaxLength(255)
  contact_person: string = undefined;

  @Column({ length: 25 })
  @MaxLength(25)
  contact_person_telp: string = undefined;

  @Column({ length: 100 })
  @MaxLength(100)
  contact_person_position: string = undefined;

  @Column({ length: 100 })
  @MaxLength(100)
  contact_person_email: string = undefined;

  @Column({ length: 255 })
  @MaxLength(255)
  contact_person2: string = undefined;

  @Column({ length: 25 })
  @MaxLength(25)
  contact_person2_telp: string = undefined;

  @Column({ length: 100 })
  @MaxLength(100)
  contact_person2_position: string = undefined;

  @Column({ length: 100 })
  @MaxLength(100)
  contact_person2_email: string = undefined;

  @Column()
  cost_by_project: number = undefined;

  @Column({ default: false })
  is_active: boolean;

  @ManyToOne(type => User, { nullable: true })
  @JoinColumn({ name: 'user_id' })
  user: User = undefined;

  @CreateDateColumn({ name: 'created_at' })
  created: Date = undefined;

  @UpdateDateColumn({ name: 'updated_at' })
  updated: Date = undefined;
}
