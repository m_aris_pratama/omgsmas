import { CustomValidationError, User } from '@omgstore/core-nestjs';
import {
  IsNotEmpty,
  IsOptional,
  MaxLength,
  validateSync
} from 'class-validator';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  JoinColumn,
  ManyToOne
} from 'typeorm';

import { Retail } from '../entities/retail.entity';
import { City } from './city.entity';
import { Team } from './team.entity';
import { NapUpload } from 'modules/project/entities/nap-upload.entity';

/* id : int
avatar: varchar(512)
retail_id : int
city_id : int
name : varchar(255)
code : varchar(10)
address : varchar(512)
zip_code : varchar(5)
telp : varchar(25)
description : varchar(1024)
contact_person : varchar(255)
contact_person_telp : varchar(25)
contact_person_position :  varchar(100)
contact_person_email : varchar(100)
contact_person2 : varchar(255)
contact_person2_telp : varchar(25)
contact_person2_position :  varchar(100)
contact_person2_email : varchar(100)
longitude: double
latitude : double
lead_id : int
is_active : bool
user_id : int
created_at : date
updated_at : date
*/
@Entity({ name: 'store' })
export class Store {
  @PrimaryGeneratedColumn()
  id: number = undefined;

  @Column({ length: 512 })
  @MaxLength(512)
  @IsOptional()
  avatar: string = undefined;

  @ManyToOne(type => Retail, { nullable: true })
  @JoinColumn({ name: 'retail_id' })
  retail: Retail = undefined;

  @ManyToOne(type => City, { nullable: true })
  @JoinColumn({ name: 'city_id' })
  city: City = undefined;

  @ManyToOne(type => Team, { nullable: true })
  @JoinColumn({ name: 'lead_id' })
  lead: Team = undefined;

  @ManyToOne(type => Team, { nullable: true })
  @JoinColumn({ name: 'member_id' })
  member: Team = undefined;

  @Column({ length: 255 })
  @IsNotEmpty()
  @MaxLength(255)
  name: string = undefined;

  @Column({ length: 10 })
  @IsNotEmpty()
  @MaxLength(10)
  code: string = undefined;

  @Column({ length: 512 })
  @MaxLength(512)
  @IsOptional()
  address: string = undefined;

  @Column({ length: 5, name: 'zip_code' })
  @MaxLength(5)
  @IsOptional()
  zipCode: string = undefined;

  @Column({ length: 25 })
  @MaxLength(25)
  @IsOptional()
  telp: string = undefined;

  @Column({ length: 1024 })
  @MaxLength(1024)
  @IsOptional()
  description: string = undefined;

  @Column({ length: 255 })
  @MaxLength(255)
  @IsOptional()
  contact_person: string = undefined;

  @Column({ length: 25 })
  @MaxLength(25)
  @IsOptional()
  contact_person_telp: string = undefined;

  @Column({ length: 100 })
  @MaxLength(100)
  @IsOptional()
  contact_person_position: string = undefined;

  @Column({ length: 100 })
  @MaxLength(100)
  @IsOptional()
  contact_person_email: string = undefined;

  @Column({ length: 255 })
  @MaxLength(255)
  @IsOptional()
  contact_person2: string = undefined;

  @Column({ length: 25 })
  @MaxLength(25)
  @IsOptional()
  contact_person2_telp: string = undefined;

  @Column({ length: 100 })
  @MaxLength(100)
  @IsOptional()
  contact_person2_position: string = undefined;

  @Column({ length: 100 })
  @MaxLength(100)
  @IsOptional()
  contact_person2_email: string = undefined;

  @Column({ type: 'double' })
  @IsNotEmpty()
  longitude: number = undefined;

  @Column({ type: 'double' })
  @IsNotEmpty()
  latitude: number = undefined;

  @Column({ default: true })
  is_open: boolean;

  @Column({ default: true })
  is_active: boolean;

  @CreateDateColumn({ name: 'created_at' })
  created: Date = undefined;

  @UpdateDateColumn({ name: 'updated_at' })
  updated: Date = undefined;

  @OneToMany(type => NapUpload, nap => nap.store)
  naps: NapUpload[];

  @BeforeInsert()
  doBeforeInsertion() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }

  @BeforeUpdate()
  doBeforeUpdate() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }
}
