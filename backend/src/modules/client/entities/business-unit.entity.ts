import { IsNotEmpty, MaxLength, validateSync } from 'class-validator';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  JoinTable,
  OneToMany,
  ManyToMany,
  PrimaryGeneratedColumn
} from 'typeorm';
import { CustomValidationError } from '@omgstore/core-nestjs';
import { Project } from '../../project/entities/project.entity';

@Entity({ name: 'business_unit' })
export class BusinessUnit {
  @PrimaryGeneratedColumn()
  id: number = undefined;
  
  @OneToMany(type => Project, project => project.businessUnit)
  project: Project[];

  @Column({ length: 100 })
  @IsNotEmpty()
  @MaxLength(100)
  name: string = undefined;

  @Column()
  @IsNotEmpty()
  cycle_started_day: number = undefined;

  @BeforeInsert()
  doBeforeInsertion() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }

  @BeforeUpdate()
  doBeforeUpdate() {
    const errors = validateSync(this, { validationError: { target: false } });
    if (errors.length > 0) {
      throw new CustomValidationError(errors);
    }
  }
}
