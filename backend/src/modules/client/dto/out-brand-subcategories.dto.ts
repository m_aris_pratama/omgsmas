import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { MetaDto } from '@omgstore/core-nestjs';
import { BrandSubcategoryDto } from '../dto/brand-subcategory.dto';

export class OutBrandSubcategoriesDto {
  @Type(() => BrandSubcategoryDto)
  @ApiModelProperty({ type: BrandSubcategoryDto, isArray: true })
  list: BrandSubcategoryDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
