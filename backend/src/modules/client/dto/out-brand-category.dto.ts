import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { BrandCategoryDto } from '../dto/brand-category.dto';

export class OutBrandCategoryDto {
  @Type(() => BrandCategoryDto)
  @ApiModelProperty({ type: BrandCategoryDto })
  brand: BrandCategoryDto;
}
