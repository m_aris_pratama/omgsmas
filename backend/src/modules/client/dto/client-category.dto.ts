import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { Exclude, Type } from 'class-transformer';
import { MaxLength } from 'class-validator';

export class ClientCategoryDto {
  @ApiModelProperty({ type: Number })
  id: number;

  @ApiModelProperty()
  name: string;
}
