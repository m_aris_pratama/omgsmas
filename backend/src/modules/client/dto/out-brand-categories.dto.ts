import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { MetaDto } from '@omgstore/core-nestjs';
import { BrandCategoryDto } from '../dto/brand-category.dto';

export class OutBrandCategoriesDto {
  @Type(() => BrandCategoryDto)
  @ApiModelProperty({ type: BrandCategoryDto, isArray: true })
  list: BrandCategoryDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
