import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { Exclude, Type } from 'class-transformer';
import { MaxLength } from 'class-validator';
import { AreaDto } from './area.dto';

export class CityDto {
  @ApiModelProperty({ type: Number })
  id: number;

  @ApiModelProperty()
  name: string;

  @Type(() => AreaDto)
  @ApiModelProperty({ type: AreaDto })
  area: AreaDto;
}
