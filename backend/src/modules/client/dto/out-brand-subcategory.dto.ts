import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { BrandSubcategoryDto } from './brand-subcategory.dto';

export class OutBrandSubcategoryDto {
  @Type(() => BrandSubcategoryDto)
  @ApiModelProperty({ type: BrandSubcategoryDto })
  brandSubcategory: BrandSubcategoryDto;
}
