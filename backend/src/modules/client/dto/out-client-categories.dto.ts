import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { MetaDto } from '@omgstore/core-nestjs';
import { ClientCategoryDto } from '../dto/client-category.dto';

export class OutClientCategoriesDto {
  @Type(() => ClientCategoryDto)
  @ApiModelProperty({ type: ClientCategoryDto, isArray: true })
  clientCategories: ClientCategoryDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
