import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { MetaDto } from '@omgstore/core-nestjs';
import { TeamDto } from '../dto/team.dto';

export class OutTeamsDto {
  @Type(() => TeamDto)
  @ApiModelProperty({ type: TeamDto, isArray: true })
  list: TeamDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
