import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { MetaDto } from '@omgstore/core-nestjs';
import { RetailDto } from '../dto/retail.dto';

export class OutRetailsDto {
  @Type(() => RetailDto)
  @ApiModelProperty({ type: RetailDto, isArray: true })
  list: RetailDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
