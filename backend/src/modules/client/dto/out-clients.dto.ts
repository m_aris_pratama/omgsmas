import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { MetaDto } from '@omgstore/core-nestjs';
import { ClientDto } from '../dto/client.dto';

export class OutClientsDto {
  @Type(() => ClientDto)
  @ApiModelProperty({ type: ClientDto, isArray: true })
  clients: ClientDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
