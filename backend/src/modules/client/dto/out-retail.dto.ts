import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { RetailDto } from '../dto/retail.dto';

export class OutRetailDto {
  @Type(() => RetailDto)
  @ApiModelProperty({ type: RetailDto })
  retail: RetailDto;
}
