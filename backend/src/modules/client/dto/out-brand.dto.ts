import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { BrandDto } from '../dto/brand.dto';

export class OutBrandDto {
  @Type(() => BrandDto)
  @ApiModelProperty({ type: BrandDto })
  brand: BrandDto;
}
