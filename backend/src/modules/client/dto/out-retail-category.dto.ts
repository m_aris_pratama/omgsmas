import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { RetailCategoryDto } from '../dto/retail-category.dto';

export class OutRetailCategoryDto {
  @Type(() => RetailCategoryDto)
  @ApiModelProperty({ type: RetailCategoryDto })
  retail: RetailCategoryDto;
}
