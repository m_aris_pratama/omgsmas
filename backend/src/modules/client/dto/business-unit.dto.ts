import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { Exclude, Type } from 'class-transformer';
import { MaxLength } from 'class-validator';

export class BusinessUnitDto {
  @ApiModelProperty({ type: Number })
  id: number;

  @ApiModelProperty()
  name: string;

  @ApiModelProperty()
  cycle_started_day: number;
}
