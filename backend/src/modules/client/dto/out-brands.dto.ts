import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { MetaDto } from '@omgstore/core-nestjs';
import { BrandDto } from '../dto/brand.dto';

export class OutBrandsDto {
  @Type(() => BrandDto)
  @ApiModelProperty({ type: BrandDto, isArray: true })
  list: BrandDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
