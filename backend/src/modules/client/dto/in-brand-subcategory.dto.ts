import { BrandSubcategoryDto } from '../dto/brand-subcategory.dto';

export class InBrandSubcategoryDto extends BrandSubcategoryDto {}
