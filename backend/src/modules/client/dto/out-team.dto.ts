import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { TeamDto } from '../dto/team.dto';

export class OutTeamDto {
  @Type(() => TeamDto)
  @ApiModelProperty({ type: TeamDto })
  team: TeamDto;
}
