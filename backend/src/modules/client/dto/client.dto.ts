import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { Exclude, Type } from 'class-transformer';
import { MaxLength } from 'class-validator';
import { ClientCategoryDto } from '../dto/client-category.dto';
import { UserDto } from '@omgstore/core-nestjs/dto/user.dto';

export class ClientDto {
  @ApiModelProperty({ type: Number })
  id: number;

  @ApiModelPropertyOptional()
  avatar: string;

  @ApiModelProperty({ type: String })
  registered: Date;

  @ApiModelProperty({ type: ClientCategoryDto })
  clientCategory: ClientCategoryDto;

  @ApiModelProperty()
  name: string;

  @ApiModelPropertyOptional()
  npwp: string;

  @ApiModelPropertyOptional()
  address: string;

  @ApiModelPropertyOptional()
  telp: string;

  @ApiModelPropertyOptional()
  fax: string;

  @ApiModelPropertyOptional()
  email: string;

  @ApiModelPropertyOptional()
  contact_project: string;

  @ApiModelPropertyOptional()
  contact_project_telp: string;

  @ApiModelPropertyOptional()
  contact_project_position: string;

  @ApiModelPropertyOptional()
  contact_project_email: string;

  @ApiModelPropertyOptional()
  contact_project_address: string;

  @ApiModelPropertyOptional()
  contact_billing: string;

  @ApiModelPropertyOptional()
  contact_billing_telp: string;

  @ApiModelPropertyOptional()
  contact_billing_position: string;

  @ApiModelPropertyOptional()
  contact_billing_email: string;

  @ApiModelPropertyOptional()
  contact_billing_address: string;

  @ApiModelProperty()
  is_active: boolean;

  @ApiModelProperty({ type: UserDto })
  user: UserDto;

  @ApiModelProperty({ type: String })
  created: Date;

  @ApiModelProperty({ type: String })
  updated: Date;
}
