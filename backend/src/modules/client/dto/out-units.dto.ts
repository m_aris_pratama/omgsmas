import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { MetaDto } from '@omgstore/core-nestjs';
import { BusinessUnitDto } from '../dto/business-unit.dto';

export class OutBusinessUnitsDto {
  @Type(() => BusinessUnitDto)
  @ApiModelProperty({ type: BusinessUnitDto, isArray: true })
  list: BusinessUnitDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
