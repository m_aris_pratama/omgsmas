import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { Exclude, Type } from 'class-transformer';
import { MaxLength } from 'class-validator';
import { BrandCategoryDto } from '../dto/brand-category.dto';
import { ClientDto } from './client.dto';
import { BrandSubcategoryDto } from './brand-subcategory.dto';
import { UserDto } from '@omgstore/core-nestjs/dto/user.dto';

export class BrandDto {
  @ApiModelProperty({ type: Number })
  id: number;

  @ApiModelProperty({ type: ClientDto })
  client: ClientDto;

  @ApiModelProperty({ type: BrandSubcategoryDto })
  brandSubcategory: BrandSubcategoryDto;

  @ApiModelProperty()
  name: string;

  @ApiModelPropertyOptional()
  address: string;

  @ApiModelPropertyOptional()
  telp: string;

  @ApiModelPropertyOptional()
  fax: string;

  @ApiModelPropertyOptional()
  email: string;

  @ApiModelPropertyOptional()
  contact_person: string;

  @ApiModelPropertyOptional()
  contact_person_telp: string;

  @ApiModelPropertyOptional()
  contact_person_position: string;

  @ApiModelPropertyOptional()
  contact_person_email: string;

  @ApiModelPropertyOptional()
  contact_person2: string;

  @ApiModelPropertyOptional()
  contact_person2_telp: string;

  @ApiModelPropertyOptional()
  contact_person2_position: string;

  @ApiModelPropertyOptional()
  contact_person2_email: string;

  @ApiModelPropertyOptional()
  is_active: boolean;

  @ApiModelProperty()
  cost_by_project: number;

  @ApiModelProperty({ type: UserDto })
  user: UserDto;

  @ApiModelProperty({ type: String })
  created: Date;

  @ApiModelProperty({ type: String })
  updated: Date;
}
