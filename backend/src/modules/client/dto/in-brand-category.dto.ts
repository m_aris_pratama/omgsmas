import { BrandCategoryDto } from '../dto/brand-category.dto';

export class InBrandCategoryDto extends BrandCategoryDto {}
