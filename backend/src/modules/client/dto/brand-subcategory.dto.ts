import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { Exclude, Type } from 'class-transformer';
import { MaxLength } from 'class-validator';
import { BrandCategoryDto } from './brand-category.dto';

export class BrandSubcategoryDto {
  @ApiModelProperty({ type: Number })
  id: number;

  @ApiModelProperty({ type: BrandCategoryDto })
  brandCategory: BrandCategoryDto;

  @ApiModelProperty()
  name: string;
}
