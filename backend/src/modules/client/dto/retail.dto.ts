import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { Exclude, Type } from 'class-transformer';
import { MaxLength } from 'class-validator';
import { RetailCategoryDto } from '../dto/retail-category.dto';
import { BusinessUnitDto } from './business-unit.dto';
import { UserDto } from '@omgstore/core-nestjs/dto/user.dto';

export class RetailDto {
  @ApiModelProperty({ type: Number })
  id: number;

  @ApiModelProperty()
  code: string;

  @ApiModelProperty()
  name: string;

  @Type(() => RetailCategoryDto)
  @ApiModelProperty({ type: RetailCategoryDto })
  retailCategory: RetailCategoryDto;

  @Type(() => BusinessUnitDto)
  @ApiModelProperty({ type: BusinessUnitDto })
  businessUnit: BusinessUnitDto;

  @ApiModelPropertyOptional()
  company: string;

  @ApiModelPropertyOptional()
  npwp: string;

  @ApiModelPropertyOptional()
  address: string;

  @ApiModelPropertyOptional()
  telp: string;

  @ApiModelPropertyOptional()
  fax: string;

  @ApiModelPropertyOptional()
  contact_person: string;

  @ApiModelPropertyOptional()
  contact_person_telp: string;

  @ApiModelPropertyOptional()
  contact_person_position: string;

  @ApiModelPropertyOptional()
  contact_person_email: string;

  @ApiModelPropertyOptional()
  contact_project: string;

  @ApiModelPropertyOptional()
  contact_project_telp: string;

  @ApiModelPropertyOptional()
  contact_project_position: string;

  @ApiModelPropertyOptional()
  contact_project_email: string;

  @ApiModelPropertyOptional()
  contact_project_address: string;

  @ApiModelPropertyOptional()
  contact_billing: string;

  @ApiModelPropertyOptional()
  contact_billing_telp: string;

  @ApiModelPropertyOptional()
  contact_billing_position: string;

  @ApiModelPropertyOptional()
  contact_billing_email: string;

  @ApiModelPropertyOptional()
  contact_billing_address: string;

  @ApiModelPropertyOptional()
  is_active: boolean;

  @ApiModelProperty()
  cost_by_project: number;

  @ApiModelProperty({ type: UserDto })
  user: UserDto;

  @ApiModelProperty({ type: String })
  created: Date;

  @ApiModelProperty({ type: String })
  updated: Date;
}
