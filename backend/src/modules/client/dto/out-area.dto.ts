import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { AreaDto } from '../dto/area.dto';

export class OutAreaDto {
  @Type(() => AreaDto)
  @ApiModelProperty({ type: AreaDto })
  area: AreaDto;
}
