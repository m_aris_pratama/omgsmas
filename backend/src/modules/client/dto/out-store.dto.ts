import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { StoreDto } from '../dto/store.dto';

export class OutStoreDto {
  @Type(() => StoreDto)
  @ApiModelProperty({ type: StoreDto })
  store: StoreDto;
}
