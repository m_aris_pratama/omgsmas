import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { BusinessUnitDto } from '../dto/business-unit.dto';

export class OutBusinessUnitDto {
  @Type(() => BusinessUnitDto)
  @ApiModelProperty({ type: BusinessUnitDto })
  unit: BusinessUnitDto;
}
