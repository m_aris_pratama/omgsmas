import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { MetaDto } from '@omgstore/core-nestjs';
import { StoreDto } from '../dto/store.dto';

export class OutStoresDto {
  @Type(() => StoreDto)
  @ApiModelProperty({ type: StoreDto, isArray: true })
  list: StoreDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
