import { RetailCategoryDto } from '../dto/retail-category.dto';

export class InRetailCategoryDto extends RetailCategoryDto {}
