import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { Exclude, Type } from 'class-transformer';
import { MaxLength } from 'class-validator';
import { Double } from 'typeorm';
import { RetailDto } from './retail.dto';
import { AreaDto } from './area.dto';
import { CityDto } from './city.dto';
import { UserDto } from '@omgstore/core-nestjs/dto/user.dto';
import { TeamDto } from './team.dto';

export class StoreDto {
  @ApiModelProperty({ type: Number })
  id: number;

  @ApiModelPropertyOptional()
  avatar: string;

  @Type(() => RetailDto)
  @ApiModelProperty({ type: RetailDto })
  retail: RetailDto;

  @Type(() => CityDto)
  @ApiModelProperty({ type: CityDto })
  city: CityDto;

  @ApiModelProperty()
  name: string;

  @ApiModelProperty()
  code: string;

  @ApiModelPropertyOptional()
  address: string;

  @ApiModelPropertyOptional()
  zipCode: string;

  @ApiModelPropertyOptional()
  telp: string;

  @ApiModelPropertyOptional()
  description: string;

  @ApiModelPropertyOptional()
  contact_person: string;

  @ApiModelPropertyOptional()
  contact_person_telp: string;

  @ApiModelPropertyOptional()
  contact_person_position: string;

  @ApiModelPropertyOptional()
  contact_person_email: string;

  @ApiModelPropertyOptional()
  contact_person2: string;

  @ApiModelPropertyOptional()
  contact_person2_telp: string;

  @ApiModelPropertyOptional()
  contact_person2_position: string;

  @ApiModelPropertyOptional()
  contact_person2_email: string;

  @ApiModelProperty({ type: Double })
  longitude: number;

  @ApiModelProperty({ type: Double })
  latitude: number;

  @ApiModelProperty({ type: TeamDto })
  lead: TeamDto;

  @ApiModelPropertyOptional()
  is_active: boolean;

  @ApiModelProperty()
  cost_by_project: number;

  @ApiModelProperty({ type: TeamDto })
  member: TeamDto;

  @ApiModelProperty({ type: String })
  created: Date;

  @ApiModelProperty({ type: String })
  updated: Date;
}
