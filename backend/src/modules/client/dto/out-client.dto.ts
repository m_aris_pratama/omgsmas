import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { ClientDto } from '../dto/client.dto';

export class OutClientDto {
  @Type(() => ClientDto)
  @ApiModelProperty({ type: ClientDto })
  client: ClientDto;
}
