import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { MetaDto } from '@omgstore/core-nestjs';
import { RetailCategoryDto } from '../dto/retail-category.dto';

export class OutRetailCategoriesDto {
  @Type(() => RetailCategoryDto)
  @ApiModelProperty({ type: RetailCategoryDto, isArray: true })
  retailCategories: RetailCategoryDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
