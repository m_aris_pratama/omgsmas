import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { MetaDto } from '@omgstore/core-nestjs';
import { CityDto } from './city.dto';

export class OutCitiesDto {
  @Type(() => CityDto)
  @ApiModelProperty({ type: CityDto, isArray: true })
  list: CityDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
