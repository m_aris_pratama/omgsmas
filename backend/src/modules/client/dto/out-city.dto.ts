import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { CityDto } from './city.dto';

export class OutCityDto {
  @Type(() => CityDto)
  @ApiModelProperty({ type: CityDto })
  city: CityDto;
}
