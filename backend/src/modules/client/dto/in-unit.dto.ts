import { BusinessUnitDto } from '../dto/business-unit.dto';

export class InBusinessUnitDto extends BusinessUnitDto {}
