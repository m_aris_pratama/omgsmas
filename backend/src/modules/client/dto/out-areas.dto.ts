import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { MetaDto } from '@omgstore/core-nestjs';
import { AreaDto } from '../dto/area.dto';

export class OutAreasDto {
  @Type(() => AreaDto)
  @ApiModelProperty({ type: AreaDto, isArray: true })
  list: AreaDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
