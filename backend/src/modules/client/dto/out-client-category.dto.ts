import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { ClientCategoryDto } from '../dto/client-category.dto';

export class OutClientCategoryDto {
  @Type(() => ClientCategoryDto)
  @ApiModelProperty({ type: ClientCategoryDto })
  client: ClientCategoryDto;
}
