import { ContentType, Group, Permission } from '@omgstore/core-nestjs';
import { plainToClass } from 'class-transformer';
import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableIndex,
  TableForeignKey,
  TableColumn
} from 'typeorm';

export class UpdateTable2019030916151 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.addColumns('store_status', [
      new TableColumn({
        name: 'created_at',
        type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
        isNullable: false,
        default: 'CURRENT_TIMESTAMP'
      }),
      new TableColumn({
        name: 'updated_at',
        type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
        default: 'CURRENT_TIMESTAMP'
      })
    ]);

    // await queryRunner.addColumn(
    //   'store',
    //   new TableColumn({
    //     name: 'is_open',
    //     type: 'boolean',
    //     default: true,
    //     isNullable: false
    //   })
    // );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
