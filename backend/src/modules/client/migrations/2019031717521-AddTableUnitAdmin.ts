import { ContentType, Group, Permission } from '@omgstore/core-nestjs';
import { plainToClass } from 'class-transformer';
import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableIndex,
  TableForeignKey,
  TableColumn
} from 'typeorm';

export class AddTableUnitAdmin2019031717521 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    // change groups
    await queryRunner.manager
      .getRepository<Group>(Group)
      .save([
        { id: 1, name: 'superAdmin', title: 'Super Admin' },
        { id: 4, name: 'unitAdmin', title: 'Business Unit Admin' }
      ]);

    // unit admin
    await queryRunner.createTable(
      new Table({
        name: 'unit_admin',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'avatar',
            type: 'varchar(255)',
            isNullable: true
          },
          {
            name: 'name',
            type: 'varchar(255)',
            isNullable: false
          },
          {
            name: 'title_name',
            type: 'varchar(10)',
            isNullable: true
          },
          {
            name: 'telp',
            type: 'varchar(25)',
            isNullable: true
          },
          {
            name: 'email',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'address',
            type: 'varchar(512)',
            isNullable: true
          },
          {
            name: 'gender',
            type: 'varchar(10)',
            isNullable: true
          },
          {
            name: 'user_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'unit_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'is_active',
            type: 'boolean',
            default: true,
            isNullable: false
          },
          {
            name: 'created_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            isNullable: false,
            default: 'CURRENT_TIMESTAMP'
          },
          {
            name: 'updated_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            default: 'CURRENT_TIMESTAMP'
          }
        ]
      }),
      true
    );
    await queryRunner.createForeignKey(
      'unit_admin',
      new TableForeignKey({
        name: 'FK_UNADM_US_USID',
        columnNames: ['user_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'user',
        onDelete: 'SET NULL'
      })
    );
    await queryRunner.createForeignKey(
      'unit_admin',
      new TableForeignKey({
        name: 'FK_UNADM_BU_BUID',
        columnNames: ['unit_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'business_unit',
        onDelete: 'SET NULL'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
