import { ContentType, Group, Permission } from '@omgstore/core-nestjs';
import { plainToClass } from 'class-transformer';
import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableIndex,
  TableForeignKey
} from 'typeorm';

export class InitTable1545131430751 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    // create table
    // client
    await queryRunner.createTable(
      new Table({
        name: 'client',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'avatar',
            type: 'varchar(255)',
            isNullable: true
          },
          {
            name: 'registered',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            isNullable: true
          },
          {
            name: 'client_category_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'name',
            type: 'varchar(255)',
            isNullable: false
          },
          {
            name: 'npwp',
            type: 'varchar(50)',
            isNullable: true
          },
          {
            name: 'address',
            type: 'varchar(512)',
            isNullable: true
          },
          {
            name: 'telp',
            type: 'varchar(25)',
            isNullable: true
          },
          {
            name: 'fax',
            type: 'varchar(25)',
            isNullable: true
          },
          {
            name: 'email',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'contact_project',
            type: 'varchar(255)',
            isNullable: true
          },
          {
            name: 'contact_project_telp',
            type: 'varchar(25)',
            isNullable: true
          },
          {
            name: 'contact_project_position',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'contact_project_email',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'contact_project_address',
            type: 'varchar(512)',
            isNullable: true
          },
          {
            name: 'contact_billing',
            type: 'varchar(255)',
            isNullable: true
          },
          {
            name: 'contact_billing_telp',
            type: 'varchar(25)',
            isNullable: true
          },
          {
            name: 'contact_billing_position',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'contact_billing_email',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'contact_billing_address',
            type: 'varchar(512)',
            isNullable: true
          },
          {
            name: 'is_active',
            type: 'boolean',
            default: true,
            isNullable: false
          },
          {
            name: 'user_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'created_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            isNullable: false,
            default: 'CURRENT_TIMESTAMP'
          },
          {
            name: 'updated_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            default: 'CURRENT_TIMESTAMP'
          }
        ]
      }),
      true
    );
    // client category
    await queryRunner.createTable(
      new Table({
        name: 'client_category',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'name',
            type: 'varchar(255)',
            isNullable: false
          }
        ]
      }),
      true
    );
    await queryRunner.createForeignKey(
      'client',
      new TableForeignKey({
        name: 'FK_CLI_CT_ID',
        columnNames: ['client_category_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'client_category',
        onDelete: 'SET NULL'
      })
    );
    // retail
    await queryRunner.createTable(
      new Table({
        name: 'retail',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'name',
            type: 'varchar(100)',
            isNullable: false
          },
          {
            name: 'code',
            type: 'varchar(10)',
            isNullable: false
          },
          {
            name: 'category_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'business_unit_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'company',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'npwp',
            type: 'varchar(50)',
            isNullable: true
          },
          {
            name: 'address',
            type: 'varchar(512)',
            isNullable: true
          },
          {
            name: 'telp',
            type: 'varchar(25)',
            isNullable: true
          },
          {
            name: 'fax',
            type: 'varchar(25)',
            isNullable: true
          },
          {
            name: 'contact_person',
            type: 'varchar(255)',
            isNullable: true
          },
          {
            name: 'contact_person_telp',
            type: 'varchar(25)',
            isNullable: true
          },
          {
            name: 'contact_person_position',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'contact_person_email',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'contact_project',
            type: 'varchar(255)',
            isNullable: true
          },
          {
            name: 'contact_project_telp',
            type: 'varchar(25)',
            isNullable: true
          },
          {
            name: 'contact_project_position',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'contact_project_email',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'contact_project_address',
            type: 'varchar(512)',
            isNullable: true
          },
          {
            name: 'contact_billing',
            type: 'varchar(255)',
            isNullable: true
          },
          {
            name: 'contact_billing_telp',
            type: 'varchar(25)',
            isNullable: true
          },
          {
            name: 'contact_billing_position',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'contact_billing_email',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'contact_billing_address',
            type: 'varchar(512)',
            isNullable: true
          },
          {
            name: 'is_active',
            type: 'boolean',
            default: true,
            isNullable: false
          },
          {
            name: 'user_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'created_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            isNullable: false,
            default: 'CURRENT_TIMESTAMP'
          },
          {
            name: 'updated_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            default: 'CURRENT_TIMESTAMP'
          }
        ]
      }),
      true
    );

    // retail category
    await queryRunner.createTable(
      new Table({
        name: 'business_unit',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'name',
            type: 'varchar(255)',
            isNullable: false
          },
          {
            name: 'cycle_started_day',
            type: 'integer',
            isNullable: false
          }
        ]
      }),
      true
    );
    await queryRunner.createForeignKey(
      'retail',
      new TableForeignKey({
        name: 'FK_RET_BU_ID',
        columnNames: ['business_unit_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'business_unit',
        onDelete: 'SET NULL'
      })
    );
    // retail category
    await queryRunner.createTable(
      new Table({
        name: 'retail_category',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'name',
            type: 'varchar(100)',
            isNullable: false
          }
        ]
      }),
      true
    );
    await queryRunner.createIndex(
      'retail_category',
      new TableIndex({
        name: 'UQ_RC_N',
        isUnique: true,
        columnNames: ['name']
      })
    );
    await queryRunner.createForeignKey(
      'retail',
      new TableForeignKey({
        name: 'FK_RET_CT_ID',
        columnNames: ['category_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'retail_category',
        onDelete: 'SET NULL'
      })
    );

    // store
    await queryRunner.createTable(
      new Table({
        name: 'store',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'avatar',
            type: 'varchar(512)',
            isNullable: true
          },
          {
            name: 'retail_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'city_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'name',
            type: 'varchar(255)',
            isNullable: false
          },
          {
            name: 'code',
            type: 'varchar(10)',
            isNullable: false
          },
          {
            name: 'address',
            type: 'varchar(512)',
            isNullable: true
          },
          {
            name: 'zip_code',
            type: 'varchar(5)',
            isNullable: true
          },
          {
            name: 'telp',
            type: 'varchar(25)',
            isNullable: true
          },
          {
            name: 'description',
            type: 'varchar(1024)',
            isNullable: true
          },
          {
            name: 'contact_person',
            type: 'varchar(255)',
            isNullable: true
          },
          {
            name: 'contact_person_telp',
            type: 'varchar(25)',
            isNullable: true
          },
          {
            name: 'contact_person_position',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'contact_person_email',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'contact_person2',
            type: 'varchar(255)',
            isNullable: true
          },
          {
            name: 'contact_person2_telp',
            type: 'varchar(25)',
            isNullable: true
          },
          {
            name: 'contact_person2_position',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'contact_person2_email',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'longitude',
            type: 'double',
            isNullable: false
          },
          {
            name: 'latitude',
            type: 'double',
            isNullable: false
          },
          {
            name: 'lead_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'is_active',
            type: 'boolean',
            default: true,
            isNullable: false
          },
          {
              name: 'is_open',
              type: 'boolean',
              default: true,
              isNullable: false
          },
          {
            name: 'member_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'created_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            isNullable: false,
            default: 'CURRENT_TIMESTAMP'
          },
          {
            name: 'updated_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            default: 'CURRENT_TIMESTAMP'
          }
        ]
      }),
      true
    );
    await queryRunner.createIndex(
      'store',
      new TableIndex({
        name: 'IDX_OTL_C',
        isUnique: true,
        columnNames: ['name']
      })
    );
    await queryRunner.createForeignKey(
      'store',
      new TableForeignKey({
        name: 'FK_OTL_RL_ID',
        columnNames: ['retail_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'retail',
        onDelete: 'SET NULL'
      })
    );
    // store status
    await queryRunner.createTable(
      new Table({
        name: 'store_status',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'store_id',
            type: 'integer',
            isNullable: false
          },
          {
            name: 'team_id',
            type: 'integer',
            isNullable: false
          },
          {
            name: 'opened',
            type: 'boolean',
            isNullable: false
          },
          {
            name: 'active',
            type: 'boolean',
            isNullable: false
          },
          {
            name: 'report',
            type: 'varchar(1024)',
            isNullable: true
          }
        ]
      }),
      true
    );
    // area
    await queryRunner.createTable(
      new Table({
        name: 'area',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'name',
            type: 'varchar(255)',
            isNullable: false
          }
        ]
      }),
      true
    );
    await queryRunner.createIndex(
      'area',
      new TableIndex({
        name: 'IDX_AR_NM',
        columnNames: ['name']
      })
    );
    // city
    await queryRunner.createTable(
      new Table({
        name: 'city',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'name',
            type: 'varchar(255)',
            isNullable: false
          },
          {
            name: 'area_id',
            type: 'integer',
            isNullable: true
          }
        ]
      }),
      true
    );
    await queryRunner.createIndex(
      'city',
      new TableIndex({
        name: 'IDX_CY_NM',
        columnNames: ['name']
      })
    );
    await queryRunner.createForeignKey(
      'store',
      new TableForeignKey({
        name: 'FK_OTL_CY_AR',
        columnNames: ['city_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'city',
        onDelete: 'SET NULL'
      })
    );
    await queryRunner.createForeignKey(
      'city',
      new TableForeignKey({
        name: 'FK_CY_AR_ID',
        columnNames: ['area_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'area',
        onDelete: 'SET NULL'
      })
    );

    // brand
    await queryRunner.createTable(
      new Table({
        name: 'brand',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'client_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'subcategory_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'name',
            type: 'varchar(100)',
            isNullable: false
          },
          {
            name: 'address',
            type: 'varchar(512)',
            isNullable: true
          },
          {
            name: 'telp',
            type: 'varchar(25)',
            isNullable: true
          },
          {
            name: 'fax',
            type: 'varchar(25)',
            isNullable: true
          },
          {
            name: 'email',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'contact_person',
            type: 'varchar(255)',
            isNullable: true
          },
          {
            name: 'contact_person_telp',
            type: 'varchar(25)',
            isNullable: true
          },
          {
            name: 'contact_person_position',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'contact_person_email',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'contact_person2',
            type: 'varchar(255)',
            isNullable: true
          },
          {
            name: 'contact_person2_telp',
            type: 'varchar(25)',
            isNullable: true
          },
          {
            name: 'contact_person2_position',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'contact_person2_email',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'cost_by_project',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'is_active',
            type: 'boolean',
            default: true,
            isNullable: false
          },
          {
            name: 'user_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'created_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            isNullable: false,
            default: 'CURRENT_TIMESTAMP'
          },
          {
            name: 'updated_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            default: 'CURRENT_TIMESTAMP'
          }
        ]
      }),
      true
    );
    await queryRunner.createForeignKey(
      'brand',
      new TableForeignKey({
        name: 'FK_BRD_CL_ID',
        columnNames: ['client_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'client',
        onDelete: 'SET NULL'
      })
    );
    // brand category
    await queryRunner.createTable(
      new Table({
        name: 'brand_category',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'name',
            type: 'varchar(255)',
            isNullable: false
          }
        ]
      }),
      true
    );
    // brand category
    await queryRunner.createTable(
      new Table({
        name: 'brand_subcategory',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'brand_category_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'name',
            type: 'varchar(255)',
            isNullable: false
          }
        ]
      }),
      true
    );
    await queryRunner.createForeignKey(
      'brand',
      new TableForeignKey({
        name: 'FK_BRD_SCT_ID',
        columnNames: ['subcategory_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'brand_subcategory',
        onDelete: 'SET NULL'
      })
    );
    await queryRunner.createForeignKey(
      'brand_subcategory',
      new TableForeignKey({
        name: 'FK_BRSC_CT_ID',
        columnNames: ['brand_category_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'brand_category',
        onDelete: 'SET NULL'
      })
    );

    // team
    await queryRunner.createTable(
      new Table({
        name: 'team',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'avatar',
            type: 'varchar(255)',
            isNullable: true
          },
          {
            name: 'name',
            type: 'varchar(255)',
            isNullable: false
          },
          {
            name: 'title_name',
            type: 'varchar(10)',
            isNullable: true
          },
          {
            name: 'telp',
            type: 'varchar(25)',
            isNullable: true
          },
          {
            name: 'email',
            type: 'varchar(100)',
            isNullable: true
          },
          {
            name: 'address',
            type: 'varchar(512)',
            isNullable: true
          },
          {
            name: 'gender',
            type: 'varchar(10)',
            isNullable: true
          },
          {
            name: 'report_to',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'user_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'position',
            type: 'varchar(10)',
            isNullable: true
          },
          {
            name: 'store_id',
            type: 'integer',
            isNullable: true
          },
          {
            name: 'is_active',
            type: 'boolean',
            default: true,
            isNullable: false
          },
          {
            name: 'created_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            isNullable: false,
            default: 'CURRENT_TIMESTAMP'
          },
          {
            name: 'updated_at',
            type: queryRunner.connection.driver.mappedDataTypes.createDate.toString(),
            default: 'CURRENT_TIMESTAMP'
          }
        ]
      }),
      true
    );
    await queryRunner.createForeignKey(
      'team',
      new TableForeignKey({
        name: 'FK_TM_US_USID',
        columnNames: ['user_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'user',
        onDelete: 'SET NULL'
      })
    );
    await queryRunner.createForeignKey(
      'team',
      new TableForeignKey({
        name: 'FK_TM_STR_ID',
        columnNames: ['store_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'store',
        onDelete: 'SET NULL'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
