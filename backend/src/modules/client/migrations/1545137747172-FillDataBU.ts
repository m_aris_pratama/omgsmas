import { plainToClass } from 'class-transformer';
import { MigrationInterface, QueryRunner } from 'typeorm';

import { Area } from '../entities/area.entity';
import { RetailCategory } from '../entities/retail-category.entity';
import { Retail } from '../entities/retail.entity';
import { Store } from '../entities/store.entity';
import { BusinessUnit } from '../entities/business-unit.entity';
import { City } from '../entities/city.entity';
import { Team } from '../entities/team.entity';

export class FillData1545137747172 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    const XLSX = require('xlsx');
    const wb = XLSX.readFile(
      'src/modules/client/migrations/business-units.xlsx'
    );
    const first_sheet_name = wb.SheetNames[0];
    const worksheet = wb.Sheets[first_sheet_name];

    // business unit
    const lsBU = [];
    for (let i = 1; i < 1000; i++) {
      const desired_cell = worksheet['A' + (i + 1)];
      const desired_value = desired_cell ? desired_cell.v : undefined;
      if (desired_value === undefined) break;
      const a = lsBU.find(el => {
        return el.name === desired_value;
      });
      if (!a) lsBU.push({ name: desired_value, cycle_started_day: 1 });
    }

    const pBU = await queryRunner.manager
      .getRepository<BusinessUnit>(BusinessUnit)
      .save(plainToClass(BusinessUnit, lsBU));

    await queryRunner.manager
      .getRepository<RetailCategory>(RetailCategory)
      .save(
        plainToClass(RetailCategory, [
          { name: 'Hypermarket' },
          { name: 'INSIDE MALL' },
          { name: 'AOUTSIDE MALL' },
          { name: 'Supermarket' }
        ])
      );

    // retail
    const lsRET = [];
    for (let i = 1; i < 1000; i++) {
      const code_value = worksheet['B' + (i + 1)]
        ? worksheet['B' + (i + 1)].v
        : undefined;
      if (code_value === undefined) break;
      const a = lsRET.find(el => {
        return el.code === code_value;
      });
      if (!a) {
        const bu_value = worksheet['A' + (i + 1)]
          ? worksheet['A' + (i + 1)].v
          : undefined;
        const name_value = worksheet['C' + (i + 1)]
          ? worksheet['C' + (i + 1)].v
          : undefined;
        lsRET.push({
          retailCategory: { id: 1 },
          businessUnit: pBU.filter(item => item.name === bu_value)[0],
          code: code_value,
          name: name_value
        });
      }
    }

    const pRET = await queryRunner.manager
      .getRepository<Retail>(Retail)
      .save(plainToClass(Retail, lsRET));

    // area
    const lsAREA = [];
    for (let i = 1; i < 1000; i++) {
      const name_value = worksheet['G' + (i + 1)]
        ? worksheet['G' + (i + 1)].v
        : undefined;
      if (name_value === undefined) break;
      const a = lsAREA.find(el => {
        return el.name === name_value;
      });
      if (!a) lsAREA.push({ name: name_value });
    }

    const pAREA = await queryRunner.manager
      .getRepository<Area>(Area)
      .save(plainToClass(Area, lsAREA));

    // city
    const lsCITY = [];
    for (let i = 1; i < 1000; i++) {
      const name_value = worksheet['F' + (i + 1)]
        ? worksheet['F' + (i + 1)].v
        : undefined;
      if (name_value === undefined) break;
      const a = lsCITY.find(el => {
        return el.name === name_value;
      });
      if (!a) {
        const area_value = worksheet['G' + (i + 1)]
          ? worksheet['G' + (i + 1)].v
          : undefined;
        lsCITY.push({
          area: pAREA.filter(item => item.name === area_value)[0],
          name: name_value
        });
      }
    }

    const pCITY = await queryRunner.manager
      .getRepository<City>(City)
      .save(plainToClass(City, lsCITY));

    // Lead
    const lsLead = [];
    for (let i = 1; i < 1000; i++) {
      const name_value = worksheet['J' + (i + 1)]
        ? worksheet['J' + (i + 1)].v
        : undefined;
      if (name_value === undefined) break;
      const a = lsLead.find(el => {
        return el.name === name_value;
      });
      if (!a)
        lsLead.push({
          name: name_value,
          position: 'lead',
          is_active: true
        });
    }

    const pLead = await queryRunner.manager
      .getRepository<Team>(Team)
      .save(plainToClass(Team, lsLead));

    // member
    const lsMember = [];
    for (let i = 1; i < 1000; i++) {
      const name_value = worksheet['I' + (i + 1)]
        ? worksheet['I' + (i + 1)].v
        : undefined;
      if (name_value === undefined) break;
      const a = lsMember.find(el => {
        return el.name === name_value;
      });
      if (!a) {
        const lead_value = worksheet['J' + (i + 1)]
          ? worksheet['J' + (i + 1)].v
          : undefined;
        lsMember.push({
          reportTo: null, //pLead.filter(item => item.name === lead_value)[0],
          name: name_value,
          position: 'member',
          is_active: true
        });
      }
    }

    const pMember = await queryRunner.manager
      .getRepository<Team>(Team)
      .save(plainToClass(Team, lsMember));

    // store
    const lsStore = [];
    for (let i = 1; i < 1000; i++) {
      const code_value = worksheet['D' + (i + 1)]
        ? worksheet['D' + (i + 1)].v
        : undefined;
      if (code_value === undefined) break;
      const a = lsStore.find(el => {
        return el.code === code_value;
      });
      if (!a) {
        const ret_value = worksheet['B' + (i + 1)]
          ? worksheet['B' + (i + 1)].v
          : undefined;
        const name_value = worksheet['E' + (i + 1)]
          ? worksheet['E' + (i + 1)].v
          : undefined;
        const city_value = worksheet['F' + (i + 1)]
          ? worksheet['F' + (i + 1)].v
          : undefined;
        const lead_value = worksheet['J' + (i + 1)]
          ? worksheet['J' + (i + 1)].v
          : undefined;
        const member_value = worksheet['I' + (i + 1)]
          ? worksheet['I' + (i + 1)].v
          : undefined;
        lsStore.push({
          retail: pRET.filter(item => item.code === ret_value)[0],
          city: pCITY.filter(item => item.name === city_value)[0],
          lead: pLead.filter(item => item.name === lead_value)[0],
          member: pMember.filter(item => item.name === member_value)[0],
          code: code_value,
          name: name_value,
          longitude: 0,
          latitude: 0
        });
      }
    }

    const pStore = await queryRunner.manager
      .getRepository<Store>(Store)
      .save(plainToClass(Store, lsStore));
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
