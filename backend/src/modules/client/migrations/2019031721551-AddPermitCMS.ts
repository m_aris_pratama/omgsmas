import { ContentType, Group, Permission } from '@omgstore/core-nestjs';
import { plainToClass } from 'class-transformer';
import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableIndex,
  TableForeignKey,
  TableColumn
} from 'typeorm';

export class AddPermitCMS2019031721551 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    // create/load content type
    const ctNewEntity = await queryRunner.manager
      .getRepository<ContentType>(ContentType)
      .save(
        plainToClass(ContentType, [
          { name: 'cms', title: 'CMS' },
          { name: 'mobile', title: 'Mobile Apps' }
        ])
      );
    // create permissions
    const nPermissions = await queryRunner.manager
      .getRepository<Permission>(Permission)
      .save(
        plainToClass(Permission, [
          {
            title: 'Dashboard',
            name: 'dash',
            contentType: ctNewEntity.find(x => x.name === 'cms')
          },
          {
            title: 'Report',
            name: 'report',
            contentType: ctNewEntity.find(x => x.name === 'cms')
          },
          {
            title: 'Attendance',
            name: 'attendance',
            contentType: ctNewEntity.find(x => x.name === 'cms')
          },
          {
            title: 'Project',
            name: 'project',
            contentType: ctNewEntity.find(x => x.name === 'cms')
          },
          {
            title: 'Manage',
            name: 'manage',
            contentType: ctNewEntity.find(x => x.name === 'cms')
          },
          {
            title: 'Mobile Apps',
            name: 'mobile',
            contentType: ctNewEntity.find(x => x.name === 'mobile')
          }
        ])
      );

    // add permissions to groups
    const gUserFO = await queryRunner.manager
      .getRepository<Group>(Group)
      .findOneOrFail({
        where: {
          name: 'userFO'
        },
        relations: ['permissions']
      });
    const gUserTL = await queryRunner.manager
      .getRepository<Group>(Group)
      .findOneOrFail({
        where: {
          name: 'userTL'
        },
        relations: ['permissions']
      });
    const gAdmin = await queryRunner.manager
      .getRepository<Group>(Group)
      .findOneOrFail({
        where: {
          id: 1
        },
        relations: ['permissions']
      });
    const UAdmin = await queryRunner.manager
      .getRepository<Group>(Group)
      .findOneOrFail({
        where: {
          id: 4
        },
        relations: ['permissions']
      });

    gUserFO.permissions = [...gUserFO.permissions, ...nPermissions];
    gUserTL.permissions = [...gUserTL.permissions, ...nPermissions];
    gAdmin.permissions = [...gAdmin.permissions, ...nPermissions];
    UAdmin.permissions = [...UAdmin.permissions, ...nPermissions];

    await queryRunner.manager
      .getRepository<Group>(Group)
      .save([gUserFO, gUserTL, gAdmin, UAdmin]);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
