import { plainToClass } from 'class-transformer';
import { MigrationInterface, QueryRunner } from 'typeorm';

import { Area } from '../entities/area.entity';
import { City } from '../entities/city.entity';
import { BrandCategory } from '../entities/brand-category.entity';
import { BrandSubcategory } from '../entities/brand-subcategory.entity';

export class FillData1545137747174 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    const XLSX = require('xlsx');
    const wb = XLSX.readFile(
      'src/modules/client/migrations/brand-categories.xlsx'
    );
    const first_sheet_name = wb.SheetNames[0];
    const worksheet = wb.Sheets[first_sheet_name];

    // category
    const lsCATE = [];
    for (let i = 1; i < 1000; i++) {
      const name_value = worksheet['B' + (i + 1)]
        ? worksheet['B' + (i + 1)].v
        : undefined;
      if (name_value === undefined) break;
      const a = lsCATE.find(el => {
        return el.name === name_value;
      });
      if (!a) lsCATE.push({ name: name_value });
    }

    const pCATE = await queryRunner.manager
      .getRepository<BrandCategory>(BrandCategory)
      .save(plainToClass(BrandCategory, lsCATE));

    // sub category
    const lsSUB = [];
    for (let i = 1; i < 1000; i++) {
      const name_value = worksheet['A' + (i + 1)]
        ? worksheet['A' + (i + 1)].v
        : undefined;
      if (name_value === undefined) break;
      const a = lsSUB.find(el => {
        return el.name === name_value;
      });
      if (!a) {
        const area_value = worksheet['B' + (i + 1)]
          ? worksheet['B' + (i + 1)].v
          : undefined;
        lsSUB.push({
          brandCategory: pCATE.filter(item => item.name === area_value)[0],
          name: name_value
        });
      }
    }

    await queryRunner.manager
      .getRepository<BrandSubcategory>(BrandSubcategory)
      .save(plainToClass(BrandSubcategory, lsSUB));
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
