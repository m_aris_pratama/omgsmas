import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { GroupDto } from '../dto/group.dto';
import { UserDto } from '../dto/user.dto';

export class AccountDto extends UserDto {
  @Type(() => GroupDto)
  @ApiModelProperty({ type: GroupDto, isArray: true })
  groups: GroupDto[];
}
