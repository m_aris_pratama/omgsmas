import { plainToClass } from 'class-transformer';
import { MigrationInterface, QueryRunner } from 'typeorm';
import { ContentType } from '../entities/content-type.entity';
import { Group } from '../entities/group.entity';
import { Permission } from '../entities/permission.entity';
import { User } from '../entities/user.entity';

export class FillData1524199022084 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    const ctPermission = await queryRunner.manager
      .getRepository<ContentType>(ContentType)
      .save(
        plainToClass(ContentType, { name: 'permission', title: 'Permission' })
      );
    const ctGroup = await queryRunner.manager
      .getRepository<ContentType>(ContentType)
      .save(plainToClass(ContentType, { name: 'group', title: 'Group' }));
    const ctContentTtype = await queryRunner.manager
      .getRepository<ContentType>(ContentType)
      .save(
        plainToClass(ContentType, {
          name: 'content-type',
          title: 'Content type'
        })
      );
    const ctUser = await queryRunner.manager
      .getRepository<ContentType>(ContentType)
      .save(plainToClass(ContentType, { name: 'user', title: 'User' }));

    const pPermissions = await queryRunner.manager
      .getRepository<Permission>(Permission)
      .save(
        plainToClass(Permission, [
          {
            title: 'Can add permission',
            name: 'add_permission',
            contentType: ctPermission
          },
          {
            title: 'Can change permission',
            name: 'change_permission',
            contentType: ctPermission
          },
          {
            title: 'Can delete permission',
            name: 'delete_permission',
            contentType: ctPermission
          },
          {
            title: 'Can add group',
            name: 'add_group',
            contentType: ctGroup
          },
          {
            title: 'Can change group',
            name: 'change_group',
            contentType: ctGroup
          },
          {
            title: 'Can delete group',
            name: 'delete_group',
            contentType: ctGroup
          },
          {
            title: 'Can add content type',
            name: 'add_content-type',
            contentType: ctContentTtype
          },
          {
            title: 'Can change content type',
            name: 'change_content-type',
            contentType: ctContentTtype
          },
          {
            title: 'Can delete content type',
            name: 'delete_content-type',
            contentType: ctContentTtype
          },
          {
            title: 'Can add user',
            name: 'add_user',
            contentType: ctUser
          },
          {
            title: 'Can change user',
            name: 'change_user',
            contentType: ctUser
          },
          {
            title: 'Can delete user',
            name: 'delete_user',
            contentType: ctUser
          },
          {
            title: 'Can read user',
            name: 'read_user',
            contentType: ctUser
          },
          {
            title: 'Can read group',
            name: 'read_group',
            contentType: ctGroup
          },
          {
            title: 'Can read permission',
            name: 'read_permission',
            contentType: ctPermission
          },
          {
            title: 'Can read content type',
            name: 'read_content-type',
            contentType: ctContentTtype
          },
          {
            title: 'Can change profile',
            name: 'change_profile',
            contentType: ctUser
          }
        ])
      );

    const gCMSAdmin = await queryRunner.manager
      .getRepository<Group>(Group)
      .save(
        plainToClass(Group, {
          name: 'cms-admin',
          title: 'CMS Admin',
          permissions: pPermissions
        })
      );
    const guserTL = await queryRunner.manager.getRepository<Group>(Group).save(
      plainToClass(Group, {
        name: 'userTL',
        title: 'Team Leader',
        permissions: pPermissions.filter(item => item.name === 'change_profile')
      })
    );
    const guserFO = await queryRunner.manager.getRepository<Group>(Group).save(
      plainToClass(Group, {
        name: 'userFO',
        title: 'Field Officer',
        permissions: pPermissions.filter(item => item.name === 'change_profile')
      })
    );
    const gClient = await queryRunner.manager.getRepository<Group>(Group).save(
      plainToClass(Group, {
        name: 'client',
        title: 'Client',
        permissions: pPermissions
      })
    );

    const tempUser = new User();
    const uUsers = await queryRunner.manager.getRepository<User>(User).save(
      plainToClass(User, [
        {
          username: 'superadmin',
          email: 'admin@admin.com',
          password: await tempUser.createPassword('12345678'),
          firstName: 'SuperAdminFirstName',
          lastName: 'SuperAdminLastName',
          isSuperuser: true,
          isStaff: false,
          isActive: true,
          groups: []
        },
        {
          username: 'cmsadmin',
          email: 'cms@admin.com',
          password: await tempUser.createPassword('12345678'),
          firstName: 'CMSAdmin1FirstName',
          lastName: 'CMSAdmin1LastName',
          isSuperuser: false,
          isStaff: false,
          isActive: true,
          groups: [gCMSAdmin]
        },
        {
          username: 'user.tl',
          email: 'user.tl@user.com',
          password: await tempUser.createPassword('12345678'),
          firstName: 'UserTL1FirstName',
          lastName: 'UserTL1LastName',
          isSuperuser: false,
          isStaff: false,
          isActive: true,
          groups: [guserTL]
        },
        {
          username: 'user.fo',
          email: 'user.fo@user.com',
          password: await tempUser.createPassword('12345678'),
          firstName: 'UserFO1FirstName',
          lastName: 'UserFO1LastName',
          isSuperuser: false,
          isStaff: false,
          isActive: true,
          groups: [guserFO]
        },
        {
          username: 'client',
          email: 'client1@client.com',
          password: await tempUser.createPassword('12345678'),
          firstName: 'Client1FirstName',
          lastName: 'Client1LastName',
          isSuperuser: false,
          isStaff: false,
          isActive: true,
          groups: [gClient]
        }
      ])
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
