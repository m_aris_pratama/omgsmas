export function transformToBoolean(value: boolean | undefined) {
  return !!value;
}

export function exportNormalize(item: Object, exportKey: Array<String>) {
  let exportData = [];
  exportKey.forEach(key => {
    const keySplit = key.split('.');
    const data = checkNested(item, keySplit);
    exportData.push(data);
  });
  return exportData;
}

function checkNested(obj: Object, args: Array<String>) {
  for (var i = 0; i < args.length; i++) {
    if (!obj || !obj.hasOwnProperty(args[`${i}`])) {
      return '';
    }
    obj = obj[args[`${i}`]];
  }
  return obj;
}