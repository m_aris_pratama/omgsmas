import { JwtStrategy } from './jwt.strategy';
import { LocalStrategySignIn, LocalStrategySignUp } from './local.strategy';

export const passportStrategies = [
  LocalStrategySignIn,
  LocalStrategySignUp,
  JwtStrategy
];
