import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Inject,
  Logger,
  Post,
  Req
} from '@nestjs/common';
import { ApiResponse, ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import {
  CORE_CONFIG_TOKEN,
  ICoreConfig,
  OutAccountDto,
  UsersService
} from '@omgstore/core-nestjs';
import { plainToClass } from 'class-transformer';
import { JsonWebTokenError } from 'jsonwebtoken';
import { SignInDto } from '../dto/sign-in.dto';
import { SignUpDto } from '../dto/sign-up.dto';
import { TokenDto } from '../dto/token.dto';
import { UserTokenDto } from '../dto/user-token.dto';
import { IJwtPayload } from '../interfaces/jwt-payload.interface';
import { AuthService } from '../services/auth.service';
import { TokenService } from '../services/token.service';
import { TeamsService } from '../../../modules/client/services/teams.service';

@ApiUseTags('auth')
@Controller('/api/auth')
export class AuthController {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    private readonly authService: AuthService,
    private readonly tokenService: TokenService,
    private readonly teamService: TeamsService,
    private readonly userService: UsersService
  ) {}
  @HttpCode(HttpStatus.OK)
  @Post('signin')
  @ApiResponse({
    status: HttpStatus.OK,
    type: UserTokenDto,
    description:
      'API View that checks the veracity of a token, returning the token if it is valid.'
  })
  async requestJsonWebTokenAfterSignIn(
    @Req() req,
    @Body() signInDto: SignInDto
  ): Promise<UserTokenDto> {
    const token = await this.tokenService.create(req.user);
    const team = await this.teamService.findByUserId({ id: req.user.id });
    if (team) req.user.id = team.id;
    return plainToClass(UserTokenDto, { status: 'ok', user: req.user, token });
  }

  @HttpCode(HttpStatus.OK)
  @Post('info')
  @ApiBearerAuth()
  @ApiResponse({
    status: HttpStatus.OK,
    type: UserTokenDto,
    description:
      'API View that checks the veracity of a token, returning the token if it is valid.'
  })
  async requestJsonWebTokenAfterInfo(@Req() req): Promise<OutAccountDto> {
    try {
      const validateTokenResult = await this.tokenService.validate(
        req.headers.authorization
      );
      if (validateTokenResult) {
        const jwtPayload: IJwtPayload = await this.tokenService.decode(
          req.headers.authorization
        );
        const { user } = await this.authService.info({ id: jwtPayload.id });
        return plainToClass(OutAccountDto, { user });
      } else {
        throw new JsonWebTokenError('invalid token');
      }
    } catch (error) {
      throw error;
    }
  }

  @HttpCode(HttpStatus.OK)
  @Post('updateProfile')
  @ApiBearerAuth()
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'API to update profile.'
  })
  async updateProfile(@Req() req, @Body() profileDto) {
    try {
      const validateTokenResult = await this.tokenService.validate(
        req.headers.authorization
      );
      if (validateTokenResult) {
        const jwtPayload: IJwtPayload = await this.tokenService.decode(
          req.headers.authorization
        );
        // update profile
        this.userService.update({ id: jwtPayload.id, item: profileDto });
        const { user } = await this.authService.info({ id: jwtPayload.id });
        return plainToClass(OutAccountDto, { user });
        return { status: 'OK' };
      } else {
        throw new JsonWebTokenError('invalid token');
      }
    } catch (error) {
      throw error;
    }
  }

  @HttpCode(HttpStatus.OK)
  @Post('updatePass')
  @ApiBearerAuth()
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'API to update profile.'
  })
  async updatePass(@Req() req, @Body() profileDto) {
    try {
      const validateTokenResult = await this.tokenService.validate(
        req.headers.authorization
      );
      if (validateTokenResult) {
        const jwtPayload: IJwtPayload = await this.tokenService.decode(
          req.headers.authorization
        );
        // update profile
        console.log(profileDto);
        const user = await this.userService.findById({ id: jwtPayload.id });
        await this.authService.changePass(
          user.user.username,
          profileDto.currentPassword,
          profileDto.newPassword
        );
        return { status: 'OK' };
      } else {
        throw new JsonWebTokenError('invalid token');
      }
    } catch (error) {
      throw error;
    }
  }
}
