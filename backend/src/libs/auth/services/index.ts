import { AuthService } from '../services/auth.service';
import { TokenService } from '../services/token.service';

export const services = [TokenService, AuthService];
