import {
  BadRequestException,
  ConflictException,
  HttpService,
  Inject,
  Injectable,
  Logger
} from '@nestjs/common';
import {
  CustomError,
  GroupsService,
  User,
  UsersService,
  ICoreConfig,
  CORE_CONFIG_TOKEN
} from '@omgstore/core-nestjs';
import { plainToClass } from 'class-transformer';
import { stringify } from 'querystring';
import { map } from 'rxjs/operators';
import { SignInDto } from '../dto/sign-in.dto';
import { SignUpDto } from '../dto/sign-up.dto';
@Injectable()
export class AuthService {
  private localUri: string;

  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    private readonly httpService: HttpService,
    private readonly usersService: UsersService,
    private readonly groupsService: GroupsService
  ) {
    if (this.coreConfig.port) {
      this.localUri = `http://${this.coreConfig.domain}:${
        this.coreConfig.port
      }`;
    } else {
      this.localUri = `http://${this.coreConfig.domain}`;
    }
  }
  async info(options: { id: number }) {
    try {
      return await this.usersService.findById(options);
    } catch (error) {
      throw error;
    }
  }
  async signIn(options: SignInDto) {
    try {
      const { user } = await this.usersService.findByUserName(options);
      if (!(await user.validatePassword(options.password))) {
        throw new CustomError('Wrong password');
      }
      return await this.usersService.update({ id: user.id, item: user });
    } catch (error) {
      throw error;
    }
  }
  async changePass(username, oldPass, newPass) {
    try {
      const { user } = await this.usersService.findByUserName({ username });
      if (!(await user.validatePassword(oldPass))) {
        throw new CustomError('Wrong password');
      }
      await user.setPassword(newPass);
      return await this.usersService.update({ id: user.id, item: user });
    } catch (error) {
      throw error;
    }
  }
  async signUp(options: SignUpDto) {
    try {
      await this.groupsService.preloadAll();
    } catch (error) {
      throw new BadRequestException('Error in load groups');
    }
    if (options.email) {
      let userOfEmail: { user };
      try {
        userOfEmail = await this.usersService.findByEmail(options);
      } catch (error) {
        userOfEmail = undefined;
      }
      if (userOfEmail) {
        throw new ConflictException(
          `User with email "${options.email}" is exists`
        );
      }
    }
    if (options.username) {
      let userOfUsername: { user };
      try {
        userOfUsername = await this.usersService.findByUserName(options);
      } catch (error) {
        userOfUsername = undefined;
      }
      if (userOfUsername) {
        throw new ConflictException(
          `User with username "${options.username}" is exists`
        );
      }
    }
    const group = this.groupsService.getGroupByName({ name: 'user' });
    const newUser = await plainToClass(User, options).setPassword(
      options.password
    );
    newUser.groups = [group];
    return this.usersService.create({ item: newUser });
  }
}
