import { defaultJwtConfig, JWT_CONFIG_TOKEN } from '../configs/jwt.config';

export const configs = [
  {
    provide: JWT_CONFIG_TOKEN,
    useValue: defaultJwtConfig
  }
];
