// use localStorage to store the authority info, which might be sent from server in actual project.
export function getAuthority(str) {
  // return localStorage.getItem('antd-pro-authority') || ['admin', 'user'];
  const authorityString =
    typeof str === 'undefined' ? localStorage.getItem('antd-pro-authority') : str;
  // authorityString could be admin, "admin", ["admin"]
  let authority;
  try {
    authority = JSON.parse(authorityString);
  } catch (e) {
    authority = authorityString;
  }
  if (typeof authority === 'string') {
    return [authority];
  }
  return authority || ['admin'];
}

export function setAuthority(authority) {
  const proAuthority = typeof authority === 'string' ? [authority] : authority;
  return localStorage.setItem('antd-pro-authority', JSON.stringify(proAuthority));
}

export function getToken(str) {
  // return localStorage.getItem('antd-pro-authority') || ['admin', 'user'];
  const tokenString =
    typeof str === 'undefined' ? localStorage.getItem('token') : str;
  // authorityString could be admin, "admin", ["admin"]
  let token;
  try {
    token = JSON.parse(tokenString);
  } catch (e) {
    token = tokenString;
  }
  return token || '';
}

export function setToken(token) {
  return localStorage.setItem('token', JSON.stringify(token));
}