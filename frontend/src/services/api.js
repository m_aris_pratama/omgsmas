import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryProjectNotice() {
  return request('/api/project/notice');
}

export async function queryActivities() {
  return request('/api/activities');
}

export async function queryRule(params) {
  return request(`/api/clients?${stringify(params)}`);
}

export async function removeRule(params) {
  return request('/api/rule', {
    method: 'POST',
    body: {
      ...params,
      method: 'delete',
    },
  });
}

export async function addRule(params) {
  return request('/api/rule', {
    method: 'POST',
    body: {
      ...params,
      method: 'post',
    },
  });
}

export async function updateRule(params) {
  return request('/api/rule', {
    method: 'POST',
    body: {
      ...params,
      method: 'update',
    },
  });
}

export async function fakeSubmitForm(params) {
  return request('/api/forms', {
    method: 'POST',
    body: params,
  });
}

export async function fakeChartData() {
  return request('/api/fake_chart_data');
}

export async function queryTags() {
  return request('/api/tags');
}

export async function queryBasicProfile() {
  return request('/api/profile/basic');
}

export async function queryAdvancedProfile() {
  return request('/api/profile/advanced');
}

// absences
// retail types
export async function queryAbsenceList(params) {
  return request(`/api/absence?${stringify(params)}`);
}

// clients
export async function queryClientList(params) {
  return request(`/api/clients?${stringify(params)}`);
}
export async function removeClientList(params) {
  const {...restParams } = params;
  return request(`/api/clients/${params.id}`, {
    method: 'DELETE',
    body: {
      ...restParams
    },
  });
}
export async function addClientList(params) {
  const { ...restParams } = params;
  return request(`/api/clients`, {
    method: 'POST',
    body: {
      ...restParams
    },
  });
}
export async function updateClientList(params) {
  const { ...restParams } = params;
  return request(`/api/clients/${params.id}`, {
    method: 'PUT',
    body: {
      ...restParams
    },
  });
}

export async function queryClientUserList(params) {
  return request(`/api/users?${stringify(params)}`);
}

// retails
export async function queryRetailList(params) {
  return request(`/api/retails?${stringify(params)}`);
}
export async function removeRetail(params) {
  const {...restParams } = params;
  return request(`/api/retails/${params.id}`, {
    method: 'DELETE',
    body: {
      ...restParams
    },
  });
}
export async function addRetail(params) {
  const { ...restParams } = params;
  return request(`/api/retails`, {
    method: 'POST',
    body: {
      ...restParams
    },
  });
}
export async function updateRetail(params) {
  const { ...restParams } = params;
  return request(`/api/retails/${params.id}`, {
    method: 'PUT',
    body: {
      ...restParams
    },
  });
}

// retail types
export async function queryRetailTypeList(params) {
  return request(`/api/retail-categories?${stringify(params)}`);
}
export async function removeRetailType(params) {
  const {...restParams } = params;
  return request(`/api/retail-categories/${params.id}`, {
    method: 'DELETE',
    body: {
      ...restParams
    },
  });
}
export async function addRetailType(params) {
  const { ...restParams } = params;
  return request(`/api/retail-categories`, {
    method: 'POST',
    body: {
      ...restParams
    },
  });
}
export async function updateRetailType(params) {
  const { ...restParams } = params;
  return request(`/api/retail-categories/${params.id}`, {
    method: 'PUT',
    body: {
      ...restParams
    },
  });
}

// stores
export async function queryStoreList(params) {
  return request(`/api/stores?${stringify(params)}`);
}
export async function removeStore(params) {
  const {...restParams } = params;
  return request(`/api/stores/${params.id}`, {
    method: 'DELETE',
    body: {
      ...restParams
    },
  });
}
export async function addStore(params) {
  const { ...restParams } = params;
  return request(`/api/stores`, {
    method: 'POST',
    body: {
      ...restParams
    },
  });
}
export async function updateStore(params) {
  const { ...restParams } = params;
  return request(`/api/stores/${params.id}`, {
    method: 'PUT',
    body: {
      ...restParams
    },
  });
}

// area
export async function queryAreaList(params) {
  return request(`/api/areas?${stringify(params)}`);
}
export async function removeArea(params) {
  const {...restParams } = params;
  return request(`/api/areas/${params.id}`, {
    method: 'DELETE',
    body: {
      ...restParams
    },
  });
}
export async function addArea(params) {
  const { ...restParams } = params;
  return request(`/api/areas`, {
    method: 'POST',
    body: {
      ...restParams
    },
  });
}
export async function updateArea(params) {
  const { ...restParams } = params;
  return request(`/api/areas/${params.id}`, {
    method: 'PUT',
    body: {
      ...restParams
    },
  });
}

// projects
export async function queryProjectList(params) {
  return request(`/api/projects?${stringify(params)}`);
}
export async function queryProject(params) {
  return request(`/api/projects/${params.id}`);
}
export async function removeProject(params) {
  const {...restParams } = params;
  return request(`/api/projects/${params.id}`, {
    method: 'DELETE',
    body: {
      ...restParams
    },
  });
}
export async function addProject(params) {
  const { ...restParams } = params;
  return request(`/api/projects`, {
    method: 'POST',
    body: {
      ...restParams
    },
  });
}
export async function updateProject(params) {
  const { ...restParams } = params;
  return request(`/api/projects/${params.id}`, {
    method: 'PUT',
    body: {
      ...restParams
    },
  });
}

// project assigns
export async function queryProjectAssignList(params) {
  return request(`/api/projectAssigns?${stringify(params)}`);
}

export async function removeProjectAssign(params) {
  const {...restParams } = params;
  return request(`/api/projectAssigns/${params.id}`, {
    method: 'DELETE',
    body: {
      ...restParams
    },
  });
}
export async function addProjectAssign(params) {
  const { ...restParams } = params;
  return request(`/api/projectAssigns`, {
    method: 'POST',
    body: {
      ...restParams
    },
  });
}
export async function updateProjectAssign(params) {
  const { ...restParams } = params;
  return request(`/api/projectAssigns/${params.id}`, {
    method: 'PUT',
    body: {
      ...restParams
    },
  });
}

// brands
export async function queryBrandList(params) {
  return request(`/api/brands?${stringify(params)}`);
}
export async function removeBrand(params) {
  const {...restParams } = params;
  return request(`/api/brands/${params.id}`, {
    method: 'DELETE',
    body: {
      ...restParams
    },
  });
}
export async function addBrand(params) {
  const { ...restParams } = params;
  return request(`/api/brands`, {
    method: 'POST',
    body: {
      ...restParams
    },
  });
}
export async function updateBrand(params) {
  const { ...restParams } = params;
  return request(`/api/brands/${params.id}`, {
    method: 'PUT',
    body: {
      ...restParams
    },
  });
}

// brand types
export async function queryBrandTypeList(params) {
  return request(`/api/brand-categories?${stringify(params)}`);
}
export async function removeBrandType(params) {
  const {...restParams } = params;
  return request(`/api/brand-categories/${params.id}`, {
    method: 'DELETE',
    body: {
      ...restParams
    },
  });
}
export async function addBrandType(params) {
  const { ...restParams } = params;
  return request(`/api/brand-categories`, {
    method: 'POST',
    body: {
      ...restParams
    },
  });
}
export async function updateBrandType(params) {
  const { ...restParams } = params;
  return request(`/api/brand-categories/${params.id}`, {
    method: 'PUT',
    body: {
      ...restParams
    },
  });
}

// tasks
export async function queryTaskList(params) {
  return request(`/api/tasks?${stringify(params)}`);
}
export async function removeTask(params) {
  const {...restParams } = params;
  return request(`/api/tasks/${params.id}`, {
    method: 'DELETE',
    body: {
      ...restParams
    },
  });
}
export async function addTask(params) {
  const { ...restParams } = params;
  return request(`/api/tasks`, {
    method: 'POST',
    body: {
      ...restParams
    },
  });
}
export async function updateTask(params) {
  const { ...restParams } = params;
  return request(`/api/tasks/${params.id}`, {
    method: 'PUT',
    body: {
      ...restParams
    },
  });
}

// API CRUD
export async function queryItems(path,params) {
  return request(`${path}?${stringify(params)}`);
}
export async function removeItem(path,params) {
  const {...restParams } = params;
  return request(`${path}/${params.id}`, {
    method: 'DELETE',
    body: {
      ...restParams
    },
  });
}
export async function addItem(path, params) {
  const { ...restParams } = params;
  return request(`${path}`, {
    method: 'POST',
    body: {
      ...restParams
    },
  });
}
export async function updateItem(path,params) {
  const { ...restParams } = params;
  return request(`${path}/${params.id}`, {
    method: 'PUT',
    body: {
      ...restParams
    },
  });
}

export async function loadItem(path,params) {
  return request(`${path}`, {
    method: 'POST',
    responseType: 'arraybuffer',
    body: params,
  });
}


export async function AccountLogin(params) {
  return request('/api/auth/signin', {
    method: 'POST',
    body: params,
  });
}

export async function fakeRegister(params) {
  return request('/api/register', {
    method: 'POST',
    body: params,
  });
}

export async function queryNotices() {
  return request('/api/notices');
}

export async function getFakeCaptcha(mobile) {
  return request(`/api/captcha?mobile=${mobile}`);
}
