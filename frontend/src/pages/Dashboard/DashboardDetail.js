import React, { PureComponent } from 'react';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import { Card, Row, Col, Progress, Tag } from 'antd';
import { connect } from 'dva';

@connect(({ dashboards }) => ({
  dashboards,
}))

class DashboardDetail extends PureComponent {

  state = {
    selectedClient: ''
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'dashboards/fetchProjCountByBusinessUnitId',
      payload: {
        id: this.props.match.params.id
      },
    });
    dispatch({
      type: 'dashboards/fetchProjCountByClient',
      payload: {
        id: this.props.match.params.id
      },
    });
  }

  doSomethingAwesome(client) {
    const { dispatch } = this.props;
    this.setState({ selectedClient: client.clientName });
    dispatch({
      type: 'dashboards/fetchProjCountByClientDetail',
      payload: {
        businessUnitId: this.props.match.params.id,
        clientId: client.id
      },
    });

  }

  percentaseKeun(val,num) {
    if (val && num) {
      let number = (val/num) * 100;
      return number.toFixed(2);
    } else {
      return 0;
    }
  }

  render() {

    const {
      dashboards: { business, clients, detail },
    } = this.props;

    const pageName = `${business.name} Dashboard Detail`;
    const totalProject = business.projects;

    const tabList = [
      {
        key: 'survey',
        tab: (
          <span>Survey</span>
        ),
      },
      {
        key: 'instalation',
        tab: (
          <span>Instalation</span>
        ),
      },
      {
        key: 'removed',
        tab: (
          <span>Removed</span>
        ),
      }
    ];

    const headerLeft = <Row type="flex" justify="space-between"><Col span={2}>Total Project</Col><Col span={2}><strong>{totalProject}</strong></Col></Row>;
    const headerRight = <Row className="head-client-proj" type="flex" justify="space-between"><Col span={1}>Prev</Col><Col span={4}>{this.state.selectedClient}</Col><Col span={1}>Next</Col></Row>;

    let clientItem = [];

    clients.forEach(item => {
      clientItem.push(
        <div onClick={() => {this.doSomethingAwesome(item)}}>
          <span class="client-proj">{item.clientName}</span>
          <Progress strokeLinecap="square" percent={this.percentaseKeun(item.jumlah,totalProject)} format={percent => <strong class="client-proj-text" >{item.jumlah} / <span class="percent">{percent}%</span></strong>} strokeColor="#C82A1C"/>
        </div>
      );
    });

    let detailItem = [];

    detail.forEach(item => {
      detailItem.push(
        <Col className="gutter-row" span={12}>
          <Card type="inner">
            <Row type="flex" justify="space-between">
              <Col span={8}>
                <Tag className="tag-dashboard">{item.programs}</Tag>
                <h2 class="brand-name">{item.brandName}</h2>
                <p class="store">Total Store: { item.storeNum}</p>
                <p><i>Cycle {item.cycle_start} - {item.cycle_end} {item.cycle_year}</i></p>
              </Col>
              <Col span={8}>
                <Progress type="circle" percent={item.survey_target} strokeColor="#C82A1C"/>
              </Col>
            </Row>
          </Card>
        </Col>
      );
    });

    return (
      <div>
        <PageHeaderWrapper title={pageName}>
          <Row gutter={[{ xs: 8, sm: 16, md: 20, lg: 20 }, 20]}>
            <Col md={6}>
              <Card title={headerLeft}>
                <p>Total Projects per Client</p>
                {clientItem}
              </Card>
            </Col>
            <Col md={18}>
              <Card
                style={{ width: '100%' }}
                tabList={tabList}
                title={headerRight}
              >
                <Row gutter={[{ xs: 8, sm: 16, md: 20, lg: 20 }, 20]} type="flex" justify="space-between">
                  {detailItem}
                </Row>
              </Card>
            </Col>
          </Row>
        </PageHeaderWrapper>
      </div>
    );
  }
}

export default DashboardDetail;
