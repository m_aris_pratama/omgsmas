import React, { PureComponent, Fragment } from 'react';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import router from 'umi/router';
import NavLink from 'umi/navlink';
import { Card, Col, Row, Tag, Icon } from 'antd';
import { connect } from 'dva';

@connect(({ dashboards }) => ({
  dashboards,
}))
class Dashboard extends PureComponent {

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'dashboards/fetchProjCountByBusinessUnit',
      payload: {},
    });
  }

  render() {
    const {
      dashboards: { dashcount },
    } = this.props;

    let items = [];
    dashcount.forEach(item => {
      items.push(
        <NavLink to={`dashboard/detail/${item.id}`}>
          <Col className="gutter-row" span={6}>
            <Card className="bu-card">
              <Tag>{item.name}</Tag>
              <h1 class="head-project">{item.projects}</h1>
              <small>Projects</small>
              <div class="bu-icon">
                <img src="/ic-project.svg" />
              </div>
            </Card>
          </Col>
        </NavLink>
      );
    });

    return (
      <div>
        <PageHeaderWrapper>
          <Row gutter={[{ xs: 8, sm: 16, md: 20, lg: 20 }, 20]}>{items}</Row>
        </PageHeaderWrapper>
      </div>
    );
  }
}

export default Dashboard;
