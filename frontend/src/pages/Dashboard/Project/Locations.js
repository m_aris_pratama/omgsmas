import React, { PureComponent,Fragment } from 'react';
import {
  Card,
  Input,
  AutoComplete
} from 'antd';
import { connect } from 'dva';
import GoogleMap from './components/GoogleMap';
import Marker from './components/Marker';

const Option = AutoComplete.Option;

// Return map bounds based on list of places
const getMapBounds = (map, maps, places) => {
  const bounds = new maps.LatLngBounds();

  places.forEach((place) => {
    bounds.extend(new maps.LatLng(
      place.latitude,
      place.longitude,
    ));
  });
  return bounds;
};

// Re-center map when resizing the window
const bindResizeListener = (map, maps, bounds) => {
  maps.event.addDomListenerOnce(map, 'idle', () => {
    maps.event.addDomListener(window, 'resize', () => {
      map.fitBounds(bounds);
    });
  });
};

// Fit map to its bounds after the api is loaded
const apiIsLoaded = (map, maps, places) => {

  if (places.length>0){
    const bounds = getMapBounds(map, maps, places);
    map.fitBounds(bounds);
    bindResizeListener(map, maps, bounds);
  }
};

@connect(({ dashboards }) => ({
  dashboards,
}))
class LocationView extends PureComponent {

  state = {
    center: [-6.1762385, 106.822843],
    zoom: 6
  };

  componentDidMount() {
    this.refreshPosition()
  }

  refreshPosition = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'dashboards/fetchAbsence',
      payload: {},
    });
    setTimeout(() => this.refreshPosition(),2*60000)
  }


  onSelect = value => {
    const {
      dashboards: { places }
    } = this.props;

    console.log(places);
    const sl = places.find(x => x.id === parseInt(value,10));
    this.setState({center: [sl.latitude,sl.longitude],zoom:16})
  }


  render(){
    const {
      dashboards: { places }
    } = this.props;

    const { center,zoom } = this.state; 

    const options = places.map(val => (
      <Option key={val.id}>
        {val.team.name}
      </Option>
    ))

    const extraContent = (
      <div>
        <AutoComplete
          dataSource={options}
          style={{ width: 200 }}
          onSelect={this.onSelect}
          placeholder="search name"
          filterOption={(inputValue, option) => option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
        />
      </div>
    );

    return (
      <Card 
        bordered={false}
        extra={extraContent}
      >
        <div style={{width:960,height:500}}>
          <GoogleMap
            zoom={zoom}
            center={center}
            bootstrapURLKeys={{
              key: 'AIzaSyB588H1pt2XE372EFK7xdm7Z3mmLXeE9Wg',
              libraries: ['places', 'geometry']
            }}
            yesIWantToUseGoogleMapApiInternals
            onGoogleApiLoaded={({ map, maps }) => apiIsLoaded(map, maps, places)}
          >                 
            {places.map(place => (
              <Marker
                key={place.id}
                title={place.team.name}
                data={place}
                lat={place.latitude}
                lng={place.longitude}
              />
            ))}
          </GoogleMap>
        </div>
      </Card>
      );
    }
}

export default LocationView;
