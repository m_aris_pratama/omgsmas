import React, { PureComponent,Fragment } from 'react';
import {
  Card,
  Table, Input,Divider
} from 'antd';
import { connect } from 'dva';

const { Search } = Input;

@connect(({ dashboards }) => ({
  dashboards,
}))
class ProjectView extends PureComponent {

  columns = [
    {
      title: 'Name',
      dataIndex: 'name',
    },
    {
      title: 'Client',
      dataIndex: 'client',
    },
    {
      title: 'Brand',
      dataIndex: 'brand',
    },
    {
      title: 'Store',
      dataIndex: 'assigns',
    },
    {
      title: 'Program',
      render: (text, record) => (
        record.program.map(x => (<span>{x.name} : {x.total}<br /></span>))
      ),
    }
  ];

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'dashboards/fetchProj',
      payload: {},
    });
  }

  render(){
    const {
      dashboards: { projs }
    } = this.props;

    const extraContent = (
      <div>
        <Search placeholder="search name" onSearch={() => ({})} />
      </div>
    );

    return (
      <Card 
        bordered={false}
      >
        <Table
          dataSource={projs}
          columns={this.columns}
        />
      </Card>
      );
    }
}

export default ProjectView;
