import React from 'react';
import styled from 'styled-components';
import { Popover } from 'antd'
import moment from 'moment';

const Wrapper = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  width: 18px;
  height: 18px;
  background-color: #ff0000;
  border: 2px solid #fff;
  border-radius: 100%;
  user-select: none;
  transform: translate(-50%, -50%);
  cursor: ${props => (props.onClick ? 'pointer' : 'default')};
  &:hover {
    z-index: 1;
  }
`;

const content = (data) => (
  <div>
    <p><strong>{data.absenceType}</strong> at {moment(data.createdAt).local().format('DD MMM YYYY HH:mm:ss')}</p>
    <p>{data.store?data.store.name:''}</p>
    <p><img alt='' src={`/api/absence/photo/${data.photo}`} style={{width:200}} /></p>
  </div>
);

const Marker = (props) => (
  <Popover content={content(props.data)} title={props.title}>
    <Wrapper />
  </Popover>
);

export default Marker;
