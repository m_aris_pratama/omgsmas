import React, { PureComponent, Fragment } from 'react';
import { Card, Table, Input, Divider, Button, Row, Col, Icon, Tabs } from 'antd';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import { connect } from 'dva';
import router from 'umi/router';
import ProjectView from './Projects';
import LocationView from './Locations';
import DashboardView from './Dashboards';

const TabPane = Tabs.TabPane;

@connect(({ projects }) => ({
  list: projects,
}))
class Project extends PureComponent {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'projects/fetch',
      payload: {
        per_page: 10,
      },
    });
  }

  onDetail = key => {
    router.push(`/project/project/detail/${key}`);
  };

  handleTableChange = (pagination, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const params = {
      cur_page: pagination.current,
      per_page: pagination.pageSize,
      ...formValues,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'projects/fetch',
      payload: params,
    });
  };

  render() {
    const {
      list: { list },
    } = this.props;

    return (
      <div>
        <PageHeaderWrapper>
          {/* } <Row gutter={16}>
            <Col span={6}>
              <Card>
                <Row gutter={10}>
                  <Col span={20}>
                    <big><strong>2</strong></big><br />
                    <small>Active Projects</small>
                  </Col>
                  <Col span={4}>
                    <Icon type="project" />
                  </Col>
                </Row>
              </Card>
            </Col>
            <Col span={6}>
              <Card>
                <Row gutter={10}>
                  <Col span={20}>
                    <big><strong>0</strong></big><br />
                    <small>Scheduled Projects</small>
                  </Col>
                  <Col span={4}>
                    <Icon type="schedule" />
                  </Col>
                </Row>
              </Card>
            </Col>
            <Col span={6}>
              <Card>
                <Row gutter={10}>
                  <Col span={20}>
                    <big><strong>2</strong></big><br />
                    <small>Clients</small>
                  </Col>
                  <Col span={4}>
                    <Icon type="contacts" />
                  </Col>
                </Row>
              </Card>
            </Col>
            <Col span={6}>
              <Card>
                <Row gutter={10}>
                  <Col span={20}>
                    <big><strong>6</strong></big><br />
                    <small>Stores</small>
                  </Col>
                  <Col span={4}>
                    <Icon type="shopping-cart" />
                  </Col>
                </Row>
              </Card>
            </Col>
    </Row> { */}
          <Row>
            <DashboardView></DashboardView>
          </Row>
          <Row>
            <Card style={{ marginTop: 0 }} bordered={false}>
              <Tabs defaultActiveKey="1">
                <TabPane tab="Projects" key="1">
                  <ProjectView />
                </TabPane>
                <TabPane tab="Teams" key="2">
                  <LocationView />
                </TabPane>
              </Tabs>
            </Card>
          </Row>
        </PageHeaderWrapper>
      </div>
    );
  }
}

export default Project;
