/* global document */
import React, { PureComponent } from 'react'
import { connect } from 'dva';
import { Form, Button, Input, message } from 'antd';

const FormItem = Form.Item;

@Form.create()
@connect(({ user }) => ({
  user
}))
class FormProfile extends PureComponent {
  formLayout = {
    labelCol: { span: 7 },
    wrapperCol: { span: 13 },
  };

  handleChangeProfileSubmit = () => {
    const { form } = this.props; 
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const { onSubmit } = this.props;
      if (onSubmit) {
        message.info('change profile on process')
        onSubmit(fieldsValue);
      }
    });
  }

  render() {
    const {
      form: { getFieldDecorator },
      current,
    } = this.props;

    return (
      <Form>
        <FormItem label="Name" {...this.formLayout}>
          {getFieldDecorator('firstName', {
        rules: [{ required: true, message: 'Please enter name' }],
        initialValue: current.firstName,
        })(<Input placeholder="name" />)}
        </FormItem>
        <FormItem label="Login Username" {...this.formLayout}>
          {getFieldDecorator('username', {
        rules: [{ required: true, message: 'Please enter login username' }],
        initialValue: current.username?current.username:'',
        })(<Input placeholder="username" disabled />)}
        </FormItem>
        <FormItem label="Email" {...this.formLayout}>
          {getFieldDecorator('email', {
        rules: [{ required: false}],
        initialValue: current.email,
        })(<Input placeholder="name@email" />)}
        </FormItem>
        <FormItem label="group" {...this.formLayout}>
          <Input placeholder={current.groups.map(x=>x.name)} disabled />
        </FormItem>
        <Button type="primary" onClick={this.handleChangeProfileSubmit}>Update Profile</Button>
      </Form>
    );
  }
}

export default FormProfile;
