/* global document */
import React, { PureComponent } from 'react'
import { connect } from 'dva';
import { Form, Button, Input, message } from 'antd';

const FormItem = Form.Item;

@Form.create()
@connect(({ user }) => ({
  user
}))
class FormPass extends PureComponent {
  formLayout = {
    labelCol: { span: 7 },
    wrapperCol: { span: 13 },
  };

  handleChangePasswordSubmit = () => {
    const { form } = this.props; 
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const { onSubmit } = this.props;
      if (onSubmit) {
        message.info('change profile on process')
        onSubmit(fieldsValue);
      }
    });
  }

  render() {
    const {
      form: { getFieldDecorator },
    } = this.props;

    return (
      <Form>
        <FormItem label="Current Password" {...this.formLayout}>
          {getFieldDecorator('currentPassword', {
            rules: [{ required: true, message: 'Please enter current password' }],
            initialValue: '',
        })(<Input placeholder="current password" />)}
        </FormItem>
        <FormItem label="New Password" {...this.formLayout}>
          {getFieldDecorator('newPassword', {
            rules: [{ required: true, message: 'Please enter new password' }],
            initialValue: '',
        })(<Input placeholder="password" />)}
        </FormItem>
        <Button type="primary" onClick={this.handleChangePasswordSubmit}>Change Password</Button>
      </Form>
    );
  }
}

export default FormPass;
