/* global document */
import React, { PureComponent } from 'react'
import { connect } from 'dva';
import { Form,Tabs, Card } from 'antd';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import FormProfile from './FormProfile'
import FormPass from './FormPass'

const TabPane = Tabs.TabPane;

@Form.create()
@connect(({ user }) => ({
  user
}))
class Profile extends PureComponent {
  formLayout = {
    labelCol: { span: 7 },
    wrapperCol: { span: 13 },
  };

  componentDidMount() {
    const { dispatch} = this.props;
    dispatch({
      type: 'user/fetchCurrent'
    });
  }

  handleChangeProfileSubmit = (values) => {
    const { dispatch} = this.props;
    dispatch({
      type: 'user/updateProfile',
      payload: {...values},
    });
  }

  handleChangePasswordSubmit = (values) => {
    const { dispatch} = this.props;
    dispatch({
      type: 'user/updatePass',
      payload: {...values},
    });
  }

  render() {
    const {
      user,
    } = this.props;

    const current = user.currentUser.user;

    return (
      <PageHeaderWrapper>
        <Card>
          <Tabs defaultActiveKey="1">
            <TabPane tab="Profile" key="1">
              <FormProfile current={current} onSubmit={this.handleChangeProfileSubmit} />
            </TabPane>
            <TabPane tab="Change Password" key="2">
              <FormPass onSubmit={this.handleChangePasswordSubmit} />
            </TabPane>
          </Tabs>
        </Card>
      </PageHeaderWrapper>
    );
  }
}

export default Profile;
