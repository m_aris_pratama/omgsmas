/* global document */
import React, { PureComponent } from 'react'
import { connect } from 'dva';
import { Form,Tabs, Card } from 'antd';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import FormProfile from './FormProfile'

const TabPane = Tabs.TabPane;

@Form.create()
@connect(({ user }) => ({
  user
}))
class UserAdd extends PureComponent {
  formLayout = {
    labelCol: { span: 7 },
    wrapperCol: { span: 13 },
  };

  componentDidMount() {
    const { dispatch} = this.props;
    dispatch({
      type: 'user/fetchCurrent'
    });
  }

  submit = (values) => {
    const { dispatch } = this.props;
    const superuser = { isSuperuser: false, isStaff: false, isActive: true, groups: [{ id: 1}] };
    dispatch({
      type: 'user/createUser',
      payload: { ...values, ...superuser },
    });
  }

  render() {
    const {
      user,
    } = this.props;

    const current = user.currentUser.user;

    return (
      <PageHeaderWrapper>
        <Card>
          <Tabs defaultActiveKey="1">
            <TabPane tab="Profile" key="1">
              <FormProfile current={current} onSubmit={this.submit} />
            </TabPane>
          </Tabs>
        </Card>
      </PageHeaderWrapper>
    );
  }
}

export default UserAdd;
