import React, { PureComponent,Fragment } from 'react';
import {
  Card,
  Table, Input,Divider
} from 'antd';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

const { Search } = Input;

class Role extends PureComponent {

  columns = [
    {
      title: 'Name',
      dataIndex: 'name',
    },
    {
      title: 'Type',
      dataIndex: 'retailCategory.name',
    },
    {
      title: 'Actions',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.showEditModal(record)}>Edit</a>
          <Divider type="vertical" />
          <a onClick={() => this.showDeleteModal(record.id)}>Delete</a>
        </Fragment>
      ),
    },
  ];

  render(){

    const metaProps = {
      showSizeChanger: true,
      showQuickJumper: false,
      pageSize: 10,
      total: 1,
      current: 1
    };

    const extraContent = (
      <div>
        <Search placeholder="search name" onSearch={() => ({})} />
      </div>
    );

    return (
      <div>
        <PageHeaderWrapper>
          <Card 
            style={{ marginTop: 24 }}
            bordered={false}
            extra={extraContent}
            title="Roles"
          >
            <Table
              pagination={metaProps}
              columns={this.columns}
            />
          </Card>
        </PageHeaderWrapper>
      </div>
      );
    }
}

export default Role;
