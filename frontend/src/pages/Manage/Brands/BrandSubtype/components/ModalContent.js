/* global document */
import React, { PureComponent } from 'react'
import moment from 'moment'
import { Form, Button, Row, Col, DatePicker, Input, Cascader,Select } from 'antd'

const { Search, TextArea } = Input;
const { Option } = Select;
const FormItem = Form.Item;

class ModalContent extends PureComponent {

  formLayout = {
    labelCol: { span: 7 },
    wrapperCol: { span: 13 },
  };

  handleSubmit = (e) => {
    const { onSubmit } = this.props;
    if (onSubmit) {
      onSubmit(e);
    }
  }

  render() {
    const {
      form: { getFieldDecorator },
      current,
      brandType,
      clients
    } = this.props;

    return (
      <Form onSubmit={this.handleSubmit}>
        <FormItem label="Type" {...this.formLayout}>
          {getFieldDecorator('brandCategory', {
            rules: [{ required: true, message: 'Please enter brand type' }],
            initialValue: current.brandCategory?current.brandCategory.id:'',
          })(
            <Select
              showSearch
              style={{ width: 200 }}
              placeholder="Select a type"
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              {brandType.list.map((val)=><Option value={val.id}>{val.name}</Option>)}
            </Select>
          )}
        </FormItem>
        <FormItem label="Name" {...this.formLayout}>
          {getFieldDecorator('name', {
            rules: [{ required: true, message: 'Please enter brand subcategory name' }],
            initialValue: current.name,
          })(<Input placeholder="name" />)}
        </FormItem>
      </Form>
    );
  }
}

export default ModalContent;
