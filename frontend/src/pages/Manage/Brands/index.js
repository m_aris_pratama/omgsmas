import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Card,
  Row,
  Col
} from 'antd';
import router from 'umi/router';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

import styles from './index.less';

@connect(({ brandSubtype,brandType, brands, loading }) => ({
  brandType,
  brandSubtype,
  brands,
  brandsLoading: loading.effects['brands/fetchAll'],
}))

class Brand extends PureComponent {

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'brandType/fetchAll',
      payload: {
        per_page: 10,
      },
    });
    dispatch({
      type: 'brands/fetchAll',
      payload: {
        per_page: 10,
      },
    });
    dispatch({
      type: 'brandSubtype/fetchAll',
      payload: {
        per_page: 10,
      },
    });
  }

  onTabChange = key => {
    const { match } = this.props;
    switch (key) {
      case 'list':
        router.push(`${match.url}/list`);
        break;
      case 'types':
        router.push(`${match.url}/types`);
        break;
        case 'subtypes':
        router.push(`${match.url}/subtypes`);
        break;
      default:
        break;
    }
  };

  render() {
    const {
      brandType,
      brandSubtype,
      brands,
      brandsLoading,
      match,
      children,
      location
    } = this.props;

    const brandsTabList = [
      {
        key: 'list',
        tab: (
          <span>Brand List</span>
        ),
      },
      {
        key: 'subtypes',
        tab: (
          <span>Brand Subcategories</span>
        ),
      },
      {
        key: 'types',
        tab: (
          <span>Brand Categories</span>
        ),
      }
    ];

    const Info = ({ title, value, bordered }) => (
      <div className={styles.headerInfo}>
        <span>{title}</span>
        <p>{value}</p>
        {bordered && <em />}
      </div>
    );
  
    return (
      <PageHeaderWrapper>
        <div className={styles.standardList}>
          <Card bordered={false}>
            <Row>
              <Col sm={8} xs={24}>
                <Info loading={brandsLoading} title="Total Brands" value={brands.total} bordered />
              </Col>
              <Col sm={8} xs={24}>
                <Info title="Brand Categories" value={brandType.total} bordered />
              </Col>
              <Col sm={8} xs={24}>
                <Info title="Brand Subcategories" value={brandSubtype.total} bordered />
              </Col>
            </Row>
          </Card>
          <Card
            className={styles.tabsCard}
            style={{ marginTop: 24 }}
            bordered={false}
            tabList={brandsTabList}
            activeTabKey={location.pathname.replace(`${match.path}/`, '')}
            onTabChange={this.onTabChange}
          >
            {children}
          </Card>
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default Brand;
