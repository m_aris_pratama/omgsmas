/* global document */
import React, { PureComponent } from 'react'
import moment from 'moment'
import { Form, Button, Row, Col, DatePicker, Input, Cascader,Select,Divider } from 'antd'

const { Search, TextArea } = Input;
const { Option } = Select;
const FormItem = Form.Item;

class ModalContent extends PureComponent {

  formLayout = {
    labelCol: { span: 7 },
    wrapperCol: { span: 13 },
  };

  handleSubmit = (e) => {
    const { onSubmit } = this.props;
    if (onSubmit) {
      onSubmit(e);
    }
  }

  render() {
    const {
      form: { getFieldDecorator },
      current,
      brandSubtype,
      clients
    } = this.props;

    return (
      <Form onSubmit={this.handleSubmit}>
        <Row>
          <Col span={12}>
            <FormItem label="Client" {...this.formLayout}>
              {getFieldDecorator('client', {
                rules: [{ required: true, message: 'Please enter client' }],
                initialValue: current.client?current.client.id:'',
              })(
                <Select
                  showSearch
                  style={{ width: 200 }}
                  placeholder="Select a Client"
                  optionFilterProp="children"
                  filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                >
                  {clients.list.map((val)=><Option value={val.id}>{val.name}</Option>)}
                </Select>
              )}
            </FormItem>
            <FormItem label="Name" {...this.formLayout}>
              {getFieldDecorator('name', {
                rules: [{ required: true, message: 'Please enter brand name' }],
                initialValue: current.name,
              })(<Input placeholder="name" />)}
            </FormItem>
            <FormItem label="Subcategory" {...this.formLayout}>
              {getFieldDecorator('brandSubcategory', {
                rules: [{ required: true, message: 'Please enter brand subcategory' }],
                initialValue: current.brandSubcategory?current.brandSubcategory.id:'',
              })(
                <Select
                  showSearch
                  style={{ width: 200 }}
                  placeholder="Select a subcategory"
                  optionFilterProp="children"
                  filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                >
                  {brandSubtype.list.map((val)=><Option value={val.id}>{val.name}</Option>)}
                </Select>
              )}
            </FormItem>
            <FormItem {...this.formLayout} label="Address">
              {getFieldDecorator('address', {
                rules: [{ message: 'please enter address', min: 5 }],
                initialValue: current.address,
              })(<TextArea rows={4} placeholder="address" />)}
            </FormItem>
            <FormItem label="Telp" {...this.formLayout}>
              {getFieldDecorator('telp', {
                rules: [{ required: false}],
                initialValue: current.telp,
              })(<Input placeholder="08..." />)}
            </FormItem>
            <FormItem label="Fax" {...this.formLayout}>
              {getFieldDecorator('fax', {
                rules: [{ required: false}],
                initialValue: current.fax,
              })(<Input placeholder="08..." />)}
            </FormItem>
            <FormItem label="Email" {...this.formLayout}>
              {getFieldDecorator('email', {
                rules: [{ required: false}],
                initialValue: current.email,
              })(<Input placeholder="name@email" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <Divider orientation="left">Contact Person 1</Divider>
            <FormItem label="Name" {...this.formLayout}>
              {getFieldDecorator('contact_person', {
                rules: [{ required: false}],
                initialValue: current.contact_person,
              })(<Input placeholder="name" />)}
            </FormItem>
            <FormItem label="Telp" {...this.formLayout}>
              {getFieldDecorator('contact_person_telp', {
                rules: [{ required: false}],
                initialValue: current.contact_person_telp,
              })(<Input placeholder="08..." />)}
            </FormItem>
            <FormItem label="Position" {...this.formLayout}>
              {getFieldDecorator('contact_person_position', {
                rules: [{ required: false}],
                initialValue: current.contact_person_position,
              })(<Input placeholder="position" />)}
            </FormItem>
            <FormItem label="Email" {...this.formLayout}>
              {getFieldDecorator('contact_person_email', {
                rules: [{ required: false}],
                initialValue: current.contact_person_email,
              })(<Input placeholder="name@email" />)}
            </FormItem>
            <Divider orientation="left">Contact Person 2</Divider>
            <FormItem label="Name" {...this.formLayout}>
              {getFieldDecorator('contact_person2', {
                rules: [{ required: false}],
                initialValue: current.contact_person2,
              })(<Input placeholder="name" />)}
            </FormItem>
            <FormItem label="Telp" {...this.formLayout}>
              {getFieldDecorator('contact_person2_telp', {
                rules: [{ required: false}],
                initialValue: current.contact_person2_telp,
              })(<Input placeholder="08..." />)}
            </FormItem>
            <FormItem label="Position" {...this.formLayout}>
              {getFieldDecorator('contact_person2_position', {
                rules: [{ required: false}],
                initialValue: current.contact_person2_position,
              })(<Input placeholder="position" />)}
            </FormItem>
            <FormItem label="Email" {...this.formLayout}>
              {getFieldDecorator('contact_person2_email', {
                rules: [{ required: false}],
                initialValue: current.contact_person2_email,
              })(<Input placeholder="name@email" />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default ModalContent;
