/* global document */
import React, { PureComponent } from 'react'
import moment from 'moment'
import { Form, Button, Row, Col, DatePicker, Input, Cascader,Select } from 'antd'

const { Search, TextArea } = Input;
const { Option } = Select;


@Form.create()
class Filter extends PureComponent {

  handleTypeChange = value => {
    const { onTypeChange } = this.props;
    if (onTypeChange) {
      onTypeChange(value);
    }
  }

  handleSubtypeChange = value => {
    const { onSubtypeChange } = this.props;
    if (onSubtypeChange) {
      onSubtypeChange(value);
    }
  }

  handleClientChange = value => {
    const { onClientChange } = this.props;
    if (onClientChange) {
      onClientChange(value);
    }
  }

  handleSearch = (value) => {
    const { onSearch } = this.props;
    if (onSearch) {
      onSearch(value);
    }
  }

  render() {
    const {
      filters,
      clients,
      brandType,
      brandSubtype
    } = this.props;

    return (
      <div style={{marginBottom:24}}>
        <Row>
          <Col span={18}>
            <span>Client : </span>
            <Select
              showSearch
              style={{ width: 150 }}
              placeholder="Select a Client"
              optionFilterProp="children"
              onSelect={this.handleClientChange}
              value={filters.clientId}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value={0}>All</Option>
              {clients.list.map((val)=><Option value={val.id}>{val.name}</Option>)}
            </Select>
            <span style={{marginLeft:16}}>Category : </span>
            <Select
              showSearch
              style={{ width: 150 }}
              placeholder="Select a Category"
              optionFilterProp="children"
              onChange={this.handleTypeChange}
              value={filters.typeId}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value={0}>All</Option>
              {brandType.list.map((val)=><Option value={val.id}>{val.name}</Option>)}
            </Select>
            <span style={{marginLeft:16}}>Subcategory : </span>
            <Select
              showSearch
              style={{ width: 150 }}
              placeholder="Select a Subcategory"
              optionFilterProp="children"
              onChange={this.handleSubtypeChange}
              value={filters.subtypeId}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value={0}>All</Option>
              {brandSubtype.list.map((val)=><Option value={val.id}>{val.name}</Option>)}
            </Select>
          </Col>
          <Col span={6}>
            <Search onSearch={this.handleSearch} justify="end" placeholder="search name" />
          </Col>
        </Row>
      </div>
    );
  }
}

export default Filter;
