import React, { PureComponent } from 'react';
import {notification, Card, Icon, Button, Form, Modal,message,Upload,Col,Row} from 'antd';
import { connect } from 'dva';
import styles from './index.less';

import Filter from './components/Filter';
import List from './components/List';
import ModalContent from './components/ModalContent';

@Form.create()
@connect(({ stores,retails,areas,cities,teams }) => ({
  stores,
  retails,
  areas,
  cities,
  teams
}))
class RetailList extends PureComponent {

  state = { visible: false, 
    current: {}
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'retails/fetch',
      payload: {per_page: 0}
    });
    dispatch({
      type: 'areas/fetch',
      payload: {per_page: 0}
    });
    dispatch({
      type: 'cities/fetch',
      payload: {per_page: 0}
    });
    dispatch({
      type: 'teams/fetch',
      payload: {per_page: 0}
    });
  }

  showAddModal = () => {
    this.setState({
      visible: true,
      current: {},
    });
  };

  showEditModal = item => {
    this.setState({
      visible: true,
      current: item,
    });
  };

  showDeleteModal = (id) => {
    Modal.confirm({
      title: 'Delete',
      content: 'Are you sure to delete this item?',
      okText: 'Confirm',
      cancelText: 'Cancel',
      onOk: () => this.deleteItem(id),
    });
  };

  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { dispatch, form } = this.props;
    const { current } = this.state;
    const id = current ? current.id : 0;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      dispatch({
        type: 'stores/submit',
        payload: { id, ...fieldsValue },
      });
      this.setState({
        visible: false,
      });
      this.openNotification();
    });
  };

  deleteItem = id => {
    const { dispatch } = this.props;
    this.setState({
      visible: true
    });
    dispatch({
      type: 'stores/submit',
      payload: { id },
    }); 
    this.setState({
      visible: false,
    });
    this.openNotification();
  };

  handlRetailChange = value => {
   const { dispatch, retails: {filters}} = this.props;
    const params = {
      cur_page: 1,
      per_page: 10
    };
    if (value > 0) params.retail_id = value;
    filters.retailId = value;
    filters.areaId = 0;
    filters.cityId = 0;
    dispatch({
      type: 'stores/saveFilter',
      payload: filters
    });
   dispatch({
      type: 'stores/fetch',
      payload: params
    });
  }

  handleAreaChange = value => {
    const { dispatch,  stores: { filters } } = this.props;
     const params = {
       cur_page: 1,
       per_page: 10
     };
     if (value > 0) params.area_id = value;
     if (filters.retailId > 0) params.retail_id = filters.retailId;
     filters.areaId = value
     filters.cityId = 0;
     dispatch({
      type: 'stores/saveFilter',
      payload: filters
    });
    dispatch({
       type: 'stores/fetch',
       payload: params
    });
  }

  handleCityChange = value => {
    const { dispatch,  stores: { filters } } = this.props;
     const params = {
       cur_page: 1,
       per_page: 10
     };
     if (value > 0) params.city_id = value;
     if (filters.retailId > 0) params.retail_id = filters.retailId;
     if (filters.areaId > 0) params.area_id = filters.areaId;
     filters.cityId = value
     dispatch({
      type: 'stores/saveFilter',
      payload: filters
    });
    dispatch({
       type: 'stores/fetch',
       payload: params
    });
  }

  handleSearch = value => {
    const { dispatch,  stores: { filters } } = this.props;
    const params = {
      cur_page: 1,
      per_page: 10
    };
    
    params.q = value;
    filters.retailId = 0;
    filters.areaId = 0;
    filters.cityId = 0;
    dispatch({
     type: 'stores/saveFilter',
     payload: filters
    });
    dispatch({
      type: 'stores/fetch',
      payload: params
    });
  }
   
  handleTableChange = (pagination, filtersArg, sorter) => {
    
    const { dispatch, stores: { filters } } = this.props;
    const params = {
      cur_page: pagination.current,
      per_page: pagination.pageSize
    };
    if (filters.cityId > 0) params.city_id = filters.cityId;
    if (filters.retailId > 0) params.retail_id = filters.retailId;
    if (filters.areaId > 0) params.area_id = filters.areaId;
    if (sorter && sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'stores/fetch',
      payload: params,
    });

  };

  openNotification = () => {
    notification.open({
      message: 'Successful Operation',
      description: 'Retail table success to changed',
      icon: <Icon type="smile" style={{ color: '#108ee9' }} />,
    });
  };

  onUpload = (info) => {
    const { dispatch } = this.props;
    if (info.file.status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === 'done') {
      dispatch({
        type: 'stores/fetch',
        payload: { cur_page: 1,
          per_page: 10}
      });
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  }

  onDownload = () => {
    window.location = '/api/stores/exportData/1'
     message.info('file downloaded successfully')
  }

  render() {
    const {
      stores: { list, meta, filters },
      loading,
      areas,
      retails,
      cities,
      teams,
      form
    } = this.props;

    const { visible, current } = this.state;

    const modalFooter = { okText: 'Save', onOk: this.handleSubmit, onCancel: this.handleCancel };

    const metaProps = {
      showSizeChanger: true,
      showQuickJumper: false,
      pageSize: meta.perPage,
      total: meta.totalResults,
      current: meta.curPage
    };
    
    return (
      <div>
        <Card
          bordered={false}
        >
          <Modal
            title={`Store ${current.id ? 'Edit' : 'Add'}`}
            className={styles.standardListForm}
            width={860}
            bodyStyle={{ padding: '28px 0 0' }}
            destroyOnClose
            visible={visible}
            {...modalFooter}
          >
            <ModalContent 
              form={form}
              current={current}
              retails={retails}
              cities={cities}
              areas={areas}
              teams={teams}
              onSubmit={this.handleSubmit}
            />
          </Modal>
          <Filter 
            filters={filters}
            cities={cities}
            retails={retails}
            areas={areas}
            onRetailChange={this.handlRetailChange}
            onAreaChange={this.handleAreaChange}
            onCityChange={this.handleCityChange}
            onSearch={this.handleSearch}
          />
          <Row style={{ marginBottom: 10}}>
            <Col span={16}>
              <Button
                type="dashed"
                style={{ width: '98%' }}
                icon="plus"
                onClick={this.showAddModal}
              >
              Add new Store
              </Button>
            </Col>
            <Col span={4}>
              <Upload
                name='file'
                action='/api/stores/importData'
                onChange={this.onUpload}
              >
                <Button
                  type="primary"
                  icon="upload"
                >
                Import Data
                </Button>
              </Upload>
            </Col>
            <Col span={4}>
              <Button
                type="primary"
                icon="download"
                style={{width:'100%'}}
                onClick={this.onDownload}
              >
              Export Data
              </Button>
            </Col>
          </Row>
          <List 
            list={list}
            metaProps={metaProps}
            loading={loading}
            onTableChange={this.handleTableChange}
            onEdit={this.showEditModal}
            onDelete={this.showDeleteModal}
          />
        </Card>
      </div>
    );
  }
}

export default RetailList;
