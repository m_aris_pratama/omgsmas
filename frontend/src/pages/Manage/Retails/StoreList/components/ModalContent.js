/* global document */
import React, { PureComponent } from 'react'
import moment from 'moment'
import { Form, Button, Row, Col, DatePicker, Input, Cascader,Select,Upload, Divider,Icon } from 'antd'

import isEmpty from 'lodash';
import GoogleMap from './GoogleMap';
import AutoComplete from './AutoComplete';
import Marker from './Marker';

const { Search, TextArea } = Input;
const { Option } = Select;
const FormItem = Form.Item;

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

class ModalContent extends PureComponent {

  formLayout = {
    labelCol: { span: 7 },
    wrapperCol: { span: 13 },
  };

  state = {
    areaId: 0,
    mapApiLoaded: false,
    mapInstance: null,
    mapApi: null,
    place: null,
    lat: -6.1762385,
    lng: 106.822843,
    center: [-6.1762385, 106.822843],
    draggable:true
  };

  componentDidMount() {
    const { current } = this.props;
    this.setState({imageName: current.avatar})
  }


  handleChange = (info) => {
    console.log(info);
    if (info.file.response) this.setState({imageName: info.file.response.fileName});
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl => this.setState({
        imageUrl,
        loading: false,
      }));
    }
  }

  onMouseDown = (childKey, childProps, mouse) => {
    this.setState({ draggable: false });
  }

  onMouseMove = (childKey, childProps, mouse) => {
    this.setState({
      lat:mouse.lat,
      lng:mouse.lng
    });
  }

  onMouseUp = (childKey, childProps, mouse) => {
    this.setState({
      lat:mouse.lat,
      lng:mouse.lng,
      center: mouse,
    })
  }

  apiHasLoaded = (map, maps) => {
    const {
      current
    } = this.props;

    this.setState({
      mapApiLoaded: true,
      mapInstance: map,
      mapApi: maps
    });

    this.setState({
      center:[current.latitude,current.longitude],
      lng: current.longitude,
      lat: current.latitude
    })
  };

  handleSubmit = (e) => {
    const { onSubmit } = this.props;
    if (onSubmit) {
      onSubmit(e);
    }
  }

  addPlace = (place) => {
    this.setState({ place,
      lng: place.geometry.location.lng(),
      lat: place.geometry.location.lat()})
  };

  render() {
    const {
      form: { getFieldDecorator },
      current,
      retails,
      areas,
      cities,
      teams
    } = this.props;

    const {loading,imageUrl,imageName} = this.state;

    const uploadButton = (
      <div>
        <Icon type={loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );

    const {areaId,
      place, mapApiLoaded, mapInstance, mapApi,lng,lat,center,draggable} = this.state;

    let lcities = [];
    if (areaId > 0)
      lcities = cities.list.filter(val => val.area.id === areaId);
    else  lcities = cities.list;

    return (
      <Form onSubmit={this.handleSubmit}>
        <Row>
          <Col span={12}>
            <FormItem label="Avatar" {...this.formLayout}>
              {getFieldDecorator('avatar', {
                rules: [{ required: false }],
                initialValue: imageName,
              })(<Input hidden />)}
              <Upload
                name="file"
                listType="picture-card"
                className="avatar-uploader"
                showUploadList={false}
                action="/api/clients/uploadImage"
                onChange={this.handleChange}
              >{imageUrl || imageName ? <img style={{ width:150}} src={imageUrl?imageUrl:`/api/clients/uploadImage/${imageName}`} alt="" /> : uploadButton}
              </Upload>
            </FormItem>
            <FormItem label="Retail" {...this.formLayout}>
              {getFieldDecorator('retail', {
              rules: [{ required: true, message: 'Please enter retail' }],
              initialValue: current.retail?current.retail.id:'',
            })(
              <Select
                showSearch
                style={{ width: 200 }}
                placeholder="Select a Client"
                optionFilterProp="children"
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              >
                {retails.list.map((val)=><Option value={val.id}>{val.name}</Option>)}
              </Select>
            )}
            </FormItem>
            <FormItem label="Area" {...this.formLayout}>
              {getFieldDecorator('area', {
            rules: [{ required: true, message: 'Please enter area' }],
            initialValue: current.city?current.city.area.id:'',
          })(
            <Select
              showSearch
              style={{ width: 200 }}
              placeholder="Select a Area"
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              onSelect={(v) => this.setState({areaId: v})}
            >
              {areas.list.map((val)=><Option value={val.id}>{val.name}</Option>)}
            </Select>
          )}
            </FormItem>
            <FormItem label="City" {...this.formLayout}>
              {getFieldDecorator('city', {
              rules: [{ required: true, message: 'Please enter city' }],
              initialValue: current.city?current.city.id:'',
            })(
              <Select
                showSearch
                style={{ width: 200 }}
                placeholder="Select a City"
                optionFilterProp="children"
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              >
                {lcities.map((val)=><Option value={val.id}>{val.name}</Option>)}
              </Select>
            )}
            </FormItem>
            <FormItem label="Code" {...this.formLayout}>
              {getFieldDecorator('code', {
              rules: [{ required: true, message: 'Please enter store code' }],
              initialValue: current.code,
            })(<Input placeholder="code" />)}
            </FormItem>
            <FormItem label="Name" {...this.formLayout}>
              {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Please enter store name' }],
              initialValue: current.name,
            })(<Input placeholder="name" />)}
            </FormItem>
            <FormItem label="Team Lead" {...this.formLayout}>
              {getFieldDecorator('lead', {
              rules: [{ required: false}],
              initialValue: current.lead?current.lead.id:'',
            })(
              <Select
                showSearch
                style={{ width: 200 }}
                placeholder="Select a Team Lead"
                optionFilterProp="children"
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              >
                {teams.list.filter(x => x.position === 'lead').map((val)=><Option value={val.id}>{val.name}</Option>)}
              </Select>
            )}
            </FormItem>
            <FormItem label="Team Member" {...this.formLayout}>
              {getFieldDecorator('member', {
              rules: [{ required: false}],
              initialValue: current.member?current.member.id:'',
            })(
              <Select
                showSearch
                style={{ width: 200 }}
                placeholder="Select a Team Member"
                optionFilterProp="children"
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              >
                {teams.list.filter(x => x.position === 'member').map((val)=><Option value={val.id}>{val.name}</Option>)}
              </Select>
            )}
            </FormItem>
            <FormItem {...this.formLayout} label="Address">
              {getFieldDecorator('address', {
                rules: [{ message: 'please enter address', min: 5 }],
                initialValue: current.address,
              })(<TextArea rows={4} placeholder="address" />)}
            </FormItem>
            <FormItem label="Telp" {...this.formLayout}>
              {getFieldDecorator('telp', {
                rules: [{ required: false}],
                initialValue: current.telp,
              })(<Input placeholder="08..." />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <div>
              {mapApiLoaded && (
                <AutoComplete map={mapInstance} mapApi={mapApi} addplace={this.addPlace} />
              )}
            </div>
            <div style={{width:380,height:500}}>
              <GoogleMap
                defaultZoom={14}
                draggable={draggable}
                onChildMouseDown={this.onMouseDown}
                onChildMouseMove={this.onMouseMove}
                onChildMouseUp={this.onMouseUp}
                center={center}
                bootstrapURLKeys={{
                  key: 'AIzaSyB588H1pt2XE372EFK7xdm7Z3mmLXeE9Wg',
                  libraries: ['places', 'geometry']
                }}
                yesIWantToUseGoogleMapApiInternals
                onGoogleApiLoaded={({ map, maps }) => this.apiHasLoaded(map, maps)}
              >
                <Marker
                //  key={place.id}
                //  text={place.name}
                  lat={lat}
                  lng={lng}
                />
              </GoogleMap>
            </div>
            <FormItem label="Longitude" {...this.formLayout}>
              {getFieldDecorator('longitude', {
                rules: [{ required: true, message: '' }],
                initialValue: lng,
              })(<Input placeholder="" />)}
            </FormItem>
            <FormItem label="Latitude" {...this.formLayout}>
              {getFieldDecorator('latitude', {
                rules: [{ required: true, message: '' }],
                initialValue: lat,
              })(<Input placeholder="" />)}
            </FormItem>
            <Divider orientation="left">Contact Person 1</Divider>
            <FormItem label="Name" {...this.formLayout}>
              {getFieldDecorator('contact_person', {
                rules: [{ required: false}],
                initialValue: current.contact_person,
              })(<Input placeholder="name" />)}
            </FormItem>
            <FormItem label="Telp" {...this.formLayout}>
              {getFieldDecorator('contact_person_telp', {
                rules: [{ required: false}],
                initialValue: current.contact_person_telp,
              })(<Input placeholder="08..." />)}
            </FormItem>
            <FormItem label="Position" {...this.formLayout}>
              {getFieldDecorator('contact_person_position', {
                rules: [{ required: false}],
                initialValue: current.contact_person_position,
              })(<Input placeholder="position" />)}
            </FormItem>
            <FormItem label="Email" {...this.formLayout}>
              {getFieldDecorator('contact_person_email', {
                rules: [{ required: false}],
                initialValue: current.contact_person_email,
              })(<Input placeholder="name@email" />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default ModalContent;
