/* global document */
import React, { PureComponent } from 'react'
import moment from 'moment'
import { Form, Button, Row, Col, DatePicker, Input, Cascader,Select } from 'antd'

const { Search, TextArea } = Input;
const { Option } = Select;


@Form.create()
class Filter extends PureComponent {

  handleRetailChange = value => {
    const { onRetailChange } = this.props;
    if (onRetailChange) {
      onRetailChange(value);
    }
  }

  handleAreaChange = value => {
    const { onAreaChange } = this.props;
    if (onAreaChange) {
      onAreaChange(value);
    }
  }

  handleCityChange = value => {
    const { onCityChange } = this.props;
    if (onCityChange) {
      onCityChange(value);
    }
  }

  handleSearch = (value) => {
    const { onSearch } = this.props;
    if (onSearch) {
      onSearch(value);
    }
  }

  render() {
    const {
      filters,
      retails,
      areas,
      cities
    } = this.props;

    let lcities = [];
    if (filters.areaId >0)
      lcities = cities.list.filter(val => val.area.id === filters.areaId);
    else  lcities = cities.list;

    return (
      <div style={{marginBottom:24}}>
        <Row>
          <Col span={18}>
            <span>Retail : </span>
            <Select
              showSearch
              style={{ width: 150 }}
              placeholder="Select a Retail"
              optionFilterProp="children"
              onSelect={this.handleRetailChange}
              value={filters.retailId}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value={0}>All</Option>
              {retails.list.map((val)=><Option value={val.id}>{val.name}</Option>)}
            </Select>
            <span style={{marginLeft:16}}>Area : </span>
            <Select
              showSearch
              style={{ width: 150 }}
              placeholder="Select a Area"
              optionFilterProp="children"
              onChange={this.handleAreaChange}
              value={filters.areaId}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value={0}>All</Option>
              {areas.list.map((val)=><Option value={val.id}>{val.name}</Option>)}
            </Select>
            <span style={{marginLeft:16}}>City : </span>
            <Select
              showSearch
              style={{ width: 150 }}
              placeholder="Select a City"
              optionFilterProp="children"
              onChange={this.handleCityChange}
              value={filters.cityId}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value={0}>All</Option>
              {lcities.map((val)=><Option value={val.id}>{val.name}</Option>)}
            </Select>
          </Col>
          <Col span={6}>
            <Search onSearch={this.handleSearch} justify="end" placeholder="search name" />
          </Col>
        </Row>
      </div>
    );
  }
}

export default Filter;
