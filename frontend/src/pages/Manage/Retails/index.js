import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Card,
  Row,
  Col
} from 'antd';
import router from 'umi/router';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

import styles from './index.less';

@connect(({ stores,retailType, retails, loading }) => ({
  retailType,
  stores,
  retails,
  retailsLoading: loading.effects['retails/fetchAll'],
}))

class Retail extends PureComponent {

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'retailType/fetchAll',
      payload: {
        per_page: 10,
      },
    });
    dispatch({
      type: 'retails/fetchAll',
      payload: {
        per_page: 10,
      },
    });
    dispatch({
      type: 'stores/fetchAll',
      payload: {
        per_page: 10,
      },
    });
  }

  onTabChange = key => {
    const { match } = this.props;
    switch (key) {
      case 'list':
        router.push(`${match.url}/list`);
        break;
      case 'types':
        router.push(`${match.url}/types`);
        break;
        case 'stores':
        router.push(`${match.url}/stores`);
        break;
      default:
        break;
    }
  };

  render() {
    const {
      retailType,
      stores,
      retails,
      retailsLoading,
      match,
      children,
      location
    } = this.props;

    const retailsTabList = [
      {
        key: 'list',
        tab: (
          <span>Retail List</span>
        ),
      },
      {
        key: 'stores',
        tab: (
          <span>Stores</span>
        ),
      },
      {
        key: 'types',
        tab: (
          <span>Retail Categories</span>
        ),
      }
    ];

    const Info = ({ title, value, bordered }) => (
      <div className={styles.headerInfo}>
        <span>{title}</span>
        <p>{value}</p>
        {bordered && <em />}
      </div>
    );
  
    return (
      <PageHeaderWrapper>
        <div className={styles.standardList}>
          <Card bordered={false}>
            <Row>
              <Col sm={8} xs={24}>
                <Info loading={retailsLoading} title="Total Retails" value={retails.total} bordered />
              </Col>
              <Col sm={8} xs={24}>
                <Info title="Stores" value={stores.total} bordered />
              </Col>
              <Col sm={8} xs={24}>
                <Info title="Retail Categories" value={retailType.total} bordered />
              </Col>
            </Row>
          </Card>
          <Card
            className={styles.tabsCard}
            style={{ marginTop: 24 }}
            bordered={false}
            tabList={retailsTabList}
            activeTabKey={location.pathname.replace(`${match.path}/`, '')}
            onTabChange={this.onTabChange}
          >
            {children}
          </Card>
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default Retail;
