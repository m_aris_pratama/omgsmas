/* global document */
import React, { PureComponent } from 'react'
import moment from 'moment'
import { Form, Button, Row, Col, DatePicker, Input, Cascader,Select,Divider } from 'antd'

const { Search, TextArea } = Input;
const { Option } = Select;
const FormItem = Form.Item;

class ModalContent extends PureComponent {

  formLayout = {
    labelCol: { span: 7 },
    wrapperCol: { span: 13 },
  };

  handleSubmit = (e) => {
    const { onSubmit } = this.props;
    if (onSubmit) {
      onSubmit(e);
    }
  }

  render() {
    const {
      form: { getFieldDecorator },
      current,
      retailType,
      retailUnit
    } = this.props;

    return (
      <Form onSubmit={this.handleSubmit}>
        <Row>
          <Col span={12}>
            <FormItem label="Business Unit" {...this.formLayout}>
              {getFieldDecorator('businessUnit', {
                  rules: [{ required: true, message: 'Please enter business unit' }],
                  initialValue: current.businessUnit?current.businessUnit.id:'',
                })(
                  <Select
                    showSearch
                    style={{ width: 200 }}
                    placeholder="Select a business unit"
                    optionFilterProp="children"
                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  >
                    {retailUnit.list.map((val)=><Option value={val.id}>{val.name}</Option>)}
                  </Select>
                )}
            </FormItem>
            <FormItem label="Category" {...this.formLayout}>
              {getFieldDecorator('retailCategory', {
                rules: [{ required: true, message: 'Please enter retail category' }],
                initialValue: current.retailCategory?current.retailCategory.id:'',
              })(
                <Select
                  showSearch
                  style={{ width: 200 }}
                  placeholder="Select a category"
                  optionFilterProp="children"
                  filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                >
                  {retailType.list.map((val)=><Option value={val.id}>{val.name}</Option>)}
                </Select>
              )}
            </FormItem>
            <FormItem label="Code" {...this.formLayout}>
              {getFieldDecorator('code', {
                rules: [{ required: true, message: 'Please enter retail code' }],
                initialValue: current.code,
              })(<Input placeholder="code" />)}
            </FormItem>
            <FormItem label="Name" {...this.formLayout}>
              {getFieldDecorator('name', {
                rules: [{ required: true, message: 'Please enter retail name' }],
                initialValue: current.name,
              })(<Input placeholder="name" />)}
            </FormItem>
            <FormItem label="Company" {...this.formLayout}>
              {getFieldDecorator('company', {
                rules: [{ required: false }],
                initialValue: current.company,
              })(<Input placeholder="company name" />)}
            </FormItem>
            <FormItem label="NPWP" {...this.formLayout}>
              {getFieldDecorator('npwp', {
                rules: [{ required: false}],
                initialValue: current.npwp,
              })(<Input placeholder="00000" />)}
            </FormItem>
            <FormItem {...this.formLayout} label="Address">
              {getFieldDecorator('address', {
                rules: [{ message: 'please enter address', min: 5 }],
                initialValue: current.address,
              })(<TextArea rows={4} placeholder="address" />)}
            </FormItem>
            <FormItem label="Telp" {...this.formLayout}>
              {getFieldDecorator('telp', {
                rules: [{ required: false}],
                initialValue: current.telp,
              })(<Input placeholder="08..." />)}
            </FormItem>
            <FormItem label="Fax" {...this.formLayout}>
              {getFieldDecorator('fax', {
                rules: [{ required: false}],
                initialValue: current.fax,
              })(<Input placeholder="08..." />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <Divider orientation="left">Contact Person</Divider>
            <FormItem label="Name" {...this.formLayout}>
              {getFieldDecorator('contact_person', {
                rules: [{ required: false}],
                initialValue: current.contact_person,
              })(<Input placeholder="name" />)}
            </FormItem>
            <FormItem label="Telp" {...this.formLayout}>
              {getFieldDecorator('contact_person_telp', {
                rules: [{ required: false}],
                initialValue: current.contact_person_telp,
              })(<Input placeholder="08..." />)}
            </FormItem>
            <FormItem label="Position" {...this.formLayout}>
              {getFieldDecorator('contact_person_position', {
                rules: [{ required: false}],
                initialValue: current.contact_person_position,
              })(<Input placeholder="position" />)}
            </FormItem>
            <FormItem label="Email" {...this.formLayout}>
              {getFieldDecorator('contact_person_email', {
                rules: [{ required: false}],
                initialValue: current.contact_person_email,
              })(<Input placeholder="name@email" />)}
            </FormItem>
            <Divider orientation="left">Contact Project</Divider>
            <FormItem label="Name" {...this.formLayout}>
              {getFieldDecorator('contact_project', {
                rules: [{ required: false}],
                initialValue: current.contact_project,
              })(<Input placeholder="name" />)}
            </FormItem>
            <FormItem label="Telp" {...this.formLayout}>
              {getFieldDecorator('contact_project_telp', {
                rules: [{ required: false}],
                initialValue: current.contact_project_telp,
              })(<Input placeholder="08..." />)}
            </FormItem>
            <FormItem label="Position" {...this.formLayout}>
              {getFieldDecorator('contact_project_position', {
                rules: [{ required: false}],
                initialValue: current.contact_project_position,
              })(<Input placeholder="position" />)}
            </FormItem>
            <FormItem label="Email" {...this.formLayout}>
              {getFieldDecorator('contact_project_email', {
                rules: [{ required: false}],
                initialValue: current.contact_project_email,
              })(<Input placeholder="name@email" />)}
            </FormItem>
            <FormItem {...this.formLayout} label="Address">
              {getFieldDecorator('contact_project_address', {
                rules: [{ message: 'please enter address', min: 5 }],
                initialValue: current.contact_project_address,
              })(<TextArea rows={4} placeholder="address" />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default ModalContent;
