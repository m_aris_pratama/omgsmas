/* global document */
import React, { PureComponent } from 'react'
import moment from 'moment'
import { Form, Button, Row, Col, DatePicker, Input, Cascader,Select } from 'antd'

const { Search, TextArea } = Input;
const { Option } = Select;


@Form.create()
class Filter extends PureComponent {

  handleTypeChange = value => {
    const { onTypeChange } = this.props;
    if (onTypeChange) {
      onTypeChange(value);
    }
  }
  
  handleUnitChange = value => {
    const { onUnitChange } = this.props;
    if (onUnitChange) {
      onUnitChange(value);
    }
  }

  handleSearch = (value) => {
    const { onSearch } = this.props;
    if (onSearch) {
      onSearch(value);
    }
  }

  render() {
    const {
      filters,
      retailType,
      retailUnit
    } = this.props;

    return (
      <div style={{marginBottom:24}}>
        <Row>
          <Col span={18}>
            <span style={{marginLeft:16}}>Business Unit : </span>
            <Select
              showSearch
              style={{ width: 160 }}
              placeholder="Select a Business Unit"
              optionFilterProp="children"
              onChange={this.handleUnitChange}
              value={filters.unitId}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value={0}>All</Option>
              {retailUnit.list.map((val)=><Option value={val.id}>{val.name}</Option>)}
            </Select>
            <span style={{marginLeft:16}}>Category : </span>
            <Select
              showSearch
              style={{ width: 160 }}
              placeholder="Select a Category"
              optionFilterProp="children"
              onChange={this.handleTypeChange}
              value={filters.typeId}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value={0}>All</Option>
              {retailType.list.map((val)=><Option value={val.id}>{val.name}</Option>)}
            </Select>
          </Col>
          <Col span={6}>
            <Search onSearch={this.handleSearch} justify="end" placeholder="search name or code" />
          </Col>
        </Row>
      </div>
    );
  }
}

export default Filter;
