import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Card,
  Row,
  Col
} from 'antd';
import router from 'umi/router';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

import styles from './index.less';

@connect(({ cities, areas, loading }) => ({
  cities,
  areas,
  areasLoading: loading.effects['areas/fetchAll'],
}))

class Area extends PureComponent {

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'areas/fetchAll',
      payload: {
        per_page: 10,
      },
    });
    dispatch({
      type: 'cities/fetchAll',
      payload: {
        per_page: 10,
      },
    });
  }

  onTabChange = key => {
    const { match } = this.props;
    switch (key) {
      case 'area':
        router.push(`${match.url}/area`);
        break;
      case 'city':
        router.push(`${match.url}/city`);
        break;
      default:
        break;
    }
  };

  render() {
    const {
      areas,
      areasLoading,
      cities,
      match,
      children,
      location
    } = this.props;

    const areasTabList = [
      {
        key: 'city',
        tab: (
          <span>Cities</span>
        ),
      },
      {
        key: 'area',
        tab: (
          <span>Areas</span>
        ),
      }
    ];

    const Info = ({ title, value, bordered }) => (
      <div className={styles.headerInfo}>
        <span>{title}</span>
        <p>{value}</p>
        {bordered && <em />}
      </div>
    );
  
    return (
      <PageHeaderWrapper>
        <div className={styles.standardList}>
          <Card bordered={false}>
            <Row>
              <Col sm={8} xs={24}>
                <Info loading={areasLoading} title="Total Areas" value={areas.total} bordered />
              </Col>
              <Col sm={8} xs={24}>
                <Info title="Total Cities" value={cities.total} bordered />
              </Col>
            </Row>
          </Card>
          <Card
            className={styles.tabsCard}
            style={{ marginTop: 24 }}
            bordered={false}
            tabList={areasTabList}
            activeTabKey={location.pathname.replace(`${match.path}/`, '')}
            onTabChange={this.onTabChange}
          >
            {children}
          </Card>
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default Area;
