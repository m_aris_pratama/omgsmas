/* global document */
import React, { PureComponent } from 'react'
import moment from 'moment'
import { Form, Button, Row, Col, DatePicker, Input, Cascader,Select,Upload,Icon } from 'antd'

const { Option } = Select;
const FormItem = Form.Item;
const { TextArea } = Input;

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

class ModalContent extends PureComponent {

  formLayout = {
    labelCol: { span: 7 },
    wrapperCol: { span: 13 },
  };

  state = {
    loading: false,
  };

  componentDidMount() {
    const { current } = this.props;
    this.setState({imageName: current.avatar})
  }

  handleChange = (info) => {
    console.log(info);
    if (info.file.response) this.setState({imageName: info.file.response.fileName});
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl => this.setState({
        imageUrl,
        loading: false,
      }));
    }
  }

  handleSubmit = (e) => {
    const { onSubmit } = this.props;
    if (onSubmit) {
      onSubmit(e);
    }
  }

  render() {
    const {
      form: { getFieldDecorator },
      current,
      units
    } = this.props;

    const {loading,imageUrl,imageName} = this.state;

    const uploadButton = (
      <div>
        <Icon type={loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );

    return (
      <Form onSubmit={this.handleSubmit}>
        <FormItem label="Avatar" {...this.formLayout}>
          {getFieldDecorator('avatar', {
            rules: [{ required: false }],
            initialValue: imageName,
          })(<Input hidden />)}
          <Upload
            name="file"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            action="/api/clients/uploadImage"
            onChange={this.handleChange}
          >{imageUrl || imageName ? <img style={{ width:150}} src={imageUrl?imageUrl:`/api/clients/uploadImage/${imageName}`} alt="" /> : uploadButton}
          </Upload>
        </FormItem>
        <FormItem label="Name" {...this.formLayout}>
          {getFieldDecorator('name', {
            rules: [{ required: true, message: 'Please enter name' }],
            initialValue: current.name,
          })(<Input placeholder="name" />)}
        </FormItem>
        <FormItem label="Login Username" {...this.formLayout}>
          {getFieldDecorator('username', {
            rules: [{ required: true, message: 'Please enter login username' }],
            initialValue: current.user?current.user.username:'',
          })(<Input placeholder="username" />)}
        </FormItem>
        <FormItem label="Login Password" {...this.formLayout}>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please enter login password' }],
            initialValue: '',
          })(<Input placeholder="password" />)}
        </FormItem>
        <FormItem label="Business Unit" {...this.formLayout}>
          {getFieldDecorator('businessUnit', {
              rules: [{ required: true, message: 'Please enter business unit' }],
              initialValue: current.businessUnit?current.businessUnit.id:'',
            })(
              <Select
                showSearch
                style={{ width: 200 }}
                placeholder="Select a business unit"
                optionFilterProp="children"
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              >
                {units.list.map((val)=><Option value={val.id}>{val.name}</Option>)}
              </Select>
            )}
        </FormItem>
        <FormItem {...this.formLayout} label="Address">
          {getFieldDecorator('address', {
            rules: [{ message: 'please enter address', min: 5 }],
            initialValue: current.address,
          })(<TextArea rows={4} placeholder="address" />)}
        </FormItem>
        <FormItem label="Telp" {...this.formLayout}>
          {getFieldDecorator('telp', {
            rules: [{ required: false}],
            initialValue: current.telp,
          })(<Input placeholder="08..." />)}
        </FormItem>
        <FormItem label="Email" {...this.formLayout}>
          {getFieldDecorator('email', {
            rules: [{ required: false}],
            initialValue: current.email,
          })(<Input placeholder="name@email" />)}
        </FormItem>
      </Form>
    );
  }
}

export default ModalContent;
