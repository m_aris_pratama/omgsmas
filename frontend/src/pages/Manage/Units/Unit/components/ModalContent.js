/* global document */
import React, { PureComponent } from 'react'
import moment from 'moment'
import { Form, Button, Row, Col, DatePicker, Input, Cascader,Select } from 'antd'

const { Option } = Select;
const FormItem = Form.Item;

class ModalContent extends PureComponent {

  formLayout = {
    labelCol: { span: 7 },
    wrapperCol: { span: 13 },
  };

  handleSubmit = (e) => {
    const { onSubmit } = this.props;
    if (onSubmit) {
      onSubmit(e);
    }
  }

  render() {
    const {
      form: { getFieldDecorator },
      current
    } = this.props;

    const week = [{id:0,name:'Sunday'},{id:1,name:'Monday'},{id:2,name:'Tuesday'},{id:3,name:'Wednesday'},{id:4,name:'Thursday'},{id:5,name:'Friday'},{id:6,name:'Saturday'}];

    return (
      <Form onSubmit={this.handleSubmit}>
        <FormItem label="Name" {...this.formLayout}>
          {getFieldDecorator('name', {
            rules: [{ required: true, message: 'Please enter business unit name' }],
            initialValue: current.name,
          })(<Input placeholder="name" />)}
        </FormItem>
        <FormItem label="Cycle" {...this.formLayout}>
          {getFieldDecorator('cycle_started_day', {
            rules: [{ required: true, message: 'Please enter start day' }],
            initialValue: current.cycle_started_day?current.cycle_started_day:1,
          })(
            <Select
              showSearch
              style={{ width: 200 }}
              placeholder="Select a Start Day"
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              {week.map((val)=><Option value={val.id}>{val.name}</Option>)}
            </Select>
          )}
        </FormItem>
      </Form>
    );
  }
}

export default ModalContent;
