/* global document */
import React, { PureComponent, Fragment } from 'react'
import moment from 'moment'
import { Divider, Table } from 'antd'

class List extends PureComponent {
  
  columns = [
    {
      title: 'Name',
      dataIndex: 'name'
    },
    {
      title: 'Cycle Started',
      render: (text, record) => (
        this.convDay(record.cycle_started_day)
      ),
    },
    {
      title: 'Actions',
      fixed: 'right',
      width:120,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleEdit(record)}>Edit</a>
          <Divider type="vertical" />
          <a onClick={() => this.handleDelete(record.id)}>Delete</a>
        </Fragment>
      ),
    },
  ];

  convDay = value => {
    const week = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    return week[value];
  }

  handleTableChange = value => {
    const { onTableChange } = this.props;
    if (onTableChange) {
        onTableChange(value);
    }
  }

  handleEdit = value => {
    const { onEdit } = this.props;
    if (onEdit) {
        onEdit(value);
    }
  }

  handleDelete = value => {
    const { onDelete } = this.props;
    if (onDelete) {
        onDelete(value);
    }
  }

  render() {
    const {
        list,
        metaProps,
        loading
    } = this.props;

    return (<Table
      dataSource={list}
      pagination={metaProps}
      onChange={this.handleTableChange}
      loading={loading}
      columns={this.columns}
    />
    );
  }
}

export default List;
