import React, { PureComponent } from 'react';
import {notification, Card, Icon, Button, Form, Modal} from 'antd';
import { connect } from 'dva';
import styles from './index.less';

import Filter from './components/Filter';
import List from './components/List';
import ModalContent from './components/ModalContent';

@Form.create()
@connect(({ units }) => ({
  units
}))
class UnitList extends PureComponent {

  state = { visible: false, 
    current: {}
  };

  showAddModal = () => {
    this.setState({
      visible: true,
      current: {},
    });
  };

  showEditModal = item => {
    this.setState({
      visible: true,
      current: item,
    });
  };

  showDeleteModal = (id) => {
    Modal.confirm({
      title: 'Delete',
      content: 'Are you sure to delete this item?',
      okText: 'Confirm',
      cancelText: 'Cancel',
      onOk: () => this.deleteItem(id),
    });
  };

  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { dispatch, form } = this.props;
    const { current } = this.state;
    const id = current ? current.id : 0;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      dispatch({
        type: 'units/submit',
        payload: { id, ...fieldsValue },
      });
      this.setState({
        visible: false,
      });
      this.openNotification();
    });
  };

  deleteItem = id => {
    const { dispatch } = this.props;
    this.setState({
      visible: true
    });
    dispatch({
      type: 'units/submit',
      payload: { id },
    }); 
    this.setState({
      visible: false,
    });
    this.openNotification();
  };


  handleSearch = value => {
    const { dispatch,  units: { filters } } = this.props;
    const params = {
      cur_page: 1,
      per_page: 10
    };
    
    params.q = value;
    filters.typeId = 0;
    filters.clientId = 0;
    dispatch({
     type: 'units/saveFilter',
     payload: filters
    });
    dispatch({
      type: 'units/fetch',
      payload: params
    });
  }
   
  handleTableChange = (pagination, filtersArg, sorter) => {
    
    const { dispatch } = this.props;
    const params = {
      cur_page: pagination.current,
      per_page: pagination.pageSize
    };
    if (sorter && sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'units/fetch',
      payload: params,
    });

  };

  openNotification = () => {
    notification.open({
      message: 'Successful Operation',
      description: 'Units table success to changed',
      icon: <Icon type="smile" style={{ color: '#108ee9' }} />,
    });
  };

  render() {
    const {
      units: { list, meta },
      loading,
      form
    } = this.props;

    const { visible, current } = this.state;

    const modalFooter = { okText: 'Save', onOk: this.handleSubmit, onCancel: this.handleCancel };

    const metaProps = {
      showSizeChanger: true,
      showQuickJumper: false,
      pageSize: meta.perPage,
      total: meta.totalResults,
      current: meta.curPage
    };
    
    return (
      <div>
        <Card
          bordered={false}
        >
          <Modal
            title={`Unit ${current.id ? 'Edit' : 'Add'}`}
            className={styles.standardListForm}
            width={640}
            bodyStyle={{ padding: '28px 0 0' }}
            destroyOnClose
            visible={visible}
            {...modalFooter}
          >
            <ModalContent 
              form={form}
              current={current}
              onSubmit={this.handleSubmit}
            />
          </Modal>
          <Filter 
            onSearch={this.handleSearch}
          />
          <Button
            type="dashed"
            style={{ width: '100%', marginBottom: 8 }}
            icon="plus"
            onClick={this.showAddModal}
          >
            Add new Unit
          </Button>
          <List 
            list={list}
            metaProps={metaProps}
            loading={loading}
            onTableChange={this.handleTableChange}
            onEdit={this.showEditModal}
            onDelete={this.showDeleteModal}
          />
        </Card>
      </div>
    );
  }
}

export default UnitList;
