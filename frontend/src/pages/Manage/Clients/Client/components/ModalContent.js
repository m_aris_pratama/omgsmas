/* global document */
import React, { PureComponent } from 'react'
import moment from 'moment'
import { Form, Button, Row, Col, DatePicker, Input, Cascader,Select,Icon,Upload, Divider } from 'antd'

const { Option } = Select;
const FormItem = Form.Item;
const { TextArea } = Input;

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

class ModalContent extends PureComponent {

  formLayout = {
    labelCol: { span: 7 },
    wrapperCol: { span: 13 },
  };

  state = {
    loading: false,
  };

  componentDidMount() {
    const { current } = this.props;
    this.setState({imageName: current.avatar})
  }

  handleChange = (info) => {
    console.log(info);
    if (info.file.response) this.setState({imageName: info.file.response.fileName});
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl => this.setState({
        imageUrl,
        loading: false,
      }));
    }
  }

  handleSubmit = (e) => {
    const { onSubmit } = this.props;
    if (onSubmit) {
      onSubmit(e);
    }
  }

  render() {
    const {
      form: { getFieldDecorator },
      current
    } = this.props;

    const {loading,imageUrl,imageName} = this.state;

    const uploadButton = (
      <div>
        <Icon type={loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );
    
    return (
      <Form onSubmit={this.handleSubmit}>
        <Row>
          <Col span={12}>
            <FormItem label="Avatar" {...this.formLayout}>
              {getFieldDecorator('avatar', {
                rules: [{ required: false }],
                initialValue: imageName,
              })(<Input hidden />)}
              <Upload
                name="file"
                listType="picture-card"
                className="avatar-uploader"
                showUploadList={false}
                action="/api/clients/uploadImage"
                onChange={this.handleChange}
              >{imageUrl || imageName ? <img style={{ width:150}} src={imageUrl?imageUrl:`/api/clients/uploadImage/${imageName}`} alt="" /> : uploadButton}
              </Upload>
            </FormItem>
            <FormItem label="Name" {...this.formLayout}>
              {getFieldDecorator('name', {
                rules: [{ required: true, message: 'Please enter client name' }],
                initialValue: current.name,
              })(<Input placeholder="name" />)}
            </FormItem>
            <FormItem label="Register Date" {...this.formLayout}>
              {getFieldDecorator('registered', {
            rules: [{ required: true, message: '' }],
            initialValue: moment(current.registered),
          })(<DatePicker />)}
            </FormItem>
            <FormItem label="NPWP" {...this.formLayout}>
              {getFieldDecorator('npwp', {
                rules: [{ required: false}],
                initialValue: current.npwp,
              })(<Input placeholder="00000" />)}
            </FormItem>
            <FormItem {...this.formLayout} label="Address">
              {getFieldDecorator('address', {
                rules: [{ message: 'please enter address', min: 5 }],
                initialValue: current.address,
              })(<TextArea rows={4} placeholder="address" />)}
            </FormItem>
            <FormItem label="Telp" {...this.formLayout}>
              {getFieldDecorator('telp', {
                rules: [{ required: false}],
                initialValue: current.telp,
              })(<Input placeholder="08..." />)}
            </FormItem>
            <FormItem label="Fax" {...this.formLayout}>
              {getFieldDecorator('fax', {
                rules: [{ required: false}],
                initialValue: current.fax,
              })(<Input placeholder="08..." />)}
            </FormItem>
            <FormItem label="Email" {...this.formLayout}>
              {getFieldDecorator('email', {
                rules: [{ required: false}],
                initialValue: current.email,
              })(<Input placeholder="name@email" />)}
            </FormItem>
          </Col>
          <Col span={12}>
            <Divider orientation="left">Contact Project</Divider>
            <FormItem label="Name" {...this.formLayout}>
              {getFieldDecorator('contact_project', {
                rules: [{ required: false}],
                initialValue: current.contact_project,
              })(<Input placeholder="name" />)}
            </FormItem>
            <FormItem label="Telp" {...this.formLayout}>
              {getFieldDecorator('contact_project_telp', {
                rules: [{ required: false}],
                initialValue: current.contact_project_telp,
              })(<Input placeholder="08..." />)}
            </FormItem>
            <FormItem label="Position" {...this.formLayout}>
              {getFieldDecorator('contact_project_position', {
                rules: [{ required: false}],
                initialValue: current.contact_project_position,
              })(<Input placeholder="position" />)}
            </FormItem>
            <FormItem label="Email" {...this.formLayout}>
              {getFieldDecorator('contact_project_email', {
                rules: [{ required: false}],
                initialValue: current.contact_project_email,
              })(<Input placeholder="name@email" />)}
            </FormItem>
            <FormItem {...this.formLayout} label="Address">
              {getFieldDecorator('contact_project_address', {
                rules: [{ message: 'please enter address', min: 5 }],
                initialValue: current.contact_project_address,
              })(<TextArea rows={4} placeholder="address" />)}
            </FormItem>
            <Divider orientation="left">Contact Billing</Divider>
            <FormItem label="Name" {...this.formLayout}>
              {getFieldDecorator('contact_billing', {
                rules: [{ required: false}],
                initialValue: current.contact_billing,
              })(<Input placeholder="name" />)}
            </FormItem>
            <FormItem label="Telp" {...this.formLayout}>
              {getFieldDecorator('contact_billing_telp', {
                rules: [{ required: false}],
                initialValue: current.contact_billing_telp,
              })(<Input placeholder="08..." />)}
            </FormItem>
            <FormItem label="Position" {...this.formLayout}>
              {getFieldDecorator('contact_billing_position', {
                rules: [{ required: false}],
                initialValue: current.contact_billing_position,
              })(<Input placeholder="position" />)}
            </FormItem>
            <FormItem label="Email" {...this.formLayout}>
              {getFieldDecorator('contact_billing_email', {
                rules: [{ required: false}],
                initialValue: current.contact_billing_email,
              })(<Input placeholder="name@email" />)}
            </FormItem>
            <FormItem {...this.formLayout} label="Address">
              {getFieldDecorator('contact_billing_address', {
                rules: [{ message: 'please enter address', min: 5 }],
                initialValue: current.contact_billing_address,
              })(<TextArea rows={4} placeholder="address" />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default ModalContent;
