/* global document */
import React, { PureComponent, Fragment } from 'react'
import moment from 'moment'
import { Divider, Dropdown, Menu,Avatar,Icon, Modal,List } from 'antd'
import styles from './List.less';

class ClientList extends PureComponent {

  handleTableChange = (page,pageSize) => {
    const { onTableChange } = this.props;
    if (onTableChange) {
        onTableChange(page,pageSize);
    }
  }

  handleEdit = value => {
    const { onEdit } = this.props;
    if (onEdit) {
        onEdit(value);
    }
  }

  handleDelete = value => {
    const { onDelete } = this.props;
    if (onDelete) {
        onDelete(value);
    }
  }

  render() {
    const {
        list,
        metaProps,
        loading
    } = this.props;

    return (<List
      size="large"
      rowKey="id"
      loading={loading}
      pagination={{onChange: this.handleTableChange, ... metaProps}}
      dataSource={list}
      renderItem={item => (
        <List.Item
          actions={[
            <a onClick={() => this.handleEdit(item)}>Edit</a>,
            <a onClick={() => this.handleDelete(item.id)}>Delete</a>
          ]}
        >
          <List.Item.Meta
            avatar={<Avatar src={`/api/clients/uploadImage/${item.avatar}`} shape="square" size="large" />}
            title={<a href={item.href}>{item.name}</a>}
            description={item.address}
          />
        </List.Item>
      )}
    />
    );
  }
}

export default ClientList;
