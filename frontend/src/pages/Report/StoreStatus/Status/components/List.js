/* global document */
import React, { PureComponent, Fragment } from 'react'
import moment from 'moment'
import { Divider, Table, Badge, Avatar, Icon, Button } from 'antd'
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';

class List extends PureComponent {
  columns = [
    {
      title: 'Date',
      render: (text, item) => (moment(item.created).format('DD MMM YYYY'))
    },
    {
      title: 'Store Name',
      dataIndex: 'store.name',
    },
    {
      title: 'Open',
      render: (text, item) => (
        item.opened ? <Icon type="check" /> : <Icon type="close" />
      ),
    },
    {
      title: 'Active',
      render: (text, item) => (
        item.active ? <Icon type="check" /> : <Icon type="close" />
      ),
    },
    {
      title: 'Note',
      dataIndex: 'report'
    },
    {
      title: 'Action',
      render: (text, item) => (
        item.active ? <Button type="danger" loading={this.props.loading} onClick={() => this.props.toggleActive(item.id)}>Disable</Button> : <Button loading={this.props.loading} onClick={() => this.props.toggleActive(item.id)} type="primary">Enable</Button>
      ),
    },
  ];

  handleTableChange = value => {
    const { onTableChange } = this.props;
    if (onTableChange) {
      onTableChange(value);
    }
  }

  handleEdit = value => {
    const { onEdit } = this.props;
    if (onEdit) {
      onEdit(value);
    }
  }

  handleDelete = value => {
    const { onDelete } = this.props;
    if (onDelete) {
      onDelete(value);
    }
  }


  render() {
    const {
      list,
      metaProps,
      loading
    } = this.props;

    return (<Table
      dataSource={list}
      pagination={metaProps}
      onChange={this.handleTableChange}
      loading={loading}
      columns={this.columns}
    />
    );
  }
}

export default List;
