import React, { PureComponent } from 'react';
import { notification, Card, Icon, Button, Form } from 'antd';
import { connect } from 'dva';
import moment from 'moment'
import styles from './index.less';

import Filter from './components/Filter';
import List from './components/List';

@Form.create()
@connect(({ storeStatus }) => ({
  storeStatus
}))
class StatusList extends PureComponent {

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'storeStatus/fetch',
      payload: {
        per_page: 10,
      },
    });
  }


  handleSearch = value => {
    const { dispatch, storeStatus: { filters } } = this.props;
    const params = {
      cur_page: 1,
      per_page: 10
    };

    params.q = value;
    filters.date = 0;
    dispatch({
      type: 'storeStatus/saveFilter',
      payload: filters
    });
    dispatch({
      type: 'storeStatus/fetch',
      payload: params
    });
  }

  handleDateChange = dates => {
    const { dispatch, storeStatus: { filters } } = this.props;
    const params = {
      cur_page: 1,
      per_page: 10
    };

    params.dateFrom = moment(dates[0]).format('YYYY-MM-DD');
    params.dateTo = moment(dates[1]).format('YYYY-MM-DD');
    filters.dateFrom = params.dateFrom;
    filters.dateTo = params.dateTo;

    dispatch({
      type: 'storeStatus/saveFilter',
      payload: filters
    });
    dispatch({
      type: 'storeStatus/fetch',
      payload: params
    });
  }

  handleTableChange = (pagination, filtersArg, sorter) => {

    const { dispatch } = this.props;
    const params = {
      cur_page: pagination.current,
      per_page: pagination.pageSize
    };
    if (sorter && sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'storeStatus/fetch',
      payload: params,
    });

  };

  toggleActive = async (id) => {
    const { dispatch, storeStatus: { filters, meta } } = this.props;
    let params = {
      ...filters,
      per_page: meta.perPage,
      cur_page: meta.curPage
    }
    this.props.loading = true;
    await dispatch({
      type: 'storeStatus/toggleActive',
      payload: {
        id,
        ...params
      },
    });
    this.props.loading = false;
  }

  render() {
    const {
      storeStatus: { list, meta },
      loading
    } = this.props;

    const metaProps = {
      showSizeChanger: true,
      showQuickJumper: false,
      pageSize: meta.perPage,
      total: meta.totalResults,
      current: meta.curPage
    };

    return (
      <div>
        <Card
          bordered={false}
        >
          <Filter
            onSearch={this.handleSearch}
            onDateChange={this.handleDateChange}
          />
          <List
            list={list}
            metaProps={metaProps}
            loading={loading}
            onTableChange={this.handleTableChange}
            onEdit={this.showEditModal}
            onDelete={this.showDeleteModal}
            toggleActive={this.toggleActive}
          />
        </Card>
      </div>
    );
  }
}

export default StatusList;
