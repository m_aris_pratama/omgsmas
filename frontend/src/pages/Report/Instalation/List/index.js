import React, { PureComponent } from 'react';
import { notification, Card, Icon, Form, Row, Col, Empty } from 'antd';
import { connect } from 'dva';

import Filter from './components/Filter';
import Instalation from './components/Instalation';

@Form.create()
@connect(({ projects, reports }) => ({
  projects,
  reports
}))
class ProjectList extends PureComponent {

  componentDidMount() {
    const { dispatch, projects: { filters } } = this.props;
    dispatch({
      type: 'projects/fetch',
      payload: {
        per_page: 0,
      },
    });
    if (filters.projectId > 0) {
      dispatch({
        type: 'reports/fetchInstalation',
        payload: { projectId: filters.projectId }
      });
    }
  }

  handleTypeChange = value => {
    const { dispatch, projects: { filters } } = this.props;

    dispatch({
      type: 'reports/fetchInstalation',
      payload: { projectId: value }
    });
    filters.projectId = value
    dispatch({
      type: 'projects/saveFilter',
      payload: filters
    });
  }

  render() {
    const {
      projects,
      reports
    } = this.props;

    return (
      <Card
        bordered={false}
      >
        {projects.list.length > 0 && (
          <Filter
            filters={projects.filters}
            fillprojects={projects.list}
            onTypeChange={this.handleTypeChange}
          />
        )}
        {projects.filters.projectId && reports.instalation ? (<Instalation
          projectId={projects.filters.projectId}
          data={reports.instalation}
        />) : ''}
      </Card>
    );
  }
}

export default ProjectList;
