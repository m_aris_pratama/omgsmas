/* global document */
import React, { PureComponent } from 'react';
import moment from 'moment';
import { Divider, Table,Card,Col,Row,Icon, Button,message } from 'antd';
import styles from './index.less';

import { Pie } from '@/components/Charts';
import List from './List';

class Instalation extends PureComponent {

  columns = [
    {
      title: 'Retail Code',
      dataIndex: 'code'
    },
    {
      title: 'Retail Name',
      dataIndex: 'name'
    },
    {
      title: 'Total Store',
      dataIndex: 'store'
    },
    {
      title: 'Installed',
      render: (text, record) => (
        record.programs.map(x => (<span>{x.x} : {x.y}<br /></span>))
      ),
    },
    {
      title: 'Not Installed',
      render: (text, record) => (
        record.notInstalled.map(x => (<span>{x.x} : {x.y}<br /></span>))
      ),
    }
  ];

  handleSummaryExport = (id) => {
    console.log('summary')
    window.location = `/api/report/downloadInstalationSummary/${id}`;
    message.info('file downloaded successfully')
  }

  handleDetailExport = (id) => {
    console.log('detail')
    window.location = `/api/report/downloadInstalationDetail/${id}`;
    message.info('file downloaded successfully')
  }

  handleImageExport = (id) => {
    console.log('image')
    window.location = `/api/report/downloadInstalationImage/${id}`;
    message.info('file downloaded successfully')
  }

  render() {
    const {
        data,
        projectId,
    } = this.props;

    const summary = [];
    const retails = [];
    if (data.programs){
      data.programs.split(',').forEach(el => {
        summary.push({x:el,y:data.assign.filter(a=>a.program === el).length})
      });
      data.assign.forEach(el => {
        if (!retails.find(x=>x.code === el.retailCode))
        { 
          const programs = [];
          const notInstalled = [];
          data.programs.split(',').forEach(e => {
            programs.push({x:e,y:data.assign.filter(a=>a.program === e && a.retailCode === el.retailCode).length})
          });
          ['PBT','RNV'].forEach(e => {
            notInstalled.push({x:e,y:data.assign.filter(a=>a.notInstall === e && a.retailCode === el.retailCode).length})
          })
          notInstalled.push({x:'DLL',y:data.assign.filter(a=>a.retailCode === el.retailCode && a.notInstall === '' && a.program === '').length})
          retails.push({
            code: el.retailCode,
            name: el.retailName,
            store: data.assign.filter(a => a.retailCode === el.retailCode).length,
            programs,
            notInstalled,
            instalation: data.assign.filter(a => a.retailCode === el.retailCode && a.program !=='').length
          });
        }
      });
    }

    const Info = ({ title, value, bordered }) => (
      <div className={styles.headerInfo}>
        <span>{title}</span>
        <p>{value}</p>
        {bordered && <em />}
      </div>
    );
  
    const expandedRowRender = (record) => (
      <List
        data={data.assign.filter(x=>x.retailCode === record.code)}
      />
    )

    return (
      <div className={styles.standardList}>
        <Card bordered={false}>
          <Row>
            <Col sm={12} xs={14} style={{paddingLeft:0}}>
              <span>Schedule to start</span>
              <div style={{padding:20}}>
                Jabodetabek : {moment(data.start_jabo).format('DD MMMM YYYY')}<br />
                Non Jabodetabek : {moment(data.start_nonjabo).format('DD MMMM YYYY')}
              </div>
            </Col>
            <Col sm={12} xs={14}>
              <span style={{marginLeft:20}}>Program</span>
              <Pie
                data={summary}
                height={128}
                hasLegend
                tooltip
              />
            </Col>
          </Row>
          <p>&nbsp;</p>
          <Row>
            <Col sm={8} xs={14}>
              <Info title="Actual/Plan" value={`${data.assign?data.assign.filter(x=>x.program !=='').length:0}/${data.assign?data.assign.length:0}`} bordered />
            </Col>
            <Col sm={8} xs={14}>
              <Info title="Actual/Plan Area JABODETABEK" value={`${data.assign?data.assign.filter(x=>x.program !=='' && x.area ==='JABODETABEK').length:0}/${data.assign?data.assign.filter(x=>x.area ==='JABODETABEK').length:0}`} bordered />
            </Col>
            <Col sm={8} xs={14}>
              <Info title="Actual/Plan Area Outer JABODETABEK" value={`${data.assign?data.assign.filter(x=>x.program !=='' && x.area !=='JABODETABEK').length:0}/${data.assign?data.assign.filter(x=>x.area !=='JABODETABEK').length:0}`} bordered />
            </Col>
            <Col sm={8} xs={14}>
              &nbsp;
            </Col>
          </Row>
        </Card>
        <Card bordered={false} extra={(<div><Button justify="end" type="primary" icon="download" style={{marginRight:10}} onClick={() => this.handleSummaryExport(projectId)}>Export Summary</Button><Button justify="end" type="primary" icon="download" style={{marginRight:10}} onClick={() => this.handleDetailExport(projectId)}>Export Details</Button><Button justify="end" type="primary" icon="download" style={{marginRight:10}} onClick={() => this.handleImageExport(projectId)}>Export Images</Button></div>)}>
          <Table
            dataSource={retails}
            expandedRowRender={expandedRowRender}
            pagination={false}
            columns={this.columns}
          //  scroll={{x: 1020}}
          />
        </Card>
      </div>
    );
  }
}

export default Instalation;
