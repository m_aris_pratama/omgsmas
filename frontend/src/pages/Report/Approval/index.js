import React, { PureComponent } from 'react';
import {notification, Card, Icon, Button, Form, Modal} from 'antd';
import { connect } from 'dva';
import styles from './index.less';

import Filter from './components/Filter';
import List from './components/List';
import ModalContent from './components/ModalContent';

@Form.create()
@connect(({ projectAssigns }) => ({
  projectAssigns
}))
class Approval extends PureComponent {

  state = { visible: false, 
    current: {}
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'projectAssigns/fetch',
      payload: {
        per_page: 10,
        approval: true
      }
    });
  }

  showAddModal = () => {
    this.setState({
      visible: true,
      current: {},
    });
  };

  showEditModal = item => {
    this.setState({
      visible: true,
      current: item,
    });
  };

  showRejectedModal = (id) => {
    Modal.confirm({
      title: 'Rejected',
      content: 'Are you sure to reject this survey?',
      okText: 'Yes',
      cancelText: 'Cancel',
      onOk: () => this.rejectedItem(id),
    });
  };

  showApprovedModal = (id) => {
    Modal.confirm({
      title: 'Approved',
      content: 'Are you sure to approved this survey?',
      okText: 'Yes',
      cancelText: 'Cancel',
      onOk: () => this.approvedItem(id),
    });
  };

  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };


  rejectedItem = item => {
    const { dispatch } = this.props;
    this.setState({
      visible: true
    });
    dispatch({
      type: 'projectAssigns/submitApproval',
      payload: { id: item.id, survey_completed:null, survey_count:item.survey_count+1 },
    });
    this.setState({
      visible: false,
    });
    this.openNotification();
  };

  approvedItem = item => {
    const { dispatch } = this.props;
    this.setState({
      visible: true
    });
    dispatch({
      type: 'projectAssigns/submitApproval',
      payload: { id: item.id, survey_approved:true },
    });
    this.setState({
      visible: false,
    });
    this.openNotification();
  };

   
  handleTableChange = (pagination, filtersArg, sorter) => {
    
    const { dispatch } = this.props;
    const params = {
      cur_page: pagination.current,
      per_page: pagination.pageSize
    };
    if (sorter && sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'projectAssigns/fetch',
      payload: params,
    });

  };

  openNotification = () => {
    notification.open({
      message: 'Successful Operation',
      description: 'ProjectAssigns table success to changed',
      icon: <Icon type="smile" style={{ color: '#108ee9' }} />,
    });
  };

  render() {
    const {
      projectAssigns: { list, meta },
      loading,
      form
    } = this.props;

    const { visible, current } = this.state;

    const modalFooter = { okText: 'Save', onOk: this.handleSubmit, onCancel: this.handleCancel };

    const metaProps = {
      showSizeChanger: true,
      showQuickJumper: false,
      pageSize: meta.perPage,
      total: meta.totalResults,
      current: meta.curPage
    };
    
    return (
      <div>
        <Card
          title='Survey Approval'
          bordered={false}
        >
          <Modal
            title={`projectAssign ${current.id ? 'Edit' : 'Add'}`}
            className={styles.standardListForm}
            width={640}
            bodyStyle={{ padding: '28px 0 0' }}
            destroyOnClose
            visible={visible}
            {...modalFooter}
          >
            <ModalContent 
              form={form}
              current={current}
              onSubmit={this.handleSubmit}
            />
          </Modal>
          {/*  <Filter 
            onSearch={this.handleSearch}
           /> */}
          <List 
            list={list}
            metaProps={metaProps}
            loading={loading}
            onTableChange={this.handleTableChange}
            onApproved={this.showApprovedModal}
            onRejected={this.showRejectedModal}
          />
        </Card>
      </div>
    );
  }
}

export default Approval;
