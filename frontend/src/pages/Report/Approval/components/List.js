/* global document */
import React, { PureComponent, Fragment } from 'react'
import moment from 'moment'
import { Divider, Table,Avatar } from 'antd'
import { format } from 'util';

import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';

class List extends PureComponent {

  state = {vmodal: false, photo: ''};
  
  columns = [
    {
      title: 'Project',
      dataIndex: 'project.name'
    },
    {
      title: 'Date',
      render: (text, record) => (moment(record.survey_completed).format('DD MMMM YYYY HH:mm'))
    },
    {
      title: 'Store',
      dataIndex: 'store.name'
    },
    {
      title: 'Field Officer',
      dataIndex: 'store.member.name'
    },
    {
      title: 'Survey Count',
      render: (text, record) => (record.survey_count+1)
    },
    {
      title: 'Tasks',
      render: (text, record) =>(
        record.taskAssign.map(x=>(<p>{x.task.name} : {x.task.type==='image'? <Avatar src={`/api/field/phototask/${x.value}`} shape="square" size="large" onClick={() => {this.showModal(`/api/field/phototask/${x.value}`); }} />:x.value}</p>))
      )
    },
    {
      title: 'Actions',
      fixed: 'right',
      width:200,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleApproved(record)}>Approved</a>
          <Divider type="vertical" />
          <a onClick={() => this.handleRejected(record)}>Rejected</a>
        </Fragment>
      ),
    },
  ];

  showModal = id => {
    this.setState({
      vmodal: true,
      photo: id
    });
  }

  handleOk = () => {
    this.setState({
      vmodal: false
    });
  }

  handleTableChange = value => {
    const { onTableChange } = this.props;
    if (onTableChange) {
        onTableChange(value);
    }
  }

  handleApproved = value => {
    const { onApproved } = this.props;
    if (onApproved) {
      onApproved(value);
    }
  }

  handleRejected = value => {
    const { onRejected } = this.props;
    if (onRejected) {
      onRejected(value);
    }
  }

  render() {
    const {
        list,
        metaProps,
        loading
    } = this.props;
    const { vmodal, photo} = this.state;

    return (
      <div>
        {vmodal && (
          <Lightbox
            mainSrc={photo}
            onCloseRequest={() => this.setState({ vmodal: false })} 
          />
        )}
        <Table
          dataSource={list}
          pagination={metaProps}
          onChange={this.handleTableChange}
          loading={loading}
          columns={this.columns}
        />
      </div>
    );
  }
}

export default List;
