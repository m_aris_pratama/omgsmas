/* global document */
import React, { PureComponent } from 'react'
import { Form, Button, Row, Col, DatePicker } from 'antd'
import moment from 'moment'

const { RangePicker } = DatePicker;
const dateFormat = 'YYYY/MM/DD';

@Form.create()
class Filter extends PureComponent {

  handleDateChange = (dates, dateString) => {
    const { onDateChange } = this.props;
    if (onDateChange) {
      if (dates.length === 2 && moment(dates[1].isAfter(dates[0])))
        onDateChange(dates);
    }
  }

  handleExport = (type) => {
    const { onExport } = this.props;
    if (onExport) {
      onExport(type);
    }
  }

  render() {

    return (
      <div style={{marginBottom:24}}>
        <Row>
          <Col span={12}>
            <RangePicker
              defaultValue={[moment().subtract(7, 'days'), moment()]}
              format={dateFormat}
              onChange={this.handleDateChange}
            />
          </Col>
          <Col span={12}>
            <Button justify="end" style={{marginRight:10}} type="primary" icon="download" onClick={() => this.handleExport(1)}>Export Summary</Button>
            <Button justify="end" style={{marginRight:10}} type="primary" icon="download" onClick={() => this.handleExport(2)}>Export Detail</Button>
            <Button justify="end" type="primary" icon="download" onClick={() => this.handleExport(3)}>Export Images</Button>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Filter;
