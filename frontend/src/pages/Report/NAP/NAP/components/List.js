/* global document */
import React, { PureComponent, Fragment } from 'react'
import moment from 'moment'
import { Divider, Table,Badge,Avatar,Icon } from 'antd'
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';

class List extends PureComponent {

  state = {vmodal: false, photo: ''};

  columns = [
    {
      title: 'Name',
      dataIndex: 'name'
    },
    {
      title: 'Retail',
      dataIndex: 'retail.name'
    },
    {
      title: 'City',
      dataIndex: 'city.name'
    },
    {
      title: 'NAP Total',
      render: (text, item) => (item.naps.filter(x => x.clean === false).length)
    },
    {
      title: 'Clean',
      render: (text, item) => (item.naps.filter(x => x.clean === true).length)
    }
  ];

  handleTableChange = value => {
    const { onTableChange } = this.props;
    if (onTableChange) {
        onTableChange(value);
    }
  }

  showModal = id => {
    this.setState({
      vmodal: true,
      photo: id
    });
  }

  handleOk = () => {
    this.setState({
      vmodal: false
    });
  }

  render() {
    const {
        list,
        metaProps,
        loading
    } = this.props;

    const { vmodal, photo} = this.state;

    const expandedRowRender = (record) => {
      const columns = [
        { title: 'Date',
        render: (text, item) => (moment(item.created).format('DD MMM YYYY'))
        },
        { title: '',width:100,
        render: (text, item) => (
          <Avatar src={`/api/field/phototask/${item.file}`} shape="square" size="large" onClick={() => {this.showModal(`/api/field/phototask/${item.file}`); }} />
          
        )},
        {
          title: 'Clean',
          render: (text, item) => (
            item.clean?<Icon type="check" />:<Icon type="close" />
          ),
        },
        { title: 'Program', dataIndex: 'program' },
        { title: 'Brand', dataIndex: 'brand' },
        { title: 'Promo', dataIndex: 'promo' },
      ];

      return (<div>
        {vmodal && (
          <Lightbox
            mainSrc={photo}
            onCloseRequest={() => this.setState({ vmodal: false })} 
          />
        )}
        <Table
          columns={columns}
          dataSource={record.naps}
          pagination={false}
        /></div>
      );
    }

    return (<Table
      dataSource={list}
      pagination={metaProps}
      onChange={this.handleTableChange}
      loading={loading}
      columns={this.columns}
      expandedRowRender={expandedRowRender}
    />
    );
  }
}

export default List;
