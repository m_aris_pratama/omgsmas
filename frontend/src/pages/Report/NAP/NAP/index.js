import React, { PureComponent } from 'react';
import {Card, Icon, Button, Form, Modal, message} from 'antd';
import { connect } from 'dva';
import moment from 'moment'
// import XLSX from 'xlsx';

import Filter from './components/Filter';
import List from './components/List';

@Form.create()
@connect(({ napUploads }) => ({
  napUploads
}))
class NAPList extends PureComponent {

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'napUploads/fetch',
      payload: {
        per_page: 10,
      },
    });
  }

  handleDateChange = dates => {
    const { dispatch,  napUploads: { filters } } = this.props;
    const params = {
      cur_page: 1,
      per_page: 10
    };
    
    params.dateFrom = moment(dates[0]).format('YYYY-MM-DD');
    params.dateTo = moment(dates[1]).format('YYYY-MM-DD');
    filters.dateFrom = moment(dates[0]).format('YYYY-MM-DD');
    filters.dateTo = moment(dates[1]).format('YYYY-MM-DD');

    dispatch({
     type: 'napUploads/saveFilter',
     payload: filters
    });
    dispatch({
      type: 'napUploads/fetch',
      payload: params
    });
  }
   
  handleTableChange = (pagination, filtersArg, sorter) => {
    
    const { dispatch } = this.props;
    const params = {
      cur_page: pagination.current,
      per_page: pagination.pageSize
    };
    if (sorter && sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'napUploads/fetch',
      payload: params,
    });

  };

  exportFile = (type) => {
    const { napUploads:{filters} } = this.props;
    
    switch(type){
      case 1:
        window.location = `/api/report/downloadNAPSummary/${filters.dateFrom}/${filters.dateTo}`;
        message.info('file downloaded successfully')
        break;
      case 2:
        window.location = `/api/report/downloadNAPDetail/${filters.dateFrom}/${filters.dateTo}`;
        message.info('file downloaded successfully')
        break;
      case 3:
        window.location = `/api/report/downloadNAPImage/${filters.dateFrom}/${filters.dateTo}`;
        message.info('file downloaded successfully')
        break;
      default:
    }
    
  };
  
  render() {
    const {
      napUploads: { list, meta },
      loading,
    } = this.props;

    const metaProps = {
      showSizeChanger: true,
      showQuickJumper: false,
      pageSize: meta.perPage,
      total: meta.totalResults,
      current: meta.curPage
    };
    
    return (
      <div>
        <Card
          bordered={false}
        >
          <Filter 
            onDateChange={this.handleDateChange}
            onExport={this.exportFile}
          />
          <List 
            list={list}
            metaProps={metaProps}
            loading={loading}
            onTableChange={this.handleTableChange}
          />
        </Card>
      </div>
    );
  }
}

export default NAPList;
