import React, { PureComponent } from 'react';
import { notification, Card, Icon, Form, Row, Col, Empty } from 'antd';
import { connect } from 'dva';

import Filter from './components/Filter';
import Survey from './components/Survey';

@Form.create()
@connect(({ projects, reports }) => ({
  projects,
  reports
}))
class ProjectList extends PureComponent {

  componentDidMount() {
    const { dispatch, projects: { filters } } = this.props;
    dispatch({
      type: 'projects/fetch',
      payload: {
        per_page: 0,
      },
    });
    if (filters.projectId > 0) {
      dispatch({
        type: 'reports/fetchSurvey',
        payload: { projectId: filters.projectId }
      });
    }
  }

  handleTypeChange = value => {
    const { dispatch, projects: { filters } } = this.props;

    dispatch({
      type: 'reports/fetchSurvey',
      payload: { projectId: value }
    });
    filters.projectId = value
    dispatch({
      type: 'projects/saveFilter',
      payload: filters
    });
  }

  render() {
    const {
      projects,
      reports
    } = this.props;

    return (
      <Card
        bordered={false}
      >
        {projects.list.length > 0 && (
          <Filter
            filters={projects.filters}
            fillprojects={projects.list}
            onTypeChange={this.handleTypeChange}
          />
        )}
        {projects.filters.projectId && reports.survey ? (<Survey
          projectId={projects.filters.projectId}
          data={reports.survey}
        />) : ''}
      </Card>
    );
  }
}

export default ProjectList;
