/* global document */
import React, { PureComponent } from 'react'
import moment from 'moment'
import { Divider, Table,Card,Col,Row,Icon,Avatar, Collapse } from 'antd'
import { connect } from 'dva';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';

const Panel = Collapse.Panel;

class List extends PureComponent {

  state = {vmodal: false, photo: ''};

  columns = [
    {
      title: 'Retail',
      dataIndex: 'retailCode',
      width: 80
    },
    {
      title: 'Store',
      dataIndex: 'store',
      width: 200
    },
    {
      title: 'Removed',
      width: 120,
      render: (text, record) => (
        <span>{record.dateRemoved?moment(record.dateRemoved).format('DD MMM YYYY'):<Icon type="close" />}</span>
      ),
    },
    {
      title: 'Tasks',
      render: (text, record) =>(
        record.tasks?<Collapse bordered={false}>{record.tasks.map(t => (<Panel header={t.date}>{t.items.map(x=>(<div>{x.task.name} : {x.task.type==='image'? <Avatar src={`/api/field/phototask/${x.value}`} shape="square" size="large" onClick={() => {this.showModal(`/api/field/phototask/${x.value}`); }} />:x.value}<br /></div>))}</Panel>))}</Collapse>:''
      )
    },
  ];


  showModal = id => {
    this.setState({
      vmodal: true,
      photo: id
    });
  }

  handleOk = () => {
    this.setState({
      vmodal: false
    });
  }

  render() {
    const {
        data
    } = this.props;
    const { vmodal, photo} = this.state;

    return (
      <div>
        {vmodal && (
          <Lightbox
            mainSrc={photo}
            onCloseRequest={() => this.setState({ vmodal: false })} 
          />
        )}
        <Table
          dataSource={data}
          columns={this.columns}
        />
      </div>
    );
  }
}

export default List;
