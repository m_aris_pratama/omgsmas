/* global document */
import React, { PureComponent } from 'react';
import moment from 'moment';
import { Divider, Table,Card,Col,Row,Icon, Button,message } from 'antd';
import styles from './index.less';

import { Pie } from '@/components/Charts';
import List from './List';

class Removed extends PureComponent {

  columns = [
    {
      title: 'Retail Code',
      dataIndex: 'code'
    },
    {
      title: 'Retail Name',
      dataIndex: 'name'
    },
    {
      title: 'Total Store',
      dataIndex: 'store'
    },
    {
      title: 'Removed',
      dataIndex: 'removed'
    },
    {
      title: 'Not Removed',
      render: (text, record) => (
        record.store - record.removed
      ),
    }
  ];

  handleSummaryExport = (id) => {
    window.location = `/api/report/downloadRemovedSummary/${id}`;
    message.info('file downloaded successfully')
  }

  handleDetailExport = (id) => {
    window.location = `/api/report/downloadRemovedDetail/${id}`;
    message.info('file downloaded successfully')
  }

  handleImageExport = (id) => {
    window.location = `/api/report/downloadRemovedImage/${id}`;
    message.info('file downloaded successfully')
  }

  render() {
    const {
        data,
        projectId
    } = this.props;

    const retails = [];
    if (data.assign){
      data.assign.forEach(el => {
        if (!retails.find(x=>x.code === el.retailCode))
        { 
          retails.push({
            code: el.retailCode,
            name: el.retailName,
            store: data.assign.filter(a => a.retailCode === el.retailCode).length,
            removed: data.assign.filter(a => a.retailCode === el.retailCode && a.dateRemoved !==null).length
          });
        }
      });
    }

    const Info = ({ title, value, bordered }) => (
      <div className={styles.headerInfo}>
        <span>{title}</span>
        <p>{value}</p>
        {bordered && <em />}
      </div>
    );
  
    const expandedRowRender = (record) => (
      <List
        data={data.assign.filter(x=>x.retailCode === record.code)}
      />
    )

    return (
      <div className={styles.standardList}>
        <Card bordered={false}>
          <Row>
            <Col sm={12} xs={14} style={{paddingLeft:0}}>
              <span>Schedule to start</span>
              <div style={{padding:20}}>
                Date : {moment(data.start).format('DD MMMM YYYY')}
              </div>
            </Col>
            <Col sm={12} xs={14}>
              &nbsp;
            </Col>
          </Row>
          <p>&nbsp;</p>
          <Row>
            <Col sm={8} xs={14}>
              <Info title="Actual/Plan" value={`${data.assign?data.assign.filter(x=>x.dateRemoved !==null).length:0}/${data.assign?data.assign.length:0}`} bordered />
            </Col>
          </Row>
        </Card>
        <Card bordered={false} extra={(<div><Button justify="end" type="primary" icon="download" style={{marginRight:10}} onClick={() => this.handleSummaryExport(projectId)}>Export Summary</Button><Button justify="end" type="primary" icon="download" style={{marginRight:10}} onClick={() => this.handleDetailExport(projectId)}>Export Details</Button><Button justify="end" type="primary" icon="download" style={{marginRight:10}} onClick={() => this.handleImageExport(projectId)}>Export Images</Button></div>)}>
          <Table
            dataSource={retails}
            expandedRowRender={expandedRowRender}
            pagination={false}
            columns={this.columns}
          //  scroll={{x: 1020}}
          />
        </Card>
      </div>
    );
  }
}

export default Removed;
