import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Card,
  Row,
  Col
} from 'antd';
import router from 'umi/router';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

import styles from './index.less';

@connect(({ projects, loading }) => ({
  projects,
  projectsLoading: loading.effects['projects/fetchAll'],
}))
class Report extends PureComponent {

  onTabChange = key => {
    const { match } = this.props;
    switch (key) {
      case 'list':
        router.push(`${match.url}/list`);
        break;
      default:
        break;
    }
  };

  render() {
    const {
      match,
      children,
      location
    } = this.props;

    const brandsTabList = [
      {
        key: 'list',
        tab: (
          <span>Project Report</span>
        ),
      }
    ];

    return (
      <PageHeaderWrapper>
        <div className={styles.standardList}>
          <Card
            className={styles.tabsCard}
            style={{ marginTop: 24 }}
            bordered={false}
            tabList={brandsTabList}
            activeTabKey={location.pathname.replace(`${match.path}/`, '')}
            onTabChange={this.onTabChange}
          >
            {children}
          </Card>
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default Report;
