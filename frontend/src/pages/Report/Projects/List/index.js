import React, { PureComponent } from 'react';
import {notification, Card, Icon, Form,Row,Col} from 'antd';
import { connect } from 'dva';
import styles from './index.less';

import Filter from './components/Filter';
import Instalation from './components/Instalation';

@Form.create()
@connect(({ projects }) => ({
  projects
}))
class ProjectList extends PureComponent {

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'projects/fetch',
      payload: {
        per_page: 0,
      },
    });
  }

  handleTypeChange = value => {
    const { dispatch, projects: {filters} } = this.props;

    dispatch({
      type: 'projects/summary',
      payload: {id:value}
   });
   filters.projectId = value
   dispatch({
    type: 'projects/saveFilter',
    payload: filters
  });
  }

  render() {
    const {
      projects
    } = this.props;
    
    return (
      <Card
        bordered={false}
      >
        <Filter 
          filters={projects.filters}
          projects={projects.list}
          onTypeChange={this.handleTypeChange}
        />
        <Instalation 
          projectId={projects.filters.projectId}
          summary={projects.summary}
        />
      </Card>
    );
  }
}

export default ProjectList;
