/* global document */
import React, { PureComponent } from 'react'
import moment from 'moment'
import { Form, Button, Row, Col, DatePicker, Input, Cascader,Select ,Card} from 'antd'

const { Search, TextArea } = Input;
const { Option } = Select;


@Form.create()
class Filter extends PureComponent {

  handleTypeChange = value => {
    const { onTypeChange } = this.props;
    if (onTypeChange) {
      onTypeChange(value);
    }
  }


  render() {
    const {
      filters,
      projects
    } = this.props;

    return (
      <div style={{marginBottom:24}}>
        <Row>
          <Col span={14}>
            &nbsp;
          </Col>
          <Col span={10}>
            <span style={{marginLeft:16}}>Project : </span>
            <Select
              showSearch
              style={{ width: 260 }}
              placeholder="Select a Project"
              optionFilterProp="children"
              onChange={this.handleTypeChange}
              value={filters.projectId}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              {projects.map((val)=><Option value={val.id}>{`${val.name} - ${val.brand.name}`}</Option>)}
            </Select>
          </Col>
        </Row>
        {filters.projectId > 0?(
          <Card bordered={false}>
            <Row>
              <Col span={4}>Client</Col>
              <Col span={8}>: {projects.find(x => x.id === filters.projectId).brand.client.name}</Col>
            </Row>
            <Row>
              <Col span={4}>Brand</Col>
              <Col span={8}>: {projects.find(x => x.id === filters.projectId).brand.name}</Col>
            </Row>
            <Row>
              <Col span={4}>Program</Col>
              <Col span={8}>: {projects.find(x => x.id === filters.projectId).name}</Col>
            </Row>
            <Row>
              <Col span={4}>Business Unit</Col>
              <Col span={8}>: {projects.find(x => x.id === filters.projectId).businessUnit.name}</Col>
            </Row>
            <Row>
              <Col span={4}>CYCLE</Col>
              <Col span={8}>: {projects.find(x => x.id === filters.projectId).cycle_start}</Col>
            </Row>
          </Card>):''}
      </div>
    );
  }
}

export default Filter;
