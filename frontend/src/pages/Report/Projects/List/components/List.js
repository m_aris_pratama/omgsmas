/* global document */
import React, { PureComponent } from 'react'
import moment from 'moment'
import { Divider, Table,Card,Col,Row,Icon,Avatar } from 'antd'
import { connect } from 'dva';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';

@connect(({ projectAssigns }) => ({
  projectAssigns
}))
class List extends PureComponent {

  state = {vmodal: false, photo: ''};

  columns = [
    {
      title: 'Retail',
      dataIndex: 'store.retail.code'
    },
    {
      title: 'Store',
      dataIndex: 'store.name'
    },
    {
      title: 'Instalation',
      width: 120,
      render: (text, record) => (
        <span>{record.instalation_completed?moment(record.instalation_completed).format('DD MMM YYYY'):<Icon type="close" />}</span>
      ),
    },
    {
      title: 'Program',
      dataIndex: 'program'
    },
    {
      title: 'Tasks',
      render: (text, record) =>(
        record.taskAssign.map(x=>(<p>{x.task.name} : {x.task.type==='image'? <Avatar src={`/api/field/phototask/${x.value}`} shape="square" size="large" onClick={() => {this.showModal(`/api/field/phototask/${x.value}`); }} />:x.value}</p>))
      )
    },
  ];

  componentDidMount() {
    const { dispatch, projectId,retailId } = this.props;
    dispatch({
      type: 'projectAssigns/saveRetail',
      payload: {id: retailId}
    });
    dispatch({
      type: 'projectAssigns/fetchByRetail',
      payload: {
        per_page: 0,
        project_id: projectId,
        retail_id: retailId,
        is_active: true,
        installed: true
      },
    });
  }

  showModal = id => {
    this.setState({
      vmodal: true,
      photo: id
    });
  }

  handleOk = () => {
    this.setState({
      vmodal: false
    });
  }

  render() {
    const {
        projectAssigns,
        loading,
        retailId
    } = this.props;
    const { vmodal, photo} = this.state;

    const r = projectAssigns.retails.find(x => x.id === retailId); 
    const list = r?r.list:[];

    return (
      <div>
        {vmodal && (
          <Lightbox
            mainSrc={photo}
            onCloseRequest={() => this.setState({ vmodal: false })} 
          />
        )}
        <Table
          dataSource={list}
          loading={loading}
          columns={this.columns}
          // scroll={{x: 960}}
        />
      </div>
    );
  }
}

export default List;
