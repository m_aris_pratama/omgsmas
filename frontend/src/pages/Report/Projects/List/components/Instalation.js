/* global document */
import React, { PureComponent } from 'react';
import moment from 'moment';
import { Divider, Table,Card,Col,Row,Icon } from 'antd';
import styles from './index.less';

import { Pie } from '@/components/Charts';
import List from './List';

class Instalation extends PureComponent {

  columns = [
    {
      title: 'Retail Code',
      dataIndex: 'code'
    },
    {
      title: 'Retail Name',
      dataIndex: 'name'
    },
    {
      title: 'Total Store',
      dataIndex: 'store'
    },
    {
      title: 'Program',
      render: (text, record) => (
        record.programs.map(x => (<span>{x.name} : {x.value}<br /></span>))
      ),
    },
    {
      title: 'Installed',
      dataIndex: 'instalation'
    },
    {
      title: 'Not Installed',
      render: (text, record) => (
        record.store - record.instalation
      ),
    }
  ];

  render() {
    const {
        summary,
        projectId
    } = this.props;

    const Info = ({ title, value, bordered }) => (
      <div className={styles.headerInfo}>
        <span>{title}</span>
        <p>{value}</p>
        {bordered && <em />}
      </div>
    );
  
    const expandedRowRender = (record) => {
     
      return (
        <List
          retailId={record.id}
          projectId={projectId}
        />);
    };

    return (
      <div className={styles.standardList}>
        <Card bordered={false}>
          <Row>
            <Col sm={4} xs={14}>
              <Info title="Survey" value={`${summary.summary?summary.summary.survey:0}/${summary.summary?summary.summary.store:0}`} bordered />
            </Col>
            <Col sm={4} xs={14}>
              <Info title="Instalation" value={`${summary.summary?summary.summary.instalation:0}/${summary.summary?summary.summary.store:0}`} bordered />
            </Col>
            <Col sm={4} xs={14}>
              <Info title="Removed" value={`${summary.summary?summary.summary.removed:0}/${summary.summary?summary.summary.store:0}`} />
            </Col>
            <Col sm={12} xs={14}>
              &nbsp; &nbsp;Program
              <Pie
                data={summary.summary?summary.summary.programs:''}
                height={128}
                hasLegend
                tooltip
              />
            </Col>
          </Row>
        </Card>
        <Card title='Instalation' bordered={false}>
          <Table
            dataSource={summary?summary.retails:''}
            expandedRowRender={expandedRowRender}
            pagination={false}
            columns={this.columns}
           // scroll={{x: 960}}
          />
        </Card>
      </div>
    );
  }
}

export default Instalation;
