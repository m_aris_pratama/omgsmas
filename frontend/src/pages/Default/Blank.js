import React, { PureComponent } from 'react';
import { FormattedMessage } from 'umi/locale';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

// eslint-disable-next-line react/prefer-stateless-function
class Blank extends PureComponent {
  render() {
    
    return (
      <PageHeaderWrapper
        title={<FormattedMessage id="app.home.introduce" />}
        content={<FormattedMessage id="app.monitor.waiting-for-implementation" />}
      />
    );
  }
}

export default Blank;
