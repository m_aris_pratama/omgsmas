/* global document */
import React, { PureComponent, Fragment } from 'react'
import moment from 'moment'
import { Divider, Table,Badge,Avatar } from 'antd'
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';

class List extends PureComponent {

  state = {vmodal: false, photo: ''};

  columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      width: 200,
    },
    {
      title: 'Pre CheckIn',
      render: (text, record) => {
        const a = record.absences.find(x=>x.absenceType === 'pre-check-in');
        if (a){
          return ( <span>{moment(a.createdAt).format('DD MMMM YYYY HH:mm')}</span>)
        }
        return ''
      },
      width: 180
    },
    {
      title: 'Location',
      render: (text, record) => {
        const a = record.absences.find(x=>x.absenceType === 'pre-check-in');
        if (a){
          return (
            <a target="_blank" href={`http://www.google.com/maps/place/${a.latitude},${a.longitude}`}>
              {a.store?a.store.name:`(${a.latitude},${a.longitude})`}
            </a>)
        }
        return '';
      },
      dataIndex: ''
    }
  ];

  handleTableChange = value => {
    const { onTableChange } = this.props;
    if (onTableChange) {
        onTableChange(value);
    }
  }

  handleEdit = value => {
    const { onEdit } = this.props;
    if (onEdit) {
        onEdit(value);
    }
  }

  handleDelete = value => {
    const { onDelete } = this.props;
    if (onDelete) {
        onDelete(value);
    }
  }

  showModal = id => {
    this.setState({
      vmodal: true,
      photo: id
    });
  }

  handleOk = () => {
    this.setState({
      vmodal: false
    });
  }

  render() {
    const {
        list,
        metaProps,
        loading
    } = this.props;
  
    const { vmodal, photo} = this.state;

    const expandedRowRender = (record) => {
      const attn = [];
      record.absences.filter(a=>a.store !== null).forEach(el => {
        let ss = attn.find(x => x.id === el.store.id && x.date === el.absenceDate);
        if (!ss) attn.push(ss={
          id: el.store.id,
          date: el.absenceDate, 
          name: el.store.name,
          longitude: el.longitude,
          latitude: el.latitude,
          checkIn: null,
          checkInPhoto: null,
          checkOut: null,
          checkOutPhoto: null
        });
        switch (el.absenceType){
          case 'check-in': 
            ss.checkIn = ss.checkIn?ss.checkIn:el.createdAt; 
            ss.checkInPhoto = ss.checkInPhoto?ss.checkInPhoto:el.photo;
            break;
          case 'check-out': 
            ss.checkOut = el.createdAt; 
            ss.checkOutPhoto = el.photo;
          break;
          default:
        }
      });

      const columns = [
        { title: 'Store', 
        render: (text, item) => (
          <a target='_blank' href={`http://www.google.com/maps/place/${item.latitude},${item.longitude}`}>
            {item.name}
          </a>
        )},
        { title: 'CheckIn', width:200,
        render: (text, item) => (
          <span>{item.checkIn?moment(item.checkIn).format('DD MMMM YYYY HH:mm'):''}</span>
        )},
        { title: '',width:100,
        render: (text, item) => (
          <Avatar src={`/api/absence/photo/${item.checkInPhoto}`} shape="square" size="large" onClick={() => {this.showModal(`/api/absence/photo/${item.checkInPhoto}`); }} />
          
        )},
        { title: 'CheckOut', width:200,
        render: (text, item) => (
          <span>{item.checkOut?moment(item.checkOut).format('DD MMMM YYYY HH:mm'):''}</span>
        )},
        { title: '',width:100,
        render: (text, item) => (
          <Avatar src={`/api/absence/photo/${item.checkOutPhoto}`} shape="square" size="large" onClick={() => {this.showModal(`/api/absence/photo/${item.checkOutPhoto}`); }} />
          
        )},
        { title: 'Duration (mt)',
        render: (text, item) => (
          <span>{item.checkOut?moment(item.checkOut).diff(moment(item.checkIn),'m'):''}</span>
        )},
      ];

      return (<div>
        {vmodal && (
          <Lightbox
            mainSrc={photo}
            onCloseRequest={() => this.setState({ vmodal: false })} 
          />
        )}
        <Table
          columns={columns}
          dataSource={attn}
          pagination={false}
        />
        </div>
      );
    }

    return (<Table
      dataSource={list}
      pagination={metaProps}
      onChange={this.handleTableChange}
      loading={loading}
      columns={this.columns}
      expandedRowRender={expandedRowRender}
    />
    );
  }
}

export default List;
