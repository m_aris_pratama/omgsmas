/* global document */
import React, { PureComponent } from 'react'
import { Form, Button, Row, Col } from 'antd'
import { ExportToCsv } from "export-to-csv"
import moment from 'moment'
@Form.create()
class Export extends PureComponent {
  state = {
    loading: false,
  }
  async exportCsv() {
    this.setState({ loading: true });
    const { dispatch, filters } = this.props;
    const params = {
      cur_page: 1,
      per_page: 999999999,
      ...filters
    };
    await dispatch({
      type: 'absenceMembers/fetchExport',
      payload: params
    });
    const { exportList } = this.props.absenceMembers;
    const exportData = this.reMapDetail(exportList);
    if (exportData.length > 0) {
      const search = typeof filters.q != 'undefined' && filters.q != '' ? 'search ' + filters.q : '';
      const options = {
        fieldSeparator: ",",
        quoteStrings: '"',
        decimalSeparator: ".",
        showLabels: true,
        showTitle: false,
        filename: `Field Officer Attendance ${moment(filters.dateFrom).format('DD MMMM YYYY')} to ${moment(filters.dateTo).format('DD MMMM YYYY')} ${search}`,
        useTextFile: false,
        useBom: true,
        headers: ['Field Officer Name', 'Store Name', 'Check In', 'Check Out', 'Duration(mt)', 'Longitude', 'Latitude']
      };
      const csvExporter = new ExportToCsv(options);
      csvExporter.generateCsv(exportData);
    }
    this.setState({ loading: false });
  }

  reMapDetail(exportList) {
    const attn = [];
    for (const record of exportList) {
      const detail = record.absences.filter(x => x.store != null);
      for (const el of detail) {
        if (el.store.id) {
          let ss = attn.find(x => x.id === el.store.id && x.date === el.absenceDate);
          if (!ss) {
            attn.push(ss = {
              personName: record.name,
              id: el.store.id,
              date: el.absenceDate,
              name: el.store.name,
              checkIn: null,
              checkOut: null,
              duration: '',
              longitude: el.longitude,
              latitude: el.latitude,
            });
          }
          switch (el.absenceType) {
            case 'check-in':
              ss.checkIn = ss.checkIn ? moment(ss.checkIn).format('DD MMMM YYYY HH:mm') : moment(el.createdAt).format('DD MMMM YYYY HH:mm');
              break;
            case 'check-out':
              ss.checkOut = moment(el.createdAt).format('DD MMMM YYYY HH:mm');
              break;
            default:
          }
          if (ss.checkOut && ss.checkIn) {
            ss.duration = ss.checkOut && ss.checkIn ? moment(ss.checkOut).diff(moment(ss.checkIn), 'm') : ''
          }
        }
      }
    }
    for (const d in attn) {
      delete attn[d].id;
      delete attn[d].date;
    }
    return attn;
  }

  render() {
    return (
      <div style={{ marginBottom: 24 }}>
        <Row>
          <Col span={6}>
            <Button icon="export" onClick={() => this.exportCsv()} loading={this.state.loading} >Export CSV</Button>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Export;
