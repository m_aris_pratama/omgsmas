/* global document */
import React, { PureComponent } from 'react'
import moment from 'moment'
import { Form, Button, Row, Col, DatePicker, Input, Cascader,Select } from 'antd'
const { Search, TextArea } = Input;
const { Option } = Select;
const dateFormat = 'YYYY/MM/DD';

const { RangePicker } = DatePicker;

@Form.create()
class Filter extends PureComponent {

  handleDateChange = (dates,dateString) => {
    const { onDateChange } = this.props;
    if (onDateChange) {
      if (dates.length === 2 && moment(dates[1].isAfter(dates[0])))
        onDateChange(dates);
    }
  }

  handleSearch = (value) => {
    const { onSearch } = this.props;
    if (onSearch) {
      onSearch(value);
    }
  }

  render() {

    return (
      <div style={{marginBottom:24}}>
        <Row>
          <Col span={18}>
            <RangePicker
              defaultValue={[moment().subtract(7, 'days'), moment()]}
              format={dateFormat}
              onChange={this.handleDateChange}
            />
          </Col>
          <Col span={6}>
            <Search onSearch={this.handleSearch} justify="end" placeholder="search name" />
          </Col>
        </Row>
      </div>
    );
  }
}

export default Filter;
