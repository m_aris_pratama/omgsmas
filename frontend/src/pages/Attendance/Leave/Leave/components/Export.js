/* global document */
import React, { PureComponent } from 'react'
import { Form, Button, Row, Col } from 'antd'
import { ExportToCsv } from "export-to-csv"
import moment from 'moment'
@Form.create()
class Export extends PureComponent {
  state = {
    loading: false,
  }
  async exportCsv() {
    this.setState({ loading: true });
    const { dispatch, filters } = this.props;
    const params = {
      cur_page: 1,
      per_page: 999999999,
      ...filters
    };
    await dispatch({
      type: 'absenceLeave/fetchExport',
      payload: params
    });
    const { exportList } = this.props.absenceLeave;
    const exportData = this.reMapDetail(exportList);
    if (exportData.length > 0) {
      const search = typeof filters.q != 'undefined' && filters.q != '' ? 'search ' + filters.q : '';
      const options = {
        fieldSeparator: ",",
        quoteStrings: '"',
        decimalSeparator: ".",
        showLabels: true,
        showTitle: false,
        filename: `Leave Report ${moment(filters.dateFrom).format('DD MMMM YYYY')} to ${moment(filters.dateTo).format('DD MMMM YYYY')} ${search}`,
        useTextFile: false,
        useBom: true,
        headers: ['Name Name', 'Leave', 'Longitude', 'Latitude', 'Note']
      };
      const csvExporter = new ExportToCsv(options);
      csvExporter.generateCsv(exportData);
    }
    this.setState({ loading: false });
  }

  reMapDetail(exportList) {
    const attn = [];
    for (const record of exportList) {
      for (const el of record.absences) {
        let ss = attn.find(x => x.id == el.id && x.absenceType === 'leave' && x.date === el.absenceDate);
        if (!ss) {
          attn.push(ss = {
            id: el.id,
            date: el.absenceDate,
            name: record.name,
            leave: el.createdAt ? moment(el.createdAt).format('DD MMMM YYYY HH:mm') : "",
            longitude: el.longitude ? el.longitude : "",
            latitude: el.latitude ? el.latitude : "",
            note: el.note ? el.note : "",
          })
        }
      }
    }
    for (const d in attn) {
      delete attn[d].id;
      delete attn[d].date;
    }
    return attn;
  }

  render() {
    return (
      <div style={{ marginBottom: 24 }}>
        <Row>
          <Col span={6}>
            <Button icon="export" onClick={() => this.exportCsv()} loading={this.state.loading} >Export CSV</Button>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Export;
