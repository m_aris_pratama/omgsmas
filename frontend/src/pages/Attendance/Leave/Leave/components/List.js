/* global document */
import React, { PureComponent, Fragment } from 'react'
import moment from 'moment'
import { Divider, Table,Badge,Avatar } from 'antd'
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';

class List extends PureComponent {

  state = {vmodal: false, photo: ''};

  columns = [
    {
      title: 'Name',
      dataIndex: 'name',
    },
    {
      title: 'Leave',
      render: (text, record) => {
        const a = record.absences.find(x=>x.absenceType === 'leave');
        if (a){
          return ( <span>{moment(a.created).format('DD MMMM YYYY HH:mm')}</span>)
        }
        return ''
      },
      width: 180
    },
    {
      title: 'Location',
      render: (text, record) => {
        const a = record.absences.find(x=>x.absenceType === 'leave');
        if (a){
          return (
            <a target="_blank" href={`http://www.google.com/maps/place/${a.latitude},${a.longitude}`}>
              {a.store?a.store.name:`(${a.latitude},${a.longitude})`}
            </a>)
        }
        return '';
      }
    },
    {
      title: 'Note',
      render: (text, record) => {
        const a = record.absences.find(x=>x.absenceType === 'leave');
        if (a){
          return (a.note)
        }
        return '';
      },
    },
  ];

  handleTableChange = value => {
    const { onTableChange } = this.props;
    if (onTableChange) {
        onTableChange(value);
    }
  }

  handleEdit = value => {
    const { onEdit } = this.props;
    if (onEdit) {
        onEdit(value);
    }
  }

  handleDelete = value => {
    const { onDelete } = this.props;
    if (onDelete) {
        onDelete(value);
    }
  }

  showModal = id => {
    this.setState({
      vmodal: true,
      photo: id
    });
  }

  handleOk = () => {
    this.setState({
      vmodal: false
    });
  }

  render() {
    const {
        list,
        metaProps,
        loading
    } = this.props;

    return (<Table
      dataSource={list}
      pagination={metaProps}
      onChange={this.handleTableChange}
      loading={loading}
      columns={this.columns}
    />
    );
  }
}

export default List;
