import React, { PureComponent } from 'react';
import { notification, Card, Icon, Button, Form } from 'antd';
import { connect } from 'dva';
import moment from 'moment'
import styles from './index.less';

import Filter from './components/Filter';
import List from './components/List';
import Export from './components/Export';

@Form.create()
@connect(({ absenceLeave }) => ({
  absenceLeave
}))
class Leave extends PureComponent {

  componentDidMount() {
    const { dispatch, absenceLeave: { filters } } = this.props;
    filters.q = '';
    dispatch({
      type: 'absenceLeave/fetch',
      payload: {
        per_page: 10,
      },
    });
  }


  handleSearch = value => {
    const { dispatch, absenceLeave: { filters } } = this.props;
    filters.q = value;
    const params = {
      cur_page: 1,
      per_page: 10,
      ...filters
    };
    dispatch({
      type: 'absenceLeave/saveFilter',
      payload: filters
    });
    dispatch({
      type: 'absenceLeave/fetch',
      payload: params
    });
  }

  handleDateChange = dates => {
    const { dispatch, absenceLeave: { filters } } = this.props;
    const params = {
      cur_page: 1,
      per_page: 10
    };

    params.dateFrom = moment(dates[0]).format('YYYY-MM-DD');
    params.dateTo = moment(dates[1]).format('YYYY-MM-DD');
    filters.dateFrom = params.dateFrom;
    filters.dateTo = params.dateTo;

    dispatch({
      type: 'absenceLeave/saveFilter',
      payload: filters
    });
    dispatch({
      type: 'absenceLeave/fetch',
      payload: params
    });
  }

  handleTableChange = (pagination, filtersArg, sorter) => {

    const { dispatch } = this.props;
    const params = {
      cur_page: pagination.current,
      per_page: pagination.pageSize
    };
    if (sorter && sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'absenceLeave/fetch',
      payload: params,
    });

  };

  render() {
    const {
      absenceLeave: { list, meta, filters },
      absenceLeave,
      loading,
      dispatch
    } = this.props;

    const metaProps = {
      showSizeChanger: true,
      showQuickJumper: false,
      pageSize: meta.perPage,
      total: meta.totalResults,
      current: meta.curPage
    };

    return (
      <div>
        <Card
          bordered={false}
        >
          <Filter
            onSearch={this.handleSearch}
            onDateChange={this.handleDateChange}
          />
          <Export dispatch={dispatch} absenceLeave={absenceLeave} filters={filters} />
          <List
            list={list}
            metaProps={metaProps}
            loading={loading}
            onTableChange={this.handleTableChange}
            onEdit={this.showEditModal}
            onDelete={this.showDeleteModal}
          />
        </Card>
      </div>
    );
  }
}

export default Leave;
