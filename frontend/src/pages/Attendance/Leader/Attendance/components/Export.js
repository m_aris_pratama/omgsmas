/* global document */
import React, { PureComponent } from 'react'
import { Form, Button, Row, Col } from 'antd'
import { ExportToCsv } from "export-to-csv"
import moment from 'moment'
@Form.create()
class Export extends PureComponent {
  state = {
    loading: false,
  }
  async exportCsv() {
    this.setState({ loading: true });
    const { dispatch, filters } = this.props;
    const params = {
      cur_page: 1,
      per_page: 999999999,
      ...filters
    };
    await dispatch({
      type: 'absenceLeads/fetchExport',
      payload: params
    });
    const { exportList } = this.props.absenceLeads;
    const exportData = this.reMapDetail(exportList);
    if (exportData.length > 0) {
      const search = typeof filters.q != 'undefined' && filters.q != '' ? 'search ' + filters.q : '';
      const options = {
        fieldSeparator: ",",
        quoteStrings: '"',
        decimalSeparator: ".",
        showLabels: true,
        showTitle: false,
        filename: `Team Lead Attendance ${moment(filters.dateFrom).format('DD MMMM YYYY')} to ${moment(filters.dateTo).format('DD MMMM YYYY')} ${search}`,
        useTextFile: false,
        useBom: true,
        headers: ['Team Lead Name', 'Pre Check In', 'Pre Check In Lat', 'Pre Check In Lng', 'Check In', 'Check In Lat', 'Check In Lng', 'Check Out', 'Check Out Lat', 'Check Out Lng']
      };
      const csvExporter = new ExportToCsv(options);
      csvExporter.generateCsv(exportData);
    }
    this.setState({ loading: false });
  }

  reMapDetail(exportList) {
    const attn = [];
    for (const record of exportList) {
      for (const el of record.absences) {
        let ss = attn.find(x => x.id === record.id && x.date === el.absenceDate);
        if (!ss) {
          attn.push(ss = {
            id: record.id,
            date: el.absenceDate,
            name: record.name,
            precheckIn: null,
            precheckInLat: null,
            precheckInLng: null,
            checkIn: null,
            checkInLat: null,
            checkInLng: null,
            checkOut: null,
            checkOutLat: null,
            checkOutLng: null,
          })
        }
        switch (el.absenceType) {
          case 'pre-check-in':
            ss.precheckIn = ss.precheckIn ? moment(ss.precheckIn).format('DD MMMM YYYY HH:mm') : moment(el.createdAt).format('DD MMMM YYYY HH:mm');
            ss.precheckInLat = ss.precheckInCoord ? ss.precheckInCoord.longitude : el.longitude;
            ss.precheckInLng = ss.precheckInCoord ? ss.precheckInCoord.latitude : el.latitude;
            break;
          case 'check-in':
            ss.checkIn = ss.checkIn ? moment(ss.checkIn).format('DD MMMM YYYY HH:mm') : moment(el.createdAt).format('DD MMMM YYYY HH:mm');
            ss.checkInLat = ss.checkInCoord ? ss.checkInLat.longitude : el.longitude;
            ss.checkInLng = ss.checkInCoord ? ss.checkInLng.latitude : el.latitude;
            break;
          case 'check-out':
            ss.checkOut = moment(el.createdAt).format('DD MMMM YYYY HH:mm');
            ss.checkOutLat = ss.checkOutCoord ? ss.checkOutLat.longitude : el.longitude;
            ss.checkOutLng = ss.checkOutCoord ? ss.checkOutLng.latitude : el.latitude;
            break;
          default:
        }
      }
    }
    for (const d in attn) {
      delete attn[d].id;
      delete attn[d].date;
    }
    return attn;
  }

  render() {
    return (
      <div style={{ marginBottom: 24 }}>
        <Row>
          <Col span={6}>
            <Button icon="export" onClick={() => this.exportCsv()} loading={this.state.loading} >Export CSV</Button>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Export;
