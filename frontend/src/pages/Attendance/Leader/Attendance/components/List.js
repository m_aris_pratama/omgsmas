/* global document */
import React, { PureComponent, Fragment } from 'react'
import moment from 'moment'
import { Divider, Table,Badge,Avatar } from 'antd'
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';

class List extends PureComponent {

  state = {vmodal: false, photo: ''};

  columns = [
    {
      title: 'Name',
      dataIndex: 'name',
    },
    {
      title: 'Pre CheckIn',
      render: (text, record) => {
        if (record.precheckIn)
          return moment(record.precheckIn).format('DD MMMM YYYY HH:mm')
        return '';
      },
      width: 180
    },
    {
      title: 'Location',
      render: (text, record) => {
        if (record.precheckInCoord){
          return (
            <a target="_blank" href={`http://www.google.com/maps/place/${record.precheckInCoord.lat},${record.precheckInCoord.lng}`}>
              ({record.precheckInCoord.lat},{record.precheckInCoord.lng})
            </a>)
        }
        return '';
      },
      dataIndex: ''
    },
    {
      title: 'CheckIn',
      render: (text, record) => {
        if (record.checkIn)
          return moment(record.checkIn).format('DD MMMM YYYY HH:mm')
        return ''
      },
      width: 180
    },
    {
      title: 'Location',
      render: (text, record) => {
        if (record.checkInCoord){
          return (
            <a target="_blank" href={`http://www.google.com/maps/place/${record.checkInCoord.lat},${record.checkInCoord.lng}`}>
              ({record.checkInCoord.lat},{record.checkInCoord.lng})
            </a>)
        }
        return '';
      },
      dataIndex: ''
    },
    {
      title: 'CheckOut',
      render: (text, record) => {
        if (record.checkOut)
          return moment(record.checkOut).format('DD MMMM YYYY HH:mm')
        return ''
      },
      width: 180
    },
    {
      title: 'Location',
      render: (text, record) => {
        if (record.checkOutCoord){
          return (
            <a target="_blank" href={`http://www.google.com/maps/place/${record.checkOutCoord.lat},${record.checkOutCoord.lng}`}>
              ({record.checkOutCoord.lat},{record.checkOutCoord.lng})
            </a>)
        }
        return '';
      },
      dataIndex: ''
    },
  ];

  handleTableChange = value => {
    const { onTableChange } = this.props;
    if (onTableChange) {
        onTableChange(value);
    }
  }

  handleEdit = value => {
    const { onEdit } = this.props;
    if (onEdit) {
        onEdit(value);
    }
  }

  handleDelete = value => {
    const { onDelete } = this.props;
    if (onDelete) {
        onDelete(value);
    }
  }

  showModal = id => {
    this.setState({
      vmodal: true,
      photo: id
    });
  }

  handleOk = () => {
    this.setState({
      vmodal: false
    });
  }

  render() {
    const {
        list,
        metaProps,
        loading
    } = this.props;

    const attnlead = [];
    list.forEach(val =>{
      val.absences.forEach(el => {
        console.log(val);
        let ss = attnlead.find(x=>x.id === val.id && x.date === el.absenceDate);
        if (!ss){
          attnlead.push(ss={
            id: val.id,
            date: el.absenceDate, 
            name: val.name,
            precheckIn: null,
            precheckInPhoto: null,
            precheckInCoord: null,
            checkIn: null,
            checkInPhoto: null,
            checkInCoord:null, 
            checkOut: null,
            checkOutPhoto: null,
            checkOutCoord: null,
          })
        }
        switch (el.absenceType){
          case 'pre-check-in': 
          ss.precheckIn = ss.precheckIn?ss.precheckIn:el.createdAt; 
          ss.precheckInPhoto = ss.precheckInPhoto?ss.precheckInPhoto:el.photo;
          ss.precheckInCoord = ss.precheckInCoord?ss.precheckInCoord:{lng:el.longitude,lat:el.latitude}
          break;
          case 'check-in': 
            ss.checkIn = ss.checkIn?ss.checkIn:el.createdAt; 
            ss.checkInPhoto = ss.checkInPhoto?ss.checkInPhoto:el.photo;
            ss.checkInCoord = ss.checkInCoord?ss.checkInCoord:{lng:el.longitude,lat:el.latitude}
            break;
          case 'check-out': 
            ss.checkOut = el.createdAt; 
            ss.checkOutPhoto = el.photo;
            ss.checkOutCoord = {lng:el.longitude,lat:el.latitude}
          break;
          default:
        }
      })
    })
    console.log(attnlead);

    return (<Table
      dataSource={attnlead.filter(x=>x.precheckIn !== null)}
      pagination={metaProps}
      onChange={this.handleTableChange}
      loading={loading}
      columns={this.columns}
    />
    );
  }
}

export default List;
