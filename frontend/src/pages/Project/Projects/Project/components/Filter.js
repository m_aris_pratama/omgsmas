/* global document */
import React, { PureComponent } from 'react'
import moment from 'moment'
import { Form, Button, Row, Col, DatePicker, Input, Cascader,Select } from 'antd'

const { Search, TextArea } = Input;
const { Option } = Select;


@Form.create()
class Filter extends PureComponent {

  handleSearch = (value) => {
    const { onSearch } = this.props;
    if (onSearch) {
      onSearch(value);
    }
  }

  render() {

    return (
      <div style={{marginBottom:24}}>
        <Row>
          <Col span={18}>
            &nbsp;
          </Col>
          <Col span={6}>
            <Search onSearch={this.handleSearch} justify="end" placeholder="search name" />
          </Col>
        </Row>
      </div>
    );
  }
}

export default Filter;
