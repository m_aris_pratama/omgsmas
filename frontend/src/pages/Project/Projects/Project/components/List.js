/* global document */
import React, { PureComponent, Fragment } from 'react'
import moment from 'moment'
import { Divider, Table } from 'antd'


class List extends PureComponent {

  columns = [
    {
      title: 'Program',
      dataIndex: 'name'
    },
    {
      title: 'Client',
      dataIndex: 'brand.client.name'
    },
    {
      title: 'Brand',
      dataIndex: 'brand.name'
    },
    {
      title: 'Business Unit',
      dataIndex: 'businessUnit.name'
    },
    {
      title: 'Description',
      dataIndex: 'information'
    },
    {
      title: 'Cycle Start',
      dataIndex: 'cycle_start'
    },
    {
      title: 'Cycle End',
      dataIndex: 'cycle_end'
    },
    {
      title: 'Actions',
      fixed: 'right',
      width:180,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleEdit(record)}>View or Edit</a>
          <Divider type="vertical" />
          <a onClick={() => this.handleDelete(record.id)}>Delete</a>
        </Fragment>
      ),
    },
  ];

  handleTableChange = value => {
    const { onTableChange } = this.props;
    if (onTableChange) {
        onTableChange(value);
    }
  }

  handleEdit = value => {
    const { onEdit } = this.props;
    if (onEdit) {
        onEdit(value);
    }
  }

  handleDelete = value => {
    const { onDelete } = this.props;
    if (onDelete) {
        onDelete(value);
    }
  }

  render() {
    const {
        list,
        metaProps,
        loading
    } = this.props;

    return (<Table
      dataSource={list}
      pagination={metaProps}
      onChange={this.handleTableChange}
      loading={loading}
      columns={this.columns}
    />
    );
  }
}

export default List;
