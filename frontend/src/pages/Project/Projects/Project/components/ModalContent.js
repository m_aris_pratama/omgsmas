/* global document */
import React, { PureComponent } from 'react';
import moment from 'moment';
import { Form, Upload, Icon, Input, Select, DatePicker, Col, Row, message, Switch } from 'antd';

const { TextArea } = Input;
const FormItem = Form.Item;
const InputGroup = Input.Group;
const { Option } = Select;

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

class ModalContent extends PureComponent {
  formLayout = {
    labelCol: { span: 7 },
    wrapperCol: { span: 13 },
  };

  state = {
    loading: false,
  };

  componentDidMount() {
    const { current } = this.props;
    this.setState({ imageName: current.photo });
  }

  handleSubmit = e => {
    const { onSubmit } = this.props;
    if (onSubmit) {
      onSubmit(e);
    }
  };

  handleChange = info => {
    console.log(info);
    if (info.file.response) this.setState({ imageName: info.file.response.fileName });
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>
        this.setState({
          imageUrl,
          loading: false,
        })
      );
    }
  };

  render() {
    const {
      form: { getFieldDecorator },
      current,
      brands,
      units,
      taskTemplates,
      programs,
    } = this.props;

    const { loading, imageUrl, imageName } = this.state;

    const uploadButton = (
      <div>
        <Icon type={loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );

    return (
      <Form onSubmit={this.handleSubmit}>
        <Row>
          <Col span={11}>
            <FormItem label="Program" {...this.formLayout}>
              {getFieldDecorator('name', {
                rules: [{ required: true, message: 'Please enter project name' }],
                initialValue: current.name,
              })(
                <Select
                  showSearch
                  style={{ width: 200 }}
                  placeholder="Select program"
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {programs.list.map(val => (
                    <Option value={val.name}>{val.name}</Option>
                  ))}
                </Select>
              )}
            </FormItem>
            <FormItem label="Brand" {...this.formLayout}>
              {getFieldDecorator('brand', {
                rules: [{ required: true, message: 'Please enter brand' }],
                initialValue: current.brand ? current.brand.id : '',
              })(
                <Select
                  showSearch
                  style={{ width: 200 }}
                  placeholder="Select a Brand"
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {brands.list.map(val => (
                    <Option value={val.id}>{val.name}</Option>
                  ))}
                </Select>
              )}
            </FormItem>
            <FormItem label="Information" {...this.formLayout}>
              {getFieldDecorator('information', {
                rules: [{ required: true }],
                initialValue: current.information,
              })(<TextArea rows={3} placeholder="information" />)}
            </FormItem>
            <FormItem label="Photo" {...this.formLayout}>
              {getFieldDecorator('photo', {
                rules: [{ required: false }],
                initialValue: imageName,
              })(<Input hidden />)}
              <Upload
                name="file"
                listType="picture-card"
                className="avatar-uploader"
                showUploadList={false}
                action="/api/clients/uploadImage"
                onChange={this.handleChange}
              >
                {imageUrl || imageName ? (
                  <img
                    style={{ width: 150 }}
                    src={imageUrl || `/api/clients/uploadImage/${imageName}`}
                    alt=""
                  />
                ) : (
                  uploadButton
                )}
              </Upload>
            </FormItem>
            <FormItem label="Cycle Year" {...this.formLayout}>
              {getFieldDecorator('cycle_year', {
                rules: [{ required: true, message: '' }],
                initialValue: current.cycle_year,
              })(<Input style={{ width: 70 }} placeholder="" />)}
            </FormItem>
            <FormItem label="Cycle Start/End" {...this.formLayout}>
              <InputGroup compact>
                {getFieldDecorator('cycle_start', {
                  rules: [{ required: true, message: '' }],
                  initialValue: current.cycle_start,
                })(<Input style={{ width: 50 }} placeholder="" />)}
                {<span>&nbsp;-&nbsp;&nbsp;</span>}
                {getFieldDecorator('cycle_end', {
                  rules: [{ required: true, message: '' }],
                  initialValue: current.cycle_end,
                })(<Input style={{ width: 50 }} placeholder="" />)}
              </InputGroup>
            </FormItem>
            <FormItem label="Business Unit" {...this.formLayout}>
              {getFieldDecorator('businessUnit', {
                rules: [{ required: true, message: 'Please enter business unit' }],
                initialValue: current.businessUnit ? current.businessUnit.id : '',
              })(
                <Select
                  disabled={!!current.businessUnit}
                  showSearch
                  style={{ width: 200 }}
                  placeholder="Select a business Unit"
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {units.list.map(val => (
                    <Option value={val.id}>{val.name}</Option>
                  ))}
                </Select>
              )}
            </FormItem>
            <FormItem label="Programs" {...this.formLayout}>
              {getFieldDecorator('programs', {
                rules: [{ required: true, message: 'Please enter programs' }],
                initialValue: current.programs ? current.programs.split(',') : [],
              })(
                <Select
                  mode="multiple"
                  showSearch
                  style={{ width: 200 }}
                  placeholder="Select programs"
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {programs.list.map(val => (
                    <Option value={val.name}>{val.name}</Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col span={13}>
            <FormItem label="Survey Date" {...this.formLayout}>
              {getFieldDecorator('survey_date', {
                rules: [{ required: true, message: '' }],
                initialValue: moment(current.survey_date),
              })(<DatePicker />)}
            </FormItem>
            <FormItem label="Survey Target (days)" {...this.formLayout}>
              {getFieldDecorator('survey_target', {
                rules: [{ required: true, message: 'please target in days' }],
                initialValue: current.survey_target,
              })(<Input style={{ width: 110 }} placeholder="target in days" />)}
            </FormItem>
            <FormItem label="Instalation Date JABO" {...this.formLayout}>
              {getFieldDecorator('instalation_date_jabo', {
                rules: [{ required: false, message: '' }],
                initialValue: moment(current.instalation_date_jabo),
              })(<DatePicker />)}
            </FormItem>
            <FormItem label="Instalation Date Others" {...this.formLayout}>
              {getFieldDecorator('instalation_date', {
                rules: [{ required: false, message: '' }],
                initialValue: moment(current.instalation_date),
              })(<DatePicker />)}
            </FormItem>
            <FormItem label="Instalation Target (days)" {...this.formLayout}>
              {getFieldDecorator('instalation_target', {
                rules: [{ required: false, message: 'please target in days' }],
                initialValue: current.instalation_target,
              })(<Input style={{ width: 110 }} placeholder="target in days" />)}
            </FormItem>
            <FormItem label="Removed Date" {...this.formLayout}>
              {getFieldDecorator('removed_date', {
                rules: [{ required: false, message: '' }],
                initialValue: moment(current.removed_date),
              })(<DatePicker />)}
            </FormItem>
            <FormItem label="Removed Target (days)" {...this.formLayout}>
              {getFieldDecorator('removed_target', {
                rules: [{ required: false, message: 'please target in days' }],
                initialValue: current.instalation_target,
              })(<Input style={{ width: 110 }} placeholder="target in days" />)}
            </FormItem>
            <FormItem label="Survey Template" {...this.formLayout}>
              {getFieldDecorator('taskSurvey', {
                rules: [{ required: true, message: 'Please enter survey tasks' }],
                initialValue: current.taskSurvey ? current.taskSurvey.id : null,
              })(
                <Select
                  showSearch
                  style={{ width: 200 }}
                  placeholder="Select a Survey Template"
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {taskTemplates.list
                    .filter(x => x.category === 1)
                    .map(val => (
                      <Option value={val.id}>{val.name}</Option>
                    ))}
                </Select>
              )}
            </FormItem>
            <FormItem label="Instalation Template" {...this.formLayout}>
              {getFieldDecorator('taskInstalation', {
                rules: [{ required: false, message: 'Please enter Instalation tasks' }],
                initialValue: current.taskInstalation ? current.taskInstalation.id : null,
              })(
                <Select
                  showSearch
                  style={{ width: 200 }}
                  placeholder="Select a Instalation Template"
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {taskTemplates.list
                    .filter(x => x.category === 2)
                    .map(val => (
                      <Option value={val.id}>{val.name}</Option>
                    ))}
                </Select>
              )}
            </FormItem>
            <FormItem label="Not Installed Template" {...this.formLayout}>
              {getFieldDecorator('taskNotInstalled', {
                rules: [{ required: false, message: 'Please enter Not Installed tasks' }],
                initialValue: current.taskNotInstalled ? current.taskNotInstalled.id : null,
              })(
                <Select
                  showSearch
                  style={{ width: 200 }}
                  placeholder="Select a Not Installed Template"
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {taskTemplates.list
                    .filter(x => x.category === 3)
                    .map(val => (
                      <Option value={val.id}>{val.name}</Option>
                    ))}
                </Select>
              )}
            </FormItem>
            <FormItem label="Removed Template" {...this.formLayout}>
              {getFieldDecorator('taskRemoved', {
                rules: [{ required: false, message: 'Please enter Removed tasks' }],
                initialValue: current.taskRemoved ? current.taskRemoved.id : null,
              })(
                <Select
                  showSearch
                  style={{ width: 200 }}
                  placeholder="Select a Removed Template"
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {taskTemplates.list
                    .filter(x => x.category === 4)
                    .map(val => (
                      <Option value={val.id}>{val.name}</Option>
                    ))}
                </Select>
              )}
            </FormItem>
            <FormItem label="Active" {...this.formLayout}>
              {getFieldDecorator('is_active', {
                rules: [{ required: false, message: 'Please choose active status' }],
                initialValue: current.is_active,
                valuePropName: 'checked',
              })(
                <Switch
                  checkedChildren={<Icon type="check" />}
                  unCheckedChildren={<Icon type="cross" />}
                />
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default ModalContent;
