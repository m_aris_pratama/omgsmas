import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Card, Row, Col } from 'antd';
import router from 'umi/router';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

import styles from './index.less';

@connect(({ tasks, taskTemplates, loading }) => ({
  tasks,
  taskTemplates,
  taskTemplatesLoading: loading.effects['taskTemplates/fetchAll'],
}))
class Area extends PureComponent {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'taskTemplates/fetchAll',
      payload: {
        per_page: 10,
      },
    });
    dispatch({
      type: 'tasks/fetchAll',
      payload: {
        per_page: 10,
      },
    });
  }

  onTabChange = key => {
    const { match } = this.props;
    switch (key) {
      case 'template':
        router.push(`${match.url}/template`);
        break;
      case 'task':
        router.push(`${match.url}/task`);
        break;
      default:
        break;
    }
  };

  render() {
    const { taskTemplates, taskTemplatesLoading, tasks, match, children, location } = this.props;

    const taskTemplatesTabList = [
      {
        key: 'task',
        tab: <span>Tasks</span>,
      },
      {
        key: 'template',
        tab: <span>Task Templates</span>,
      },
    ];

    const Info = ({ title, value, bordered }) => (
      <div className={styles.headerInfo}>
        <span>{title}</span>
        <p>{value}</p>
        {bordered && <em />}
      </div>
    );

    return (
      <PageHeaderWrapper>
        <div className={styles.standardList}>
          <Card bordered={false}>
            <Row>
              <Col sm={8} xs={24}>
                <Info
                  loading={taskTemplatesLoading}
                  title="Total Templates"
                  value={taskTemplates.total}
                  bordered
                />
              </Col>
              <Col sm={8} xs={24}>
                <Info title="Total Tasks" value={tasks.total} bordered />
              </Col>
            </Row>
          </Card>
          <Card
            className={styles.tabsCard}
            style={{ marginTop: 24 }}
            bordered={false}
            tabList={taskTemplatesTabList}
            activeTabKey={location.pathname.replace(`${match.path}/`, '')}
            onTabChange={this.onTabChange}
          >
            {children}
          </Card>
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default Area;
