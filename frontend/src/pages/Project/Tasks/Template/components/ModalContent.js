/* global document */
import React, { PureComponent } from 'react';
import moment from 'moment';
import { Form, Button, Row, Col, DatePicker, Input, Cascader, Select } from 'antd';

const { Option } = Select;
const FormItem = Form.Item;

class ModalContent extends PureComponent {
  formLayout = {
    labelCol: { span: 7 },
    wrapperCol: { span: 13 },
  };

  handleSubmit = e => {
    const { onSubmit } = this.props;
    if (onSubmit) {
      onSubmit(e);
    }
  };

  render() {
    const {
      form: { getFieldDecorator },
      current,
    } = this.props;

    const cat = [{id:1,name:'survey'},{id:2,name:'instalation'},{id:3,name:'not installed'},{id:4,name:'removed'}];

    return (
      <Form onSubmit={this.handleSubmit}>
        <FormItem label="Name" {...this.formLayout}>
          {getFieldDecorator('name', {
            rules: [{ required: true, message: 'Please enter teamplate name' }],
            initialValue: current.name,
          })(<Input placeholder="name" />)}
        </FormItem>
        <FormItem label="Category" {...this.formLayout}>
          {getFieldDecorator('category', {
            rules: [{ required: true, message: 'Please enter category' }],
            initialValue: current.category,
          })(
            <Select
              showSearch
              style={{ width: 200 }}
              placeholder="Select a Category"
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              {cat.map((val)=><Option value={val.id}>{val.name}</Option>)}
            </Select>
          )}
        </FormItem>
      </Form>
    );
  }
}

export default ModalContent;
