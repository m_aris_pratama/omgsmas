/* global document */
import React, { PureComponent, Fragment } from 'react';
import moment from 'moment';
import { Divider, Table } from 'antd';

class List extends PureComponent {

  columns = [
    {
      title: 'Name',
      dataIndex: 'name',
    },
    {
      title: 'Category',
      render: (text, record) => (
        this.getCategory(record.category)
      ),
    },
    {
      title: 'Actions',
      fixed: 'right',
      width: 120,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleEdit(record)}>Edit</a>
          <Divider type="vertical" />
          <a onClick={() => this.handleDelete(record.id)}>Delete</a>
        </Fragment>
      ),
    },
  ];

  getCategory = inp => {
    const cat = [{id:1,name:'survey'},{id:2,name:'instalation'},{id:3,name:'not installed'},{id:4,name:'removed'}];
    return cat.find(x => x.id === inp).name  
  }

  handleTableChange = value => {
    const { onTableChange } = this.props;
    if (onTableChange) {
      onTableChange(value);
    }
  };

  handleEdit = value => {
    const { onEdit } = this.props;
    if (onEdit) {
      onEdit(value);
    }
  };

  handleDelete = value => {
    const { onDelete } = this.props;
    if (onDelete) {
      onDelete(value);
    }
  };

  render() {
    const { list, metaProps, loading } = this.props;

    return (
      <Table
        dataSource={list}
        pagination={metaProps}
        onChange={this.handleTableChange}
        loading={loading}
        columns={this.columns}
      />
    );
  }
}

export default List;
