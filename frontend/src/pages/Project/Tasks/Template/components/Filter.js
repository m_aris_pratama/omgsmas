/* global document */
import React, { PureComponent } from 'react';
import moment from 'moment';
import { Form, Button, Row, Col, DatePicker, Input, Cascader, Select } from 'antd';

const { Search, TextArea } = Input;
const { Option } = Select;

@Form.create()
class Filter extends PureComponent {
  handleSearch = value => {
    const { onSearch } = this.props;
    if (onSearch) {
      onSearch(value);
    }
  };

  handleCatChange = value => {
    const { onCatChange } = this.props;
    if (onCatChange) {
      onCatChange(value);
    }
  }

  render() {
    const {
      filters
    } = this.props;
    const cat = [{id:1,name:'survey'},{id:2,name:'instalation'},{id:3,name:'not installed'},{id:4,name:'removed'}];

    return (
      <div style={{ marginBottom: 24 }}>
        <Row>
          <Col span={18}>
            <span style={{marginLeft:16}}>Category : </span>
            <Select
              showSearch
              style={{ width: 160 }}
              placeholder="Select a Category"
              optionFilterProp="children"
              onChange={this.handleCatChange}
              value={filters.category}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value={0}>All</Option>
              {cat.map((val)=><Option value={val.id}>{val.name}</Option>)}
            </Select>
          </Col>
          <Col span={6}>
            <Search onSearch={this.handleSearch} justify="end" placeholder="search name" />
          </Col>
        </Row>
      </div>
    );
  }
}

export default Filter;
