/* global document */
import React, { PureComponent } from 'react';
import moment from 'moment';
import { Form, Button, Row, Col, DatePicker, Input, Cascader, Select } from 'antd';

const { Search, TextArea } = Input;
const { Option } = Select;

@Form.create()
class Filter extends PureComponent {
  handleTemplateChange = value => {
    const { onTemplateChange } = this.props;
    if (onTemplateChange) {
      onTemplateChange(value);
    }
  };

  handleSearch = value => {
    const { onSearch } = this.props;
    if (onSearch) {
      onSearch(value);
    }
  };

  render() {
    const { filters, taskTemplates } = this.props;

    return (
      <div style={{ marginBottom: 24 }}>
        <Row>
          <Col span={18}>
            <span style={{ marginLeft: 16 }}>Template : </span>
            <Select
              showSearch
              style={{ width: 160 }}
              placeholder="Select a Template"
              optionFilterProp="children"
              onChange={this.handleTemplateChange}
              value={filters.templateId}
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              <Option value={0}>All</Option>
              {taskTemplates.list.map(val => (
                <Option value={val.id}>{val.name}</Option>
              ))}
            </Select>
          </Col>
          <Col span={6}>
            <Search onSearch={this.handleSearch} justify="end" placeholder="search name" />
          </Col>
        </Row>
      </div>
    );
  }
}

export default Filter;
