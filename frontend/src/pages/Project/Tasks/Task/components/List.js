/* global document */
import React, { PureComponent, Fragment } from 'react';
import moment from 'moment';
import { Divider, Table,Icon } from 'antd';

class List extends PureComponent {
  columns = [
    {
      title: 'Template',
      dataIndex: 'template.name',
      width: 120,
    },
    {
      title: 'Name',
      dataIndex: 'name',
      width: 150,
    },
    {
      title: 'Required',
      dataIndex: 'required',
      render: (text, row) => (
        <span>{row.required?<Icon type="check" />:<Icon type="close" />}</span>
      ),
      width: 100,
    },
    {
      title: 'Type',
      dataIndex: 'type',
      width: 120
    },
    {
      title: 'Options',
      dataIndex: 'options',
      width: 200,
    },
    {
      title: 'Logic',
      dataIndex: 'logic'
    },
    {
      title: 'Actions',
      fixed: 'right',
      width: 120,
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleEdit(record)}>Edit</a>
          <Divider type="vertical" />
          <a onClick={() => this.handleDelete(record.id)}>Delete</a>
        </Fragment>
      ),
    },
  ];

  handleTableChange = value => {
    const { onTableChange } = this.props;
    if (onTableChange) {
      onTableChange(value);
    }
  };

  handleEdit = value => {
    const { onEdit } = this.props;
    if (onEdit) {
      onEdit(value);
    }
  };

  handleDelete = value => {
    const { onDelete } = this.props;
    if (onDelete) {
      onDelete(value);
    }
  };

  render() {
    const { list, metaProps, loading } = this.props;

    return (
      <Table
        dataSource={list}
        pagination={metaProps}
        onChange={this.handleTableChange}
        loading={loading}
        columns={this.columns}
        width={1100}
      />
    );
  }
}

export default List;
