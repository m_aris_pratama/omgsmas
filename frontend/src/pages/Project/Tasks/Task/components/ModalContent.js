/* global document */
import React, { PureComponent } from 'react';
import moment from 'moment';
import { Form, Input, Checkbox, Select } from 'antd';

const { Search, TextArea } = Input;
const { Option } = Select;
const FormItem = Form.Item;

class ModalContent extends PureComponent {
  formLayout = {
    labelCol: { span: 7 },
    wrapperCol: { span: 13 },
  };

  state = {
    checked: true,
  };

  handleSubmit = e => {
    const { onSubmit } = this.props;
    if (onSubmit) {
      onSubmit(e);
    }
  };

  render() {
    const {
      form: { getFieldDecorator },
      current,
      taskTemplates,
    } = this.props;

    const {checked} = this.state;

    const types = ['dropdown','image','text'];

    return (
      <Form onSubmit={this.handleSubmit}>
        <FormItem label="Template" {...this.formLayout}>
          {getFieldDecorator('template', {
            rules: [{ required: true, message: 'Please enter Template' }],
            initialValue: current.template ? current.template.id : '',
          })(
            <Select
              showSearch
              style={{ width: 200 }}
              placeholder="Select a Template"
              optionFilterProp="children"
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {taskTemplates.list.map(val => (
                <Option value={val.id}>{val.name}</Option>
              ))}
            </Select>
          )}
        </FormItem>
        <FormItem label="Name" {...this.formLayout}>
          {getFieldDecorator('name', {
            rules: [{ required: true, message: 'Please enter task name' }],
            initialValue: current.name,
          })(<Input placeholder="name" />)}
        </FormItem>
        <FormItem label="Required" {...this.formLayout}>
          {getFieldDecorator('required', {
            rules: [{ required: false, message: 'Required or no' }],
          })(<Checkbox defaultChecked={current.required?current.required:0} onChange={()=> {this.setState({checked: !checked}); }} />)}
        </FormItem>
        <FormItem label="Type" {...this.formLayout}>
          {getFieldDecorator('type', {
            rules: [{ required: true, message: 'Please Type of Input' }],
            initialValue: current.type ? current.type : '',
          })(
            <Select
              showSearch
              style={{ width: 200 }}
              placeholder="Select a Type of Input"
              optionFilterProp="children"
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {types.map(val => (
                <Option value={val}>{val}</Option>
              ))}
            </Select>
          )}
        </FormItem>
        <FormItem help="if input is more than 1, separate it with commas (,)" label="Options" {...this.formLayout}>
          {getFieldDecorator('options', {
            rules: [{ required: false}],
            initialValue: current.options?current.options:''
          })(<Input placeholder="options" />)}
        </FormItem>
        <FormItem help="example: SPK = image" label="Logic" {...this.formLayout}>
          {getFieldDecorator('logic', {
            rules: [{ required: false}],
            initialValue: current.logic?current.logic:'',
          })(<Input placeholder="logic" />)}
        </FormItem>
      </Form>
    );
  }
}

export default ModalContent;
