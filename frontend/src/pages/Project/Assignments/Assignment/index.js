import React, { PureComponent } from 'react';
import { notification, Card, Icon, Button, Form, Modal } from 'antd';
import { connect } from 'dva';
import styles from './index.less';

import Filter from './components/Filter';
import List from './components/List';
import ModalContent from './components/ModalContent';

@Form.create()
@connect(({ projects, projectAssigns }) => ({
  projectAssigns,
  projects
}))
class AssignList extends PureComponent {

  state = {
    visible: false,
    current: {}
  };

  componentDidMount() {
    const { dispatch, projectAssigns: { filters } } = this.props;
    dispatch({
      type: 'projects/fetch',
      payload: {
        per_page: 0,
      },
    });
    if (filters.projectId > 0) {
      dispatch({
        type: 'projectAssigns/fetch',
        payload: {
          per_page: 0,
          project_id: filters.projectId
        },
      });
    }
  }

  showAddModal = () => {
    this.setState({
      visible: true,
      current: {},
    });
  };

  showEditModal = item => {
    this.setState({
      visible: true,
      current: item,
    });
  };

  showDeleteModal = (id) => {
    Modal.confirm({
      title: 'Delete',
      content: 'Are you sure to delete this item?',
      okText: 'Confirm',
      cancelText: 'Cancel',
      onOk: () => this.deleteItem(id),
    });
  };

  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { dispatch, form } = this.props;
    const { current } = this.state;
    const id = current ? current.id : 0;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      dispatch({
        type: 'projectAssigns/submit',
        payload: { id, ...fieldsValue },
      });
      this.setState({
        visible: false,
      });
      this.openNotification();
    });
  };

  deleteItem = id => {
    const { dispatch } = this.props;
    this.setState({
      visible: true
    });
    dispatch({
      type: 'projectAssigns/submit',
      payload: { id },
    });
    this.setState({
      visible: false,
    });
    this.openNotification();
  };

  handleTypeChange = value => {
    const { dispatch, projectAssigns: { filters } } = this.props;
    const params = {
      //    cur_page: 1,
      per_page: 0
    };
    if (value > 0) params.project_id = value;
    filters.projectId = value
    dispatch({
      type: 'projectAssigns/saveFilter',
      payload: filters
    });
    dispatch({
      type: 'projectAssigns/fetch',
      payload: params
    });
  }

  handleSearch = value => {
    const { dispatch, projectAssigns: { filters } } = this.props;
    const params = {
      cur_page: 1,
      per_page: 10
    };

    params.q = value;
    filters.typeId = 0;
    filters.clientId = 0;
    dispatch({
      type: 'projectAssigns/saveFilter',
      payload: filters
    });
    dispatch({
      type: 'projectAssigns/fetch',
      payload: params
    });
  }


  handleChecked = (record) => {
    const { dispatch } = this.props;
    record.is_active = !record.is_active;
    dispatch({
      type: 'projectAssigns/submit',
      payload: { ...record },
    });
    // this.openNotification();
  };


  handlePriorityChecked = (record) => {
    const { dispatch } = this.props;
    record.is_priority = !record.is_priority;
    dispatch({
      type: 'projectAssigns/submit',
      payload: { ...record },
    });
    //  this.openNotification();
  };

  openNotification = () => {
    notification.open({
      message: 'Successful Operation',
      description: 'Project Assignments table success to changed',
      icon: <Icon type="smile" style={{ color: '#108ee9' }} />,
    });
  };

  render() {
    const {
      projectAssigns: { list, filters },
      projects,
      loading,
      form
    } = this.props;

    const { visible, current } = this.state;

    const modalFooter = { okText: 'Save', onOk: this.handleSubmit, onCancel: this.handleCancel };

    return (
      <div>
        <Card
          bordered={false}
        >
          <Modal
            title={`Assignment ${current.id ? 'Edit' : 'Add'}`}
            className={styles.standardListForm}
            width={640}
            bodyStyle={{ padding: '28px 0 0' }}
            destroyOnClose
            visible={visible}
            {...modalFooter}
          >
            <ModalContent
              form={form}
              current={current}
              onSubmit={this.handleSubmit}
            />
          </Modal>
          {projects.list.length > 0 && (
            <Filter
              onSearch={this.handleSearch}
              filters={filters}
              fillprojects={projects.list}
              onTypeChange={this.handleTypeChange}
            />
          )}
          {projects.filters.projectId ? (<List
            list={list}
            loading={loading}
            onChecked={this.handleChecked}
            onPriorityChecked={this.handlePriorityChecked}
            onEdit={this.showEditModal}
            onDelete={this.showDeleteModal}
          />) : ''}
        </Card>
      </div>
    );
  }
}

export default AssignList;
