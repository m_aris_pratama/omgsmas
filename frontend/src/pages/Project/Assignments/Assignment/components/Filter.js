/* global document */
import React, { PureComponent } from 'react'
import moment from 'moment'
import { Form, Button, Row, Col, DatePicker, Input, Cascader, Select, Card } from 'antd'
import _ from 'lodash'
import { connect } from 'dva';

const { Option } = Select;

@Form.create()
@connect(({ projects }) => ({
  projects,
}))
class Filter extends PureComponent {

  handleTypeChange = value => {
    const { onTypeChange } = this.props;
    if (onTypeChange) {
      onTypeChange(value);
    }
  }

  handleYearChange = value => {
    const { dispatch, filters, fillprojects } = this.props
    filters.year = value
    const cycles = _.uniq(_.map(_.filter(fillprojects, ['businessUnit.id', filters.unit, 'cycle_year', filters.year]), 'cycle_start')).sort((a, b) => { return b - a })
    filters.cycle = cycles.length > 0 ? cycles[0] : null;
    dispatch({
      type: 'projects/saveFilter',
      payload: filters
    });
    if (filters.cycle) {
      this.handleCycleChange(filters.cycle);
    }
  }

  handleCycleChange = value => {
    const { dispatch, filters, fillprojects } = this.props
    filters.cycle = value
    const prjs = _.filter(fillprojects, (x) => {
      return x.taskSurvey && x.survey_date && x.businessUnit.id === filters.unit && x.cycle_year === filters.year && x.cycle_start === filters.cycle
    })
    filters.projectId = prjs.length > 0 ? prjs[0].id : null;
    dispatch({
      type: 'projects/saveFilter',
      payload: filters
    });
    if (filters.projectId) {
      this.handleTypeChange(filters.projectId);
    }
  }

  handleBUChange = value => {
    const { dispatch, filters, fillprojects } = this.props
    filters.unit = value
    const years = _.uniq(_.map(_.filter(fillprojects, ['businessUnit.id', filters.unit]), 'cycle_year')).sort((a, b) => { return b - a });
    filters.year = years.length > 0 ? years[0] : null;
    dispatch({
      type: 'projects/saveFilter',
      payload: filters
    });
    if (filters.year) {
      this.handleYearChange(filters.year);
    }
  }

  componentDidMount() {
    const { fillprojects } = this.props;
    const units = _.uniqBy(_.map(fillprojects, 'businessUnit'), 'id');
    if (units.length > 0) {
      this.handleBUChange(units[0].id);
    }
  }

  render() {
    const {
      filters,
      fillprojects
    } = this.props;

    const units = _.uniqBy(_.map(fillprojects, 'businessUnit'), 'id');
    const years = _.uniq(_.map(_.filter(fillprojects, ['businessUnit.id', filters.unit]), 'cycle_year')).sort((a, b) => { return b - a });
    let cycles = _.uniq(_.map(_.filter(fillprojects, ['businessUnit.id', filters.unit, 'cycle_year', filters.year]), 'cycle_start')).sort((a, b) => { return b - a });
    const prjs = _.filter(fillprojects, (x) => {
      return x.taskSurvey && x.survey_date && x.businessUnit.id === filters.unit && x.cycle_year === filters.year && x.cycle_start === filters.cycle
    })

    return (
      <div style={{ marginBottom: 0 }}>
        <Row>
          <Col span={24}>
            <span style={{ marginLeft: 16 }}>Business Unit : </span>
            <Select
              showSearch
              style={{ width: 200 }}
              optionFilterProp="children"
              onChange={this.handleBUChange}
              value={filters.unit}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              {units.map((val) => <Option value={val.id}>{val.name}</Option>)}
            </Select>
            <span style={{ marginLeft: 16 }}>Year : </span>
            <Select
              showSearch
              style={{ width: 80 }}
              optionFilterProp="children"
              onChange={this.handleYearChange}
              value={filters.year}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              {years.map((val) => <Option value={val}>{val}</Option>)}
            </Select>
            <span style={{ marginLeft: 16 }}>Cycle : </span>
            <Select
              showSearch
              style={{ width: 70 }}
              optionFilterProp="children"
              onChange={this.handleCycleChange}
              value={filters.cycle}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              {cycles.map((val) => <Option value={val}>{val}</Option>)}
            </Select>
            <span style={{ marginLeft: 16 }}>Project : </span>
            <Select
              showSearch
              style={{ width: 260 }}
              placeholder="Select a Project"
              optionFilterProp="children"
              onChange={this.handleTypeChange}
              value={filters.projectId}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              {prjs.map((val) => <Option value={val.id}>{`${val.name} - ${val.brand.name}`}</Option>)}
            </Select>
          </Col>
        </Row>
        {filters.projectId > 0 ? (
          <Card bordered={false}>
            <Row>
              <Col span={4}>Client</Col>
              <Col span={8}>: {fillprojects.find(x => x.id === filters.projectId).brand.client.name}</Col>
            </Row>
            <Row>
              <Col span={4}>Brand</Col>
              <Col span={8}>: {fillprojects.find(x => x.id === filters.projectId).brand.name}</Col>
            </Row>
            <Row>
              <Col span={4}>Program</Col>
              <Col span={8}>: {fillprojects.find(x => x.id === filters.projectId).name}</Col>
            </Row>
            <Row>
              <Col span={4}>Business Unit</Col>
              <Col span={8}>: {fillprojects.find(x => x.id === filters.projectId).businessUnit.name}</Col>
            </Row>
            <Row>
              <Col span={4}>CYCLE</Col>
              <Col span={8}>: {fillprojects.find(x => x.id === filters.projectId).cycle_start}</Col>
            </Row>
          </Card>) : ''}
      </div>
    );
  }
}

export default Filter;