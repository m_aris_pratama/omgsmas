/* global document */
import React, { PureComponent } from 'react'
import moment from 'moment'
import { Form, Button, Row, Col, DatePicker, Input, Cascader,Select } from 'antd'

const { Option } = Select;
const FormItem = Form.Item;

class ModalContent extends PureComponent {

  formLayout = {
    labelCol: { span: 7 },
    wrapperCol: { span: 13 },
  };

  handleSubmit = (e) => {
    const { onSubmit } = this.props;
    if (onSubmit) {
      onSubmit(e);
    }
  }

  render() {
    const {
      form: { getFieldDecorator },
      current
    } = this.props;

    return (
      <Form onSubmit={this.handleSubmit}>
        <FormItem label="Name" {...this.formLayout}>
          {getFieldDecorator('name', {
            rules: [{ required: true, message: 'Please enter business unit name' }],
            initialValue: current.name,
          })(<Input placeholder="name" />)}
        </FormItem>
        <FormItem label="Cycle" {...this.formLayout}>
          {getFieldDecorator('cycle_started_day', {
            rules: [{ required: true, message: '' }],
            initialValue: current.cycle_started_day,
          })(<Input placeholder="" />)}
        </FormItem>
      </Form>
    );
  }
}

export default ModalContent;
