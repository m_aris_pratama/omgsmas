/* global document */
import React, { PureComponent, Fragment } from 'react'
import moment from 'moment'
import { Divider,  Table,Checkbox } from 'antd'


class ListView extends PureComponent {


  columns = [
    {
      title: 'Code',
      width: 120,
      dataIndex: 'code'
    },
    {
      title: 'Retail Name',
      dataIndex: 'name'
    },
  ];

  handleChecked = value => {
    const { onChecked } = this.props;
    if (onChecked) {
      onChecked(value);
    }
  }

  handlePriorityChecked = value => {
    const { onPriorityChecked } = this.props;
    if (onPriorityChecked) {
      onPriorityChecked(value);
    }
  }
 
  render() {
    const {
        list,
        loading
    } = this.props;

    const retails = [];
    const selected = [];
    list.forEach(el => {
      if (el.is_active) selected.push(el.id)
      const a = retails.find(x => x.id === el.store.retail.id);
      if (a){
        a.stores.push({key:el.id,...el})
      }
      else{
        retails.push({
          ... el.store.retail,
          stores: [{key:el.id,...el}]
        })
      }
    });

    const expandedRowRender = (record) => {
      
      // const selectedStore = [440];

      const columns = [
       /* {
          title: '',
          width: 70,
          render: (text, r) => (
            <Checkbox checked={r.is_active} onChange={() => this.handleChecked(r)} />
          ),
        }, */
        {
          title: 'Store Name',
          dataIndex: 'store.name'
        },
        {
          title: 'Priority',
          render: (text, r) => (
            <Checkbox checked={r.is_priority} onChange={() => this.handlePriorityChecked(r)} />
          ),
        },
      ];

     // const {selectedStore} = this.state;
      const rowSelection = {
        selectedRowKeys: selected,
        onSelect: (r,s) => {
          this.handleChecked({is_active:s,...r})
        },
        onSelectAll: (selected, selectedRows, changeRows) => {
          changeRows.forEach(val=>{
            this.handleChecked({is_active:selected,...val})
          })
       
        },
      };

      return (
        <Table
          rowSelection={rowSelection}
          dataSource={record.stores}
          columns={columns}    
        />);
    };

    return (<Table
      dataSource={retails}
      expandedRowRender={expandedRowRender}
      loading={loading}
      columns={this.columns}
    />
    );
  }
}

export default ListView;
