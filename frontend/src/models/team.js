import { queryItems, removeItem, addItem, updateItem } from '@/services/api'

export default {
  namespace: 'teams',

  state: {
    filters:{
      leadId: 0
    },
    meta: {
      perPage:0,
      totalResults:0,
      curPage:0
    },
    total: 0,
    list: [],
    item:{}
  },

  effects: {
    *fetchAll({ payload }, { call, put }) {
      const response = yield call(queryItems, "/api/teams", payload);
      yield put({
        type: 'queryAll',
        payload: response 
      });
    },
    *fetch({ payload }, { call, put }) {    
      const response = yield call(queryItems, "/api/teams", payload);
      yield put({
        type: 'queryList',
        payload: response 
      });
    },
    *fetchOne({ payload }, { call, put }) {    
      const response = yield call(queryItems, `/api/teams/${ payload.id}`);
      yield put({
        type: 'queryOne',
        payload: response 
      });
    },
    *submit({ payload }, { call, put }) {
      let callback;
      if (payload.id) {
        callback = Object.keys(payload).length === 1 ? removeItem : updateItem;
      } else {
        callback = addItem;
      }
      yield call(callback, "/api/teams", payload); // post
      const response = yield call(queryItems, "/api/teams");
      yield put({
        type: 'queryAll',
        payload: response 
      });
    },
  },

  reducers: {
    queryAll(state, action) {
      return {
        ...state,
        total: action.payload.meta.totalResults,
        list: action.payload.list,
        meta: action.payload.meta
      };
    },
    queryList(state, action) {
      return {
        ...state,
        list: action.payload.list,
        meta: action.payload.meta
      };
    },
    queryOne(state, action) {
      return {
        ...state,
        item: action.payload.team
      };
    },
    saveFilter(state, action) {
      return {
        ...state,
        filters: action.payload
      };
    }
  },
};