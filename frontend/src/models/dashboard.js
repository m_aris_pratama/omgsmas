import { queryItems } from '@/services/api';

export default {
  namespace: 'dashboards',

  state: {
    places: [],
    projs: [],
    dashcount: [],
    business: {},
    clients: [],
    detail: []
  },

  effects: {
    *fetchAbsence({ payload }, { call, put }) {
      const response = yield call(queryItems, '/api/absence/last', payload);
      yield put({
        type: 'queryAbsence',
        payload: response,
      });
    },
    *fetchProj({ payload }, { call, put }) {
      const response = yield call(queryItems, '/api/projects/dash/1', payload);
      yield put({
        type: 'queryProj',
        payload: response,
      });
    },
    *fetchProjCountByClient({ payload }, { call, put }) {
      const response = yield call(queryItems, `/api/projects/dashboardByClient/${payload.id}`);
      yield put({
        type: 'queryProjCountClient',
        payload: response,
      });
    },
    *fetchProjCountByClientDetail({ payload }, { call, put }) {
      const response = yield call(queryItems, `/api/projects/dashboardByClientDetail/${payload.businessUnitId}/${payload.clientId}`);
      yield put({
        type: 'queryProjCountClientDetail',
        payload: response,
      });
    },
    *fetchProjCountByBusinessUnit({ payload }, { call, put }) {
      const response = yield call(queryItems, '/api/units/dashboard/1', payload);
      yield put({
        type: 'queryProjCountBusinessUnit',
        payload: response,
      });
    },
    *fetchProjCountByBusinessUnitId({ payload }, { call, put }) {
      const response = yield call(queryItems, `/api/units/dashboardByBusinessUnit/${payload.id}`);
      yield put({
        type: 'queryProjCountBusinessUnitId',
        payload: response,
      });
    },
  },

  reducers: {
    queryAbsence(state, action) {
      return {
        ...state,
        places: action.payload.pos,
      };
    },
    queryProj(state, action) {
      return {
        ...state,
        projs: action.payload,
      };
    },
    queryProjCountClient(state, action) {
      return {
        ...state,
        clients: action.payload,
      };
    },
    queryProjCountBusinessUnit(state, action) {
      return {
        ...state,
        dashcount: action.payload,
      };
    },
    queryProjCountBusinessUnitId(state, action) {
      return {
        ...state,
        business: action.payload,
      };
    },
    queryProjCountClientDetail(state, action) {
      return {
        ...state,
        detail: action.payload,
      };
    },
  },
};
