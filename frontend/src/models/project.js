import { queryItems, removeItem, addItem, updateItem } from '@/services/api'

export default {
  namespace: 'projects',

  state: {
    filters:{
      projectId: 0,
      year: 0,
      cycle: 0,
      unit: 0
    },
    meta: {
      perPage:0,
      totalResults:0,
      curPage:0
    },
    total: 0,
    list: [],
    summary: []
  },

  effects: {
    *fetchAll({ payload }, { call, put }) {
      const response = yield call(queryItems, "/api/projects", payload);
      yield put({
        type: 'queryAll',
        payload: response 
      });
    },
    *fetch({ payload }, { call, put }) {    
      const response = yield call(queryItems, "/api/projects", payload);
      yield put({
        type: 'queryList',
        payload: response 
      });
    },
    *summary({ payload }, { call, put }) {    
      const response = yield call(queryItems, `/api/projects/summary/${payload.id}`);
      yield put({
        type: 'querySummary',
        payload: response 
      });
    },
    *submit({ payload }, { call, put }) {
      let callback;
      if (payload.id) {
        callback = Object.keys(payload).length === 1 ? removeItem : updateItem;
      } else {
        callback = addItem;
      }
      yield call(callback, "/api/projects", payload); // post
      const response = yield call(queryItems, "/api/projects");
      yield put({
        type: 'queryAll',
        payload: response 
      });
    },
  },

  reducers: {
    queryAll(state, action) {
      return {
        ...state,
        total: action.payload.meta.totalResults,
        list: action.payload.list,
        meta: action.payload.meta
      };
    },
    queryList(state, action) {
      return {
        ...state,
        list: action.payload.list,
        meta: action.payload.meta
      };
    },
    querySummary(state, action) {
      return {
        ...state,
        summary: action.payload,
      };
    },
    saveFilter(state, action) {
      return {
        ...state,
        filters: action.payload
      };
    }
  },
};