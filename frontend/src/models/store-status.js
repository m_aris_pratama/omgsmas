import { queryItems } from '@/services/api';

export default {
  namespace: 'storeStatus',

  state: {
    filters: {
      date: 0,
      dateFrom: '',
      dateTo: ''
    },
    meta: {
      perPage: 0,
      totalResults: 0,
      curPage: 0
    },
    list: []
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(queryItems, "/api/report/storeStatus", payload);
      yield put({
        type: 'queryList',
        payload: response
      });
    },
    *toggleActive({ payload }, { call, put }) {
      const toggle = yield call(queryItems, "/api/report/storeStatus/toggleActive", { id: payload.id });
      delete payload.id;
      const response = yield call(queryItems, "/api/report/storeStatus", payload);
      yield put({
        type: 'queryList',
        payload: response
      });
    }
  },

  reducers: {
    queryList(state, action) {
      return {
        ...state,
        list: action.payload.list,
        meta: action.payload.meta
      };
    },
    saveFilter(state, action) {
      return {
        ...state,
        filters: action.payload
      };
    }
  },
};