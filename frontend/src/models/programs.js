import { queryItems, removeItem, addItem, updateItem ,loadItem} from '@/services/api'

export default {
  namespace: 'programs',

  state: {
    filters:{},
    meta: {
      perPage:0,
      totalResults:0,
      curPage:0
    },
    total: 0,
    list: []
  },

  effects: {
    *fetchAll({ payload }, { call, put }) {
      const response = yield call(queryItems, "/api/programs", payload);
      yield put({
        type: 'queryAll',
        payload: response 
      });
    },
    *fetch({ payload }, { call, put }) {    
      const response = yield call(queryItems, "/api/programs", payload);
      yield put({
        type: 'queryList',
        payload: response 
      });
    },
    *submit({ payload }, { call, put }) {
      let callback;
      if (payload.id) {
        callback = Object.keys(payload).length === 1 ? removeItem : updateItem;
      } else {
        callback = addItem;
      }
      yield call(callback, "/api/programs", payload); // post
      const response = yield call(queryItems, "/api/programs", payload);
      yield put({
        type: 'queryAll',
        payload: response 
      });
    },
    *download({ payload }, { call, put }) {    
      const response = yield call(loadItem, "/api/programs/exportData");
      yield put({
        type: 'getDownload',
        payload: response 
      });
    },
  },

  reducers: {
    queryAll(state, action) {
      return {
        ...state,
        total: action.payload.meta.totalResults,
        list: action.payload.list,
        meta: action.payload.meta
      };
    },
    queryList(state, action) {
      return {
        ...state,
        list: action.payload.list,
        meta: action.payload.meta
      };
    },
    saveFilter(state, action) {
      return {
        ...state,
        filters: action.payload
      };
    },
    getDownload(state, action) {
   //   saveAs(new Blob([action.payload],{type:"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"}), "test.xlsx");
      console.log(new Blob([action.payload]))
      return {
        ...state
      };
    }
  },
};