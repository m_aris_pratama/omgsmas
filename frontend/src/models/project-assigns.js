import { queryItems, removeItem, addItem, updateItem } from '@/services/api'

export default {
  namespace: 'projectAssigns',

  state: {
    filters:{
      projectId: 0,
      year: 0,
      cycle: 0,
      unit: 0
    },
    meta: {
      perPage:0,
      totalResults:0,
      curPage:0
    },
    list: []
  },

  effects: {
    *fetch({ payload }, { call, put }) {    
      const response = yield call(queryItems, "/api/projectAssigns", payload);
      yield put({
        type: 'queryList',
        payload: response 
      });
    },
    *submit({ payload }, { call, put }) {
      let callback;
      if (payload.id) {
        callback = Object.keys(payload).length === 1 ? removeItem : updateItem;
      } else {
        callback = addItem;
      }
      yield call(callback, "/api/projectAssigns", payload); // post
      const response = yield call(queryItems, "/api/projectAssigns",{per_page:0,project_id:payload.project.id});
      yield put({
        type: 'queryList',
        payload: response
      });
    },
    *submitApproval({ payload }, { call, put }) {
      let callback;
      if (payload.id) {
        callback = Object.keys(payload).length === 1 ? removeItem : updateItem;
      } else {
        callback = addItem;
      }
      yield call(callback, "/api/projectAssigns", payload); // post
      const response = yield call(queryItems, "/api/projectAssigns",{per_page:0,approval:false});
      yield put({
        type: 'queryList',
        payload: response
      });
    },
  },

  reducers: {
    queryList(state, action) {
      return {
        ...state,
        list: action.payload.list,
        meta: action.payload.meta
      };
    },
    saveFilter(state, action) {
      return {
        ...state,
        filters: action.payload
      };
    }
  },
};