import { queryItems, removeItem, addItem, updateItem } from '@/services/api'

export default {
  namespace: 'unitAdmins',

  state: {
    filters:{
      unitId:0
    },
    meta: {
      perPage:0,
      totalResults:0,
      curPage:0
    },
    total: 0,
    list: []
  },

  effects: {
    *fetch({ payload }, { call, put }) {    
      const response = yield call(queryItems, "/api/unitAdmins", payload);
      yield put({
        type: 'queryList',
        payload: response 
      });
    },
    *submit({ payload }, { call, put }) {
      let callback;
      if (payload.id) {
        callback = Object.keys(payload).length === 1 ? removeItem : updateItem;
      } else {
        callback = addItem;
      }
      yield call(callback, "/api/unitAdmins", payload); // post
      const response = yield call(queryItems, "/api/unitAdmins");
      yield put({
        type: 'queryList',
        payload: response 
      });
    },
  },

  reducers: {
    queryList(state, action) {
      return {
        ...state,
        list: action.payload.list,
        meta: action.payload.meta
      };
    },
    saveFilter(state, action) {
      return {
        ...state,
        filters: action.payload
      };
    }
  },
};