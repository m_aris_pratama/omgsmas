import { queryItems } from '@/services/api';

export default {
  namespace: 'reports',

  state: {
    instalation: [],
    survey: [],
    removed: []
  },

  effects: {
    *fetchInstalation({ payload }, { call, put }) {
      const response = yield call(queryItems, `/api/report/instalation/${payload.projectId}`);
      yield put({
        type: 'queryInstalation',
        payload: response 
      });
    },
    *fetchSurvey({ payload }, { call, put }) {
      const response = yield call(queryItems, `/api/report/survey/${payload.projectId}`);
      yield put({
        type: 'querySurvey',
        payload: response 
      });
    },
    *fetchRemoved({ payload }, { call, put }) {
      const response = yield call(queryItems, `/api/report/removed/${payload.projectId}`);
      yield put({
        type: 'queryRemoved',
        payload: response 
      });
    }
  },

  reducers: {
    queryInstalation(state, action) {
      return {
        ...state,
        instalation: action.payload
      };
    },
    querySurvey(state, action) {
      return {
        ...state,
        survey: action.payload
      };
    },
    queryRemoved(state, action) {
      return {
        ...state,
        removed: action.payload
      };
    },
  },
};