import { query as queryUsers, queryCurrent } from '@/services/user';
import { addItem } from '@/services/api'

export default {
  namespace: 'user',

  state: {
    list: [],
    currentUser: {},
  },

  effects: {
    *fetch(_, { call, put }) {
      const response = yield call(queryUsers);
      yield put({
        type: 'save',
        payload: response,
      });
    },
    *fetchCurrent(_, { call, put }) {
      const response = yield call(queryCurrent);
      yield put({
        type: 'saveCurrentUser',
        payload: response,
      });
    },
    *updateProfile({ payload }, { call, put }) {
      const response = yield call(addItem, "/api/auth/updateProfile", payload); // post
      yield put({
        type: 'saveCurrentUser',
        payload: response,
      });
    },
    *updatePass({ payload }, { call, put }) {
      yield call(addItem, "/api/auth/updatePass", payload); // post
    },
    *createUser({ payload }, { call, put }) {
      yield call(addItem, "/api/users", payload); // post
    }
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        list: action.payload,
      };
    },
    saveCurrentUser(state, action) {
      return {
        ...state,
        currentUser: action.payload || {},
      };
    },
    changeNotifyCount(state, action) {
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          notifyCount: action.payload.totalCount,
          unreadCount: action.payload.unreadCount,
        },
      };
    },
  },
};
