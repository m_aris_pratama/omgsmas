import { queryItems } from '@/services/api';

export default {
  namespace: 'absenceLeads',

  state: {
    filters:{
      date: 0,
      dateFrom: '',
      dateTo: '',
      q: '',
    },
    meta: {
      perPage:0,
      totalResults:0,
      curPage:0
    },
    list: [],
    exportList: []
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(queryItems, "/api/absence/leads", payload);
      yield put({
        type: 'queryList',
        payload: response 
      });
    },
    *fetchExport({ payload }, { call, put }) {
      const response = yield call(queryItems, "/api/absence/leads", payload);
      yield put({
        type: 'queryExportList',
        payload: response
      });
    }
  },

  reducers: {
    queryList(state, action) {
      return {
        ...state,
        list: action.payload.list,
        meta: action.payload.meta
      };
    },
    queryExportList(state, action) {
      return {
        ...state,
        exportList: action.payload.list
      };
    },
    saveFilter(state, action) {
      return {
        ...state,
        filters: action.payload
      };
    }
  },
};