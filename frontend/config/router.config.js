export default [
   // tos
   {
    path: '/tos/privacy_policy',
    component: './Privacy',
  },
  // user
  {
    path: '/user',
    component: '../layouts/UserLayout',
    routes: [
      { path: '/user', redirect: '/user/login' },
      { path: '/user/login', component: './User/Login' },
    ],
  },
  // app
  {
    path: '/',
    component: '../layouts/BasicLayout',
    Routes: ['src/pages/Authorized'],
    authority: ['superAdmin', 'unitAdmin','userTL'],
    routes: [
      // dashboard
      { path: '/', redirect: '/dashboard' },
      {
        path: '/dashboard',
        name: 'dashboard',
        icon: 'dashboard',
        component: './Dashboard/Dashboard',
      },
      {
        path: "/dashboard/detail/:id",
        name: "Detail",
        component: './Dashboard/DashboardDetail',
        hideInMenu: true
      },
      // report
      {
        path: '/report',
        icon: 'home',
        name: 'Report',
        authority: ['superAdmin', 'unitAdmin'],
        routes: [
        /*  {
            path: '/report/project',
            name: 'Projects',
            component: './Report/Projects',
            routes: [
              {
                path: '/report/project',
                redirect: '/report/project/list',
              },
              {
                path: '/report/project/list',
                component: './Report/Projects/List',
              },
            ],
          }, */
          {
            path: '/report/survey',
            name: 'Survey',
            component: './Report/Survey',
            routes: [
              {
                path: '/report/survey',
                redirect: '/report/survey/list',
              },
              {
                path: '/report/survey/list',
                component: './Report/Survey/List',
              },
            ],
          },
          {
            path: '/report/instalation',
            name: 'Instalation',
            component: './Report/Instalation',
            routes: [
              {
                path: '/report/instalation',
                redirect: '/report/instalation/list',
              },
              {
                path: '/report/instalation/list',
                component: './Report/Instalation/List',
              },
            ],
          },
          {
            path: '/report/removed',
            name: 'Removed',
            component: './Report/Removed',
            routes: [
              {
                path: '/report/removed',
                redirect: '/report/removed/list',
              },
              {
                path: '/report/removed/list',
                component: './Report/Removed/List',
              },
            ],
          },
          {
            path: '/report/approval',
            name: 'Survey Approval',
            component: './Report/Approval',

          },
          {
            path: '/report/nap',
            name: 'NAP',
            component: './Report/NAP',
            routes: [
              {
                path: '/report/nap',
                redirect: '/report/nap/list',
              },
              {
                path: '/report/nap/list',
                component: './Report/NAP/NAP/',
              },
            ],
          },
          {
            path: '/report/store',
            name: 'Store Status',
            component: './Report/StoreStatus',
            routes: [
              {
                path: '/report/store',
                redirect: '/report/store/list',
              },
              {
                path: '/report/store/list',
                component: './Report/StoreStatus/Status',
              },
            ],
          },
        ],
      },
      // attendance
      {
        path: '/attendance',
        icon: 'form',
        name: 'Attendance',
        routes: [
          {
            path: '/attendance/fo',
            name: 'Field Officer',
            component: './Attendance/Officer',
            routes: [
              {
                path: '/attendance/fo',
                redirect: '/attendance/fo/list',
              },
              {
                path: '/attendance/fo/list',
                component: './Attendance/Officer/Attendance/',
              },
            ],
          },
          {
            path: '/attendance/tl',
            name: 'Team Lead',
            component: './Attendance/Leader',
            routes: [
              {
                path: '/attendance/tl',
                redirect: '/attendance/tl/list',
              },
              {
                path: '/attendance/tl/list',
                component: './Attendance/Leader/Attendance/',
              },
            ],
          },
          {
            path: '/attendance/leave',
            name: 'Leave',
            component: './Attendance/Leave',
            routes: [
              {
                path: '/attendance/leave',
                redirect: '/attendance/leave/list',
              },
              {
                path: '/attendance/leave/list',
                component: './Attendance/Leave/Leave/',
              },
            ],
          },
        ],
      },
      // projects
      {
        path: '/project',
        icon: 'form',
        name: 'Project & Task',
        authority: ['superAdmin', 'unitAdmin'],
        routes: [
          {
            path: '/project/project',
            name: 'Project',
            component: './Project/Projects',
            routes: [
              {
                path: '/project/project',
                redirect: '/project/project/list',
              },
              {
                path: '/project/project/list',
                component: './Project/Projects/Project',
              },
            ],
          },
          {
            path: '/project/assign',
            name: 'Project Assignment',
            component: './Project/Assignments',
            routes: [
              {
                path: '/project/assign',
                redirect: '/project/assign/list',
              },
              {
                path: '/project/assign/list',
                component: './Project/Assignments/Assignment',
              },
            ],
          },
          {
            path: '/project/task',
            name: 'Task Templates',
            component: './Project/Tasks',
            routes: [
              {
                path: '/project/task',
                redirect: '/project/task/task',
              },
              {
                path: '/project/task/task',
                component: './Project/Tasks/Task',
              },
              {
                path: '/project/task/template',
                component: './Project/Tasks/Template',
              },
            ],
          },
        ],
      },
      // manage
      {
        path: '/manage',
        icon: 'form',
        name: 'Manage',
        authority: ['superAdmin'],
        routes: [
          {
            path: '/manage/client',
            name: 'Client',
            component: './Manage/Clients',
            routes: [
              {
                path: '/manage/client',
                redirect: '/manage/client/list',
              },
              {
                path: '/manage/client/list',
                component: './Manage/Clients/Client',
              },
            ],
          },
          {
            path: '/manage/brand',
            name: 'Brand',
            component: './Manage/Brands',
            routes: [
              {
                path: '/manage/brand',
                redirect: '/manage/brand/list',
              },
              {
                path: '/manage/brand/list',
                component: './Manage/Brands/BrandList',
              },
              {
                path: '/manage/brand/types',
                component: './Manage/Brands/BrandType',
              },
              {
                path: '/manage/brand/subtypes',
                component: './Manage/Brands/BrandSubtype',
              },
            ],
          },
          {
            path: '/manage/retail',
            name: 'Retail & Store',
            component: './Manage/Retails',
            routes: [
              {
                path: '/manage/retail',
                redirect: '/manage/retail/list',
              },
              {
                path: '/manage/retail/list',
                component: './Manage/Retails/RetailList',
              },
              {
                path: '/manage/retail/types',
                component: './Manage/Retails/RetailType',
              },
              {
                path: '/manage/retail/stores',
                component: './Manage/Retails/StoreList',
              },
            ],
          },
          {
            path: '/manage/team',
            name: 'Team',
            component: './Manage/Teams',
            routes: [
              {
                path: '/manage/team',
                redirect: '/manage/team/list',
              },
              {
                path: '/manage/team/list',
                component: './Manage/Teams/Team',
              },
            ],
          },
          {
            path: '/manage/area',
            name: 'Area & City',
            component: './Manage/Areas',
            routes: [
              {
                path: '/manage/area',
                redirect: '/manage/area/city',
              },
              {
                path: '/manage/area/area',
                component: './Manage/Areas/Area',
              },
              {
                path: '/manage/area/city',
                component: './Manage/Areas/City',
              },
            ],
          },
          {
            path: '/manage/program',
            name: 'Program',
            component: './Manage/Programs',
            routes: [
              {
                path: '/manage/program',
                redirect: '/manage/Program/list',
              },
              {
                path: '/manage/program/list',
                component: './Manage/Programs/Program',
              },
            ],
          },
          {
            path: '/manage/unit',
            name: 'Business Unit',
            component: './Manage/Units',
            routes: [
              {
                path: '/manage/unit',
                redirect: '/manage/unit/list',
              },
              {
                path: '/manage/unit/list',
                component: './Manage/Units/Unit',
              },
            ],
          },
          {
            path: '/manage/cmsadmin',
            name: 'Business Unit Admin',
            component: './Manage/Admins',
            routes: [
              {
                path: '/manage/cmsadmin',
                redirect: '/manage/cmsadmin/list',
              },
              {
                path: '/manage/cmsadmin/list',
                component: './Manage/Admins/Admin',
              },
            ],
          },
          /* ,
          {
            path: '/manage/rule',
            name: 'Role & Permission',
            component: './Manage/Roles/Role',
          }, */
        ],
      },
      // account
      {
        path: '/account',
        name: 'account',
        icon: 'user',
        routes:
        [
          // profile
          {
            path: '/account/profile',
            name: 'profile',
            component: './Profile/Profile',
          },
          // User
          {
            path: '/account/user',
            name: 'User',
            component: './UserAdd/UserAdd',
          }
        ],
      },
      {
        name: 'exception',
        icon: 'warning',
        path: '/exception',
        hideInMenu: true,
        routes: [
          // exception
          {
            path: '/exception/403',
            name: 'not-permission',
            component: './Exception/403',
          },
          {
            path: '/exception/404',
            name: 'not-find',
            component: './Exception/404',
          },
          {
            path: '/exception/500',
            name: 'server-error',
            component: './Exception/500',
          },
          {
            path: '/exception/trigger',
            name: 'trigger',
            hideInMenu: true,
            component: './Exception/TriggerException',
          },
        ],
      },
      {
        component: '404',
      },
    ],
  },
];
