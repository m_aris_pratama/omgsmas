# README #

OMG Store Monitoring

## Requirements
```
NodeJS v10.0+
NPM v6.0+
```

## NGINX Configuration
```
server {
	listen 80;
	listen [::]:80;
	#root /var/www/html;
    #server_name omg.unskip.co;

    #backend
	location /api {
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-NginX-Proxy true;
        proxy_pass http://127.0.0.1:5000;
        proxy_set_header Host $http_host;
        proxy_cache_bypass $http_upgrade;
        proxy_redirect off;
    }

    #frontend
	location / {
		root /home/hendri/omgstore/frontend/dist;
		index index.html index.htm;
	}
```

### Backend Installation

copy file `production.env-example` to `production.env`

#### Env parameter

Field       |  Description 	| Example Value
------------| --------------|--------------
DOMAIN | URL/IP Server | https://omg.unskip.co
PORT | server port | 5000
EXTERNAL_PORT | external port | 5000
JWT_SECRET_KEY | JWT secret key (random key for autentication security) | secret_key123
JWT_AUTH_HEADER_PREFIX | JWT prefix | JWT
DATABASE_URL | database configuration mysql://(username):(password)@(domain):(port)/(dbname) | mysql://root:123@localhost:3306/omgstore
GOOGLEMAP_KEY | google map key | AIzaSyB588H1pt2XE372EFK7xdm7Z3mmLXeE9Wg

### Running
```
npm install
npm run build
npm run start
```

### Update
```
npm run restart
```

### Frontend Installation

copy file `.env-example` to `.env`

#### Env parameter

Field       |  Description 	| Example Value
------------| --------------|--------------
GOOGLEMAP_KEY | google map key | AIzaSyB588H1pt2XE372EFK7xdm7Z3mmLXeE9Wg

### Running
```
npm install
npm run build
```




